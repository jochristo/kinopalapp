﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Binding
{
    public static class SettingsUtilities
    {
        private static bool _isFavoritesMode;
        private static bool _isRecurrenceMode;

        public static bool FavoritesMode
        {
            get { return _isFavoritesMode; }
            set { _isFavoritesMode = value; }
        }

        public static bool RecurrenceMode
        {
            get { return _isRecurrenceMode; }
            set { _isRecurrenceMode = value; }
        }

    }
}
