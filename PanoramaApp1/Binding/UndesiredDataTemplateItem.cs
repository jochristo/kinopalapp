﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Kino.Binding
{
    public class UndesiredDataTemplateItem : INotifyPropertyChanged
    {
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { this._isSelected = value; NotifyPropertyChanged("_isSelected"); }
        }

        private Brush _backgroundColor;
        public Brush BackgroundColor
        {
            get { return _backgroundColor; }
            set { this._backgroundColor = value; NotifyPropertyChanged("_backgroundColor"); }
        }

        public UndesiredDataTemplateItem()
        {
            this.IsSelected = false;
            this.BackgroundColor = (Brush)Application.Current.Resources["PhoneInactiveBrush"];
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
