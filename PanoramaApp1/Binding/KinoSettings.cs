﻿using Kino.Binding;
using Kino.Data;
using KinoGenerator;
using KinoGenerator.Statistics;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Binding
{
    /// <summary>
    /// Binding class for kino's settings
    /// </summary>
    public class KinoSettings : INotifyPropertyChanged
    {
        private bool _isFavoritesMode;
        private bool _isRecurrenceMode;
        private List<int> _excludedNumbers;
        private int _excludedCount;
        private string _excludedCountStatus;
        private List<Sequence> _savedSequences;
        private bool _IsSavedSequencesRepeated;
        private bool _IsSavedSequencesSwitchEnabled;
        private SavedSequencesItems _observableSavedSequences;
        private int _savedSequencesCount;
        private StatisticalDataItems _observableStatisticalDataItems;
        private int _maxFrequency;
        private string _drawsCount;
        private string _totalSavedSequences;

        private HashSet<Sequence> _personalSequences;
        private HashSet<Sequence> _fuckingSequences;
        private ObservableCollection<Sequence> _observableFuckingSequences;

        // Default constructor, no settings set yet
        public KinoSettings()
        {
            this._isFavoritesMode = false;
            this._isRecurrenceMode = false;
            this._excludedNumbers = new List<int>();
            this._savedSequences = new List<Sequence>();
            this._IsSavedSequencesRepeated = false;
            this._IsSavedSequencesSwitchEnabled = false;
            this._excludedCount = 0;
            this._excludedCountStatus = "No numbers defined";
            this._observableSavedSequences = new SavedSequencesItems();
            this._savedSequencesCount = 0;
            this._observableStatisticalDataItems = new StatisticalDataItems();
            this._maxFrequency = 0;
            this._drawsCount = " (0 draws)";
            this._totalSavedSequences = "No items";

            this._personalSequences = new HashSet<Sequence>();
            _fuckingSequences = new HashSet<Sequence>();
            _observableFuckingSequences = new ObservableCollection<Sequence>();
        }


        /// <summary> Holds the hash set of all stored in the phone sequence items.
        /// </summary>
        public HashSet<Sequence> FuckingSequences
        {
            get { return _fuckingSequences; }
            set
            {
                _fuckingSequences = value;
                NotifyPropertyChanged("FuckingSequences");

                // Set observable saved sequences collection value
                var collection = new ObservableCollection<Sequence>();
                foreach (var n in _fuckingSequences)
                {
                    var s = new FuckingSequence();
                    s.Numbers = n.Numbers;               
                    collection.Add(s);
                }

                ObservableFuckingSequences = collection;
                var count = ObservableFuckingSequences.Count;

                this.DrawsCount = count.ToString(); // update draws count 
                var countAsString = count.ToString();
                if (count == 0)
                    countAsString = "No";
                this.TotalSavedSequences = countAsString + " item(s)";

                // Set binding flags
                if (_fuckingSequences != null && _fuckingSequences.Count > 0)
                {
                    if (IsRecurrenceMode)
                        IsSavedSequencesRepeatedSwitchEnabled = true;
                    else
                        IsSavedSequencesRepeatedSwitchEnabled = false;
                }
                else if (_fuckingSequences == null || (_fuckingSequences != null && _fuckingSequences.Count == 0))
                {
                    IsSavedSequencesRepeatedSwitchEnabled = false;
                }

            }
        }

        public ObservableCollection<Sequence> ObservableFuckingSequences
        {
            get
            {
                return _observableFuckingSequences;

            }
            private set
            {

                _observableFuckingSequences = value;
                NotifyPropertyChanged("ObservableFuckingSequences");
                if (_observableSavedSequences != null)
                {
                    SavedSequencesCount = _observableSavedSequences.Count;
                }
            }
        }

        public void UpdateFuckingSequencesSource()
        {
            var collection = new ObservableCollection<Sequence>();
            var list = new HashSet<Sequence>();
            /*
            if (this.FuckingSequences != null && this.FuckingSequences.Count > 0)
            {
                // Set observable saved sequences collection value
                foreach (var n in FuckingSequences)
                {
                    var s = new FuckingSequence();
                    s.Numbers = n.Numbers;                    
                    list.Add(s);
                }
                SavedSequences.Clear(); // forget about this in 1.0.5
                PersonalSequences = list;
            }
            */
            foreach (var s in FuckingSequences)
            {
                var fuckingSequence = new FuckingSequence();
                fuckingSequence.Numbers = s.Numbers;
                collection.Add(fuckingSequence); //s
            }
            this.ObservableFuckingSequences = collection;
        }




        /// <summary> Holds the hash set of all stored in the phone sequence items.
        /// </summary>
        public HashSet<Sequence> PersonalSequences
        {
            get { return _personalSequences; }
            set
            {                
                _personalSequences = value;
                NotifyPropertyChanged("PersonalSequences");

                // Set observable saved sequences collection value
                var collection = new SavedSequencesItems();
                foreach (var n in _personalSequences)
                {
                    var s = new SavedSequence();
                    s.Numbers = n.Numbers;
                    s.Items = n.Numbers;
                    collection.Add(s);
                }

                ObservableSavedSequences = collection;
                var count = ObservableSavedSequences.Count;

                this.DrawsCount = count.ToString(); // update draws count 
                var countAsString = count.ToString();
                if (count == 0)
                    countAsString = "No";
                this.TotalSavedSequences = countAsString + " item(s)";

                // Set binding flags
                if (_personalSequences != null && _personalSequences.Count > 0)
                {
                    if (IsRecurrenceMode)
                        IsSavedSequencesRepeatedSwitchEnabled = true;
                    else
                        IsSavedSequencesRepeatedSwitchEnabled = false;
                }
                else if (_personalSequences == null || (_personalSequences != null && _personalSequences.Count == 0))
                {
                    IsSavedSequencesRepeatedSwitchEnabled = false;
                }

            }
        }

        /// <summary>
        /// Updates Observable collection of personal sequences hashset.
        /// </summary>
        public void UpdateObservableCollectionSource()
        {
            if(this.PersonalSequences != null && this.PersonalSequences.Count > 0)
            {
                // Set observable saved sequences collection value
                var collection = new SavedSequencesItems();
                foreach (var n in PersonalSequences)
                {
                    var s = new SavedSequence();
                    s.Items = n.Numbers;
                    s.Numbers = n.Numbers;
                    collection.Add(s);
                }
                this.ObservableSavedSequences = collection;
            }
        }

        /// <summary>
        /// Updates Observable collection of personal sequences hashset.
        /// </summary>
        public void UpdateFromSavedSequencesSource()
        {
            var collection = new SavedSequencesItems();
            var list = new HashSet<Sequence>();
            if (this.SavedSequences != null && this.SavedSequences.Count > 0)
            {
                // Set observable saved sequences collection value
                foreach (var n in SavedSequences)
                {
                    var s = new SavedSequence();
                    s.Numbers = n.Numbers;
                    s.Items = n.Numbers;
                    list.Add(s);
                }
                SavedSequences.Clear(); // forget about this in 1.0.5
                PersonalSequences = list;
            }

            foreach (var s in PersonalSequences)
            {
                //collection.Add(s);
                var fuckingSequence = new SavedSequence(); // DO this for bound data selector to load from iso storage settings map
                fuckingSequence.Numbers = s.Numbers;
                fuckingSequence.Items = s.Numbers;
                collection.Add(fuckingSequence); //s
            }
            ObservableSavedSequences = collection;
        }

        /// <summary>
        /// Gets or sets the favorite numbers mode value.
        /// </summary>
        public bool IsFavoritesMode
        {
            get { return _isFavoritesMode; }
            set
            { 
                _isFavoritesMode = value;
                NotifyPropertyChanged("IsFavoritesMode");
                IsFavoritesModeValue = value.ToString();
                NotifyPropertyChanged("IsFavoritesModeValue");
            }
        }

        /// <summary>
        /// Gets or sets the recursive mode value.
        /// </summary>
        public bool IsRecurrenceMode
        {
            get { return _isRecurrenceMode; }
            set
            {
                _isRecurrenceMode = value;
                NotifyPropertyChanged("IsRecurrenceMode");
                IsRecurrenceModeValue = value.ToString();
                NotifyPropertyChanged("IsRecurrenceModeValue");

                if (_isRecurrenceMode == false)
                    IsSavedSequencesRepeatedSwitchEnabled = false;
                
                else
                {
                    //if (SavedSequences != null && SavedSequences.Count > 0)
                    //    IsSavedSequencesRepeatedSwitchEnabled = true;
                    if (PersonalSequences != null && PersonalSequences.Count > 0)
                        IsSavedSequencesRepeatedSwitchEnabled = true;
                }
                
                
            }
        }

        /// <summary>
        /// Gets the text value of the favorites mode.
        /// </summary>
        public string IsFavoritesModeValue
        {
            get
            {
                if (_isFavoritesMode) return "On";
                else return "Off";
            }
            private set{ }
        }

        /// <summary>
        /// Gets the text value of the recurrence mode.
        /// </summary>
        public string IsRecurrenceModeValue
        {
            get
            {
                if (_isRecurrenceMode) return "On";
                else return "Off";
            }
            private set { }
        }

        /// <summary>
        /// Gets or sets the list of the numbers to be excluded by the kino generator.
        /// </summary>
        public List<int> ExcludedNumbers
        {
            get { return _excludedNumbers; }
            set
            { 
                _excludedNumbers = value;
                NotifyPropertyChanged("ExcludedNumbers");
                if (_excludedNumbers != null)
                {
                    ExcludedCount = _excludedNumbers.Count;
                }
                else
                    ExcludedCount = 0;
            }
        }

        /// <summary>
        /// Gets the count of the excluded numbers.
        /// </summary>
        public int ExcludedCount
        {
            get
            {
                var count = 0;
                if (ExcludedNumbers != null)
                    count =  ExcludedNumbers.Count;

                return count;
            }
            private set
            {
                _excludedCount = value;
                NotifyPropertyChanged("ExcludedCount");
                ExcludedCountStatus = _excludedCount.ToString() + " numbers defined";
            }
        }

        /// <summary>
        /// Gets the text value of the count of the excluded numbers.
        /// </summary>
        public string ExcludedCountStatus
        {
            get
            {
                return _excludedCountStatus;
            }
            private set
            {
                _excludedCountStatus = value;
                NotifyPropertyChanged("ExcludedCountStatus");
            }
        }


        /// <summary>
        /// Holds the saved sequences stored in generator's application isolated storage.
        /// </summary>
        public List<Sequence> SavedSequences
        {
            get {
                return _savedSequences; }
            set
            { 
                _savedSequences = value;
                NotifyPropertyChanged("SavedSequences");

                // Set observable saved sequences collection value
                var collection = new SavedSequencesItems();
                foreach (var n in _savedSequences)
                {
                    var s = new SavedSequence();
                    s.Items = n.Numbers;
                    collection.Add(s);
                }
                
                ObservableSavedSequences = collection;
                var count = ObservableSavedSequences.Count;
                this.DrawsCount = count.ToString(); // update draws count
                //this.DrawsCount = "("+count.ToString()+" draws)"; // update draws count

                //UpdateStatisticalData(); //update stats for saved numbers

                var countAsString = count.ToString();
                if (count == 0)
                    countAsString = "No";
                this.TotalSavedSequences = countAsString + " item(s)";

                // Set binding flags
                /*
                if (_savedSequences != null && _savedSequences.Count > 0)
                {
                    if (IsRecurrenceMode)
                        IsSavedSequencesRepeatedSwitchEnabled = true;
                    else
                        IsSavedSequencesRepeatedSwitchEnabled = false;
                }
                else if (_savedSequences == null || (_savedSequences != null && _savedSequences.Count == 0))
                {
                    IsSavedSequencesRepeatedSwitchEnabled = false;
                }
                */
            }
        }

        /// <summary>
        /// Gets or sets the value of the saved sequences repeat toggle switch.
        /// </summary>
        public bool IsSavedSequencesRepeated
        {
            get { return _IsSavedSequencesRepeated; }
            set
            {
                _IsSavedSequencesRepeated = value;
                NotifyPropertyChanged("IsSavedSequencesRepeated");
                SavedSequencesRepeatValue = value.ToString();
                NotifyPropertyChanged("SavedSequencesRepeatValue");
            }
        }

        /// <summary>
        /// Gets the text status of the toggle switch for setting the save sequences repeat mode.
        /// </summary>
        public string SavedSequencesRepeatValue
        {
            get
            {
                if (IsSavedSequencesRepeated) return "On";
                else return "Off";
            }
            private set { }
        }

        // Gets or sets the boolean value to define if the recurring saved sequences switch is enabled or not.
        public bool IsSavedSequencesRepeatedSwitchEnabled
        {
            get
            {
                return _IsSavedSequencesSwitchEnabled;
            }
            private set
            {
                _IsSavedSequencesSwitchEnabled = value;
                NotifyPropertyChanged("IsSavedSequencesRepeatedSwitchEnabled");
            }
        }

        /// <summary>
        /// Copies sequence items into an observable collection list to be used in items source binding scenarios.
        /// </summary>
        public SavedSequencesItems ObservableSavedSequences
        {
            get
            {                
                return _observableSavedSequences;
                
            }
            private set
            {  

                _observableSavedSequences = value;
                NotifyPropertyChanged("ObservableSavedSequences");
                if (_observableSavedSequences != null)
                {
                    SavedSequencesCount = _observableSavedSequences.Count;                    
                }
            }
        }


        public int SavedSequencesCount
        {
            get { return _savedSequencesCount; }
            set
            {
                _savedSequencesCount = value;
                NotifyPropertyChanged("SavedSequencesCount");
            }
        }

        /// <summary>
        /// Gets the collection of statistical data items for each of the numbers used by kino game generator, i.e. 1-80., and
        /// </summary>
        public StatisticalDataItems ObservableStatisticalDataItems
        {
            get
            {
                return _observableStatisticalDataItems;
            }
            set
            {
                _observableStatisticalDataItems = value;
                NotifyPropertyChanged("ObservableStatisticalDataItems");
            }
        }

        /// <summary>
        /// Gets or sets the value of the maximum frequency found in saved generated numbers.
        /// </summary>
        public int MaxFrequency
        {
            get { return _maxFrequency; }
            set { _maxFrequency = value; NotifyPropertyChanged("MaxFrequency"); }
        }

        
        /// <summary>
        /// Gets or sets the value of the number of draws.
        /// </summary>
        public string DrawsCount
        {
            get { return _drawsCount; }
            set { _drawsCount = value; NotifyPropertyChanged("DrawsCount"); }
        }
        

        /// <summary>
        /// Updates the statistical data of kino settings and sets the binding properties.
        /// </summary>
        public void UpdateStatisticalData()
        {
            if (ObservableStatisticalDataItems.Count != 0)
                ObservableStatisticalDataItems.Clear();
            ObservableStatisticalDataItems = GetNumberFrequencies();
            DrawsCount = SavedSequencesCount.ToString();
            //MaxFrequency = ObservableStatisticalDataItems.Max(c => c.Frequency);//? ObservableStatisticalDataItems.Max(c => c.Frequency)*10 : 100;            
        }


        /// <summary>
        /// Updates the statistical data of kino settings and sets the binding properties.
        /// </summary>
        public void UpdateStatisticalData(HashSet<Sequence> sequences)
        {
            ObservableStatisticalDataItems.Clear();
            ObservableStatisticalDataItems = GetNumberFrequencies(sequences);
            //ObservableStatisticalDataItems = GetFrequenciesOverdues(sequences); 
            DrawsCount = "(" + sequences.Count.ToString() + " draws)";
            
        }


        /// <summary>
        /// Returns the frequency (occurrence) of the numbers in the given sequence set.
        /// </summary>
        /// <returns></returns>
        public static StatisticalDataItems GetNumberFrequencies(HashSet<Sequence> sequences)
        {
            var map = new Dictionary<int, int>(); //Holds the number(key) and its frequency value (value).                                 
            StatisticalDataItems items = new StatisticalDataItems();
            for (var i = 1; i <= 80; i++)
            {
                map.Add(i, 0); // number/frequency map
            }
            foreach (var s in sequences)
            {
                foreach (var number in s.Numbers)
                {
                    map[number]++; // store frequency
                }
            }
            var statistics = new KinoStatistics();
            //var overdues = statistics.Overdues(sequences, null); //calculate number overdues
            var overdues = Overdues(sequences); //calculate number overdues
            
                
            foreach (var item in map)
            {
                StatisticalDataEntry entry = new StatisticalDataEntry();
                entry.Tag = item.Key.ToString();
                entry.Value = item.Key;
                entry.Frequency = item.Value;
                entry.Overdues = overdues[item.Key]; //store number skips
                items.Add(entry);                    
            }

            //items.Sort(new StatisticalDataEntrySorter.FrequencySorter() 
            //    { Order = StatisticalDataEntrySortingOrder.Desc }); //Sort by frequency                       

            var max = items.Max(c => c.Frequency);
            var maxOverdue = items.Max(c => c.Overdues);
            foreach (var item in items)
            {
                var temp = item.Frequency;
                if (max == 0)
                    item.Percentage = 0;
                else
                    item.Percentage = (Convert.ToDouble(temp) / Convert.ToDouble(max)) * 100;

                var temp2 = item.Overdues;
                if (maxOverdue == 0)
                    item.OverduePercentage = 0;
                else
                    item.OverduePercentage = (Convert.ToDouble(temp2) / Convert.ToDouble(maxOverdue)) * 100;
            }
            return items;            
        }

        /// <summary>
        /// Returns the frequency (occurrence) of the numbers stored in isolated storage.
        /// </summary>
        /// <returns></returns>
        public StatisticalDataItems GetNumberFrequencies()
        {
            //var sequences = SavedSequences;
            var sequences = this.PersonalSequences; // use hashset instead as source

            var map = new Dictionary<int, int>(); //Holds the number(key) and its frequency value (value).           
            StatisticalDataItems items = new StatisticalDataItems();            
            for(var i = 1; i <= 80; i++)
            {
                map.Add(i, 0);                             
            }

            foreach(var s in sequences)
            {
                foreach(var number in s.Numbers)
                {
                    map[number]++;                 
                }
            }

            foreach(var item in map)
            {
                StatisticalDataEntry entry = new StatisticalDataEntry();
                entry.Tag = item.Key.ToString();            
                entry.Value = item.Key;
                entry.Frequency = item.Value;
                items.Add(entry);                
            }
            var max = items.Max(c => c.Frequency);
            foreach (var item in items)
            {
                var temp = item.Frequency;
                if (max == 0)
                    item.Percentage = 0;
                else
                    item.Percentage = (Convert.ToDouble(temp) / Convert.ToDouble(max)) * 100;
            }
            return items;            
        }


        /// <summary>
        /// Returns the frequency (occurrence) of the numbers stored in isolated storage.
        /// </summary>
        /// <returns></returns>
        public static StatisticalDataItems GetFrequenciesOverdues(HashSet<Sequence> sequences)
        {
            //var sequences = SavedSequences;
            var map = new Dictionary<int, int>(); //Holds the number(key) and its frequency value (value).   
            var map2 = new Dictionary<int, StatisticalMap>();
            //var statisticalMap = new StatisticalMap<int,int>();                  
            StatisticalDataItems items = new StatisticalDataItems();

            for (var i = 1; i <= 80; i++)
            {
                var statisticalMap = new StatisticalMap();
                //statisticalMap[0] = 0;
                map.Add(i, 0);
                map2.Add(i, statisticalMap);
                var frequency = 0;
                var overdue = 0;
                foreach (var s in sequences)
                {
                    foreach (var number in s.Numbers)
                    {
                        if(number == i) //up the frequency
                        {
                            var smap = new StatisticalMap();
                            frequency++;
                            smap.Add(frequency,0);
                            map2[i] = smap;
                        }                            
                        else //up the overdues
                        {
                            var smap = new StatisticalMap();
                            overdue++;
                            smap.Add(0,overdue);
                            map2[i] = smap;
                        }
                    }
                }                
            }

            /*
            foreach (var s in sequences)
            {
                foreach (var number in s.Numbers)
                {
                    map[number]++;
                }
            }
            */

            foreach (var item in map2)
            {
                StatisticalDataEntry entry = new StatisticalDataEntry();
                entry.Tag = item.Key.ToString();
                entry.Value = item.Key;
                entry.Frequency = item.Value.Key;
                entry.Overdues = item.Value[item.Value.Key];
                items.Add(entry);
            }
            var max = items.Max(c => c.Frequency);
            foreach (var item in items)
            {
                var temp = item.Frequency;
                if (max == 0)
                    item.Percentage = 0;
                else
                    item.Percentage = (Convert.ToDouble(temp) / Convert.ToDouble(max)) * 100;
            }

            return items;
        }

        /// <summary>
        /// Returns the total number of saved sequences.
        /// Used in ListHeader  data template binding
        /// </summary>
        public string TotalSavedSequences
        {
            get
            {
                return _totalSavedSequences;
            }
            set { _totalSavedSequences = value; NotifyPropertyChanged("TotalSavedSequences"); }
        }

        /// <summary>
        /// Delegate to handle the property changed event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Notifies that a property has changed.
        /// </summary>
        /// <param name="info"></param>
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        /// <summary>
        /// Calculates the overdues of the numbers in the given sequences for the given set.
        /// </summary>
        /// <param name="sequences"></param>
        /// <param name="set"></param>
        public static Dictionary<int, int> Overdues(ICollection<Sequence> sequences)
        {
            var statsSequence = new StatisticalSequence<PairItem>();
            var overdues = new Dictionary<int, int>();            
            var counterMap = new Dictionary<int, int>(); // set number, overdue
            for (var i = 1; i <= 80; i++)
            {
                counterMap.Add(i, 0);
            }

            if (sequences == null)
                throw new ArgumentException("Collections cannot be null.");

            foreach (var s in KinoSettings.Set)
            {
                foreach (var seq in sequences)
                {
                    if (!seq.Numbers.Contains(s))
                    {
                        counterMap[s]++;
                    }
                    else
                    {
                        counterMap[s] = 0;
                    }
                }
                overdues[s] = counterMap[s];
            }
            return overdues;
        }


        /// <summary>
        /// Returns the frequency (occurrence) of the numbers in the given sequence set.
        /// </summary>
        /// <returns></returns>
        public static StatisticalDataItems GetNumFrequenciesFromCollection(Collection<Sequence> sequences)
        {
            var map = new Dictionary<int, int>(); //Holds the number(key) and its frequency value (value).                                 
            StatisticalDataItems items = new StatisticalDataItems();
            for (var i = 1; i <= 80; i++)
            {
                map.Add(i, 0); // number/frequency map
            }
            foreach (var s in sequences)
            {
                foreach (var number in s.Numbers)
                {
                    map[number]++; // store frequency
                }
            }
            var statistics = new KinoStatistics();            
            var overdues = Overdues(sequences); //calculate number overdues
            
            foreach (var item in map)
            {
                StatisticalDataEntry entry = new StatisticalDataEntry();
                entry.Tag = item.Key.ToString();
                entry.Value = item.Key;
                entry.Frequency = item.Value;
                entry.Overdues = overdues[item.Key]; //store number skips
                items.Add(entry);
            }

            var max = items.Max(c => c.Frequency);
            var maxOverdue = items.Max(c => c.Overdues);
            foreach (var item in items)
            {
                var temp = item.Frequency;
                if (max == 0)
                    item.Percentage = 0;
                else
                    item.Percentage = (Convert.ToDouble(temp) / Convert.ToDouble(max)) * 100;

                var temp2 = item.Overdues;
                if (maxOverdue == 0)
                    item.OverduePercentage = 0;
                else
                    item.OverduePercentage = (Convert.ToDouble(temp2) / Convert.ToDouble(maxOverdue)) * 100;
            }
            return items;
        }

        public static List<int> Set
        {            
            get
            {
                var set = new List<int>();
                for (var i = 1; i <= 80; i++)
                {
                    set.Add(i);
                }
                return set;
            }
        }

    }
}
