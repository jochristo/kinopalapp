﻿using Kino.Data.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Binding
{
    public class DownloadedDrawsViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<Draw> _DownloadedDraws = new ObservableCollection<Draw>();
        //private int CurrentIndexPosition = 0;
        public ObservableCollection<Draw> DownloadedDraws
        {
            get
            {
                return this._DownloadedDraws;
            }
            set
            {
                this._DownloadedDraws = value;
                NotifyPropertyChanged("DownloadedDraws");
            }
        }
        private IncrementalLoader<Draw> IncrementalDrawLoader = new IncrementalLoader<Draw>(); // Incremental loader of downloaded draws


        public async Task LoadMoreDraws(Draw currentDraw)
        {
            //var startPos = 0;
            if (currentDraw != null)
            {
                var index = this.DownloadedDraws.IndexOf(currentDraw);
                if (index < 3)
                {
                    return;
                }
            }
            
            // Load next batch of draws from downloaded.
            var currentBatch = await this.IncrementalDrawLoader.LoadNextBatch();

            foreach (var draw in currentBatch)
            {
                this.DownloadedDraws.Add(draw);
                //CurrentIndexPosition++;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }  
    }
}
