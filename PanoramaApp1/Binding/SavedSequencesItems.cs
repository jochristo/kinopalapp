﻿using Kino.IsolatedSettings;
using KinoGenerator;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Kino.Binding
{
    /// <summary>
    /// Holds the saved sequences in isolated storage file and binds them to the item source collection of a component.
    /// </summary>
    public class SavedSequencesItems : ObservableCollection<Sequence>
    {
        public SavedSequencesItems()
        {
        }
    }

    /// <summary>
    /// Represent an item in the list of saved sequences of a multi selector UI control.
    /// </summary>
    public class SavedSequence : Sequence, INotifyPropertyChanged
    {
        private string _strValue;
        private List<int> _items;
        private ObservableCollection<NumberItem> _sequenceItems;
        private bool _pressed;
       
        public SavedSequence()
        {
            _strValue = string.Empty;
            _items = new List<int>();
        }

        public List<int> Items
        {
            get { return _items; }
            set
            {
                _items = value;
                base.Numbers = value;
                NotifyPropertyChanged("Items");
                if(_items != null)
                {
                    foreach (var n in _items)
                    {
                        this._strValue += n.ToString() + " ";
                    }

                    // set count
                    this.Count = _items.Count;
                }
            }
        }

        public new bool Add(int number)
        {
            Numbers.Add(number);
            Items.Add(number);
            return true;
        }

        public ObservableCollection<Sequence> Sequences
        {
            get
            {
                var collection = new ObservableCollection<Sequence>();
                var sequence = new Sequence();
                foreach (var n in Items)
                {
                    sequence.Add(n);                    
                }                
                collection.Add(sequence);
                return collection;
            }
            private set {  }
        }

        /// <summary>
        /// Gets the binding property of the control to display the string value of the integers in the saved sequence.
        /// </summary>
        public string StringOfSequence
        {
            get
            {
                return this._strValue;
            }
            private set
            {
                this._strValue = value;
                NotifyPropertyChanged("StringOfSequence");
            }
        }

        public ObservableCollection<NumberItem> SequenceItems
        {            
            get
            {
                var collection = new ObservableCollection<NumberItem>();
                Items.Sort();                
                foreach (var n in Items)
                {
                    var item = new NumberItem();
                    item.Value = n;
                    collection.Add(item);
                }
                return collection;
            }
            private set { _sequenceItems = value; }
        }

        public int Count
        {
            get
            {
                return this.Items.Count;
            }
            private set { NotifyPropertyChanged("Count"); }
        }

        public bool Pressed
        {
            get { return _pressed; }
            set
            {
                _pressed = value;
                NotifyPropertyChanged("Pressed");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }


    }

    public class SavedSequenceTemplateItems : ObservableCollection<int>
    {
        public SavedSequenceTemplateItems()
        {

        }

    }

    public class NumberItem
    {
        private int _value;
        private bool _isBonusNumber;
        private System.Windows.Media.Brush _backColor;

        public int Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public bool IsBonusNumber
        {
            get { return _isBonusNumber; }
            set
            {
                _isBonusNumber = value;
            }
        }

        public System.Windows.Media.Brush BackColor
        {
            get
            {
                return this._backColor;
            }
            set
            {
                this._backColor = value;                
            }
        }

        public NumberItem()
        {
        }

    }
}
