﻿using KinoGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Binding
{
    /// <summary>
    /// Represents a numeric sequence of a draw including date and time.
    /// </summary>
    public class DrawSequence : SavedSequence
    {
        private DateTime _drawDateTime; // datetime of the sequence
        public DrawSequence()
        {
            
        }

        /// <summary>
        /// Gets or set the datetime of the draw sequence.
        /// </summary>
        public DateTime DrawDateTime
        {
            get { return _drawDateTime; }
            set { _drawDateTime = value; }
        }
    }
}
