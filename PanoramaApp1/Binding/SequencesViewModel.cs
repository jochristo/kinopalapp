﻿using KinoGenerator;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Binding
{
    /// <summary>
    /// Provides the data source of Sequences to binding elements
    /// </summary>
    public class SequencesViewModel<T> : INotifyPropertyChanged where T : class, new()
    {        
        private ObservableCollection<T> _Items; // Holds current items
        private ObservableCollection<T> _Source;
        private int NextAccessIndex = 0;

        public ObservableCollection<T> Items
        {
            get
            {
                return this._Items;
            }
            set
            {
                this._Items = value;
                NotifyPropertyChanged("Items");
            }
        }
        
        public ObservableCollection<T> Source
        {
            get
            {
                return this._Source;
            }
            set
            {
                this._Source = value;
                NotifyPropertyChanged("Source");
            }
        }
        /// <summary>
        /// Init constructor
        /// </summary>
        public SequencesViewModel()
        {
            this._Items = new ObservableCollection<T>();
            this._Source = new ObservableCollection<T>();
        }

        /// <summary>
        /// Loads the first batch of given size from given collection.
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="size"></param>
        public void LoadFirstBatch(int size)
        {
            if (this.Source == null)
                throw new ArgumentException("collection cannot be null.");
            else if(size == 0)
                throw new ArgumentException("Batch size requested cannot be zero.");

            if (Source.Count != 0)
            {
                var max = size;
                var index = 0;                
                while (index <= max && index <= Source.Count - 1)
                {
                    NextAccessIndex++;
                    this.Items.Add(Source.ElementAt(index));
                    index++;
                }
            }
        }

        /// <summary>
        /// Loads the next batch of given size from given collection.
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="size"></param>
        public void LoadNextBatch(int size)
        {
            if (size == 0)
                throw new ArgumentException("Size cannot be null.");

            var index = this.NextAccessIndex; // +1; // start position
            var limit = this.Source.Count;
            var max = size + index;
            while (index <= max && index <= limit - 1)
            {
                NextAccessIndex++;                
                this.Items.Add(this.Source.ElementAt(index));
                index++;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        } 
    }
}
