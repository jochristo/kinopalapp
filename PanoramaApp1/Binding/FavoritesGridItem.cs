﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Kino.Binding
{
    /// <summary>
    /// Represents a numeric item binding property.
    /// </summary>
    public class FavoritesGridItem : INotifyPropertyChanged
    {
        private int _value;
        private string _text;
        private Brush _backColor;
        private bool _isSelected;

        public FavoritesGridItem()
        {

        }

        public FavoritesGridItem(int i)
        {
            this._value = i;
            this._text = i.ToString();
            this._isSelected = false;
        }

        private int Value
        {
            get
            { 
                return this._value; 
            }
            set
            { 
                this._value = value;
                NotifyPropertyChanged("Value");
            }
        }

        public string text
        {
            get 
            { 
                return this._text;
            }
            set
            {
                this._text = value;
                NotifyPropertyChanged("text");
            }
        }

        public Brush BackColor
        {
            get
            { 
                return this._backColor;
            }
            set
            {
                this._backColor = value;
                NotifyPropertyChanged("BackColor");
            }
        }

        public bool IsSelected
        {
            get
            { 
                return this._isSelected;
            }
            set 
            { 
                this._isSelected = value;
                NotifyPropertyChanged("IsSelected");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }        
    }

    /// <summary>
    /// Populate collection with numbers data from one to 80
    /// </summary>
    public class FavoritesGridItemCollection : ObservableCollection<FavoritesGridItem>
    {
        public FavoritesGridItemCollection()
        {
            for (var i = 1; i < 81; i++)
            {
                this.Add(new FavoritesGridItem(i));
            }
            
        }
    }
}
