﻿using Kino.Data.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Web.Http;

namespace Kino.Binding
{
    public class IncrementalLoader<T> where T : class
    {
        //private int CurrentPageNumber;
        private bool isCurrentlyLoading;
        public ObservableCollection<Draw> DownloadedDraws { get; set; }
        private int maxDrawsDisplayed = 3;
        //private string BaseUrl;
        private int CurrentIndex = 0;

        public IncrementalLoader(ObservableCollection<Draw> DownloadedDraws)
        {
            this.DownloadedDraws = DownloadedDraws;
        }

        public IncrementalLoader()
        {
            
        }


        public async Task<ObservableCollection<T>> LoadNextBatch()
        {
            if (this.isCurrentlyLoading)
            {
                // call in progress
                return null;
            }

            var list = new ObservableCollection<Draw>();
            this.isCurrentlyLoading = true;
            
            if(CurrentIndex > this.DownloadedDraws.Count - 1 - maxDrawsDisplayed)
            {
                return null;
            }

            var startingIndex = CurrentIndex;
            var iterationLimit = startingIndex + maxDrawsDisplayed;
            for (var i = startingIndex; i <= iterationLimit; i++)
            {
                list.Add(this.DownloadedDraws[startingIndex]);
                await Task.Delay(2000);                
            }

            this.isCurrentlyLoading = false;
            return list as ObservableCollection<T>;
        }
    }
}
