﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Binding
{
    /// <summary>
    /// Represents an items source collection of Kino.Binding.DrawSequence objects used in data binding.
    /// </summary>
    public class DrawSequenceItems : ObservableCollection<DrawSequence>
    {
        public DrawSequenceItems() { }
    }
}
