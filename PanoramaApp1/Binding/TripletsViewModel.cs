﻿using KinoGenerator;
using KinoGenerator.Statistics;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Binding
{
    /// <summary>View model class for numeric Triplet object and its frequencies.
    /// </summary>
    public class TripletsViewModel : INotifyPropertyChanged, INotifyCollectionChanged
    {
        private HashSet<Sequence> _source;
        private ulong _totalTripletsCounter = 0;        
        private HashSet<Triplet> _fuckingTriplets;
        private ObservableCollection<Triplet> _triplets;       

        public TripletsViewModel()
        {
            this._source = new HashSet<Sequence>();
            this._triplets = new ObservableCollection<Triplet>();
            this._fuckingTriplets = new HashSet<Triplet>();            
        }

        /// <summary>
        /// Gets or sets the KinoGenerator.Sequence source to calculate the triplets and its frequencies
        /// </summary>
        public HashSet<Sequence> Source
        {
            get { return this._source; }
            set { this._source = value; }
        }

        public HashSet<Triplet> FuckingTriplets
        {
            get { return _fuckingTriplets; }
            set { _fuckingTriplets = value; }
        }

        /// <summary>
        /// Gets or sets the observable collection property containing the three integer values.
        /// </summary>
        public ObservableCollection<Triplet> Triplets
        {
            get { return this._triplets; }
            set { this._triplets = value; NotifyPropertyChanged("Triplets"); }
        }

        public ulong TotalTripletsCounter
        {
            get { return _totalTripletsCounter; }
            set { _totalTripletsCounter = value; NotifyPropertyChanged("TotalTripletsCounter"); }
        }


        public void MaximumTripletsCalculation()
        {
            var maxTripletsCombinations = KinoGenerator.Kino.MaximumKCombinations(20, 3) * (ulong)this.Source.Count();
            this.TotalTripletsCounter = maxTripletsCombinations;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        public event NotifyCollectionChangedEventHandler CollectionChanged;
    }
}
