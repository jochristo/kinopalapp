﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Kino.Binding.Converter
{
    /// <summary>
    /// Converts the frequency of numbers (integer) value to percentage value.
    /// </summary>
    public class FrequencyValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var frequencyPair = (Dictionary<int, int>)value;
            var keys = frequencyPair.Keys;
            int f = 0;
            int max = 0;
            if(keys != null && keys.Count ==1)
            {
               foreach(var k in keys)
               {
                   f = k;
                   max = frequencyPair[k];
               }
            }

            if (max != 0)
                return f / max * 100;

            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
