﻿using KinoGenerator;
using KinoGenerator.Statistics;
using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Binding
{
    /// <summary>
    /// View model class for numeric Pair object and its frequencies.
    /// </summary>
    public class PairsViewModel : INotifyPropertyChanged, INotifyCollectionChanged
    {
        private HashSet<Sequence> _source;
        private ObservableCollection<Pair> _pairs;
        private RangeObservableCollection<Pair> _mypairs;
        private HashSet<Pair> _fuckingPairs;
        private int NextAccessIndex = 0;
        private double _pairsCounter = 0;
        private ulong _totalPairsCounter = 0;
        private List<Pair> _firstPairBatch;

        private HashSet<DownloadedSequence> _sequences;
                
        public PairsViewModel()
        {
            this._pairs = new ObservableCollection<Pair>();
            this._source = new HashSet<Sequence>();
            this._mypairs = new RangeObservableCollection<Pair>();
            this._fuckingPairs = new HashSet<Pair>();
            this._firstPairBatch = new List<Pair>();            
        }

        /// <summary>
        /// Gets or sets the observable collection property containing the two integer values.
        /// </summary>
        public ObservableCollection<Pair> Pairs
        {
            get { return this._pairs; }
            set { this._pairs = value; NotifyPropertyChanged("Pairs"); }
        }

        /// <summary>
        /// Gets or sets the KinoGenerator.Sequence source to calculate the pairs and its frequencies
        /// </summary>
        public HashSet<Sequence> Source
        {
            get { return this._source; }
            set { this._source = value; }
        }


        public RangeObservableCollection<Pair> MyPairs
        {
            get { return _mypairs; }
            set { _mypairs = value; NotifyPropertyChanged("MyPairs"); }
        }

        public HashSet<Pair> FuckingPairs
        {
            get { return _fuckingPairs; }
            set { _fuckingPairs = value; }
        }

        public List<Pair> FirstPairBatch
        {
            get { return _firstPairBatch; }
            set { _firstPairBatch = value; }
        }

        public double PairsCounter
        {
            get { return _pairsCounter; }
            set { _pairsCounter = value; NotifyPropertyChanged("PairsCounter"); }
        }

        public ulong TotalPairsCounter
        {
            get { return _totalPairsCounter; }
            set { _totalPairsCounter = value; NotifyPropertyChanged("TotalPairsCounter"); }
        }

        /// <summary>
        /// Gets or sets the downloaded KinoGenerator.Sequence  set to calculate the pairs and its frequencies
        /// </summary>
        public HashSet<DownloadedSequence> Sequences
        {
            get { return this._sequences; }
            set { this._sequences = value; }
        }

        /// <summary>
        /// Produces a dictionary collection of the numeric pairs and its frequencies found in given collection of sequences
        /// </summary>
        /// <param name="sequences">The input source containing the numeric sequences to get pairs and its frequencies</param>
        /// <returns></returns>
        public void CalcPairs()
        {
            var sequences = this._source;
            if (sequences == null)
                throw new ArgumentException("Collection cannot be null.");
            var dictionary = new Dictionary<Sequence, int>(new SequenceComparer()); // key: sequence, value: frequency
            //var index = 0; //index position in pairs collection
            var isFirstLoad = false;

            //pairs stuff
            foreach (var s in sequences)
            {
                var row = 0;
                var numbers = s.Numbers;                
                var count = numbers.Count();
                var limit = count - 2;
                do
                {
                    var secondItemPos = row + 1;
                    do
                    {
                        var firstItemPos = row;
                        var firstItem = numbers.ElementAt(firstItemPos);
                        var secondItem = numbers.ElementAt(secondItemPos);
                        var sequence = new Sequence();
                        sequence.Numbers.Add(firstItem);
                        sequence.Numbers.Add(secondItem);

                        var match = sequences.Where(t => t.Numbers.Contains(firstItem) && t.Numbers.Contains(secondItem));
                        if (match != null && match.Count() > 0)
                        {
                            var frequency = match.Count(); // get maximum pair frequency found on list
                            var pair = new Pair() { Frequency = frequency, NumberOne = firstItem, NumberTwo = secondItem }; // new Pair object                            
                            var existing = this._pairs.SingleOrDefault(t=>t.NumberOne == pair.NumberOne && t.NumberTwo == pair.NumberTwo); // find existing pair
                            if (existing != null)
                            {
                                if (MyPairs.Count() <= 9)
                                {
                                    MyPairs.Add(existing);
                                    if (MyPairs.Count() == 2)
                                    {
                                        isFirstLoad = false;
                                    }
                                }

                                var indexOf = this.Pairs.IndexOf(existing);
                                existing.Frequency++;
                                if(indexOf > 0)
                                {
                                    var top = this.Pairs[0];
                                    var previous = this.Pairs[indexOf - 1];
                                    if (indexOf == 1)
                                    {
                                        if (existing.Frequency > top.Frequency) // current frequency is greater that previous element's frequency
                                        {
                                            Pairs.Move(indexOf, indexOf - 1);
                                        }
                                    }
                                    else
                                    {
                                        if (top.Frequency > existing.Frequency)
                                        {
                                            var firstGreater = this._pairs.FirstOrDefault(t => t.Frequency <= existing.Frequency); // find first smaller frequency on the list compares to currently added pair item (pair)
                                            if (firstGreater != null)
                                            {
                                                if (existing.Frequency >= firstGreater.Frequency) // current frequency is greater than first with smaller frequency pair iten on the list)
                                                {
                                                    var firstGreaterIndexOf = this.Pairs.IndexOf(firstGreater);
                                                    Pairs.RemoveAt(indexOf);
                                                    Pairs.Insert(firstGreaterIndexOf, existing);                                                    
                                                }
                                            }
                                        }
                                        else if (existing.Frequency >= top.Frequency) // top element's frequency is smaller that current element's frequency, swap
                                        {                                            
                                            Pairs.RemoveAt(indexOf);
                                            Pairs.Insert(0, existing);                                            
                                        }
                                    }
                                }

                                
                                /*
                                if(isFirstLoad)
                                {
                                    LoadFirstBatch(1);
                                    isFirstLoad = true; 
                                }
                                
                                if (MyPairs.Count() <= 9)
                                {
                                    MyPairs.Add(existing);                                    
                                    if (MyPairs.Count() == 2)
                                    {
                                        isFirstLoad = false;                                       
                                    }
                                }
                                */
                                
                            }
                            else // new pair item
                            {
                                if (MyPairs.Count() <= 9)
                                {
                                    MyPairs.Add(pair);
                                    if (MyPairs.Count() == 2)
                                    {
                                        isFirstLoad = true;
                                    }
                                }         

                                this.Pairs.Add(pair);
                                var indexOf = this.Pairs.IndexOf(pair);                                
                                if (indexOf > 0)
                                {                                    
                                    var top = this.Pairs[0];
                                    var previous = this.Pairs[indexOf - 1];
                                    if (indexOf == 1) // list contains only 2 items, compare with top element
                                    {
                                        if(top.Frequency < pair.Frequency) // top element's frequency is smaller that current element's frequency, swap
                                        {
                                            Pairs.Move(indexOf, indexOf - 1);                                            
                                        }
                                    }
                                    else // list contains more than 2 elements, compare with top and other elements too
                                    {
                                        if (top.Frequency > pair.Frequency)
                                        {
                                            var firstGreater = this._pairs.FirstOrDefault(t => t.Frequency <= pair.Frequency); // find first smaller frequency on the list compares to currently added pair item (pair)
                                            if (firstGreater != null)                                                
                                            {
                                                if (pair.Frequency >= firstGreater.Frequency) // current frequency is greater than first with smaller frequency pair iten on the list)
                                                {
                                                    var firstGreaterIndexOf = this.Pairs.IndexOf(firstGreater);
                                                    Pairs.RemoveAt(indexOf);
                                                    Pairs.Insert(firstGreaterIndexOf, pair);                                                    
                                                }
                                            }
                                        }
                                        else if (pair.Frequency >= top.Frequency)// top element's frequency is smaller that current element's frequency, swap
                                        {
                                            Pairs.RemoveAt(indexOf);
                                            Pairs.Insert(0, pair);
                                        }
                                    }
                                }

                                
                                /*
                                if (isFirstLoad)
                                {
                                    LoadFirstBatch(1);
                                    isFirstLoad = false; 
                                }
                                
                                if (MyPairs.Count() <= 9)
                                {
                                    MyPairs.Add(pair);                                    
                                    if (MyPairs.Count() == 2)
                                    {
                                        isFirstLoad = true;
                                    }
                                }
                                */
                            }

                            
                        }

                        secondItemPos++; // get next second item
                    }
                    while (secondItemPos < count && numbers != null);
                    secondItemPos = 0;
                    row++; // get next row in sequences

                } while (row < limit && numbers != null);
            }
            
            //return dictionary;
        }

        /// <summary>
        /// Loads the first batch of given size from given collection.
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="size"></param>
        public void LoadFirstBatch(int size)
        {
            if (this.Pairs == null)
                throw new ArgumentException("collection cannot be null.");
            else if (size == 0)
                throw new ArgumentException("Batch size requested cannot be zero.");

            if (Pairs.Count != 0)
            {
                var max = size;
                var index = 0;
                while (index <= max && index <= Pairs.Count - 1)
                {
                    NextAccessIndex++;
                    this.MyPairs.Add(FuckingPairs.ElementAt(index));
                    index++;
                }
            }
        }

        /// <summary>
        /// Loads the next batch of given size from given collection.
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="size"></param>
        public void LoadNextBatch(int size)
        {
            if (size == 0)
                throw new ArgumentException("Size cannot be null.");

            var index = this.NextAccessIndex; // +1; // start position
            var limit = this.Pairs.Count;
            var max = size + index;
            while (index <= max && index <= limit - 1)
            {
                NextAccessIndex++;
                this.MyPairs.Add(this.Pairs.ElementAt(index));
                index++;
            }
        }

        /// <summary>
        /// Loads the next batch of given size from given collection.
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="size"></param>
        public void LoadTopTenBatch(int size)
        {
            if (size == 0)
                throw new ArgumentException("Size cannot be null.");

            var index = this.NextAccessIndex; // +1; // start position
            var limit = this.Pairs.Count;
            var max = size + index;
            while (index <= max)// && index <= limit - 1)
            {
                NextAccessIndex++;
                this.MyPairs.Add(this.FuckingPairs.ElementAt(index));
                index++;
            }
        }

        public void LoadTopTenDesc()
        {
            var topTen = this.Pairs.OrderByDescending(t => t.Frequency).Take(10);
            this.MyPairs.Clear();
            foreach(var s in topTen)
            {
                MyPairs.Add(s);
            }
        }

        /// <summary>
        /// Produces a dictionary collection of the numeric pairs and its frequencies found in given collection of sequences
        /// </summary>
        /// <param name="sequences">The input source containing the numeric sequences to get pairs and its frequencies</param>
        /// <returns></returns>
        public void GetMyPairs()
        {
            var sequences = this._source;
            if (sequences == null)
                throw new ArgumentException("Collection cannot be null.");

            var dictionary = new Dictionary<Sequence, int>(new SequenceComparer()); // key: sequence, value: frequency                        

            //pairs stuff
            foreach (var s in sequences)
            {
                var row = 0;
                var numbers = s.Numbers;
                var count = numbers.Count();
                var limit = count - 2;
                do
                {
                    var secondItemPos = row + 1;
                    do
                    {
                        var firstItemPos = row;
                        var firstItem = numbers.ElementAt(firstItemPos);
                        var secondItem = numbers.ElementAt(secondItemPos);
                        var sequence = new Sequence();
                        sequence.Numbers.Add(firstItem);
                        sequence.Numbers.Add(secondItem);

                        // initialize new pair object and frequency
                        var frequency = 1;                        
                        var pair = new Pair() { Frequency = frequency, NumberOne = firstItem, NumberTwo = secondItem }; // new Pair object

                        // find existing pair in the collection
                        var existing = this.Pairs.SingleOrDefault(t => t.NumberOne == pair.NumberOne && t.NumberTwo == pair.NumberTwo);                        
                        if(existing != null)
                        {
                            // increase current frequency of found pair, replace item
                            var currentFrequency = existing.Frequency;
                            currentFrequency++;
                            var indexOf = this.Pairs.IndexOf(existing);
                            existing.Frequency = currentFrequency;
                            //this.Pairs[indexOf] = existing;                            
                        }
                        else
                        {
                            // add new pair object
                            this.Pairs.Add(pair);
                            
                        }                            

                        /*
                        // load first batch in the observable collection to update UI
                        if (this.Pairs.Count() == 2)
                        {
                            this.LoadFirstBatch(1);
                        }
                        else if (this.Pairs.Count() == 10)
                        {
                            this.LoadTopTenBatch(7);
                        }
                        */

                        secondItemPos++; // get next second item
                    }
                    while (secondItemPos < count && numbers != null);
                    secondItemPos = 0;
                    row++; // get next row in sequences

                } while (row < limit && numbers != null);
            }
                        
            var sorted = new ObservableCollection<Pair>(this.Pairs.OrderByDescending(t => t.Frequency));
            var topTen = new ObservableCollection<Pair>(sorted.Take(10));
            this.MyPairs.AddRange(topTen);
        }


        public IEnumerable<Pair> GetPairsList()
        {            
            var sequences = this._source;
            if (sequences == null)
                throw new ArgumentException("Collection cannot be null.");
                      
            //pairs stuff
            
            foreach (var s in sequences)
            {
                var row = 0;
                var numbers = s.Numbers;
                var count = numbers.Count();
                var limit = count - 2;
                do
                {
                    var secondItemPos = row + 1;
                    do
                    {
                        var firstItemPos = row;
                        var firstItem = numbers.ElementAt(firstItemPos);
                        var secondItem = numbers.ElementAt(secondItemPos);
                        var sequence = new Sequence();
                        sequence.Numbers.Add(firstItem);
                        sequence.Numbers.Add(secondItem);

                        // initialize new pair object and frequency
                        var frequency = 1;
                        var pair = new Pair() { Frequency = frequency, NumberOne = firstItem, NumberTwo = secondItem }; // new Pair object

                        // find existing pair in the collection
                        var existing = this.FuckingPairs.SingleOrDefault(t => t.NumberOne == pair.NumberOne && t.NumberTwo == pair.NumberTwo);
                        if (existing != null)
                        {
                            // increase current frequency of found pair, replace item
                            var currentFrequency = existing.Frequency;
                            currentFrequency++;
                            //var indexOf = this.FuckingPairs.IndexOf(existing);
                            existing.Frequency = currentFrequency;                           
                        }
                        else
                        {
                            // add new pair object
                            this.FuckingPairs.Add(pair);
                        }

                        this.PairsCounter++;                        
                        this.MyPairs.Add(pair); // add to observable collection
                        if(this.MyPairs.Count() == 20)
                        {
                            foreach(var p in this.MyPairs)
                            {
                                this.FirstPairBatch.Add(p);
                            }
                        }
                        secondItemPos++; // get next second item
                    }
                    while (secondItemPos < count && numbers != null);
                    secondItemPos = 0;
                    row++; // get next row in sequences

                } while (row < limit && numbers != null);
            }
            // return top ten element with the highest frequency value
            return this.FuckingPairs.OrderByDescending(t=>t.Frequency).Take(10);
            
        }
        
                
        public Task<IEnumerable<Pair>> GetMyPairsAsync()
        {
            return Task<IEnumerable<Pair>>.Run(() => { return this.GetPairsList(); });
        }

        public Task<Pair> HighestFrequencyPairAsync(Pair pair, out bool isNewPair)
        {
            isNewPair = false;
            bool isNewItem = false;
            var result = FindHighestFrequencyPair(pair, out isNewItem);
            return Task<Pair>.Run(() => {
                var highest = result;                
                return highest;
            });
        }

        public Pair FindHighestFrequencyPair(Pair pair, out bool isNewItem)
        {
            // find existing pair in the collection
            isNewItem = false;
            var existing = this.FuckingPairs.SingleOrDefault(t => t.NumberOne == pair.NumberOne && t.NumberTwo == pair.NumberTwo);
            if (existing != null)
            {
                // increase current frequency of found pair, replace item
                var currentFrequency = existing.Frequency;
                currentFrequency++;                
                existing.Frequency = currentFrequency;
                return existing;
            }
            else
            {
                // add new pair object
                isNewItem = true;
                this.FuckingPairs.Add(pair);
                return pair;
            }
        }


        public async void GetPairsListAsync()
        {
            var sequences = this._source;
            if (sequences == null)
                throw new ArgumentException("Collection cannot be null.");

            //pairs stuff
            foreach (var s in sequences)
            {
                var row = 0;
                var numbers = s.Numbers;
                var count = numbers.Count();
                var limit = count - 2;
                do
                {
                    var secondItemPos = row + 1;
                    do
                    {
                        var firstItemPos = row;
                        var firstItem = numbers.ElementAt(firstItemPos);
                        var secondItem = numbers.ElementAt(secondItemPos);
                        var sequence = new Sequence();
                        sequence.Numbers.Add(firstItem);
                        sequence.Numbers.Add(secondItem);

                        // initialize new pair object and frequency
                        var frequency = 1;
                        var pair = new Pair() { Frequency = frequency, NumberOne = firstItem, NumberTwo = secondItem }; // new Pair object

                        // find existing pair in the collection
                        var isNewItem = false;
                        var task = await HighestFrequencyPairAsync(pair, out isNewItem);                       

                        secondItemPos++; // get next second item
                    }
                    while (secondItemPos < count && numbers != null);
                    secondItemPos = 0;
                    row++; // get next row in sequences

                } while (row < limit && numbers != null);
            }
            // return top ten element with the highest frequency value
            //return this.FuckingPairs.OrderByDescending(t => t.Frequency).Take(10);
            var order = this.FuckingPairs.OrderByDescending(t => t.Frequency).Take(10);
            //this.MyPairs.Clear();
            foreach(var s in order)
            {
                MyPairs.Add(s);
            }

        }

        public void MaxPairsCalc()
        {
            var maxPairCombinations = KinoGenerator.Kino.MaximumKCombinations(20, 2) * (ulong)this.Source.Count();
            this.TotalPairsCounter = maxPairCombinations;
        }

        public double UpdatePairsCounter()
        {
            return this.PairsCounter;
        }


        public void PopulateFirstPairBatch()
        {
            if(this.FuckingPairs != null)
            {
                foreach (var p in this.FuckingPairs)
                {
                    this.FirstPairBatch.Add(p);
                }
            }
        }
        

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        public event NotifyCollectionChangedEventHandler CollectionChanged;
    }



    public class RangeObservableCollection<T> : ObservableCollection<T>
    {
        private bool _suppressNotification = false;

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (!_suppressNotification)
                base.OnCollectionChanged(e);
        }

        
        public void AddRange(IEnumerable<T> list)
        {
            if (list == null)
                throw new ArgumentNullException("list");

            _suppressNotification = true;

            foreach (T item in list)
            {
                Add(item);
            }
            _suppressNotification = false;
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
    }

}
