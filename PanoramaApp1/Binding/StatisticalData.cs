﻿using Kino.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Kino.Binding
{
    /// <summary>
    /// Represents the statistical data of drawn kino numbers and their frequencies.
    /// </summary>
    public class StatisticalDataItems : ObservableCollection<StatisticalDataEntry> 
    {
        public StatisticalDataItems(List<StatisticalDataEntry> list) : base(list) { }

        public StatisticalDataItems() { }

        public int Max
        {
            get
            {
                return this.Max;
            }            
        }
    }

    /// <summary>
    /// Represents a statistical entry (number's tag, value, frequency) for a number drawn by kino generator
    /// </summary>
    public class StatisticalDataEntry : INotifyPropertyChanged
    {
        private string _tag; //text box test
        private int _value; // numeric value
        private int _frequency; // value of frequency
        private int _maximum;
        private string _frequencyText;
        private Dictionary<int, int> _frequencyMaxPair;
        private double _fpercentage;
        private int _overdues;
		private string _overduesText;
        private double _oPercentage;
        private double _oBarIndicatorWidth;
        private double _oBarTrackwidth;
        private double _oCounterPercentage;

        public StatisticalDataEntry()
        {

        }

        public string Tag
        {
            get { return _tag; }
            set { _tag = value; NotifyPropertyChanged("Tag"); }

        }

        public int Value
        {
            get { return _value; }
            set { _value = value; NotifyPropertyChanged("Value"); }
        }

        public int Frequency
        {
            get { return _frequency; }
            set { _frequency = value; NotifyPropertyChanged("Frequency"); _frequencyText = _frequency.ToString(); }
        }

        public int Maximum
        {
            get { return _maximum; }
            set 
			{
				_maximum = value; //Convert.ToInt32(Math.Round(value * 1.07)); 				
				NotifyPropertyChanged("Maximum"); }
        }

        public string FrequencyText
        {
            get { return _frequencyText; } //+ " occurrence(s)"; }
            private set { _frequencyText = value; NotifyPropertyChanged("FrequencyText"); }
        }

        public Dictionary<int,int> FrequencyMaxPair
        {
            get { return _frequencyMaxPair; }
            private set
            {
                var pair = new Dictionary<int, int>();
                pair.Add(Frequency, Maximum);
                _frequencyMaxPair = pair;
                NotifyPropertyChanged("FrequencyMaxPair");
            }
        }

        public double Percentage
        {
            get { return _fpercentage; }
            set { _fpercentage = value; NotifyPropertyChanged("Percentage"); }
        }

        public int Overdues
        {
            get { return _overdues; }
            set { _overdues = value; NotifyPropertyChanged("Overdues"); OverduesText = _overdues.ToString(); }
        }

        public string OverduesText
        {
            get { return _overduesText; }
            set { _overduesText = value; NotifyPropertyChanged("OverduesText"); }
        }		
		
        public double OverduePercentage
        {
            get { return _oPercentage; }
            set
            { 
                _oPercentage = value; NotifyPropertyChanged("OverduePercentage");

                var defaultWidth = 200.0;
                var overdues = Math.Round(OverduePercentage);
                Width = Math.Round(defaultWidth * overdues / 100);
                var nonValued = 100.0 - overdues;
                OverduesBarIndicatorWidth = Math.Round((defaultWidth * nonValued / 100));

                OverdueCounterPercentage = 100 - OverduePercentage; //Calculate counter overdues percentage for the non-value percentage.
            }
        }

        /// <summary>
        /// Gets or set the remaining percentage for the progressbar to display as non-valued.
        /// </summary>
        public double OverdueCounterPercentage
        {
            get { return _oCounterPercentage; }
            set
            {
                _oCounterPercentage = value;
                NotifyPropertyChanged("OverdueCounterPercentage");
            }
        }

        public double Width
        {
            get
            {
                return _oBarTrackwidth;
            }
            set
            {
                _oBarTrackwidth = value; NotifyPropertyChanged("Width");
            }
        }

        public HorizontalAlignment BarTrackHorizontalAlignment
        {
            get { return System.Windows.HorizontalAlignment.Right; }
        }

        public Brush FillColor
        {
            get
            {
                SolidColorBrush brush = new SolidColorBrush(new Color(){ R=0,B=62,G=151});
                return brush;
            }
        }

        public double OverduesBarIndicatorWidth
        {
            get
            {
                return _oBarIndicatorWidth;
            }
            set { _oBarIndicatorWidth = value; NotifyPropertyChanged("OverduesBarIndicatorWidth"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

    }
}
