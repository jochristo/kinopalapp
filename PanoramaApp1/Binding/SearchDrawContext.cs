﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Kino.Binding
{
    /// <summary>
    /// Data context for search buttons in search view.
    /// </summary>
    public class SearchDrawContext : INotifyPropertyChanged
    {
        private const string _last = "last";
        private string _drawNumber;
        private DateTime _fromDate;
        private DateTime _toDate;
        //private string _fromDateString;
        //private string _toDateString;
        private List<string> _listOfDates;
        private DateCriteria _dates;

        private const string inputFormat = "d/M/yyyy HH:mm:ss";
        private const string outputFormat = "dd-MM-yyyy";
        private CultureInfo provider = CultureInfo.CurrentCulture;

        // Bind to IsEnabled property of DatePicker/Button controls in viewmodel
        private bool _isFromDateValid;
        private bool _isToDateValid;
        private bool _isFetchDatesBtnEnabled;
        public bool _isSingleDateTimeMode;
        private bool _isSingleDateValid;
        private bool SingleSearchButtonState = true;
        private bool DualSearchButtonState = true;

        public string Last
        {
            get { return _last; }
        }

        public string DrawNumber
        {
            get { return _drawNumber; }
            set { _drawNumber = value; NotifyPropertyChanged("DrawNumber"); }
        }

        public DateTime FromDate
        {
            get { return _fromDate; }
            set
            {
                _fromDate = value;
                NotifyPropertyChanged("FromDate");                
                DateCriteria dc = new DateCriteria();
                Dates.Start = _fromDate;
                dc = Dates;
                Dates = dc;
            }
        }

        public DateTime ToDate
        {
            get { return _toDate; }
            set
            { 
                _toDate = value; 
                NotifyPropertyChanged("ToDate");
                DateCriteria dc = new DateCriteria();                
                Dates.End = _toDate;
                dc = Dates;
                Dates = dc;
            }
        }

        public DateCriteria Dates
        {
            get
            {
                return _dates;
            }
            set
            {
                _dates = value; 
                NotifyPropertyChanged("Dates");
            }
        }

        public bool IsFromDateValid
        {
            get { return _isFromDateValid; }
            set
            {
                _isFromDateValid = value;
                NotifyPropertyChanged("IsFromDateValid");
                if (IsFromDateValid == false)
                    IsFetchDatesBtnEnabled = false; //(IsFromDateValid == true && IsToDateValid == true) ? true : false;
                else if (IsFromDateValid == true)
                    IsFetchDatesBtnEnabled = IsToDateValid == true ? true : false; 

                SingleSearchButtonState = IsFetchDatesBtnEnabled; //copy state
            }
        }

        public bool IsToDateValid
        {
            get { return _isToDateValid; }
            set
            {
                _isToDateValid = value;
                NotifyPropertyChanged("IsToDateValid");
                if (_isToDateValid == false)
                    IsFetchDatesBtnEnabled = false; //(IsFromDateValid == true && IsToDateValid == true) ? true : false;
                else
                    IsFetchDatesBtnEnabled = IsFromDateValid == true ? true : false; 
                //IsFetchDatesBtnEnabled = (IsFromDateValid == true && IsToDateValid == true) ? true : false;
                SingleSearchButtonState = IsFetchDatesBtnEnabled; //copy state
            }
        }


        public bool IsSingleDateValid
        {
            get { return _isSingleDateValid; }
            set
            {
                _isSingleDateValid = value;
                NotifyPropertyChanged("IsSingleDateValid");
                IsFetchDatesBtnEnabled = value;
                DualSearchButtonState = IsFetchDatesBtnEnabled;
            }
        }

        public bool IsSingleDateTimeMode
        {
            get { return _isSingleDateTimeMode; }
            set
            {
                _isSingleDateTimeMode = value;
                NotifyPropertyChanged("IsSingleDateTimeMode");
                if (_isSingleDateTimeMode == false)
                    this.IsFetchDatesBtnEnabled = SingleSearchButtonState;
                else
                    this.IsFetchDatesBtnEnabled = DualSearchButtonState;
            }
        }

        public bool IsFetchDatesBtnEnabled
        {
            get
            {
                return _isFetchDatesBtnEnabled;
            }
            set
            {
                _isFetchDatesBtnEnabled = value; 
                NotifyPropertyChanged("IsFetchDatesBtnEnabled");
            }
        }


        public SearchDrawContext()
        {
            this._drawNumber = "draw number";
            this._toDate = System.DateTime.Now;
            this._fromDate = System.DateTime.Now;            
            this._listOfDates = new List<string>();
            _listOfDates.Add(null);
            _listOfDates.Add(null);
            _dates = new DateCriteria();
            _isFromDateValid = true;
            _isToDateValid = true;
            IsSingleDateTimeMode = false; // assume on start dual dates search mode is set
            IsFetchDatesBtnEnabled = true;
        }


        /// <summary>
        /// Marks all datetime and button isEnable state as true on rendering the results
        /// </summary>
        public void ResetValidationStatus()
        {
            this.IsFetchDatesBtnEnabled = true;
            this.IsFromDateValid = true;
            this.IsSingleDateValid = true;
            this.IsToDateValid = true;
        }

        /// <summary>
        /// Delegate to handle the property changed event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Notifies that a property has changed.
        /// </summary>
        /// <param name="info"></param>
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }

    /// <summary>
    /// Represents a command parameter object holding two DateTime properties.
    /// </summary>
    public class DateCriteria
    {
        private const string inputFormat = "d/M/yyyy HH:mm:ss";
        private const string outputDateFormat = "dd-MM-yyyy";
        private const string outputTimeFormat = "HH:mm";
        private CultureInfo provider = CultureInfo.CurrentCulture;
        private CultureInfo elGR = new CultureInfo("el-GR");
        private DateTime _start;
        private DateTime _end;

        public DateTime Start
        {
            get
            {
                return _start;
            }
            set
            {
                _start = value;
            }
        }
        public DateTime End
        {
            get
            {
                return _end;
            }
            set
            {
                _end = value;
            }
        }

        public string StartAsString
        {
            get
            {
                //DateTime date = DateTime.ParseExact(this.Start.ToString(), inputFormat, provider);
                string date = Start.ToString(outputDateFormat, provider);
                //return date.ToString(outputFormat);
                return date;
            }
        }
        public string EndAsString
        {
            get
            {
                //DateTime date = DateTime.ParseExact(this.End.ToString(), inputFormat, provider);
                string date = End.ToString(outputDateFormat, provider);
                var time = End.ToString(outputTimeFormat, provider); //get time
                //return date.ToString(outputFormat);
                return date+" "+time;
            }
        }

        public DateCriteria()
        {
            Start = DateTime.Now;
            End = DateTime.Now;
        }

        
        public string StartTime
        {
            get{return Start.ToString(outputTimeFormat, provider);} //get time}            
        }

        public string EndTime
        {
            get { return End.ToString(outputTimeFormat, provider); } //get time}  
        }
        
    }
}
