﻿using Kino.Resources;
using System.Collections.Generic;

namespace PanoramaApp1
{
    /// <summary>
    /// Provides access to string resources.
    /// </summary>
    public class LocalizedStrings
    {
        private static AppResources _localizedResources = new AppResources();

        public AppResources LocalizedResources { get { return _localizedResources; } }

        public static Dictionary<string,string> GreekLabels()
        {
            var dic = new Dictionary<string, string>();
            dic.Add("Settings", "Ρυθμίσεις");
            return dic;
        }
    }
}