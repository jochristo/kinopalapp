﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Data
{
    /// <summary>
    /// Represents a draw's start and end date validation result.
    /// </summary>
    public enum DrawDateValidationResult
    {
        Invalid = 0,
        Valid = 1
    }
}
