﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Data
{
    public class JsonAsync
    {
        /// <summary>
        /// Downloads the resource at the specified Uri and with given WebClient as an synchronous operation using a task object.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="uri"></param>
        /// <returns></returns>
        public static async Task<string> DownloadStringTaskAsync(WebClient client, Uri uri)
        {
            var tcs = new TaskCompletionSource<string>();

            client.DownloadStringCompleted += (o, e) =>
            {
                if (e.Cancelled)
                    tcs.SetCanceled();
                else
                {
                    try
                    {
                        tcs.SetResult(e.Result);
                    }
                    catch(System.Exception ex)
                    {
                        tcs.SetException(ex);
                    }
                }
            };
            client.DownloadStringAsync(uri);            
            return await tcs.Task;
        }

        /// <summary>
        /// Downloads the resource at the specified Uri and with given WebClient as an synchronous operation using a task object.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="uri"></param>
        /// <returns></returns>
        public static async Task<string> DownloadJsonStringTaskAsync(WebClient client, Uri uri)
        {
            var tcs = new TaskCompletionSource<string>();

                client.DownloadStringCompleted += (o, e) =>
                {
                    if (e.Error != null ) //&& e.Error.InnerException != null)
                        tcs.SetException(e.Error);
                    else if (e.Cancelled)
                        tcs.SetCanceled();
                    else
                        tcs.SetResult(e.Result);
                };                
                client.DownloadStringAsync(uri); //download the resource
                return await tcs.Task;            
        }

        private static void DownloadProgressCallback(object sender, DownloadProgressChangedEventArgs e)
        {
            // Displays the operation identifier, and the transfer progress.
            Console.WriteLine("{0}    downloaded {1} of {2} bytes. {3} % complete...",
                (string)e.UserState,
                e.BytesReceived,
                e.TotalBytesToReceive,
                e.ProgressPercentage);
        }

    }
}
