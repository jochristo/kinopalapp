﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Data.Exception
{
    public class InvalidUrlException : ApplicationException
    {
        public InvalidUrlException()
        {

        }
        public InvalidUrlException(string message) : base(message)
        {

        }
        public InvalidUrlException(string message, System.Exception inner) : base(message,inner)
        {           

        }
    }
}
