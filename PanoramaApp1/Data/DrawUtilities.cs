﻿using Kino.Binding;
using Kino.Data.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Data
{
    /// <summary>
    /// Contains functions to parse System.DateTime objects.
    /// </summary>
    public static class DrawUtilities
    {
        /// <summary>
        /// Gets the dates in string format "dd-mm-yyy" between the two given System.DateTime objects.
        /// </summary>
        /// <param name="low"></param>
        /// <param name="high"></param>
        /// <returns>System.Collections.List</string></returns>
        public static List<String> GetDates(DateTime low, DateTime high)
        {
            string outputFormat = "dd-MM-yyyy";
            CultureInfo provider = CultureInfo.CurrentCulture;
            CultureInfo elGR = new CultureInfo("el-GR");

            var dates = new List<string>();
            var set = new HashSet<string>();
            var calendar = new System.Globalization.GregorianCalendar();            
            var lowDay = low.Day;
            var highDay = high.Day;
            var lowMonth = calendar.GetMonth(low);
            var highMonth = calendar.GetMonth(high);
            var lowMonthDays = calendar.GetDaysInMonth(low.Year, low.Month);
            var highMonthDays = calendar.GetDaysInMonth(high.Year, high.Month);
            var lowYear = low.Year;
            var highYear = high.Year;
                        
            /*
            if(high.Year > DateTime.Now.Year)            
                highYear = DateTime.Now.Year;            
            if (highMonth > DateTime.Now.Month)
                highMonth = DateTime.Now.Month;
            if (highDay > DateTime.Now.Day)
                highDay = DateTime.Now.Day;
            */

            // check if high dateTime is beyond present year, month, day etc.
            if (high.Year > DateTime.Now.Year)
            {
                highYear = DateTime.Now.Year;
                if (highMonth > DateTime.Now.Month)
                    highMonth = DateTime.Now.Month;
                if (highDay > DateTime.Now.Day)
                    highDay = DateTime.Now.Day;
            }

            var yearSpan = highYear - lowYear + 1; // set highest span year limit
            var currentYear = lowYear; // start with low year as current year and
            for (var i = 0; i < yearSpan; i++) // start iteration
            {
                if (currentYear <= highYear)
                {
                    for (var m = lowMonth; m <= 12; m++)
                    {
                        lowMonthDays = calendar.GetDaysInMonth(currentYear, m);
                        var maxMonthDays = lowMonthDays;
                        if (m == highMonth && currentYear == highYear)
                            maxMonthDays = highDay;
                        for (var d = lowDay; d <= maxMonthDays; d++)
                        {
                            var date = new DateTime(currentYear, m, d, calendar);
                            var converted = date.ToString(outputFormat, provider);
                            set.Add(converted);
                        }

                        if (m == highMonth && currentYear == highYear)
                            break;

                        lowDay = 1;
                    }
                    if (currentYear == highYear)
                        break;

                    lowMonth = 1;
                }
                if (currentYear == highYear)
                    break;

                currentYear++;
            }//end for
                return set.ToList<string>();            
        }


        /// <summary>
        /// Validates given date time against current date and time.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static bool IsDrawDateValid(DateTime date)
        {
            if (date > DateTime.Now)
                return false;
            return true;
        }

        /// <summary>
        /// Returns the validation result of given date criteria.
        /// </summary>
        /// <param name="dates"></param>
        /// <returns></returns>
        public static DrawDateValidationResult Validation(DateTime date, out string message)
        {
            var result = DrawDateValidationResult.Valid;
            message = String.Empty;
            if (date > DateTime.Now)
            {
                result = DrawDateValidationResult.Invalid;
                message = "Date should be earlier than current date";
            }
            return result;
        }

        /// <summary>
        /// Compares the given draw time in string format with the given draw's time.
        /// </summary>
        /// <param name="draw"></param>
        /// <param name="drawTime"></param>
        /// <returns></returns>
        public static bool IsDrawTimeFound(Draw draw, string drawTime)
        {
            string inputFormat = "dd-MM-yyyyTHH:mm:ss";
            CultureInfo provider = CultureInfo.CurrentCulture;
            DateTime date = DateTime.ParseExact(draw.drawTime, inputFormat, provider);
            string timeFormat = "HH:mm";
            var time = date.ToString(timeFormat);
            if (drawTime == time)
                return true;
            return false;
        }
        
        /// <summary>
        /// Validates low and high date. Valid period should be one month long
        /// </summary>
        /// <param name="low"></param>
        /// <param name="high"></param>
        /// <returns></returns>
        public static bool IsStatisticsDatesPeriodValid(DateTime low, DateTime high)
        {
            var lowMonth = low.Month;
            var highMonth = high.Month;
            var lowDay = low.Day;
            var highDay = high.Day;
            var calendar = new System.Globalization.GregorianCalendar();
            var lowMonthDays = calendar.GetDaysInMonth(low.Year, low.Month);
            var highMonthDays = calendar.GetDaysInMonth(high.Year, high.Month);
            if(highMonth - lowMonth > 1)
            {                
                return false;
            }
            else if(highMonth - lowMonth == 1)
            {
                if(lowDay < highDay)
                {
                    return false;
                }
                
            }
            return true;
        }

        /// <summary>
        /// Validates low and high date. Valid period should be 10 days long
        /// </summary>
        /// <param name="low"></param>
        /// <param name="high"></param>
        /// <returns></returns>
        public static bool IsSearchRangeValid(DateTime low, DateTime high)
        {
            var lowDate = new DateTime(low.Year, low.Month, low.Day);
            var highDate = new DateTime(high.Year, high.Month, high.Day);
            var calendar = new System.Globalization.GregorianCalendar();
            var lowMonthDays = calendar.GetDaysInMonth(lowDate.Year, lowDate.Month);
            var highMonthDays = calendar.GetDaysInMonth(highDate.Year, highDate.Month);
            if (highDate.Year != lowDate.Year)
            {
                return false;
            }
            else if (highDate.Month - lowDate.Month > 1)
            {
                return false;
            }
            else if (highDate.Month - lowDate.Month == 1)
            {
                if (lowDate.Day < highDate.Day)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Returns 24-h search based draws according to last draw's time on the given list.
        /// </summary>
        /// <param name="draws"></param>
        /// <returns></returns>
        public static Collection<Draw> GetActualDraws(List<Draw> draws)
        {
            // the output draws
            var drawCache = new Collection<Draw>();

            // define start and end date, times
            var lastDayDrawTime = draws[draws.Count - 1].drawTime;
            var lastDraw = draws[draws.Count - 1];
            var firstDayDrawTime = draws[0].drawTime;
            var ci = new CultureInfo("el-GR");
            ci.DateTimeFormat = new DateTimeFormatInfo();
            ci.DateTimeFormat.FullDateTimePattern = "dd-MM-yyyyTHH:mm:ss";
            var firstDateTime = DateTime.ParseExact(firstDayDrawTime, "dd-MM-yyyyTHH:mm:ss", ci);
            var greek = new CultureInfo("el-GR").DateTimeFormat;
            DateTime parsedLastDraw = DateTime.ParseExact(lastDayDrawTime, "dd-MM-yyyyTHH:mm:ss", ci);
            var today = DateTime.Now;

            var isLastDrawDateEarlyThanToday = (parsedLastDraw.Month == today.Month && parsedLastDraw.Day < today.Day)
                || (parsedLastDraw.Month < today.Month);

            // find draws not to be skipped: relative to last draw's date and time
            //var notSkipped = draws.Where(d => !IsDrawTimeToSkip(d, firstDateTime, parsedLastDraw));
            var notSkipped = draws.Where(d => !IsDrawSkipped(d, firstDateTime, parsedLastDraw)); 
            var notSkippedToList = notSkipped.ToList();
            foreach(var d in notSkippedToList)
            {
                //d.results.Sort();
                drawCache.Add(d);
            }            
            return drawCache;
        }

        public static bool IsDrawTimeToSkip(Draw jsonDraw)
        {
            var ci = new CultureInfo("el-GR");
            ci.DateTimeFormat = new DateTimeFormatInfo();
            ci.DateTimeFormat.FullDateTimePattern = "dd-MM-yyyyTHH:mm:ss";
            var datetime = DateTime.ParseExact(jsonDraw.drawTime, "dd-MM-yyyyTHH:mm:ss", ci);
            if (datetime.Year == DateTime.Now.Year && datetime.Month == DateTime.Now.Month && datetime.Day == DateTime.Now.Day)
            {
                // empty results - earlier than current day 09:00
                if (jsonDraw.results.Count == 0)
                {
                    return true; // to be skipped
                }
            }
            else if(datetime.Year == DateTime.Now.Year && datetime.Month == DateTime.Now.Month && datetime.Day == DateTime.Now.Day)
            {

            }
            return false;  
        }

        /// <summary>
        /// Determines if given json draw is to be included/skipped in search within a list.
        /// Criteria include the last and first datetime property of the given json draw object.
        /// </summary>
        /// <param name="jsonDraw"></param>
        /// <param name="firstDrawDateTime"></param>
        /// <param name="lastDrawDateTime"></param>
        /// <returns></returns>
        public static bool IsDrawTimeToSkip(Draw jsonDraw, DateTime firstDrawDateTime, DateTime lastDrawDateTime)
        {
            var ci = new CultureInfo("el-GR");
            ci.DateTimeFormat = new DateTimeFormatInfo();
            ci.DateTimeFormat.FullDateTimePattern = "dd-MM-yyyyTHH:mm:ss";
            var datetime = DateTime.ParseExact(jsonDraw.drawTime, "dd-MM-yyyyTHH:mm:ss", ci);

            //var today = DateTime.Now;
            if (jsonDraw.results.Count == 0)
            {
                lastDrawDateTime = lastDrawDateTime.AddDays(-1); // remove one day
            }
            
            // same search month, same day -> include all draws from 09:00 to 22:00
            if (datetime.Year == lastDrawDateTime.Year && datetime.Month == lastDrawDateTime.Month && datetime.Day == lastDrawDateTime.Day)
            {
                return false;
            }
            // different low and high month in search range
            else if(datetime.Year == lastDrawDateTime.Year && datetime.Month == lastDrawDateTime.Month && datetime.Day < lastDrawDateTime.Day)
            {
                if (datetime.Month == firstDrawDateTime.Month && datetime.Day == firstDrawDateTime.Day)
                {
                    if (datetime.Hour < lastDrawDateTime.Hour)
                    {
                        return true;
                    }
                    else if(datetime.Hour == lastDrawDateTime.Hour && datetime.Minute < lastDrawDateTime.Minute)
                    {
                        return true;
                    }
                }
            }
            else if (datetime.Year == lastDrawDateTime.Year && datetime.Month < lastDrawDateTime.Month)
            {
                if (datetime.Month == firstDrawDateTime.Month && datetime.Day == firstDrawDateTime.Day)
                {
                    if (datetime.Hour < lastDrawDateTime.Hour)
                    {
                        return true;
                    }
                    else if (datetime.Hour == lastDrawDateTime.Hour && datetime.Minute < lastDrawDateTime.Minute)
                    {
                        return true;
                    }
                }
            }
            return false;
        }


        public static Collection<Draw> TakeTenDaysOldDraws(Collection<Draw> Draws)
        {
            var lastDrawDate = Draws[Draws.Count - 1].drawTime;
            var ci = new CultureInfo("el-GR");
            ci.DateTimeFormat = new DateTimeFormatInfo();
            ci.DateTimeFormat.FullDateTimePattern = "dd-MM-yyyyTHH:mm:ss";
            var last= DateTime.ParseExact(lastDrawDate, "dd-MM-yyyyTHH:mm:ss", ci);
            var skipped = Draws.Where(d => !IsDrawTimeOlderThanTenDays(d, last)).ToList();
            return new Collection<Draw>(skipped);            
        }


        public static bool IsDrawTimeOlderThanTenDays(Draw jsonDraw, DateTime lastDrawDateTime)
        {
            var ci = new CultureInfo("el-GR");
            ci.DateTimeFormat = new DateTimeFormatInfo();
            ci.DateTimeFormat.FullDateTimePattern = "dd-MM-yyyyTHH:mm:ss";
            var datetime = DateTime.ParseExact(jsonDraw.drawTime, "dd-MM-yyyyTHH:mm:ss", ci);
            var tenDaysEarlier = lastDrawDateTime.AddDays(-10);

            if (datetime >= tenDaysEarlier)
            {
                return false;
            }

            return true;


            /*
            // same search month, same day -> include all draws from 09:00 to 22:00
            if (datetime.Year == lastDrawDateTime.Year && datetime.Month == lastDrawDateTime.Month && datetime.Day == lastDrawDateTime.Day)
            {
                return false;
            }
            // different low and high month in search range
            else if (datetime.Year == lastDrawDateTime.Year && datetime.Month == lastDrawDateTime.Month && lastDrawDateTime.Day - datetime.Day <= 10)
            {
                //if (datetime.Month == lastDrawDateTime.Month && lastDrawDateTime.Day - datetime.Day == 10)
                //{
                    if (datetime.Hour > lastDrawDateTime.Hour)
                    {
                        return false;
                    }
                    else if (datetime.Hour == lastDrawDateTime.Hour && datetime.Minute < lastDrawDateTime.Minute)
                    {
                        return true;
                    }
                //}
            }
            else if (datetime.Year == lastDrawDateTime.Year && datetime.Month < lastDrawDateTime.Month)
            {
                if (datetime.Month == lastDrawDateTime.Month && datetime.Day == lastDrawDateTime.Day)
                {
                    if (datetime.Hour < lastDrawDateTime.Hour)
                    {
                        return true;
                    }
                    else if (datetime.Hour == lastDrawDateTime.Hour && datetime.Minute < lastDrawDateTime.Minute)
                    {
                        return true;
                    }
                }
            }
            return true;
            */
        }

        /// <summary>
        /// Determines if current JSON Draw's date and time are within the search range
        /// and the skip result is set and returned.
        /// </summary>
        /// <param name="jsonDraw"></param>
        /// <param name="firstDrawDateTime"></param>
        /// <param name="lastDrawDateTime"></param>
        /// <returns></returns>
        public static bool IsDrawSkipped(Draw jsonDraw, DateTime firstDrawDateTime, DateTime lastDrawDateTime)
        {
            var ci = new CultureInfo("el-GR");
            ci.DateTimeFormat = new DateTimeFormatInfo();
            ci.DateTimeFormat.FullDateTimePattern = "dd-MM-yyyyTHH:mm:ss";
            var datetime = DateTime.ParseExact(jsonDraw.drawTime, "dd-MM-yyyyTHH:mm:ss", ci);

            var now = DateTime.Now;
            var today = new DateTime(now.Year, now.Month, now.Day); //, now.Hour, now.Minute, now.Second) ;
            var lastDay = new DateTime(lastDrawDateTime.Year, lastDrawDateTime.Month, lastDrawDateTime.Day);

            if (jsonDraw.results.Count == 0)
            {
                lastDrawDateTime = lastDrawDateTime.AddDays(-1); // remove one day
            }

            // last search date is earlier than today
            if (lastDrawDateTime < today)
            {
                return false;
            }
            // last search date is today
            else if (lastDrawDateTime >= today)
            {
                // same search month, same day -> include all draws from 09:00 to 22:00
                if (datetime.Year == lastDrawDateTime.Year && datetime.Month == lastDrawDateTime.Month && datetime.Day == lastDrawDateTime.Day)
                {
                    return false;
                }
                // different low and high month in search range
                else if (datetime.Year == lastDrawDateTime.Year && datetime.Month == lastDrawDateTime.Month && datetime.Day < lastDrawDateTime.Day)
                {
                    if (datetime.Month == firstDrawDateTime.Month && datetime.Day == firstDrawDateTime.Day)
                    {
                        if (datetime.Hour < lastDrawDateTime.Hour)
                        {
                            return true;
                        }
                        else if (datetime.Hour == lastDrawDateTime.Hour && datetime.Minute < lastDrawDateTime.Minute)
                        {
                            return true;
                        }
                    }
                }
                else if (datetime.Year == lastDrawDateTime.Year && datetime.Month < lastDrawDateTime.Month)
                {
                    if (datetime.Month == firstDrawDateTime.Month && datetime.Day == firstDrawDateTime.Day)
                    {
                        if (datetime.Hour < lastDrawDateTime.Hour)
                        {
                            return true;
                        }
                        else if (datetime.Hour == lastDrawDateTime.Hour && datetime.Minute < lastDrawDateTime.Minute)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;        
        }
    }
}
