﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Data
{
    /// <summary>
    /// Represents a statistical dictionary of frequency(key)/overdue(value) pair.
    /// </summary>
    public class StatisticalMap : Dictionary<int,int>
    {
        public StatisticalMap()
        {           
        }

        public int Key
        {
            get
            {
                var keys = this.Keys;
                if (keys != null && keys.Count == 1)
                    return keys.ElementAt(0);
                return 0;
            }

        }
    }
}
