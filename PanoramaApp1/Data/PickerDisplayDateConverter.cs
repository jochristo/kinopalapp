﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Kino.Binding
{
    public class PickerDisplayDateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                ///Convert Class throws exception so can not convert to date time
                string TheCurrentDate = value.ToString();

                string[] Values = TheCurrentDate.Split('/');
                string Day, Month, Year;

                Day = Values[1];
                Month = Values[0];
                Year = Values[2];

                string retvalue = string.Format("{0}/{1}/{2}", Day, Month, Year);
                return retvalue;
            }
            return value;
        }
                
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
