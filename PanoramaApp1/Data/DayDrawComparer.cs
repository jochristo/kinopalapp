﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Data
{
    /// <summary>
    /// Determines if two Kino.Data.DayDraw objects given are equal.    
    /// </summary>
    public class DayDrawComparer : IEqualityComparer<DayDraw>
    {
        public bool Equals(DayDraw a, DayDraw b)
        {
            if (a != null && b != null)
            {
                if (a.Date == b.Date)
                {
                    if (a.Draws != null && b.Draws != null)
                    {
                        if (a.Draws.draw != null && b.Draws.draw != null)
                        {
                            if(a.Draws.draw.Count == b.Draws.draw.Count)
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public int GetHashCode(DayDraw obj)
        {
            return base.GetHashCode();
        }
    }

    /// <summary>
    /// Determines if the two Kino.Data.DayDraw objects' Date property is equal.
    /// </summary>
    public static class DaydrawDateComparer
    {
        public static bool IsEqual(DayDraw a, DayDraw b)
        {
            if (a != null && b != null)
            {
                if (a.Date == b.Date)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
