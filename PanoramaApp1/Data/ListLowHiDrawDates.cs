﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Data
{
    public static class ListLowHiDrawDates
    {
        //private DateTime _low;
        //private DateTime _high;

        public static List<String> GetDates(DateTime low, DateTime high)
        {
            string outputFormat = "dd-MM-yyyy";
            CultureInfo provider = CultureInfo.CurrentCulture;
            CultureInfo elGR = new CultureInfo("el-GR");

            var dates = new List<string>();
            var set = new HashSet<string>();
            var calendar = new System.Globalization.GregorianCalendar();
            var lowDay = low.Day;
            var highDay = high.Day;
            var lowMonth = calendar.GetMonth(low);
            var highMonth = calendar.GetMonth(high);
            var lowMonthDays = calendar.GetDaysInMonth(low.Year, low.Month);
            var highMonthDays = calendar.GetDaysInMonth(high.Year, high.Month);
            var lowYear = low.Year;
            var highYear = high.Year;
            var yearSpan = highYear - lowYear + 1;

            var currentYear = lowYear;
            for (var i = 0; i < yearSpan; i++)
            {                
                if (currentYear <= highYear)
                {
                    for (var m = lowMonth; m <= 12; m++)
                    {
                        lowMonthDays = calendar.GetDaysInMonth(currentYear, m);
                        var maxMonthDays = lowMonthDays;
                        if (m == highMonth && currentYear == highYear)
                            maxMonthDays = highDay;
                        for (var d = lowDay; d <= maxMonthDays; d++)
                        {
                            var date = new DateTime(currentYear, m, d, calendar);
                            var converted = date.ToString(outputFormat, provider);
                            set.Add(converted);
                        }

                        if (m == highMonth && currentYear == highYear)
                            break;

                        lowDay = 1;
                    }
                    if (currentYear == highYear)
                        break;

                    lowMonth = 1;                    
                }
                if (currentYear == highYear)
                    break;

                currentYear++;
            }            
            return set.ToList<string>();
        }
    }
}
