﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Data
{
    /// <summary>
    /// Represents the type of the sorting order.
    /// </summary>
    public enum OrderingType
    {
        Asc = 0,
        Desc = 1,
        Undefined = 2
    }
}
