﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Kino.Binding
{
    /// <summary> Converts the JSON date time string into date time format string object
    /// </summary>
    public class DrawDateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //var sample = "07-12-2014T09:00:00";
            string inputFormat = "dd-MM-yyyyTHH:mm:ss";
            CultureInfo provider = CultureInfo.CurrentCulture;
            string dateTime = (string)value;
            DateTime date = DateTime.ParseExact(dateTime, inputFormat, provider);
            //string displayFormat = "dd/MM/yyyy HH:mm";
            string displayFormat = "dd/MM/yyyy";
            return date.ToString(displayFormat);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string strValue = value as string;
            DateTime resultDateTime;
            if (DateTime.TryParse(strValue, out resultDateTime))
            {
                return resultDateTime;
            }
            return DependencyProperty.UnsetValue;
        }
    }
}
