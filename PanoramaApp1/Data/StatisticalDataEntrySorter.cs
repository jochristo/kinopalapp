﻿using Kino.Binding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Data
{
    /// <summary>
    /// Provides ways of sorting two Kino.Data.StatisticalDataEntry instances.
    /// </summary>
    public class StatisticalDataEntrySorter
    {
        /// <summary>
        /// Compares using the Frequency property of the two Kino.Data.StatisticalDataEntry objects
        /// </summary>
        public class FrequencySorter : IComparer<StatisticalDataEntry>
        {
            private StatisticalDataEntrySortingOrder _orderType;

            public FrequencySorter() { }
            public int Compare(StatisticalDataEntry x, StatisticalDataEntry y)
            {
                if (this.Order == StatisticalDataEntrySortingOrder.Desc)
                    return y.Frequency.CompareTo(x.Frequency); //desc
                else
                    return x.Frequency.CompareTo(y.Frequency); //asc
            }

            public StatisticalDataEntrySortingOrder Order
            {
                get { return _orderType; }
                set { _orderType = value; }
            }
        }

        /// <summary>
        /// Compares using the Value property of the two Kino.Data.StatisticalDataEntry objects
        /// </summary>
        public class ValueSorter : IComparer<StatisticalDataEntry>
        {
            private StatisticalDataEntrySortingOrder _orderType;
            public int Compare(StatisticalDataEntry x, StatisticalDataEntry y)
            {
                if (this.Order == StatisticalDataEntrySortingOrder.Desc)
                    return y.Value.CompareTo(x.Frequency); //desc
                else
                    return x.Value.CompareTo(y.Frequency); //asc
            }
            public StatisticalDataEntrySortingOrder Order
            {
                get { return _orderType; }
                set { _orderType = value; }
            }
        }
    }

    /// <summary>
    /// Defines the sorting order type when sorting Kino.Data.StatisticalDateEntry objects on a collection.
    /// </summary>
    public enum StatisticalDataEntrySortingOrder
    {
        Asc = 0,
        Desc = 1
    }
}
