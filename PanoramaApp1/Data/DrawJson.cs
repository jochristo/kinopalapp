﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Kino.Binding;
using System.Windows.Media;
using System.Windows;

namespace Kino.Data.Json
{

        /*
        (C) Data for draws or competitions of a specific date can be obtained through the URL:
        http://applications.opap.gr/DrawsRestServices/{game}/drawDate/{date}.{extension}
        //{"draws":{"draw":[{"drawTime":"31-12-2014T09:00:00","drawNo":477351,"results":[1,5,7,8,14,18,24,28,32,40,41,47,50,55,59,69,72,73,76,77]},{"drawTime":"31-12-2014T09:05:00","drawNo":477352,"results":[1,3,5,15,19,22,24,25,27,35,41,45,47,53,54,58,63,65,66,74]},{"drawTime":"31-12-2014T09:10:00","drawNo":477353,"results":[6,7,8,12,13,14,21,24,28,29,34,35,39,40,43,46,47,69,72,74]},{"drawTime":"31-12-2014T09:15:00","drawNo":477354,"results":[4,6,10,11,13,22,24,25,27,30,31,36,43,48,49,54,59,60,67,74]},{"drawTime":"31-12-2014T09:20:00","drawNo":477355,"results":[4,5,6,9,13,17,22,25,26,27,34,38,41,45,50,62,63,64,69,71]}}
        */
  
    /// <summary>
    /// Rerpresents a JSON Draw entry object downloaded via a web REST service.
    /// </summary>
    public class Draw
    {
        public string drawTime { get; set; }
        public int drawNo { get; set; }
        public List<int> results { get; set; }        
        public ObservableCollection<NumberItem> ResultsItems
        {            
            get
            {
                
                var collection = new ObservableCollection<NumberItem>();
                var bonusNumber = results[results.Count - 1];
                foreach (var n in results)
                {
                    var item = new NumberItem();
                    item.Value = n;
                    item.IsBonusNumber = false;
                    if (item.Value == bonusNumber)
                    {
                        //paint different backcolor in UI                    
                        RadialGradientBrush radialGradientBrush = new RadialGradientBrush();
                        GradientStop blackGS = new GradientStop();
                        blackGS.Color = ((SolidColorBrush)Application.Current.Resources["PhoneAccentBrush"]).Color;
                        blackGS.Offset = 0.0;
                        radialGradientBrush.GradientStops.Add(blackGS);
                        GradientStop phoneAccentBrushGS = new GradientStop();
                        var brush = (SolidColorBrush)Application.Current.Resources["PhoneAccentBrush"];
                        var color = brush.Color;
                        phoneAccentBrushGS.Color = color;
                        phoneAccentBrushGS.Offset = 1.0;
                        radialGradientBrush.GradientStops.Add(phoneAccentBrushGS);
                        item.BackColor = radialGradientBrush;
                        item.IsBonusNumber = true;
                    }
                    else
                    {
                        item.BackColor = (SolidColorBrush)Application.Current.Resources["PhoneBackgroundBrush"];
                    }                    

                    collection.Add(item);
                }
                var ordered = collection.OrderBy(t => t.Value);
                collection = new System.Collections.ObjectModel.ObservableCollection<NumberItem>(ordered.ToList());
                return collection;
            }
            
            set
            {

            }            
        }


        
    }

    public class Draws
    {
        public List<Draw> draw { get; set; }
    }

    public class RootObject
    {
        public Draws draws { get; set; }
    }


    public class RootObjectLast
    {
        public Draw draw { get; set; }
    }
}
