﻿using Kino.Data.Json;
using KinoGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Data
{
    /// <summary>
    /// Represent a day's draw downloaded by the user.
    /// </summary>
    public class DayDraw
    {        
        private string _date;
        private Draws _draws;

        public DayDraw()
        {      
            
        }

        public Draws Draws
        {
            get { return _draws; }
            set { _draws = value; }
        }

        public string Date
        {
            get { return _date; }
            set { _date = value; }
        }
    }
}
