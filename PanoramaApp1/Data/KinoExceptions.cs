﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Exceptions
{
    public class KinoExceptions
    {
        public class InternetConnectionProfileException : Exception
        {
            public InternetConnectionProfileException()
            {
                
            }
            public InternetConnectionProfileException(string message) : base(message)
            {
            }

            public InternetConnectionProfileException(string message, Exception inner) : base(message, inner)
            {
            }
        }

        public class InvalidKinoGameUrlException : Exception
        {
            public InvalidKinoGameUrlException()
            {

            }
            public InvalidKinoGameUrlException(string message) : base(message)
            {
            }

            public InvalidKinoGameUrlException(string message, Exception inner) : base(message, inner)
            {
            }
        }

        public class InvalidDrawDateException : Exception
        {
            public InvalidDrawDateException()
            {

            }
            public InvalidDrawDateException(string message) : base(message)
            {
            }

            public InvalidDrawDateException(string message, Exception inner) : base(message, inner)
            {
            }
        }

        public class NoDrawFoundException : Exception
        {
            public NoDrawFoundException()
            {

            }
            public NoDrawFoundException(string message) : base(message)
            {
            }

            public NoDrawFoundException(string message, Exception inner) : base(message, inner)
            {
            }
        }
    }
}
