﻿using Kino.Data.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Data
{
    public class JsonDrawTimeComparer
    {
        public static bool IsDrawTimeEarlier(Draw previous, Draw last )
        {
            if (previous != null && last != null)
            {
                var ci = new CultureInfo("el-GR");
                ci.DateTimeFormat = new DateTimeFormatInfo();
                ci.DateTimeFormat.FullDateTimePattern = "dd-MM-yyyyTHH:mm:ss";
                var lastDrawTime = DateTime.ParseExact(last.drawTime, "dd-MM-yyyyTHH:mm:ss", ci);
                var previousDrawTime = DateTime.ParseExact(previous.drawTime, "dd-MM-yyyyTHH:mm:ss", ci);
                if (previousDrawTime.Hour < lastDrawTime.Hour || (previousDrawTime.Hour == lastDrawTime.Hour && previousDrawTime.Minute < lastDrawTime.Minute))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
