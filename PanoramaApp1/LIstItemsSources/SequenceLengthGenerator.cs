﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Kino
{
	public static class SequenceLengthGenerator
	{

		
		public static List<int> Populate()
		{
			List<int> list = new List<int>();
			for(int i = 1; i <= 12; i++)
			{
				list.Add(i);
			}
			return list;
		}

        public static List<ListPickerItem> ListPickerItems()
        {
            List<Microsoft.Phone.Controls.ListPickerItem> list = new List<ListPickerItem>();
            ListPickerItem pickerItem = new ListPickerItem();            
            for (int i = 1; i <= 12; i++)
            {
                list.Add(new ListPickerItem());
            }

            return list;
        }
	}
}