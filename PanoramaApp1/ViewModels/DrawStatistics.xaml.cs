﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Kino.IsolatedSettings;
using Kino.Binding;
using System.Windows.Media;
using System.Windows.Shapes;
using Kino.Data;
using System.Collections.ObjectModel;
using Kino.Transition;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;

namespace Kino
{
    public partial class DrawStatistics : PhoneApplicationPage
    {
        private IsoKinoSettings isolatedKinoSettings; // Holds the isolated storage kino settings
        private OrderingType orderingType; // ordering type set by user
        //private Uri ascIconUri = new Uri("/Assets/Icons/dark/appbar.sort.numeric.ascending.png", UriKind.Relative); //asc icon uri
        private Uri ascIconUri; // = new Uri("/Assets/Icons/dark/appbar.arrow.up.png", UriKind.Relative); //asc icon uri
		//appbar.arrow.up.png		
        //private Uri descIconUri = new Uri("/Assets/Icons/dark/appbar.sort.numeric.descending.png", UriKind.Relative); //desc icon uri
        private Uri descIconUri; // = new Uri("/Assets/Icons/dark/appbar.arrow.down.png", UriKind.Relative); //desc icon uri
        private ObservableCollection<StatisticalDataEntry> frequencySortedAsc; // holds the items in asceding order
        private ObservableCollection<StatisticalDataEntry> frequencySortedDesc; // holds the items in descending order
        private ObservableCollection<StatisticalDataEntry> skipsSortedAsc; // holds the items in asceding order
        private ObservableCollection<StatisticalDataEntry> skipsSortedDesc; // holds the items in descending order

        public DrawStatistics()
        {
            InitializeComponent();
            SystemTray.SetIsVisible(this, false);   // hide status bar
            InitializeIsolatedKinoSettings();       // Initialize isolated storage kino settings            
            //SetBindings();                          // Set UI control bindings to kino settings  
            orderingType = OrderingType.Undefined; // assume undefine at first run
            SkipsSortImageBtn.Visibility = System.Windows.Visibility.Collapsed;
            ascIconUri = new Uri("/KinoPalApp;component/Assets/Icons/dark/appbar.arrow.up.png", UriKind.Relative);
            descIconUri = new Uri("/KinoPalApp;component/Assets/Icons/dark/appbar.arrow.down.png", UriKind.Relative);
            
        }

        /// <summary>
        /// Initializes app's isolated storage kino settings.
        /// </summary>
        private void InitializeIsolatedKinoSettings()
        {
            isolatedKinoSettings = new IsoKinoSettings(); // Get kino settings key from iso storage if any, otherwise create it
            var kinoSettings = isolatedKinoSettings.KinoSettings;

            if (kinoSettings == null) // key was not found, create one
            {
                isolatedKinoSettings.KinoSettings = new KinoSettings();
            }
        }

        /// <summary>
        /// Sets UI control binding properties to KinoSettings
        /// </summary>
        private void SetBindings()
        {
            this.FrequencyNumbersList.DataContext = isolatedKinoSettings.KinoSettings;
            this.FrequencyNumbersList.SetBinding(LongListMultiSelector.ItemsSourceProperty, new System.Windows.Data.Binding() { Mode = System.Windows.Data.BindingMode.TwoWay, Path = new PropertyPath("ObservableStatisticalDataItems") });
        }

        /// <summary>
        /// OnNavigatedTo Event
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            // Load search dates state stored in search viewmodel
            if (PhoneApplicationService.Current.State.ContainsKey("fromDate"))
                this.lowDate.Text = (string)PhoneApplicationService.Current.State["fromDate"];
            if (PhoneApplicationService.Current.State.ContainsKey("toDate"))
                this.highDate.Text = (string)PhoneApplicationService.Current.State["toDate"];
            if (PhoneApplicationService.Current.State.ContainsKey("totalDraws"))
                this.TotalDraws.Text = (string)PhoneApplicationService.Current.State["totalDraws"];

            // updates sorted lists from iso storage file
            var context = isolatedKinoSettings.KinoSettings;
            frequencySortedAsc = new ObservableCollection<StatisticalDataEntry>(context.ObservableStatisticalDataItems.OrderBy(c => c.Frequency));
            frequencySortedDesc = new ObservableCollection<StatisticalDataEntry>(context.ObservableStatisticalDataItems.OrderByDescending(c => c.Frequency));
            skipsSortedAsc = new ObservableCollection<StatisticalDataEntry>(context.ObservableStatisticalDataItems.OrderBy(c => c.Overdues));
            skipsSortedDesc = new ObservableCollection<StatisticalDataEntry>(context.ObservableStatisticalDataItems.OrderByDescending(c => c.Overdues));

        }

        private void FrequencyNumbersList_Loaded(object sender, RoutedEventArgs e)
        {
            /*
            var selector = sender as LongListMultiSelector;
            var itemTemplate = selector.ItemTemplate as DataTemplate;
            var content = itemTemplate.LoadContent() as Grid;
            var children = VisualTreeHelper.GetChildrenCount(content);
            //var selectorItem = VisualTreeHelper.GetChild(content, 0) as LongListMultiSelectorItem;
            //var grid = VisualTreeHelper.GetChild(content, 0) as StackPanel;
            var mainStackPanel = VisualTreeHelper.GetChild(content, 0) as StackPanel;
            var innerPanel = VisualTreeHelper.GetChild(mainStackPanel, 1) as StackPanel;
            var innerPanel2 = VisualTreeHelper.GetChild(innerPanel, 0) as StackPanel; //container of ProgressBar
            var progressBar = VisualTreeHelper.GetChild(innerPanel2, 1) as ProgressBar;
            //progressBar.Template.
            //itemTemplate = VisualTreeHelper.GetChild(progressBar, 0) as DataTemplate;
            var mainGrid = VisualTreeHelper.GetChild(itemTemplate, 0) as Grid;
            var innerGrid = VisualTreeHelper.GetChild(mainGrid, 0) as Grid;
            var pbarTrack = VisualTreeHelper.GetChild(innerGrid, 0) as Rectangle; //the non-valued track
            var pbarIndicator = VisualTreeHelper.GetChild(innerGrid, 1) as Rectangle; //the value track
            pbarTrack.Fill = (Brush)Application.Current.Resources["PhoneForegroundBrush"];            
            */

            //SetBindings();
            /*
            var items = this.isolatedKinoSettings.KinoSettings.ObservableStatisticalDataItems;
            FrequencyNumbersList.ItemsSource = new ObservableCollection<StatisticalDataEntry>();
            foreach (var item in items)
            {
                this.FrequencyNumbersList.ItemsSource.Add(item);
                //FrequencyNumbersList.UpdateLayout();
                //var container = FrequencyNumbersList.ContainerFromItem(item) as ContentControl;
                //if(container != null)
                //    KinoTransition.SlideUpFadeIn(container);       
            }
            */
        }

        private void FrequencySortBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var button = sender as Button;
            var context = this.isolatedKinoSettings.KinoSettings;

            SkipsSortImageBtn.Visibility = System.Windows.Visibility.Collapsed; //disable skips sorting image
            FrequencySortImageBtn.Visibility = System.Windows.Visibility.Visible; //enable frequency sorting image

            // order list based on the existing ordering type
            if (orderingType == OrderingType.Undefined || orderingType == OrderingType.Desc)
            {
                KinoTransition.SlideDownFadeOut(FrequencyNumbersList);
                FrequencyNumbersList.ItemsSource.Clear();
                foreach (var item in frequencySortedAsc)
                {
                    this.FrequencyNumbersList.ItemsSource.Add(item);                
                }
                KinoTransition.SlideUpFadeIn(FrequencyNumbersList);                
                orderingType = OrderingType.Asc;
                SetSortingIcon(FrequencySortImageBtn, orderingType);
            }
            else
            {
                KinoTransition.SlideUpFadeOut(FrequencyNumbersList);                
                FrequencyNumbersList.ItemsSource.Clear();
                foreach (var item in frequencySortedDesc)
                {
                    this.FrequencyNumbersList.ItemsSource.Add(item);
                }
                KinoTransition.SlideDownFadeIn(FrequencyNumbersList);           
                orderingType = OrderingType.Desc;
                SetSortingIcon(FrequencySortImageBtn, orderingType);
            }
        }

        private void SkipsSortBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var items = this.isolatedKinoSettings.KinoSettings.ObservableStatisticalDataItems;
            FrequencyNumbersList.ItemsSource = new ObservableCollection<StatisticalDataEntry>();
            foreach (var item in items)
            {
                this.FrequencyNumbersList.ItemsSource.Add(item);
                //FrequencyNumbersList.UpdateLayout();
                //var container = FrequencyNumbersList.ContainerFromItem(item) as FrameworkElement;
                //if(container != null)
                //    KinoTransition.SlideUpFadeIn(container);       
            }

            /*
            var button = sender as Button;
            var context = this.isolatedKinoSettings.KinoSettings;
            SkipsSortImageBtn.Visibility = System.Windows.Visibility.Visible; //enable skips sorting image
            FrequencySortImageBtn.Visibility = System.Windows.Visibility.Collapsed; //disable frequency sorting image

            // order list based on the existing ordering type
            if (orderingType == OrderingType.Undefined || orderingType == OrderingType.Desc)
            {
                KinoTransition.SlideDownFadeOut(FrequencyNumbersList);
                FrequencyNumbersList.ItemsSource.Clear();
                foreach (var item in skipsSortedAsc)
                {
                    this.FrequencyNumbersList.ItemsSource.Add(item);
                }
                KinoTransition.SlideUpFadeIn(FrequencyNumbersList);
                orderingType = OrderingType.Asc;
                SetSortingIcon(SkipsSortImageBtn, orderingType);
            }
            else
            {
                KinoTransition.SlideUpFadeOut(FrequencyNumbersList);
                FrequencyNumbersList.ItemsSource.Clear();
                foreach (var item in skipsSortedDesc)
                {
                    this.FrequencyNumbersList.ItemsSource.Add(item);
                }
                KinoTransition.SlideDownFadeIn(FrequencyNumbersList);
                orderingType = OrderingType.Desc;
                SetSortingIcon(SkipsSortImageBtn, orderingType);
            }
            */
        }

        /// <summary>
        /// Changes button's background icon based on given ordering type
        /// </summary>
        private void SetSortingIcon(Button button, OrderingType orderingType)
        {
            if (button != null)
            {
                for (var k = 0; k < VisualTreeHelper.GetChildrenCount(button); k++)
                {
                    var grid = VisualTreeHelper.GetChild(button, k) as Grid;
                    if (grid != null)
                    {
                        var gridChildren = grid.Children;
                        if (gridChildren != null && gridChildren.Count == 1)
                        {
                            var buttonBorder = gridChildren[0] as Border;
                            //var imageBorder = buttonBorder.Child as Border;
                            var contentControl = buttonBorder.Child as ContentControl;
                            var image = contentControl.Content as Image;
                            Image img = new Image();
                            BitmapImage bm = new BitmapImage();
                            if (orderingType == OrderingType.Asc)
                                bm.UriSource = ascIconUri;
                            else if (orderingType == OrderingType.Desc)
                                bm.UriSource = descIconUri;

                            image.Source = bm;
                        }
                    }
                }
            }
        }

        private void FrequencySortDescBtn_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            var context = this.isolatedKinoSettings.KinoSettings;
            //context.ObservableStatisticalDataItems.Sort(new StatisticalDataEntrySorter.FrequencySorter() 
            //    { Order = StatisticalDataEntrySortingOrder.Asc });
            ObservableCollection<StatisticalDataEntry> sorted =
                new ObservableCollection<StatisticalDataEntry>(context.ObservableStatisticalDataItems.OrderByDescending(c => c.Frequency));
            //Delete(this, FrequencyNumbersList, null);
            context.ObservableStatisticalDataItems.Clear();
            foreach (var item in sorted)
            {
                context.ObservableStatisticalDataItems.Add(item);
            }

            /*
            TurnstileTransition turnstileTransition = new TurnstileTransition();
            turnstileTransition.Mode = TurnstileTransitionMode.ForwardIn;
            this.FrequencyNumbersList.ItemsSource = sorted;
            var findex = 2;
            foreach (var item in FrequencyNumbersList.ItemsSource)
            {

                var itemTemplate = FrequencyNumbersList.ItemTemplate as DataTemplate;
                var content = itemTemplate.LoadContent() as Grid;
                var children = VisualTreeHelper.GetChildrenCount(content);
                var stackPanel = VisualTreeHelper.GetChild(content, 0) as StackPanel;
                ITransition animation = turnstileTransition.GetTransition(stackPanel);
                animation.Begin();

                //TurnstileFeatherEffect.SetFeatheringIndex(stackPanel, findex);
                //KinoTransition.SlideLeftFadeIn(stackPanel);
                //findex++;
            }
            */
        }

        private void FrequencyNumbersList_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

        }

        static void Delete(PhoneApplicationPage page,
            UIElement item, Action callback)
        {
            // setup
            var _Scale = new ScaleTransform
            {
                ScaleX = 1,
                ScaleY = 1,
                CenterX = item.RenderSize.Width / 2,
                CenterY = item.RenderSize.Height / 2,
            };
            var _Translate = new TranslateTransform { X = 0, Y = 0 };
            var _Group = new TransformGroup();
            _Group.Children.Add(_Scale);
            _Group.Children.Add(_Translate);
            item.RenderTransform = _Group;

            // animate
            var _Storyboard = new Storyboard { };

            // scale X
            var _ScaleAnimateX = new DoubleAnimation
            {
                To = .75,
                Duration = TimeSpan.FromSeconds(.25)
            };
            _Storyboard.Children.Add(_ScaleAnimateX);
            Storyboard.SetTarget(_ScaleAnimateX, _Scale);
            Storyboard.SetTargetProperty(_ScaleAnimateX,
                new PropertyPath(ScaleTransform.ScaleXProperty));

            // scale Y
            var _ScaleAnimateY = new DoubleAnimation
            {
                To = .75,
                Duration = TimeSpan.FromSeconds(.25)
            };
            _Storyboard.Children.Add(_ScaleAnimateY);
            Storyboard.SetTarget(_ScaleAnimateY, _Scale);
            Storyboard.SetTargetProperty(_ScaleAnimateY,
                new PropertyPath(ScaleTransform.ScaleYProperty));

            // translate (location)
            var _TranslateAnimate = new DoubleAnimation
            {
                To = page.RenderSize.Height,
                BeginTime = TimeSpan.FromSeconds(.45),
                Duration = TimeSpan.FromSeconds(.25)
            };
            _Storyboard.Children.Add(_TranslateAnimate);
            Storyboard.SetTarget(_TranslateAnimate, _Translate);
            Storyboard.SetTargetProperty(_TranslateAnimate,
                new PropertyPath(TranslateTransform.YProperty));

            // finalize
            _TranslateAnimate.Completed += (s, arg) =>
            {
                if (callback != null)
                    callback();
            };
            _Storyboard.Begin();
        }

        private void FrequencyNumbersList_LayoutUpdated(object sender, EventArgs e)
        {
            var selector = sender as LongListMultiSelector;
            TurnstileTransition tt = new TurnstileTransition();
            tt.Mode = TurnstileTransitionMode.BackwardIn;
            selector.IsSelectionEnabled = true;
            var itemCount = selector.ItemsSource.Count;
            for (int i = 0; i < itemCount; i++)
            {
                UIElement element = selector.ContainerFromItem(selector.ItemsSource[i]) as UIElement;
                ITransition animation = tt.GetTransition(element);
                animation.Begin();
            }
        }
        

    }
}