﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Kino.ItemsSource;
using System.Windows.Media;

namespace Kino
{
    public partial class MinutesPickerUri : PhoneApplicationPage
    {
        public MinutesPickerUri()
        {
            InitializeComponent();
			this.MinutesSelector.ItemsSource = new MinutesItems();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            if (e.Uri.OriginalString == "/ViewModels/Search.xaml") // select hours
            {
                var selected = "00";
                if (MinutesSelector.SelectedItem == null)
                {
                    if (PhoneApplicationService.Current.State.ContainsKey("Minutes"))
                        selected = PhoneApplicationService.Current.State["Minutes"] as string;

                    PhoneApplicationService.Current.State["Minutes"] = selected;
                }
                else
                    PhoneApplicationService.Current.State["Minutes"] = ((MinutesItem)MinutesSelector.SelectedItem).Value;
            }
        }



        private void MinutesSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selector = sender as LongListSelector;
            var selected = selector.SelectedItem as MinutesItem;
            selected.Selected = (Brush)Application.Current.Resources["PhoneAccentBrush"];

            if (e.RemovedItems != null)
            {
                if (e.RemovedItems.Count != 0)
                {
                    foreach (var r in e.RemovedItems)
                    {
                        if (r != null)
                        {
                            var item = r as MinutesItem;
                            item.Selected = (Brush)Application.Current.Resources["PhoneBackgroundBrush"];
                        }
                    }
                }
            }
            if (selected != null)
                this.NavigationService.GoBack();
        }
    }
}