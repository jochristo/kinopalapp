﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json;
using Kino.Data.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using Windows.Networking.Connectivity;
using System.Globalization;

namespace Kino.ViewModels
{
    public partial class DrawData : PhoneApplicationPage
    {
        public DrawData()
        {
            InitializeComponent();            
            
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var fromValue = this.fromDate.ValueString;
            var toValue = this.toDate.ValueString;
            string inputFormat = "dd-MM-yyyyTHH:mm:ss";
            CultureInfo provider = CultureInfo.CurrentCulture;
            DateTime fromDate = DateTime.ParseExact(fromValue, inputFormat, provider);
            DateTime toDate = DateTime.ParseExact(toValue, inputFormat, provider);
            string restDateFormat = "dd-MM-yyyy";
            var jsonDate1 = fromDate.ToString(restDateFormat);
            var jsonDate2 = fromDate.ToString(restDateFormat);

            Calendar calendar = new System.Globalization.GregorianCalendar();
            var fromMonth = calendar.GetMonth(fromDate);
            var ToMonth = calendar.GetMonth(toDate);


            string uri = "http://applications.opap.gr/DrawsRestServices/kino/drawDate/08-01-2015.json";

                WebClient client = new WebClient();
                client.Headers["Accept"] = "application/json";
                client.DownloadStringAsync(new Uri(uri));
                client.DownloadStringCompleted += (s1, e1) =>
                {
                    var profile = NetworkInformation.GetInternetConnectionProfile();
                    if (profile == null)
                    {
                        MessageBoxResult result = MessageBox.Show("Please check your data and/or WiFi connection.", "No internet connection available.", MessageBoxButton.OK);
                    }
                    else
                    {

                        try
                        {
                            var jsonObj = JObject.Parse(e1.Result.ToString());
                            var deserialized = JsonConvert.DeserializeObject<RootObject>(e1.Result.ToString());
                            if (e1.Result == "{}")
                            {
                                MessageBoxResult result = MessageBox.Show("Please check with OPAP web services.", "The date of the draw is invalid.", MessageBoxButton.OK);
                            }
                            if (deserialized.draws.draw == null)
                            {
                                MessageBoxResult result = MessageBox.Show("Please check with OPAP web services.", "The game given in the URL of web services is invalid.", MessageBoxButton.OK);
                            }
                            if (deserialized.draws.draw != null && deserialized.draws.draw.Count == 0) //invalid date given
                            {
                                MessageBoxResult result = MessageBox.Show("Please check with OPAP web services.", "The date given in the URL is invalid.", MessageBoxButton.OK);
                            }
                            DataSelector.ItemsSource = deserialized.draws.draw;
                        }
                        catch (TargetInvocationException ex)
                        {
                            var innerException = ex.InnerException;
                            if (innerException is WebException)
                            {
                                var we = innerException as WebException;
                                var message = string.Empty;
                                if (we.Response is HttpWebResponse)
                                {
                                    switch (((HttpWebResponse)we.Response).StatusCode)
                                    {
                                        case HttpStatusCode.NotFound:
                                            message = "The URL has invalid characters.";
                                            break;
                                        case HttpStatusCode.InternalServerError:
                                            message = "The server returned an internal error.";
                                            break;
                                        default:
                                            throw ex;

                                    }
                                    MessageBoxResult result = MessageBox.Show("Please check with OPAP web services.", message, MessageBoxButton.OK);
                                }
                            }
                        }
                    }
                };

        }

        //private event OpenReadCompletedEventHandler dataFetched;
        private void dataFetchedCompleted(object sender, OpenReadCompletedEventArgs e)
        {
            var profile = NetworkInformation.GetInternetConnectionProfile();
            if (profile == null)
            {
                MessageBoxResult result = MessageBox.Show("Please check your data and/or WiFi connection.", "No internet connection available.", MessageBoxButton.OK);
            }
            else
            {

                try
                {
                    var jsonObj = JObject.Parse(e.Result.ToString());
                    var deserialized = JsonConvert.DeserializeObject<RootObject>(e.Result.ToString());
                    if (e.Result.ToString() == "{}")
                    {
                        MessageBoxResult result = MessageBox.Show("Please check with OPAP web services.", "The date of the draw is invalid.", MessageBoxButton.OK);
                    }
                    if (deserialized.draws.draw == null)
                    {
                        MessageBoxResult result = MessageBox.Show("Please check with OPAP web services.", "The game given in the URL of web services is invalid.", MessageBoxButton.OK);
                    }
                    if (deserialized.draws.draw != null && deserialized.draws.draw.Count == 0) //invalid date given
                    {
                        MessageBoxResult result = MessageBox.Show("Please check with OPAP web services.", "The date given in the URL is invalid.", MessageBoxButton.OK);
                    }
                    DataSelector.ItemsSource = deserialized.draws.draw;
                }
                catch (TargetInvocationException ex)
                {
                    var innerException = ex.InnerException;
                    if (innerException is WebException)
                    {
                        var we = innerException as WebException;
                        var message = string.Empty;
                        if (we.Response is HttpWebResponse)
                        {
                            switch (((HttpWebResponse)we.Response).StatusCode)
                            {
                                case HttpStatusCode.NotFound:
                                    message = "The URL has invalid characters.";
                                    break;
                                case HttpStatusCode.InternalServerError:
                                    message = "The server returned an internal error.";
                                    break;
                                default:
                                    throw ex;

                            }
                            MessageBoxResult result = MessageBox.Show("Please check with OPAP web services.", message, MessageBoxButton.OK);
                        }
                    }
                }
            }

        }
        
    
    }
}