﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Kino.ItemsSource;
using Microsoft.Phone.Controls.Primitives;
using System.Windows.Media;

namespace Kino
{
    public partial class HoursPickerUri : PhoneApplicationPage
    {
        public HoursPickerUri()
        {
            InitializeComponent();

            this.HoursSelector.ItemsSource = new HourItems();            
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            if (e.Uri.OriginalString == "/ViewModels/Search.xaml") // select hours
            {
                var selected = DateTime.Now.ToString("HH");
                if (HoursSelector.SelectedItem == null)
                {
                    if (PhoneApplicationService.Current.State.ContainsKey("Hour"))
                        selected = PhoneApplicationService.Current.State["Hour"] as string;

                    PhoneApplicationService.Current.State["Hours"] = selected;
                }
                else
                    PhoneApplicationService.Current.State["Hours"] = ((HourItem)HoursSelector.SelectedItem).Value;
            }
        }



        private void HoursSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selector = sender as LongListSelector; 
            var selected = selector.SelectedItem as HourItem;
            selected.Selected = (Brush)Application.Current.Resources["PhoneAccentBrush"];

            if (e.RemovedItems != null)
            {
                if (e.RemovedItems.Count != 0)
                {
                    foreach (var r in e.RemovedItems)
                    {
                        if (r != null)
                        {
                            var item = r as HourItem;
                            item.Selected = (Brush)Application.Current.Resources["PhoneBackgroundBrush"];
                        }
                    }
                }
            }
            if(selected != null)
                this.NavigationService.GoBack();
        }




    }
}