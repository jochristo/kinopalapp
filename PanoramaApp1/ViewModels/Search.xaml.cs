﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Reflection;
using Newtonsoft.Json.Linq;
using Kino.Data.Json;
using Newtonsoft.Json;
using Windows.Networking.Connectivity;
using System.Globalization;
using System.Windows.Media;
using Kino.Binding;
using Kino.IsolatedSettings;
using KinoGenerator;
using Kino.Data;
using System.Threading.Tasks;
using KinoGenerator.Statistics;
using Kino.Data.Exception;
using Kino.ItemsSource;
using Kino.Transition;

namespace Kino
{
    public partial class Search : PhoneApplicationPage
    {
        public bool _isDateMode { get; set; }
        public string draw { get; set; }

        public Kino.Binding.SearchDrawContext context;
        private IsoKinoSettings isolatedKinoSettings;
        private HashSet<Sequence> Sequences; // Cache of downloaded Sequences for use in rendering statistics
        private HashSet<DayDraw> DayDraws;
        private DayDrawComparer dayDrawComparer;
        private List<Draw> DownloadedDraws = new List<Draw>();
        private List<string> _Dates = new List<string>(); //Holds the string-parsed dates in the range of search criteria
        private string _lowDate = ""; //the low date criteria
        private bool IsSingleDateTimeMode;
        
        
        public Search()
        {
            InitializeComponent();
            _isDateMode = false;
            this.fetchByNumber.IsEnabled = false;
            context = new Binding.SearchDrawContext();
            SetBindings();
            InitializeIsolatedKinoSettings();       // Initialize isolated storage kino settings 
            Sequences = new HashSet<Sequence>();
            dayDrawComparer = new DayDrawComparer();
            DayDraws = new HashSet<DayDraw>(dayDrawComparer);
            progressStatus.Visibility = System.Windows.Visibility.Collapsed; // prepare visibility   

			// set hour/minute buttons content
            this.HourButton.Content = new HourItem().Value;
			this.MinutesButton.Content = new MinutesItem().Value;
            this.singleDate.Value = DateTime.Now;
        }


        /// <summary>
        /// Initializes app's isolated storage kino settings.
        /// </summary>
        private void InitializeIsolatedKinoSettings()
        {
            isolatedKinoSettings = new IsoKinoSettings(); // Get kino settings key from iso storage if any, otherwise create it
            var kinoSettings = isolatedKinoSettings.KinoSettings;

            if (kinoSettings == null) // key was not found, create one
            {
                isolatedKinoSettings.KinoSettings = new KinoSettings();
            }
        }

        /// <summary>
        /// Sets the binding properties of controls.
        /// </summary>
        private void SetBindings()
        {

            fetch.DataContext = context;
            fetch.SetBinding(Button.CommandParameterProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("Last")
            });

            fetchByNumber.DataContext = context;
            fetchByNumber.SetBinding(Button.CommandParameterProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("DrawNumber")
            });

            fetchByDates.DataContext = context;
            fetchByDates.SetBinding(Button.CommandParameterProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("Dates")
            });

            drawNo.DataContext = context;
            drawNo.SetBinding(TextBox.TextProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("DrawNumber")
            });

            toDate.DataContext = context;
            toDate.SetBinding(DatePicker.ValueProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("ToDate")
            });

            fromDate.DataContext = context;
            fromDate.SetBinding(DatePicker.ValueProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("FromDate")
            });

            /*
            fetchByDates.DataContext = context;
            fetchByDates.SetBinding(Button.IsEnabledProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("IsFetchDatesBtnEnabled")
            });
            */
            

            /*
            this.singleDate.DataContext = context;
            singleDate.SetBinding(DatePicker.ValueProperty, new System.Windows.Data.Binding()
            {
                Mode = System.Windows.Data.BindingMode.TwoWay,
                Path = new PropertyPath("FromDate")
            });
            */
        }

        /// <summary>
        /// Handles the navigation from this view event.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            /*
            if (e.Uri.OriginalString == "/Views/Statistics.xaml")
            {
                if (Sequences.Count != 0)
                {
                    this.isolatedKinoSettings.KinoSettings.UpdateStatisticalData(this.Sequences);
                }
            }
            */
            if (e.Uri.OriginalString == "/ViewModels/DrawStatistics.xaml")
            {
                if (Sequences.Count != 0)
                {
                    this.isolatedKinoSettings.KinoSettings.UpdateStatisticalData(this.Sequences);
                }

                PhoneApplicationService.Current.State["fromDate"] = context.Dates.StartAsString;
                PhoneApplicationService.Current.State["toDate"] = context.Dates.EndAsString;
                PhoneApplicationService.Current.State["totalDraws"] = isolatedKinoSettings.KinoSettings.DrawsCount.ToString();
            }
            //else if (e.Uri.OriginalString == "/ViewModels/Results.xaml")
            else if (e.Uri.OriginalString == "/Views/MyResults.xaml")
            {
                this.bydate.IsChecked = false; // uncheck radio
                context.ResetValidationStatus(); //  reset model's IsEnabled binding properties

                // Hide dates search panel
                this.datePanel.Visibility = System.Windows.Visibility.Collapsed;

                // single mode 
                if (this.IsSingleDateTimeMode == true)
                {
                    PhoneApplicationService.Current.State["IsSingleDateTimeMode"] = true;
                    PhoneApplicationService.Current.State["singleDate"] = this.singleDate.Value.Value.ToString("dd/MM/yyyy");
                    PhoneApplicationService.Current.State["Hours"] = this.HourButton.Content as string;
                    PhoneApplicationService.Current.State["Minutes"] = this.MinutesButton.Content as string;
                    PhoneApplicationService.Current.State["Dates"] = _Dates;

                    // reset dual mode control states
                    this.fromDate.Value = DateTime.Now;
                    this.toDate.Value = DateTime.Now;
                }
                else
                {
                    //dual mode
                    PhoneApplicationService.Current.State["IsSingleDateTimeMode"] = false;
                    PhoneApplicationService.Current.State["Dates"] = _Dates;
                    PhoneApplicationService.Current.State["LowDate"] = this.fromDate.Value.Value.ToString("dd/MM/yyyy");//_lowDate;                 
                    PhoneApplicationService.Current.State["toDate"] = this.toDate.Value.Value.ToString("dd/MM/yyyy");

                    // reset dual mode control states
                    this.singleDate.Value = DateTime.Now;
                    this.HourButton.Content = new HourItem().Value;
                    this.MinutesButton.Content = new MinutesItem().Value;
                }
            }
            else if (e.Uri.OriginalString == "/ViewModels/HoursPickerUri.xaml")
            {
                //save state
                PhoneApplicationService.Current.State["Hours"] = this.HourButton.Content as string;
            }    
            else if (e.Uri.OriginalString == "/ViewModels/MinutesPickerUri.xaml")
            {
                //save state
                PhoneApplicationService.Current.State["Minutes"] = this.MinutesButton.Content as string;
            } 			
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e.NavigationMode == NavigationMode.Back) // navigation back from hours selection page
            {
                if (PhoneApplicationService.Current.State.ContainsKey("Hours"))
                {
                    this.HourButton.Content = (string)PhoneApplicationService.Current.State["Hours"];
                    // disable minutes if value is 22
                    if ((string)this.HourButton.Content == "22")
                    {
                        this.MinutesButton.Content = "00";
                        this.MinutesButton.IsEnabled = false;
                    }
                    else
                        this.MinutesButton.IsEnabled = true;
                }
                if (PhoneApplicationService.Current.State.ContainsKey("Minutes"))
                    if ((string)this.HourButton.Content != "22")
                        this.MinutesButton.Content = (string)PhoneApplicationService.Current.State["Minutes"];
                

            }
        }

        #region radio/text tap events

        private void bylastdraw_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (lastdrawPanel.Visibility == System.Windows.Visibility.Collapsed)
            {
                // first tap
                if (numberPanel.Visibility == System.Windows.Visibility.Collapsed && datePanel.Visibility == System.Windows.Visibility.Collapsed)
                {
                    SlideRightFadeIn(lastdrawPanel);
                    lastdrawPanel.Visibility = System.Windows.Visibility.Visible;
                }

                else if (this.numberPanel.Visibility == System.Windows.Visibility.Visible)
                {
                    SlideRightFadeOut(numberPanel);
                    SlideRightFadeIn(lastdrawPanel);
                    lastdrawPanel.Visibility = System.Windows.Visibility.Visible;
                    numberPanel.Visibility = System.Windows.Visibility.Collapsed;
                }
                else if (this.datePanel.Visibility == System.Windows.Visibility.Visible)
                {
                    SlideRightFadeOut(datePanel);
                    SlideRightFadeIn(lastdrawPanel);
                    lastdrawPanel.Visibility = System.Windows.Visibility.Visible;
                    datePanel.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }

        private void bynumber_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (numberPanel.Visibility == System.Windows.Visibility.Collapsed)
            {
                // first tap
                if (lastdrawPanel.Visibility == System.Windows.Visibility.Collapsed && datePanel.Visibility == System.Windows.Visibility.Collapsed)
                {
                    SlideRightFadeIn(numberPanel);
                    numberPanel.Visibility = System.Windows.Visibility.Visible;
                }

                else if (this.lastdrawPanel.Visibility == System.Windows.Visibility.Visible)
                {
                    SlideLeftFadeOut(lastdrawPanel);
                    SlideLeftFadeIn(numberPanel);
                    numberPanel.Visibility = System.Windows.Visibility.Visible;
                    lastdrawPanel.Visibility = System.Windows.Visibility.Collapsed;
                }
                else if (this.datePanel.Visibility == System.Windows.Visibility.Visible)
                {
                    SlideRightFadeOut(datePanel);
                    SlideRightFadeIn(numberPanel);
                    numberPanel.Visibility = System.Windows.Visibility.Visible;
                    datePanel.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }

        private void bydate_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (datePanel.Visibility == System.Windows.Visibility.Collapsed)
            {
                // first tap
                if (numberPanel.Visibility == System.Windows.Visibility.Collapsed && lastdrawPanel.Visibility == System.Windows.Visibility.Collapsed)
                {
                    SlideRightFadeIn(datePanel);
                    datePanel.Visibility = System.Windows.Visibility.Visible;
                }

                else if (this.numberPanel.Visibility == System.Windows.Visibility.Visible)
                {
                    SlideLeftFadeOut(numberPanel);
                    SlideLeftFadeIn(datePanel);
                    datePanel.Visibility = System.Windows.Visibility.Visible;
                    numberPanel.Visibility = System.Windows.Visibility.Collapsed;
                }
                else if (this.lastdrawPanel.Visibility == System.Windows.Visibility.Visible)
                {
                    SlideLeftFadeOut(lastdrawPanel);
                    SlideLeftFadeIn(datePanel);
                    datePanel.Visibility = System.Windows.Visibility.Visible;
                    lastdrawPanel.Visibility = System.Windows.Visibility.Collapsed;
                }
            }

        }

        private void drawNo_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var textbox = sender as TextBox;
            textbox.Text = "";
            this.fetchByNumber.IsEnabled = true;
        }

        #endregion radio/text tap events

        #region fetch button click events

        /// <summary>
        /// Fetches last draw
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void  fetch_Click(object sender, RoutedEventArgs e)
        {
            this._isDateMode = false;
            this.FrequenciesButton.Visibility = System.Windows.Visibility.Collapsed;

            // Show progress indicator
            this.progress.Visibility = System.Windows.Visibility.Visible;
            this.progressStatus.Visibility = System.Windows.Visibility.Visible;
            this.MainPivot.IsEnabled = false;

            var button = sender as Button;
            var param = button.CommandParameter.ToString();
            //string uri = "http://applications.opap.gr/DrawsRestServices/kino/drawDate/08-01-2015.json";
            string uri = "http://applications.opap.gr/DrawsRestServices/kino/" + param + ".json";

            WebClient client = new WebClient();
            client.Headers["Accept"] = "application/json";
            client.DownloadStringAsync(new Uri(uri));
            client.DownloadStringCompleted += (s1, e1) =>
            {
                var profile = NetworkInformation.GetInternetConnectionProfile();
                if (profile == null) //|| (!profile.IsWlanConnectionProfile && !profile.IsWwanConnectionProfile))                
                {
                    this.progress.Visibility = System.Windows.Visibility.Collapsed;
                    this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                    this.MainPivot.IsEnabled = true;
                    MessageBoxResult result = MessageBox.Show("Please check your data and/or WiFi connection.", "No internet connection available", MessageBoxButton.OK);
                }
                else
                {
                    try
                    {
                        var jsonObj = JObject.Parse(e1.Result.ToString());
                        var deserialized = JsonConvert.DeserializeObject<RootObject>(e1.Result.ToString());
                        var des2 = JsonConvert.DeserializeObject<RootObjectLast>(e1.Result.ToString());
                        if (e1.Result == "{}")
                        {
                            this.progress.Visibility = System.Windows.Visibility.Collapsed;
                            this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                            this.MainPivot.IsEnabled = true;
                            MessageBoxResult result = MessageBox.Show("Please check with OPAP web services.", "The query returned no data", MessageBoxButton.OK);
                        }
                        else
                        {
                            if (des2 == null)
                                DataSelector.ItemsSource = deserialized.draws.draw;

                            List<Draw> list = new List<Draw>();
                            list.Add(des2.draw);
                            DataSelector.ItemsSource = list;
                            if (DataSelector.ItemsSource.Count != 0)
                            {
                                // Show progress indicator
                                this.progress.Visibility = System.Windows.Visibility.Collapsed;
                                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                                this.MainPivot.IsEnabled = true;
                                this.MainPivot.SelectedItem = resultsPivot;
                            }
                        }
                    }
                    catch (TargetInvocationException ex)
                    {
                        var innerException = ex.InnerException;
                        if (innerException is WebException)
                        {
                            var we = innerException as WebException;
                            var message = string.Empty;
                            if (we.Response is HttpWebResponse)
                            {
                                switch (((HttpWebResponse)we.Response).StatusCode)
                                {
                                    case HttpStatusCode.NotFound:
                                        message = "The URL has invalid characters";
                                        break;
                                    case HttpStatusCode.InternalServerError:
                                        message = "The server returned an internal error";
                                        break;
                                    case HttpStatusCode.BadRequest:
                                        message = "The URL has invalid characters";
                                        break;
                                    default:
                                        message = "Something went wrong";
                                        break;

                                }
                                // Show progress indicator
                                this.progress.Visibility = System.Windows.Visibility.Collapsed;
                                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                                this.MainPivot.IsEnabled = true;
                                MessageBoxResult result = MessageBox.Show("Please check with OPAP web services.", message, MessageBoxButton.OK);
                            }
                        }
                    }
                }
            };

        }

        /// <summary>
        /// Fetches by date range.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fetchByDates_Click(object sender, RoutedEventArgs e)
        {
            this._isDateMode = true;
            var button = sender as Button;
            var param = button.CommandParameter as DateCriteria;
            var jsonDate1 = param.StartAsString;
            var jsonDate2 = param.EndAsString;
            Calendar calendar = new System.Globalization.GregorianCalendar();
            var dates = DrawUtilities.GetDates(param.Start, param.End);

            foreach (var date in dates)
            {

                //string uri = "http://applications.opap.gr/DrawsRestServices/kino/drawDate/08-12-2016.json";
                //string uri = "http://applications.opap.gr/DrawsRestServices/kino/drawDate/" + jsonDate1 + ".json";
                string uri = "http://applications.opap.gr/DrawsRestServices/kino/drawDate/" + date + ".json";

                WebClient client = new WebClient();
                client.Headers["Accept"] = "application/json";
                client.DownloadStringAsync(new Uri(uri));
                client.DownloadStringCompleted += (s1, e1) =>
                {
                    var profile = NetworkInformation.GetInternetConnectionProfile();
                    if (profile == null)
                    {
                        MessageBoxResult result = MessageBox.Show("Please check your data and/or WiFi connection.", "No internet connection available.", MessageBoxButton.OK);
                    }
                    else
                    {
                        try
                        {
                            var jsonObj = JObject.Parse(e1.Result.ToString());
                            var deserialized = JsonConvert.DeserializeObject<RootObject>(e1.Result.ToString());
                            MessageBoxResult result = new MessageBoxResult();

                            if (e1.Result == "{}")
                            {
                                result = MessageBox.Show("Please check with OPAP web services.", "The date of the draw is invalid.", MessageBoxButton.OK);
                            }

                            else if (deserialized.draws.draw == null)
                            {
                                result = MessageBox.Show("Please check with OPAP web services.", "The game given in the URL of web services is invalid.", MessageBoxButton.OK);
                            }
                            else if (deserialized.draws.draw != null && deserialized.draws.draw.Count == 0)
                            {
                                result = MessageBox.Show("Please select a different date.", "No draws found.", MessageBoxButton.OK);
                            }

                            if (result != MessageBoxResult.OK)
                            {
                                var draw = deserialized.draws.draw;
                                this.Sequences.Clear();
                                foreach (var d in draw)
                                {
                                    var sequence = new Sequence();
                                    foreach (var item in d.results)
                                    {
                                        sequence.Add(item);
                                    }
                                    this.Sequences.Add(sequence);
                                }
                                DayDraw currentDraw = new DayDraw();
                                currentDraw.Date = jsonDate1;
                                currentDraw.Draws = deserialized.draws;
                                // store to page cache
                                var exists = DayDraws.Contains<DayDraw>(currentDraw);
                                if (exists == false)
                                {
                                    var daysCurrentDraw = DayDraws.SingleOrDefault(d => d.Date == jsonDate1); //check for same day draws
                                    if (currentDraw != null && DaydrawDateComparer.IsEqual(currentDraw, daysCurrentDraw))
                                    {
                                        DayDraws.Remove(daysCurrentDraw); //remove previous day's draws
                                        DayDraws.Add(currentDraw); //update with latest downloads
                                    }
                                    else
                                        DayDraws.Add(currentDraw);
                                }

                                DataSelector.ItemsSource = deserialized.draws.draw; // set item source
                                if (deserialized.draws.draw.Count != 0)
                                    this.FrequenciesButton.Visibility = System.Windows.Visibility.Visible;
                                if (DataSelector.ItemsSource.Count != 0)
                                    this.MainPivot.SelectedItem = resultsPivot;
                            }
                        }
                        catch (TargetInvocationException ex)
                        {
                            var innerException = ex.InnerException;
                            if (innerException is WebException)
                            {
                                var we = innerException as WebException;
                                var message = string.Empty;
                                if (we.Response is HttpWebResponse)
                                {
                                    switch (((HttpWebResponse)we.Response).StatusCode)
                                    {
                                        case HttpStatusCode.NotFound:
                                            message = "The URL has invalid characters.";
                                            break;
                                        case HttpStatusCode.InternalServerError:
                                            message = "The server returned an internal error.";
                                            break;
                                        case HttpStatusCode.BadRequest:
                                            message = "The URL has invalid characters.";
                                            break;
                                        default:
                                            message = "Something went wrong.";
                                            break;

                                    }
                                    MessageBoxResult result = MessageBox.Show("Please check with OPAP web services.", message, MessageBoxButton.OK);
                                }
                            }
                        }
                    }
                };

            }

        }

        /// <summary>
        /// Fetches by specified draw number.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fetchByNumber_Click(object sender, RoutedEventArgs e)
        {
            this._isDateMode = false;
            this.FrequenciesButton.Visibility = System.Windows.Visibility.Collapsed;
            var button = sender as Button;
            var text = this.drawNo.Text;
            
            if (text.Contains(',') || text == "," || text.Contains('.') || text == ".")
            {
                //MessageBoxResult result = MessageBox.Show("Please provide a valid integer number.", "Invalid draw number", MessageBoxButton.OK);
                error.Visibility = System.Windows.Visibility.Visible;
                drawNo.BorderBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0x02, 02)); //#FFFF0202
                if (error.Visibility != System.Windows.Visibility.Collapsed)                
                    SlideRightFadeIn(error);
                                
                error.Text = "Invalid draw number";
                drawNo.Text = string.Empty;
            }
            else if(string.IsNullOrWhiteSpace(text))
            {
                //MessageBoxResult result = MessageBox.Show("Please provide an integer number.", "Draw number is empty", MessageBoxButton.OK);
                error.Visibility = System.Windows.Visibility.Visible;
                drawNo.BorderBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0x02, 02)); //#FFFF0202
                if (error.Visibility != System.Windows.Visibility.Collapsed)
                    SlideRightFadeIn(error);
                error.Text = "Draw number is empty";
                drawNo.Text = string.Empty;
            }            
            else
            {
                // Show progress indicator
                this.progress.Visibility = System.Windows.Visibility.Visible;
                this.progressStatus.Visibility = System.Windows.Visibility.Visible;
                this.MainPivot.IsEnabled = false;

                if(error.Visibility == System.Windows.Visibility.Visible)
                {
                    drawNo.BorderBrush = (Brush)Application.Current.Resources["PhoneBorderBrush"];
                    SlideLeftFadeOut(error);
                    error.Visibility = System.Windows.Visibility.Collapsed;
                }
                var param = button.CommandParameter.ToString();
                string uri = "http://applications.opap.gr/DrawsRestServices/kino/" + param + ".json";

                WebClient client = new WebClient();
                client.Headers["Accept"] = "application/json";
                client.DownloadStringAsync(new Uri(uri));
                client.DownloadStringCompleted += (s1, e1) =>
                {
                    var profile = NetworkInformation.GetInternetConnectionProfile();
                    if (profile == null) // || (!profile.IsWlanConnectionProfile && !profile.IsWwanConnectionProfile))
                    {
                        this.progress.Visibility = System.Windows.Visibility.Collapsed;
                        this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                        this.MainPivot.IsEnabled = true;
                        MessageBoxResult result = MessageBox.Show("Please check your data and/or WiFi connection.", "No internet connection available", MessageBoxButton.OK);
                    }
                    else
                    {
                        try
                        {
                            var jsonObj = JObject.Parse(e1.Result.ToString());
                            var deserialized = JsonConvert.DeserializeObject<RootObjectLast>(e1.Result.ToString());
                            if (deserialized != null)
                            {
                                if (deserialized.draw == null) // draw not found
                                {
                                    this.progress.Visibility = System.Windows.Visibility.Collapsed;
                                    this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                                    this.MainPivot.IsEnabled = true;
                                    MessageBoxResult result = MessageBox.Show("Please provide a valid number.", "The draw was not found", MessageBoxButton.OK);
                                }
                                else
                                {
                                    List<Draw> list = new List<Draw>();
                                    list.Add(deserialized.draw);
                                    DataSelector.ItemsSource = list;
                                    if (DataSelector.ItemsSource.Count != 0)
                                    {
                                        this.progress.Visibility = System.Windows.Visibility.Collapsed;
                                        this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                                        this.MainPivot.IsEnabled = true;
                                        this.MainPivot.SelectedItem = resultsPivot;
                                    }
                                }
                            }
                        }
                        catch (TargetInvocationException ex)
                        {
                            var innerException = ex.InnerException;
                            if (innerException is WebException)
                            {
                                var we = innerException as WebException;
                                var message = string.Empty;
                                if (we.Response is HttpWebResponse)
                                {
                                    switch (((HttpWebResponse)we.Response).StatusCode)
                                    {
                                        case HttpStatusCode.NotFound:
                                            message = "The URL has invalid characters";
                                            break;
                                        case HttpStatusCode.InternalServerError:
                                            message = "The server returned an internal error";
                                            break;
                                        case HttpStatusCode.BadRequest:
                                            message = "The URL has invalid characters";
                                            break;
                                        default:
                                            message = "Something went wrong";
                                            break;

                                    }
                                    // Show progress indicator
                                    this.progress.Visibility = System.Windows.Visibility.Collapsed;
                                    this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                                    this.MainPivot.IsEnabled = true;
                                    MessageBoxResult result = MessageBox.Show("Please check with OPAP web services.", message, MessageBoxButton.OK);
                                }
                            }
                        }
                    }
                };
            }
        }
        
        /// <summary>
        /// Fetches by date range.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void fetchWithDates_Click(object sender, RoutedEventArgs e)
        {
            this._isDateMode = true;
            this.Sequences.Clear(); // Clear the Sequence cache
            var button = sender as Button;
            var param = button.CommandParameter as DateCriteria; // Gets the dates command parameters
            var jsonDate1 = param.StartAsString;
            //var jsonDate2 = param.EndAsString;
            Calendar calendar = new System.Globalization.GregorianCalendar();
            var draws = new List<Draw>(); //Holds the selector items source
            MessageBoxResult result = new MessageBoxResult(); // The error message

            this.progress.Visibility = System.Windows.Visibility.Visible;
            this.progressStatus.Visibility = System.Windows.Visibility.Visible;
            this.MainPivot.IsEnabled = false;

            // hide error messages
            if (errorStart.Visibility == System.Windows.Visibility.Visible)
            {
                fromDate.BorderBrush = (Brush)Application.Current.Resources["PhoneBorderBrush"];
                SlideLeftFadeOut(errorStart);
                errorStart.Visibility = System.Windows.Visibility.Collapsed;
            }
            if (errorEnd.Visibility == System.Windows.Visibility.Visible)
            {
                toDate.BorderBrush = (Brush)Application.Current.Resources["PhoneBorderBrush"];
                SlideLeftFadeOut(errorEnd);
                errorEnd.Visibility = System.Windows.Visibility.Collapsed;
            }

            // Fetch data
            var dates = DrawUtilities.GetDates(param.Start, param.End); // get all dates between start and end criteria
            foreach (var date in dates)
            {
                string uri = "http://applications.opap.gr/DrawsRestServices/kino/drawDate/" + date + ".json";
                WebClient client = new WebClient();
                client.Headers["Accept"] = "application/json";
                //var task = JsonAsync.DownloadStringTaskAsync(client, new Uri(uri)); //download the json resource
                //string Result = await task; //wait to complete the json download before parsing

                var profile = NetworkInformation.GetInternetConnectionProfile();
                if (profile == null)
                {
                    this.progress.Visibility = System.Windows.Visibility.Collapsed;
                    this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                    this.MainPivot.IsEnabled = true;
                    result = MessageBox.Show("Please check your data and/or WiFi connection.", "No internet connection available", MessageBoxButton.OK);
                    break;
                }
                else
                {
                    try
                    {
                        var task = JsonAsync.DownloadStringTaskAsync(client, new Uri(uri)); //download the json resource
                        string Result = await task; //wait to complete the json download before parsing
                        var jsonObj = JObject.Parse(Result);
                        var deserialized = JsonConvert.DeserializeObject<RootObject>(Result);
                        //MessageBoxResult result = new MessageBoxResult();

                        if (Result == "{}")
                        {
                            this.progress.Visibility = System.Windows.Visibility.Collapsed;
                            this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                            this.MainPivot.IsEnabled = true;
                            result = MessageBox.Show("Please check with OPAP web services.", "The date of the draw is invalid", MessageBoxButton.OK);
                            break;
                        }
                        else if (deserialized.draws.draw == null)
                        {
                            this.progress.Visibility = System.Windows.Visibility.Collapsed;
                            this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                            this.MainPivot.IsEnabled = true;
                            result = MessageBox.Show("Please check with OPAP web services.", "The game given in the URL of web services is invalid", MessageBoxButton.OK);
                            break;
                        }
                        else if (deserialized.draws.draw != null && deserialized.draws.draw.Count == 0)
                        {
                            this.progress.Visibility = System.Windows.Visibility.Collapsed;
                            this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                            this.MainPivot.IsEnabled = true;
                            result = MessageBox.Show("Please select a different date.", "No draws found", MessageBoxButton.OK);
                            break;
                        }
                        else //if (result != MessageBoxResult.OK)
                        {
                            var draw = deserialized.draws.draw;
                            foreach (var d in draw)
                            {
                                var sequence = new Sequence();
                                foreach (var item in d.results)
                                {
                                    sequence.Add(item);
                                }
                                this.Sequences.Add(sequence); // add to cache
                            }
                            DayDraw currentDraw = new DayDraw();
                            currentDraw.Date = jsonDate1;
                            currentDraw.Draws = deserialized.draws;
                            var exists = DayDraws.Contains<DayDraw>(currentDraw); // check day's draws
                            if (exists == false)
                            {
                                var daysCurrentDraw = DayDraws.SingleOrDefault(d => d.Date == jsonDate1); //check for same day draws
                                if (currentDraw != null && DaydrawDateComparer.IsEqual(currentDraw, daysCurrentDraw))
                                {
                                    DayDraws.Remove(daysCurrentDraw); //remove previous day's draws
                                    DayDraws.Add(currentDraw); //update with latest downloads
                                }
                                else
                                    DayDraws.Add(currentDraw);
                            }
                            var list = deserialized.draws.draw;
                            foreach (var d in list)
                            {
                                draws.Add(d); //add to draw items
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        var innerException = ex.InnerException;       
                        if (innerException is WebException)
                        {
                            var we = innerException as WebException;
                            var message = string.Empty;
                            if (we.Response is HttpWebResponse)
                            {
                                switch (((HttpWebResponse)we.Response).StatusCode)
                                {
                                    case HttpStatusCode.NotFound:
                                        message = "The requested resource does not exist on the server";
                                        break;
                                    case HttpStatusCode.InternalServerError:
                                        message = "The server returned an internal error";
                                        break;
                                    case HttpStatusCode.BadRequest:
                                        message = "The request could not be understood by the server";
                                        break;
                                    default:
                                        message = "Something went wrong";
                                        break;
                                }
                                this.progress.Visibility = System.Windows.Visibility.Collapsed;
                                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                                this.MainPivot.IsEnabled = true;
                                MessageBoxResult error = MessageBox.Show("Please check with OPAP web services.", message, MessageBoxButton.OK);
                            }
                        }
                    }
                }
            }

            // data downloaded, prepare item source
            if (result == MessageBoxResult.None) //ok
            {
                var stats = new KinoStatistics();
                var overdues = stats.Overdues(this.Sequences, null);
                if (draws.Count != 0)
                    this.FrequenciesButton.Visibility = System.Windows.Visibility.Visible;

                DataSelector.ItemsSource = draws; // set items source
                this.progress.Visibility = System.Windows.Visibility.Collapsed;
                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                this.MainPivot.IsEnabled = true;
                this.MainPivot.SelectedItem = resultsPivot;
            }
            else
            {
                this.progress.Visibility = System.Windows.Visibility.Collapsed; // stop displaying progress bar
                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                this.MainPivot.IsEnabled = true;
            }
        }

        #endregion fetch button click events

        /// <summary>
        /// Handles the DownloadStringCompleted event of a System.Net.WebClient object.
        /// </summary>
        private void OnDownloadStringCompleted(object sender, DownloadStringCompletedEventArgs eventArgs)
        {
            var button = this.fetchByDates as Button;
            var param = button.CommandParameter as DateCriteria;
            var jsonDate1 = param.StartAsString;
            var jsonDate2 = param.EndAsString;

            var Result = eventArgs.Result;
            var profile = NetworkInformation.GetInternetConnectionProfile();
            if (profile == null)
            {
                MessageBoxResult result = MessageBox.Show("Please check your data and/or WiFi connection.", "No internet connection available.", MessageBoxButton.OK);
            }
            else
            {
                try
                {
                    var jsonObj = JObject.Parse(Result.ToString());
                    var deserialized = JsonConvert.DeserializeObject<RootObject>(Result.ToString());
                    MessageBoxResult result = new MessageBoxResult();

                    if (Result == "{}")
                    {
                        result = MessageBox.Show("Please check with OPAP web services.", "The date of the draw is invalid.", MessageBoxButton.OK);
                    }

                    else if (deserialized.draws.draw == null)
                    {
                        result = MessageBox.Show("Please check with OPAP web services.", "The game given in the URL of web services is invalid.", MessageBoxButton.OK);
                    }
                    else if (deserialized.draws.draw != null && deserialized.draws.draw.Count == 0)
                    {
                        result = MessageBox.Show("Please select a different date.", "No draws found.", MessageBoxButton.OK);
                    }

                    if (result != MessageBoxResult.OK)
                    {
                        var draw = deserialized.draws.draw;
                        this.Sequences.Clear();
                        foreach (var d in draw)
                        {
                            var sequence = new Sequence();
                            foreach (var item in d.results)
                            {
                                sequence.Add(item);
                            }
                            this.Sequences.Add(sequence);
                        }
                        DayDraw currentDraw = new DayDraw();
                        currentDraw.Date = jsonDate1;
                        currentDraw.Draws = deserialized.draws;
                        // store to page cache
                        var exists = DayDraws.Contains<DayDraw>(currentDraw);
                        if (exists == false)
                        {
                            var daysCurrentDraw = DayDraws.SingleOrDefault(d => d.Date == jsonDate1); //check for same day draws
                            if (currentDraw != null && DaydrawDateComparer.IsEqual(currentDraw, daysCurrentDraw))
                            {
                                DayDraws.Remove(daysCurrentDraw); //remove previous day's draws
                                DayDraws.Add(currentDraw); //update with latest downloads
                            }
                            else
                                DayDraws.Add(currentDraw);
                        }

                        var list = deserialized.draws.draw;
                        foreach (var d in list)
                        {
                            DownloadedDraws.Add(d);
                        }

                        /*
                        DataSelector.ItemsSource = deserialized.draws.draw; // set item source
                        if (deserialized.draws.draw.Count != 0)
                            this.FrequenciesButton.Visibility = System.Windows.Visibility.Visible;
                        if (DataSelector.ItemsSource.Count != 0)
                            this.MainPivot.SelectedItem = resultsPivot;
                        */
                    }
                }
                catch (TargetInvocationException ex)
                {
                    var innerException = ex.InnerException;
                    if (innerException is WebException)
                    {
                        var we = innerException as WebException;
                        var message = string.Empty;
                        if (we.Response is HttpWebResponse)
                        {
                            switch (((HttpWebResponse)we.Response).StatusCode)
                            {
                                case HttpStatusCode.NotFound:
                                    message = "The URL has invalid characters.";
                                    break;
                                case HttpStatusCode.InternalServerError:
                                    message = "The server returned an internal error.";
                                    break;
                                case HttpStatusCode.BadRequest:
                                    message = "The URL has invalid characters.";
                                    break;
                                default:
                                    message = "Something went wrong.";
                                    break;

                            }
                            MessageBoxResult result = MessageBox.Show("Please check with OPAP web services.", message, MessageBoxButton.OK);
                        }
                    }
                }
            }

        }
        
        /// <summary>
        /// Downloads the resource at the specified Uri and with given WebClient as an synchronous operation using a task object.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="uri"></param>
        /// <returns></returns>
        private static async Task<string> DownloadStringTaskAsync(WebClient client, Uri uri)
        {
            var tcs = new TaskCompletionSource<string>();

            client.DownloadStringCompleted += (o, e) =>
            {
                if (e.Error != null && e.Error.InnerException != null)
                    tcs.SetException(e.Error.InnerException);
                else if (e.Cancelled)
                    tcs.SetCanceled();
                else
                    tcs.SetResult(e.Result);
            };
            client.DownloadStringAsync(uri);
            return await tcs.Task;
        }

        #region Radio Checked Events

        private void bylastdraw_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            var button = sender as RadioButton;
            button.Background = (Brush)Application.Current.Resources["PhoneAccentBrush"];
            if (this.error.Visibility == System.Windows.Visibility.Visible)
            {
                drawNo.BorderBrush = (Brush)Application.Current.Resources["PhoneBorderBrush"];
                error.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void bynumber_Checked(object sender, RoutedEventArgs e)
        {
            var button = sender as RadioButton;
            button.Background = (Brush)Application.Current.Resources["PhoneAccentBrush"];
        }

        private void bydate_Checked(object sender, RoutedEventArgs e)
        {
            var button = sender as RadioButton;
            button.Background = (Brush)Application.Current.Resources["PhoneAccentBrush"];
            if (this.error.Visibility == System.Windows.Visibility.Visible)
            {
                drawNo.BorderBrush = (Brush)Application.Current.Resources["PhoneBorderBrush"];
                error.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void bylastdraw_Unchecked(object sender, RoutedEventArgs e)
        {
            var button = sender as RadioButton;
            button.ClearValue(BackgroundProperty);
        }

        private void bynumber_Unchecked(object sender, RoutedEventArgs e)
        {
            var button = sender as RadioButton;
            button.ClearValue(BackgroundProperty);
        }

        private void bydate_Unchecked(object sender, RoutedEventArgs e)
        {
            var button = sender as RadioButton;
            button.ClearValue(BackgroundProperty);
        }

        #endregion Radio Checked Events

        private void drawNo_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textbox = sender as TextBox;
            //textbox.text
        }
                
        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            //this.NavigationService.Navigate(new Uri("/Views/Statistics.xaml", UriKind.Relative));
            this.NavigationService.Navigate(new Uri("/ViewModels/DrawStatistics.xaml", UriKind.Relative));
        }

        private void drawNo_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter) (sender as TextBox)
                .GetBindingExpression(TextBox.TextProperty).UpdateSource();
        }

        private void fromDate_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var picker = sender as DatePicker;
            
        }


        #region Transition effects

        private void SlideUpFadeIn(FrameworkElement element)
        {
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideUpFadeIn;
            ITransition trans = slide.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        private void SlideUpFadeOut(FrameworkElement element)
        {
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideUpFadeOut;
            ITransition trans = slide.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        private void SlideDownFadeIn(FrameworkElement element)
        {
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideDownFadeIn;
            ITransition trans = slide.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        private void SlideDownFadeOut(FrameworkElement element)
        {
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideDownFadeOut;
            ITransition trans = slide.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        private void SlideRightFadeIn(FrameworkElement element)
        {
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideRightFadeIn;
            ITransition trans = slide.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        private void SlideLeftFadeIn(FrameworkElement element)
        {
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideLeftFadeIn;
            ITransition trans = slide.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        private void SlideLeftFadeOut(FrameworkElement element)
        {
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideLeftFadeOut;
            ITransition trans = slide.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        private void SlideRightFadeOut(FrameworkElement element)
        {
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideRightFadeOut;
            ITransition trans = slide.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        #endregion Transition effects

        #region ValueChangedEvents

        private void fromDate_ValueChanged(object sender, DateTimeValueChangedEventArgs e)
        {
            var message = string.Empty;
            var validation = DrawUtilities.Validation(e.NewDateTime.Value,out message);
            
            if(validation == DrawDateValidationResult.Invalid)
            {
                this.errorStart.Visibility = System.Windows.Visibility.Visible;
                this.errorStart.Text = message;
                this.fromDate.BorderBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0x02, 02)); //#FFFF0202
                if (errorStart.Visibility != System.Windows.Visibility.Collapsed)
                    SlideRightFadeIn(errorStart);         

                //update context properties
                this.context.IsFromDateValid = false;

                // NEW - IsFetchDatesBtnEnabled not working problem!!!
                this.fetchByDates.IsEnabled = false;

            }            
            else
            {
                this.fromDate.BorderBrush = (Brush)Application.Current.Resources["PhoneBorderBrush"];
                this.errorStart.Visibility = System.Windows.Visibility.Collapsed;
                
                if (e.NewDateTime.Value.Date > this.toDate.Value.Value.Date)
                {
                    this.fromDate.BorderBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0x02, 02)); //#FFFF0202
                    this.errorStart.Visibility = System.Windows.Visibility.Visible;
                    this.errorStart.Text = "Start date should be earlier than end date";
                    SlideRightFadeIn(errorStart);
                    //update context properties
                    this.context.IsFromDateValid = false;

                    // NEW - IsFetchDatesBtnEnabled not working problem!!!
                    this.fetchByDates.IsEnabled = false;


                }
                else
                {
                    // allow date span of one month only
                    //if (!DrawUtilities.IsStatisticsDatesPeriodValid(e.NewDateTime.Value.Date, this.toDate.Value.Value.Date))
                    if (!DrawUtilities.IsSearchRangeValid(e.NewDateTime.Value.Date, this.toDate.Value.Value.Date))
                    {
                        this.toDate.BorderBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0x02, 02)); //#FFFF0202
                        this.errorEnd.Visibility = System.Windows.Visibility.Visible;
                        this.errorEnd.Text = "Please select one month period";
                        SlideRightFadeIn(errorEnd);

                        this.context.IsToDateValid = false;

                        // NEW - IsFetchDatesBtnEnabled not working problem!!!
                        this.fetchByDates.IsEnabled = false;
                    }
                    /*
                    var month = e.NewDateTime.Value.Date.Month;
                    if (this.toDate.Value.Value.Date.Month - month > 1)
                    {
                        this.fromDate.BorderBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0x02, 02)); //#FFFF0202
                        this.errorStart.Visibility = System.Windows.Visibility.Visible;
                        this.errorStart.Text = "Please select one month period";
                        SlideRightFadeIn(errorStart);

                        this.context.IsFromDateValid = false;

                        // NEW - IsFetchDatesBtnEnabled not working problem!!!
                        this.fetchByDates.IsEnabled = false;
                    }
                    */
                    else
                    {

                        //update context properties
                        this.context.IsFromDateValid = true;
                        this.fetchByDates.IsEnabled = true;
                        if (this.errorEnd.Visibility == System.Windows.Visibility.Visible)
                        {
                            validation = DrawUtilities.Validation(toDate.Value.Value.Date, out message);
                            if (validation == DrawDateValidationResult.Valid)
                            {
                                this.errorEnd.Visibility = System.Windows.Visibility.Collapsed;
                                this.toDate.BorderBrush = (Brush)Application.Current.Resources["PhoneBorderBrush"];
                                this.context.IsToDateValid = true;

                                // NEW - IsFetchDatesBtnEnabled not working problem!!!
                                this.fetchByDates.IsEnabled = true;
                            }
                        }
                    }
                }
            }
        }

        private void toDate_ValueChanged(object sender, DateTimeValueChangedEventArgs e)
        {
            var message = string.Empty;
            var validation = DrawUtilities.Validation(e.NewDateTime.Value, out message);
            
            if (validation == DrawDateValidationResult.Invalid)
            {
                this.errorEnd.Visibility = System.Windows.Visibility.Visible;
                this.errorEnd.Text = message;
                this.toDate.BorderBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0x02, 02)); //#FFFF0202
                if (errorEnd.Visibility != System.Windows.Visibility.Collapsed)
                    SlideRightFadeIn(errorEnd);

                this.context.IsToDateValid = false;

                // NEW - IsFetchDatesBtnEnabled not working problem!!!
                this.fetchByDates.IsEnabled = false;

            }
            else
            {
                this.toDate.BorderBrush = (Brush)Application.Current.Resources["PhoneBorderBrush"];
                this.errorEnd.Visibility = System.Windows.Visibility.Collapsed;
                
                if (e.NewDateTime.Value.Date < this.fromDate.Value.Value.Date)
                {
                    this.toDate.BorderBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0x02, 02)); //#FFFF0202
                    this.errorEnd.Visibility = System.Windows.Visibility.Visible;
                    this.errorEnd.Text = "End date should be later than or equal to start date";
                    SlideRightFadeIn(errorEnd);

                    this.context.IsToDateValid = false;

                    // NEW - IsFetchDatesBtnEnabled not working problem!!!
                    this.fetchByDates.IsEnabled = false;
                    
                }
                else
                {
                    // allow date span of one month only
                    /*
                    var month = e.NewDateTime.Value.Date.Month;
                    if (month - this.fromDate.Value.Value.Date.Month > 1)
                    {
                        this.toDate.BorderBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0x02, 02)); //#FFFF0202
                        this.errorEnd.Visibility = System.Windows.Visibility.Visible;
                        this.errorEnd.Text = "Please select one month period";
                        SlideRightFadeIn(errorEnd);

                        this.context.IsToDateValid = false;

                        // NEW - IsFetchDatesBtnEnabled not working problem!!!
                        this.fetchByDates.IsEnabled = false;
                    }
                    */

                    //if (!DrawUtilities.IsStatisticsDatesPeriodValid(this.fromDate.Value.Value.Date, e.NewDateTime.Value.Date))
                    if (!DrawUtilities.IsSearchRangeValid(this.fromDate.Value.Value.Date, e.NewDateTime.Value.Date))                            
                    {
                        this.toDate.BorderBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0x02, 02)); //#FFFF0202
                        this.errorEnd.Visibility = System.Windows.Visibility.Visible;
                        this.errorEnd.Text = "Please select one month period";
                        SlideRightFadeIn(errorEnd);

                        this.context.IsToDateValid = false;

                        // NEW - IsFetchDatesBtnEnabled not working problem!!!
                        this.fetchByDates.IsEnabled = false;
                    }
                    else
                    {

                        this.context.IsToDateValid = true;
                        this.fetchByDates.IsEnabled = true;
                        if (this.errorStart.Visibility == System.Windows.Visibility.Visible)
                        {
                            validation = DrawUtilities.Validation(fromDate.Value.Value.Date, out message);
                            if (validation == DrawDateValidationResult.Valid)
                            {
                                this.errorStart.Visibility = System.Windows.Visibility.Collapsed;
                                this.fromDate.BorderBrush = (Brush)Application.Current.Resources["PhoneBorderBrush"];
                                this.context.IsFromDateValid = true;

                                // NEW - IsFetchDatesBtnEnabled not working problem!!!
                                this.fetchByDates.IsEnabled = true;
                            }
                        }
                    }
                }
            }
        }

        private void singleDate_ValueChanged(object sender, DateTimeValueChangedEventArgs e)
        {
            var message = string.Empty;
            var validation = DrawUtilities.Validation(e.NewDateTime.Value, out message);
            if (validation == DrawDateValidationResult.Invalid)
            {
                this.SingleDateError.Visibility = System.Windows.Visibility.Visible;
                this.SingleDateError.Text = message;
                this.singleDate.BorderBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0x02, 02)); //#FFFF0202
                if (SingleDateError.Visibility != System.Windows.Visibility.Collapsed)
                    SlideRightFadeIn(SingleDateError);

                this.context.IsSingleDateValid = false;
            }
            else
            {
                this.singleDate.BorderBrush = (Brush)Application.Current.Resources["PhoneBorderBrush"];
                this.SingleDateError.Visibility = System.Windows.Visibility.Collapsed;
                this.context.IsSingleDateValid = true;
            }
        }

        #endregion ValueChangedEvents



        /// <summary>
        /// Fetches by date range.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NEW_fetchWithDates_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            var param = button.CommandParameter as DateCriteria; // Gets the dates command parameters
            var jsonDate1 = param.StartAsString;
            
            // hide error messages
            if (errorStart.Visibility == System.Windows.Visibility.Visible)
            {
                fromDate.BorderBrush = (Brush)Application.Current.Resources["PhoneBorderBrush"];
                SlideLeftFadeOut(errorStart);
                errorStart.Visibility = System.Windows.Visibility.Collapsed;
            }
            if (errorEnd.Visibility == System.Windows.Visibility.Visible)
            {
                toDate.BorderBrush = (Brush)Application.Current.Resources["PhoneBorderBrush"];
                SlideLeftFadeOut(errorEnd);
                errorEnd.Visibility = System.Windows.Visibility.Collapsed;
            }

            //var dates = DrawUtilities.GetDates(param.Start, param.End); // get all dates between start and end criteria
            //_Dates = dates; // list of dates state
            //_lowDate = jsonDate1; // low date state
            
            // change  Dates state if in single date+time search mode
            if (this.datetimePanel.Visibility == System.Windows.Visibility.Visible)
            {
                var singleDate = this.singleDate.Value.Value;
                _Dates = DrawUtilities.GetDates(singleDate, singleDate);
                IsSingleDateTimeMode = true; // flag

            }
            else
            {
                IsSingleDateTimeMode = false;
                var dates = DrawUtilities.GetDates(param.Start, param.End); // get all dates between start and end criteria
                _Dates = dates; // list of dates state
                _lowDate = jsonDate1; // low date state
            }

            var profile = NetworkInformation.GetInternetConnectionProfile();
            if (profile == null)// || (!profile.IsWlanConnectionProfile && !profile.IsWwanConnectionProfile))            
            {
                this.progress.Visibility = System.Windows.Visibility.Collapsed;
                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                this.MainPivot.IsEnabled = true;
                MessageBoxResult result = MessageBox.Show("Please check your data and/or WiFi connection.", "No internet connection available", MessageBoxButton.OK);
            }
            else
            {
                //this.NavigationService.Navigate(new Uri("/ViewModels/Results.xaml", UriKind.Relative));
                this.NavigationService.Navigate(new Uri("/Views/MyResults.xaml", UriKind.Relative));
            }
        }


        private void lowhighradio_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: Add event handler implementation here.
        }

        private void lowhighradio_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: Add event handler implementation here.
        }

        private void lowhighradio_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (dateCriteriaPanel.Visibility == System.Windows.Visibility.Collapsed)
            {
                // first tap
                if (datetimePanel.Visibility == System.Windows.Visibility.Collapsed)
                {
                    SlideLeftFadeIn(dateCriteriaPanel);
                    dateCriteriaPanel.Visibility = System.Windows.Visibility.Visible;
                }

                else if (datetimePanel.Visibility == System.Windows.Visibility.Visible)
                {
                    SlideRightFadeOut(datetimePanel);
                    SlideRightFadeIn(dateCriteriaPanel);
                    dateCriteriaPanel.Visibility = System.Windows.Visibility.Visible;
                    datetimePanel.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }


        private void dateandtimeradio_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: Add event handler implementation here.
        }

        private void dateandtimeradio_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: Add event handler implementation here.
        }

        private void dateandtimeradio_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (datetimePanel.Visibility == System.Windows.Visibility.Collapsed)
            {
                // first tap
                if (dateCriteriaPanel.Visibility == System.Windows.Visibility.Collapsed)
                {
                    SlideRightFadeIn(datetimePanel);
                    datetimePanel.Visibility = System.Windows.Visibility.Visible;
                }

                else if (dateCriteriaPanel.Visibility == System.Windows.Visibility.Visible)
                {
                    SlideLeftFadeOut(datetimePanel);
                    SlideLeftFadeIn(datetimePanel);
                    datetimePanel.Visibility = System.Windows.Visibility.Visible;
                    dateCriteriaPanel.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }

        private void MinutesTextBox_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
        }

        private void HourButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/ViewModels/HoursPickerUri.xaml", UriKind.Relative)); 
        }

        private void MinutesButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	this.NavigationService.Navigate(new Uri("/ViewModels/MinutesPickerUri.xaml", UriKind.Relative)); 
        }

        private void lowhighmode_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            this.datetimemode.Visibility = System.Windows.Visibility.Visible;
            this.datetimePanel.Visibility = System.Windows.Visibility.Collapsed;
            this.dateCriteriaPanel.Visibility = System.Windows.Visibility.Visible;
            lowhighmode.Visibility = System.Windows.Visibility.Collapsed;
            KinoTransition.SlideRightFadeIn(this.datetimemode);            
            KinoTransition.SlideRightFadeIn(this.dateCriteriaPanel);
        }

        private void datetimemode_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            this.lowhighmode.Visibility = System.Windows.Visibility.Visible;
            this.dateCriteriaPanel.Visibility = System.Windows.Visibility.Collapsed;
            this.datetimePanel.Visibility = System.Windows.Visibility.Visible;
            datetimemode.Visibility = System.Windows.Visibility.Collapsed;
            KinoTransition.SlideLeftFadeIn(this.lowhighmode);
            KinoTransition.SlideLeftFadeIn(this.datetimePanel);            
        }

        private void DualDatesModeBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            this.fetchByDates.IsEnabled = this.context.IsToDateValid == true && this.context.IsFromDateValid == true;

            this.SingleDateModeBtn.Visibility = System.Windows.Visibility.Visible;
            this.datetimePanel.Visibility = System.Windows.Visibility.Collapsed;
            this.dateCriteriaPanel.Visibility = System.Windows.Visibility.Visible;
            DualDatesModeBtn.Visibility = System.Windows.Visibility.Collapsed;
            KinoTransition.SlideRightFadeIn(this.SingleDateModeBtn);
            KinoTransition.SlideRightFadeIn(this.dateCriteriaPanel);
            this.context.IsSingleDateTimeMode = false;
        }

        private void SingleDateModeBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            this.DualDatesModeBtn.Visibility = System.Windows.Visibility.Visible;
            this.dateCriteriaPanel.Visibility = System.Windows.Visibility.Collapsed;
            this.datetimePanel.Visibility = System.Windows.Visibility.Visible;
            this.SingleDateModeBtn.Visibility = System.Windows.Visibility.Collapsed;
            KinoTransition.SlideLeftFadeIn(this.DualDatesModeBtn);
            KinoTransition.SlideLeftFadeIn(this.datetimePanel);
            this.context.IsSingleDateTimeMode = true;
            this.fetchByDates.IsEnabled = true;
        }




    }
}