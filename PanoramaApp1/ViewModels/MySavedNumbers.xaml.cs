﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Kino.IsolatedSettings;
using Kino.Binding;
using KinoGenerator;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Kino
{
    public partial class MySavedNumbers : PhoneApplicationPage
    {
        // Holds the isolated storage kino settings
        private IsoKinoSettings isolatedKinoSettings;
        SequencesViewModel<Sequence> svm = new SequencesViewModel<Sequence>();
        private const int _InitialSavedItemsLoadsize = 10;

        // Bound collection of data
        public ObservableCollection<Sequence> Items { get; set; }
        // Data loading identification
        public bool IsLoading = false;
        // Thread lock object
        private object o = new object();
        //private int _InternalIndex = 0;

        public MySavedNumbers()
        {

            InitializeComponent();
            //SystemTray.SetIsVisible(this, false);                        
            InitializeAppBarProperties();
            InitializeIsolatedKinoSettings();
            SetBindings();
            PreloadSavedSequences();

            // display loading status in system tray
            if (svm.Source.Count != 0)
            {
                // To show the progress of the system tray
                ProgressIndicator progress = new ProgressIndicator
                {
                    IsVisible = true,
                    IsIndeterminate = true,
                    Text = "Loading numbers..."
                };
                SystemTray.SetProgressIndicator(this, progress);
            }

            svm.LoadFirstBatch(9);
            //SystemTray.SetProgressIndicator(this, null);
            //SystemTray.IsVisible = false;


        }


        /// <summary>
        /// Initializes the class to hold the isolated application settings of kino generator app.
        /// </summary>
        private void InitializeIsolatedKinoSettings()
        {
            // Get kino settings key from iso storage if any, otherwise create it
            isolatedKinoSettings = new IsoKinoSettings();
            var kinoSettings = isolatedKinoSettings.KinoSettings;

            // key was not found, create one
            if (kinoSettings == null)
            {
                isolatedKinoSettings.KinoSettings = new KinoSettings();
            }
        }

        /// <summary>
        /// Sets the value of the items source of stored sequences selector control.
        /// </summary>
        private void SetItemsSource()
        {
            NumbersSelector.DataContext = isolatedKinoSettings.KinoSettings;
            NumbersSelector.SetBinding(LongListMultiSelector.ItemsSourceProperty,
                new System.Windows.Data.Binding()
                {
                    Mode = System.Windows.Data.BindingMode.TwoWay,
                    Path = new PropertyPath("ObservableSavedSequences")
                });

            //Set binding to control to display the count of saved items
            //this.SavedSequencesCounter.DataContext = isolatedKinoSettings.KinoSettings;
            //this.SavedSequencesCounter.SetBinding(TextBlock.TextProperty, new System.Windows.Data.Binding() { Mode = System.Windows.Data.BindingMode.OneWay, Path = new PropertyPath("SavedSequencesCount") });
        }

        private void SetBindings()
        {
            //Set binding to control to display the count of saved items
            this.SavedSequencesCounter.DataContext = isolatedKinoSettings.KinoSettings;
            this.SavedSequencesCounter.SetBinding(TextBlock.TextProperty, new System.Windows.Data.Binding() { Mode = System.Windows.Data.BindingMode.OneWay, Path = new PropertyPath("SavedSequencesCount") });
        }

        /// <summary>
        /// Binds the property path for the preload of item source property of longlistselector of saved sequences
        /// </summary>
        private void PreloadSavedSequences()
        {
            // BUG: Something nulls the obs. collection. Update it from PersonalSequences hashset property in iso storage 
            //this.isolatedKinoSettings.KinoSettings.UpdateObservableCollectionSource();

            this.isolatedKinoSettings.KinoSettings.UpdateFromSavedSequencesSource();
            //this.isolatedKinoSettings.KinoSettings.UpdateFuckingSequencesSource();
            svm.Source = this.isolatedKinoSettings.KinoSettings.ObservableSavedSequences;
            //svm.Source = this.isolatedKinoSettings.KinoSettings.ObservableFuckingSequences;
            NumbersSelector.DataContext = svm;
            NumbersSelector.SetBinding(LongListMultiSelector.ItemsSourceProperty,
                new System.Windows.Data.Binding()
                {
                    Mode = System.Windows.Data.BindingMode.TwoWay,
                    Path = new PropertyPath("Items")                    
                });
        }

        #region Application Bar code
        private void InitializeAppBarProperties()
        {
            ApplicationBar = new ApplicationBar();
            ApplicationBar.Mode = ApplicationBarMode.Default;
            ApplicationBar.Opacity = .9;
            ApplicationBar.IsVisible = true;
            ApplicationBar.IsMenuEnabled = true;

        }

        private void CreateDeleteAppBarButton()
        {
            ApplicationBarIconButton button2 = new ApplicationBarIconButton();
            button2.IconUri = new Uri("/Assets/Icons/delete.png", UriKind.Relative);
            button2.Text = "delete";
            ApplicationBar.Buttons.Add(button2);
            //button2.Click += new EventHandler(DeleteButton_Click);
            button2.Click += new EventHandler(DeleteMyFuckingSequence_Click); // NEW FUCKING STUFF TO SPEED THINGS UP!!!
            button2.IsEnabled = true;
        }

        private void CreateSelectAppBarButton()
        {
            ApplicationBarIconButton button1 = new ApplicationBarIconButton();
            button1.IconUri = new Uri("/Assets/Icons/manage.png", UriKind.Relative);
            button1.Text = "select";
            ApplicationBar.Buttons.Add(button1);
            button1.Click += new EventHandler(SelectButton_Click);
            button1.IsEnabled = true;
        }

        private void CreateSelectAllAppBarButton()
        {
            ApplicationBarIconButton button = new ApplicationBarIconButton();
            button.IconUri = new Uri("/Assets/Icons/manage.png", UriKind.Relative);
            button.Text = "select all";
            ApplicationBar.Buttons.Add(button);
            button.Click += new EventHandler(SelectAllButton_Click);
            button.IsEnabled = true;
        }

        private void UpdateSelectAllAppBarMenuItem()
        {
            if (ApplicationBar != null && ApplicationBar.IsVisible)
            {
                ApplicationBar.MenuItems.Clear();
                ApplicationBarMenuItem menu = new ApplicationBarMenuItem();
                menu.Text = "select all";
                ApplicationBar.MenuItems.Add(menu);
                menu.Click += new EventHandler(SelectAllMenuITem_Click);
                if (NumbersSelector.IsSelectionEnabled)
                    menu.IsEnabled = true;
            }
        }

        private void UpdateUnSelectAllAppBarMenuItem()
        {
            if (ApplicationBar != null && ApplicationBar.IsVisible)
            {
                ApplicationBar.MenuItems.Clear();
                ApplicationBarMenuItem menu = new ApplicationBarMenuItem();
                menu.Text = "unselect all";
                ApplicationBar.MenuItems.Add(menu);
                menu.Click += new EventHandler(UnSelectAllMenuITem_Click);
                if (NumbersSelector.IsSelectionEnabled)
                    menu.IsEnabled = true;
            }
        }

        private void SelectButton_Click(object sender, EventArgs e)
        {
            // Enable selection mode            
            UpdateSelectAllAppBarMenuItem();
            NumbersSelector.IsSelectionEnabled = true;
        }

        private void SelectAllButton_Click(object sender, EventArgs e)
        {
            var count = NumbersSelector.ItemsSource.Count;
            for (var i = 0; i < count; i++)
            {
                var item = NumbersSelector.ItemsSource[i];
                var container = NumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                if (container == null)
                {
                    // item has't been assigned to UI
                    NumbersSelector.ScrollTo(item);
                    container = NumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                }
                container.IsSelected = true;
            }

            UpdateUnSelectAllAppBarMenuItem();
        }

        private void SelectAllMenuITem_Click(object sender, EventArgs e)
        {
            var count = NumbersSelector.ItemsSource.Count;
            for (var i = 0; i < count; i++)
            {
                var item = NumbersSelector.ItemsSource[i];
                var container = NumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                if (container == null)
                {
                    // item has't been assigned to UI
                    NumbersSelector.ScrollTo(item);
                    container = NumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                }
                container.IsSelected = true;
            }
            UpdateUnSelectAllAppBarMenuItem();
        }

        private void UnSelectAllMenuITem_Click(object sender, EventArgs e)
        {
            var count = NumbersSelector.ItemsSource.Count;
            for (var i = 0; i < count; i++)
            {
                var item = NumbersSelector.ItemsSource[i];
                var container = NumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                if (container == null)
                {
                    // item has't been assigned to UI
                    NumbersSelector.ScrollTo(item);
                    container = NumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                }
                container.IsSelected = false;
            }

            ApplicationBar.MenuItems.Clear();

        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            // copy iso storage list
            var storedSequences = isolatedKinoSettings.KinoSettings.SavedSequences;
            var ItemsClone = this.svm.Items;
            var clone = new ObservableCollection<Sequence>(); // clone of Items
            var deleted = new ObservableCollection<Sequence>(); // deleted items            
            var count = NumbersSelector.ItemsSource.Count;
            for (var i = 0; i < count; i++)
            {
                var item = NumbersSelector.ItemsSource[i];
                var container = NumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;

                // problem solved if numbers list is way beyond 40 items
                if (container == null)
                {
                    // item has't been assigned to UI
                    NumbersSelector.ScrollTo(item);
                    container = NumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                }

                if (container != null)
                {
                    var sequence = item as Sequence;
                    var match = storedSequences.SingleOrDefault(c => CompareSequence.IsEqual(c, sequence));
                    if (container.IsSelected == true)
                    {
                        if (match != null)
                        {
                            storedSequences.Remove(match); // remove from temp stored sequences list
                            deleted.Add(match); // add to deleted items
                        }
                    }
                }
            }

            //Reset iso storage list value to propagate to the binding target
            isolatedKinoSettings.KinoSettings.SavedSequences = storedSequences; //update isolated storage file sequences
            this.svm.Source = isolatedKinoSettings.KinoSettings.ObservableSavedSequences; //update view model
            foreach (var s in this.svm.Items)
            {
                var seq = s as SavedSequence;
                if (deleted.SingleOrDefault(c => CompareSequence.IsEqual(c, seq)) == null)
                {
                    clone.Add(seq); // copy non-deleted items
                }
            }

            this.svm.Items = clone; // refresh  observable collection - update UI selector list items source

            // Disable the foreach loop in case the tilt effect is not enabled after item deletion
            /*
            // Create fade up transition for remaining items except at index 0 position
            foreach (var item in NumbersSelector.ItemsSource)
            {
                var container = NumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                var index = NumbersSelector.ItemsSource.IndexOf(item);
                if (index != 0)
                {
                    SlideTransition slide = new SlideTransition();
                    slide.Mode = SlideTransitionMode.SlideUpFadeIn;
                    ITransition trans = slide.GetTransition(container);
                    trans.Completed += delegate { trans.Stop(); };
                    trans.Begin();
                }                
            }            
            */

            if (NumbersSelector.ItemsSource.Count == 0)
            {
                ApplicationBar.IsVisible = false;
                NumbersSelector.IsSelectionEnabled = false;
            }
        }

        #endregion Application Bar code

        #region Main List Selector

        private void NumbersSelector_IsSelectionEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {            
            var selector = sender as LongListMultiSelector;
            //var layout = selector.LayoutMode;
            //var gridCellSize = selector.GridCellSize;

            // Update app bar buttons
            if ((bool)e.NewValue == true)
            {
                //selector.GridCellSize = new Size(0, 0);
                //selector.LayoutMode = LongListSelectorLayoutMode.List;
                ApplicationBar.Buttons.RemoveAt(0);
            }
            else
            {
                //selector.GridCellSize = new Size(468,62);
                //selector.LayoutMode = LongListSelectorLayoutMode.Grid;

                if (ApplicationBar.Buttons.Count > 0)
                {
                    ApplicationBar.Buttons.Clear();
                }
                ApplicationBar.IsVisible = true;
                ApplicationBar.MenuItems.Clear();
                CreateSelectAppBarButton();
            }
        }

        private void NumbersSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {            
            var selector = sender as LongListMultiSelector;
            if (e.AddedItems.Count > 0)
            {
                ApplicationBar.IsVisible = true;
                if (e.AddedItems.Count == 1 && selector.SelectedItems.Count == 1)
                {
                    CreateDeleteAppBarButton();
                }
                if (selector.SelectedItems.Count == selector.ItemsSource.Count)
                    UpdateUnSelectAllAppBarMenuItem();
            }
            else if (e.RemovedItems.Count == 1)
            {
                UpdateSelectAllAppBarMenuItem();
                if (!selector.IsSelectionEnabled)
                    ApplicationBar.MenuItems.Clear();
            }
        }

        private void NumbersSelector_Loaded(object sender, RoutedEventArgs e)
        {
            // Attach bindings
            //SetItemsSource();
            //PreloadSavedSequences(); // set bindings and load first batch of items

            if (NumbersSelector.ItemsSource != null && NumbersSelector.ItemsSource.Count == 0) // new stuff 1.0.5
                ApplicationBar.IsVisible = false;
            else
                ApplicationBar.IsVisible = true;

            if (NumbersSelector.ItemsSource != null && NumbersSelector.ItemsSource.Count > 0)
                CreateSelectAppBarButton();

            //SystemTray.ProgressIndicator = null;
            //SystemTray.IsVisible = false;
        }

        #endregion

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (NumbersSelector.IsSelectionEnabled)
            {
                e.Cancel = true;
                NumbersSelector.IsSelectionEnabled = false;
            }
        }


        private void DeleteEntry_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var button = sender;
            var menuItem = sender as MenuItem;
            Sequence selected = (menuItem).DataContext as Sequence;
            MessageBoxResult result = MessageBox.Show("Do you want to delete the sequence?", "Delete sequence?", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
            {
                var savedSequences = isolatedKinoSettings.KinoSettings.SavedSequences;
                var match = savedSequences.SingleOrDefault(c => CompareSequence.IsEqual(c, selected));
                if (match == null)
                    return;
                else
                {
                    savedSequences.Remove(match);
                    isolatedKinoSettings.KinoSettings.SavedSequences = savedSequences; // reset list                    
                    NumbersSelector.UpdateLayout();

                    if (NumbersSelector.ItemsSource.Count == 0)
                    {
                        ApplicationBar.IsVisible = false;
                        NumbersSelector.IsSelectionEnabled = false;
                    }
                }
            }
        }

        private void NEW_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var button = sender;
            var menuItem = sender as MenuItem;
            Sequence selected = (menuItem).DataContext as Sequence;

            var ItemsClone = this.svm.Items;
            var clone = new ObservableCollection<Sequence>(); // clone of Items
            var deleted = new ObservableCollection<Sequence>(); // deleted items 

            MessageBoxResult result = MessageBox.Show("Do you want to delete the sequence?", "Delete sequence?", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
            {
                var savedSequences = isolatedKinoSettings.KinoSettings.SavedSequences;
                var match = savedSequences.SingleOrDefault(c => CompareSequence.IsEqual(c, selected));
                if (match == null)
                    return;
                else
                {
                    savedSequences.Remove(match);
                    deleted.Add(match); // add to deleted items
                    isolatedKinoSettings.KinoSettings.SavedSequences = savedSequences; //update isolated storage file sequences
                    this.svm.Source = isolatedKinoSettings.KinoSettings.ObservableSavedSequences; //update view model
                    foreach (var s in this.svm.Items)
                    {
                        var seq = s as SavedSequence;
                        if (deleted.SingleOrDefault(c => CompareSequence.IsEqual(c, seq)) == null)
                        {
                            clone.Add(seq); // copy non-deleted items
                        }
                    }
                    this.svm.Items = clone; // refresh  observable collection - update UI selector list items source

                    //isolatedKinoSettings.KinoSettings.SavedSequences = savedSequences; // reset list                    
                    //NumbersSelector.UpdateLayout();

                    if (NumbersSelector.ItemsSource.Count == 0)
                    {
                        ApplicationBar.IsVisible = false;
                        NumbersSelector.IsSelectionEnabled = false;
                    }
                }
            }
        }

        private void ContextMenu_Unload(object sender, RoutedEventArgs e)
        {
            ContextMenu conmen = (sender as ContextMenu);
            conmen.ClearValue(FrameworkElement.DataContextProperty);
        }

        private void ContextMenu_Opened(object sender, RoutedEventArgs e)
        {
            var menu = (ContextMenu)sender;
            var owner = (FrameworkElement)menu.Owner;
            if (owner.DataContext != menu.DataContext)
                menu.DataContext = owner.DataContext;

        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var button = sender;
            var menuItem = sender as MenuItem;
            Sequence selected = (menuItem).DataContext as Sequence;
            MessageBoxResult result = MessageBox.Show("Do you want to delete the sequence?", "Delete sequence?", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
            {
                var savedSequences = isolatedKinoSettings.KinoSettings.SavedSequences;
                var match = savedSequences.SingleOrDefault(c => CompareSequence.IsEqual(c, selected));
                if (match == null)
                    return;
                else
                {
                    savedSequences.Remove(match);
                    isolatedKinoSettings.KinoSettings.SavedSequences = savedSequences; // reset list                    
                    NumbersSelector.UpdateLayout();

                    if (NumbersSelector.ItemsSource.Count == 0)
                    {
                        ApplicationBar.IsVisible = false;
                        NumbersSelector.IsSelectionEnabled = false;
                    }
                }
            }
        }

        private void NumbersSelector_ItemRealized(object sender, ItemRealizationEventArgs e)
        {
            LongListSelector selector = sender as LongListSelector;
            var items = selector.ItemsSource;
            var index = items.IndexOf(e.Container.Content);
            if (index != -1)
            {
                if (items != null)
                {

                    //if (index!= 0 && items.Count - index == 1) //reached size if first batch
                    if (items.Count - index == 10) //reached size if first batch
                    {
                        this.svm.LoadNextBatch(15);  //load next batch when third from last item is realized
                    }
                }
            }

            if (this.svm.Items.Count <= 14)
            {
                SystemTray.SetProgressIndicator(this, null);
                SystemTray.SetIsVisible(this, false);
            }
        }


        void SavedItems_ItemRealized(object sender, ItemRealizationEventArgs e)
        {
            // Because the event will be multiple threads to enter, so adding thread lock, code can only be a single thread to execute control below
            lock (o)
            {
                if (!IsLoading && !NumbersSelector.IsSelectionEnabled)
                {
                    if (e.ItemKind == LongListSelectorItemKind.Item)
                    {
                        if ((e.Container.Content as Sequence).Equals(NumbersSelector.ItemsSource[NumbersSelector.ItemsSource.Count - 1]))
                        {
                            // Set IsLoading to true, in the loading process, ban repeatedly into the
                            IsLoading = true;

                            // Simulated background time-consuming tasks pull data of the scene
                            Task.Factory.StartNew(async () =>
                            {
                                await Task.Delay(100);

                                // Call the UI threads add data
                                this.Dispatcher.BeginInvoke(() =>
                                {                                    
                                    // load next batch
                                    svm.LoadNextBatch(15);
                                    
                                    // Modify the state of loading
                                    IsLoading = false;
                                });
                            });
                        }

                    }
                }

            }            

            // disable progress bar when reached last item in ui
            if (this.svm.Items.Count == this.svm.Source.Count)
            {
                SystemTray.SetProgressIndicator(this, null);
                SystemTray.SetIsVisible(this, false);
            }
        }

        private void NumbersSelector_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var position = e.OriginalSource;
        }

        #region NEW DELETE SAVED SEQUENCE FUNCTION - USING HASHSET<SEQUENCE>

        private void DeleteMyFuckingSequence_Click(object sender, EventArgs e)
        {
            // copy iso storage list            
            var storedSequences = isolatedKinoSettings.KinoSettings.PersonalSequences;
            var ItemsClone = this.svm.Items;
            var clone = new ObservableCollection<Sequence>(); // clone of Items
            var deletedSet = new HashSet<Sequence>(); //deleted items            
            var count = NumbersSelector.ItemsSource.Count;
            for (var i = 0; i < count; i++)
            {
                var item = NumbersSelector.ItemsSource[i];
                var container = NumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;

                // problem solved if numbers list is way beyond 40 items
                if (container == null)
                {
                    // item has't been assigned to UI
                    NumbersSelector.ScrollTo(item);
                    container = NumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                }

                if (container != null)
                {
                    var sequence = item as Sequence;
                    var match = storedSequences.Contains(sequence); //.SingleOrDefault(c => CompareSequence.IsEqual(c, sequence));
                    if (container.IsSelected == true)
                    {
                        if (match == true)
                        {
                            storedSequences.Remove(sequence); // remove from temp stored sequences list                            
                            deletedSet.Add(sequence);
                        }
                    }
                }
            }

            //Reset iso storage list value to propagate to the binding target
            isolatedKinoSettings.KinoSettings.PersonalSequences = storedSequences; //update isolated storage file sequences
            this.svm.Source = isolatedKinoSettings.KinoSettings.ObservableSavedSequences; //update view model
            foreach (var s in this.svm.Items)
            {
                var seq = s as SavedSequence;
                if (!deletedSet.Contains(seq))
                {
                    clone.Add(seq);
                }
            }
            this.svm.Items = clone; // refresh  observable collection - update UI selector list items source
            if (NumbersSelector.ItemsSource.Count == 0)
            {
                ApplicationBar.IsVisible = false;
                NumbersSelector.IsSelectionEnabled = false;
            }
        }

        private void DeleteMyFuckingSequenceMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var button = sender;
            var menuItem = sender as MenuItem;
            SavedSequence selected = (menuItem).DataContext as SavedSequence;

            var ItemsClone = this.svm.Items;
            var clone = new ObservableCollection<Sequence>(); // clone of Items
            var deletedSet = new HashSet<Sequence>(); //deleted items  
            var deleted = new ObservableCollection<Sequence>(); // deleted items 

            MessageBoxResult result = MessageBox.Show("Do you want to delete the sequence?", "Delete sequence?", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
            {
                //var savedSequences = isolatedKinoSettings.KinoSettings.SavedSequences;
                var storedSequences = isolatedKinoSettings.KinoSettings.PersonalSequences;
                //var match = savedSequences.SingleOrDefault(c => CompareSequence.IsEqual(c, selected));
                var match = storedSequences.Contains(selected);
                if (match != true)
                    return;
                else
                {
                    //savedSequences.Remove(match);
                    storedSequences.Remove(selected);
                    //deleted.Add(match); // add to deleted items
                    deletedSet.Add(selected);
                    //isolatedKinoSettings.KinoSettings.SavedSequences = savedSequences; //update isolated storage file sequences
                    isolatedKinoSettings.KinoSettings.PersonalSequences = storedSequences;
                    this.svm.Source = isolatedKinoSettings.KinoSettings.ObservableSavedSequences; //update view model
                    foreach (var s in this.svm.Items)
                    {
                        var seq = s as SavedSequence;
                        /*
                        if (deleted.SingleOrDefault(c => CompareSequence.IsEqual(c, seq)) == null)
                        {
                            clone.Add(seq); // copy non-deleted items
                        }
                        */
                        if (!deletedSet.Contains(seq))
                        {
                            clone.Add(seq);
                        }
                    }
                    this.svm.Items = clone; // refresh  observable collection - update UI selector list items source

                    //isolatedKinoSettings.KinoSettings.SavedSequences = savedSequences; // reset list                    
                    //NumbersSelector.UpdateLayout();

                    if (NumbersSelector.ItemsSource.Count == 0)
                    {
                        ApplicationBar.IsVisible = false;
                        NumbersSelector.IsSelectionEnabled = false;
                    }
                }
            }
        }

        #endregion




    }




}