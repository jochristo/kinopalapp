﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System;
using System.Windows;
using System.Windows.Navigation;
using System.Windows.Media;
using Kino.Binding;
using System.Windows.Controls;
using Microsoft.Phone.Controls.Primitives;
using Kino.Data;
using System.Collections.ObjectModel;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Data;
using Kino.IsolatedSettings;
using PanoramaApp1;

namespace Kino
{
    public partial class Settings : PhoneApplicationPage
    {
        private Uri _navigationFrom;
        public Uri NavigationFrom
        {
            get { return _navigationFrom; }
            internal set { _navigationFrom = value; }
        }

        private KinoSettings _kinoSettings;
        public KinoSettings KinoSettings
        {
            get { return _kinoSettings; }
            set { _kinoSettings = value; }
        }

        // Holds the isolated storage kino settings
        private IsoKinoSettings isolatedKinoSettings;

        public Settings()
        {                      
            InitializeComponent();
            SystemTray.SetIsVisible(this, false); 
			
			// Disable visibility for favorite-related components
			this.AddFavNumbersButton.IsEnabled = false;
            this.AddFavNumbersButton.Visibility = System.Windows.Visibility.Collapsed;			
			this.ExcludedStatus.Visibility = System.Windows.Visibility.Collapsed;
			this.FavoritesBorder.Visibility = System.Windows.Visibility.Collapsed;

            // Initialize isolated settings
            InitializeIsolatedKinoSettings();

            // Initialize kino settings for data context and binding
            SetBindings();
            
            // Initialize app bar
            InitializeAppBarProperties();

            //this.SettingsPivot.Title = LocalizedStrings.GreekLabels()["Settings"];
        }

        /// <summary>
        /// Initializes the class to hold the isolated application settings of kino generator app.
        /// </summary>
        private void InitializeIsolatedKinoSettings()
        {
            // Get kino settings key from iso storage if any, otherwise create it
            isolatedKinoSettings = new IsoKinoSettings();
            var kinoSettings = isolatedKinoSettings.KinoSettings;

            // key was not found, create one
            if (kinoSettings == null)
            {
                isolatedKinoSettings.KinoSettings = new KinoSettings();
            }
        }

        /// <summary>
        /// Sets the data context and attaches the bindings for the kino settings UI controls.
        /// </summary>
        private void SetBindings()
        {
            this.RecurrenceModeSwitch.DataContext = this.isolatedKinoSettings.KinoSettings;
            this.FavoriteModeSwitch.DataContext = this.isolatedKinoSettings.KinoSettings;
            this.FavoriteModeValueTextBox.DataContext = this.isolatedKinoSettings.KinoSettings;
            this.RecurrenceValueTextBox.DataContext = this.isolatedKinoSettings.KinoSettings;

            this.FavoriteModeSwitch.SetBinding(ToggleSwitchButton.IsCheckedProperty, new System.Windows.Data.Binding() { Mode = System.Windows.Data.BindingMode.TwoWay, Path = new PropertyPath("IsFavoritesMode") });
            this.RecurrenceModeSwitch.SetBinding(ToggleSwitchButton.IsCheckedProperty, new System.Windows.Data.Binding() { Mode = System.Windows.Data.BindingMode.TwoWay, Path = new PropertyPath("IsRecurrenceMode") });
            this.FavoriteModeValueTextBox.SetBinding(TextBlock.TextProperty, new System.Windows.Data.Binding() { Mode = System.Windows.Data.BindingMode.TwoWay, Path = new PropertyPath("IsFavoritesModeValue") });
            this.RecurrenceValueTextBox.SetBinding(TextBlock.TextProperty, new System.Windows.Data.Binding() { Mode = System.Windows.Data.BindingMode.TwoWay, Path = new PropertyPath("IsRecurrenceModeValue") });

            this.ExcludedStatus.DataContext = this.isolatedKinoSettings.KinoSettings;
            this.ExcludedStatus.SetBinding(TextBlock.TextProperty, new System.Windows.Data.Binding() { Mode= BindingMode.TwoWay, Path = new PropertyPath("ExcludedCountStatus") });

            this.RecurSavedSeqSwitch.DataContext = this.isolatedKinoSettings.KinoSettings;
            this.RecurSavedSeqValueTextBox.DataContext = this.isolatedKinoSettings.KinoSettings;
            this.RecurSavedSeqSwitch.SetBinding(ToggleSwitchButton.IsCheckedProperty, new System.Windows.Data.Binding() { Mode = System.Windows.Data.BindingMode.TwoWay, Path = new PropertyPath("IsSavedSequencesRepeated") });
            this.RecurSavedSeqSwitch.SetBinding(ToggleSwitchButton.IsEnabledProperty, new System.Windows.Data.Binding() { Mode = System.Windows.Data.BindingMode.TwoWay, Path = new PropertyPath("IsSavedSequencesRepeatedSwitchEnabled") });
            this.RecurSavedSeqValueTextBox.SetBinding(TextBlock.TextProperty, new System.Windows.Data.Binding() { Mode = System.Windows.Data.BindingMode.TwoWay, Path = new PropertyPath("SavedSequencesRepeatValue") });
        }

        /// <summary>
        /// Re assignes the data context's property values and updates UI controls data context
        /// </summary>
        private void UpdateSettingsDataContext()
        {
            // change data context property values to have effect on the binding control
            //var ks = new KinoSettings();
            var ks = new KinoSettings();
            ks = this.KinoSettings;            
            ks.ExcludedNumbers = KinoSettings.ExcludedNumbers;
            //ks.Favorites = KinoSettings.Favorites;
            ks.IsFavoritesMode = KinoSettings.IsFavoritesMode;
            ks.IsRecurrenceMode = KinoSettings.IsRecurrenceMode;

            var isosettings = this.isolatedKinoSettings.KinoSettings;
            // update UI data context
            this.RecurrenceModeSwitch.DataContext = isosettings;
            this.FavoriteModeSwitch.DataContext = isosettings;
            this.FavoriteModeValueTextBox.DataContext = isosettings;
            this.RecurrenceValueTextBox.DataContext = isosettings;
            this.ExcludedStatus.DataContext = isosettings;
        }
        
        /// <summary>
        /// Initializes the application bar and its buttons.
        /// </summary>
        private void InitializeAppBarProperties()
        {
            ApplicationBar = new ApplicationBar();
            ApplicationBar.Mode = ApplicationBarMode.Default;            
            ApplicationBar.Opacity = 1.0;
            ApplicationBar.IsVisible = false;
            ApplicationBar.IsMenuEnabled = false;
        }

        
        /// <summary>
        /// Enables the favorites mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FavoriteModeSwitch_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
			// Enable visibility
            //this.FavoriteModeValueTextBox.Text = "ON";
			this.AddFavNumbersButton.IsEnabled = true;
            this.AddFavNumbersButton.Visibility = System.Windows.Visibility.Visible;
			this.FavoritesBorder.Visibility = System.Windows.Visibility.Visible;
			
			// Change brushes
            var phoneForegroundBrush = (Brush)Application.Current.Resources["PhoneForegroundBrush"];
			var phoneSubtleBrush = (Brush)Application.Current.Resources["PhoneSubtleBrush"];
            this.FavoriteModeDescription.Foreground = phoneSubtleBrush;
		
            // Prepare button transition effect						
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideDownFadeIn;
            ITransition trans = slide.GetTransition(AddFavNumbersButton);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
			
			/*
			// Prepare Recurrence block transition effect			           			
			slide.Mode = SlideTransitionMode.SlideDownFadeIn;
            trans = slide.GetTransition(SettingsStack);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();						
			*/
			
            // Prepare text block transition effect
            this.ExcludedStatus.Visibility = System.Windows.Visibility.Visible;          
            slide.Mode = SlideTransitionMode.SlideDownFadeIn;
            trans = slide.GetTransition(this.ExcludedStatus);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();

        }

        /// <summary>
        /// Disables the favorites mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FavoriteModeSwitch_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
			// Disable visibility
        	//this.FavoriteModeValueTextBox.Text = "OFF";          
            this.AddFavNumbersButton.IsEnabled = false;
			this.FavoritesBorder.Visibility = System.Windows.Visibility.Collapsed;
			
			// Change brushes
            var phoneInactiveBrush = (Brush)Application.Current.Resources["PhoneInactiveBrush"];
            this.FavoriteModeDescription.Foreground = phoneInactiveBrush;
			
			// Prepare Recurrence block transition effect
			SlideTransition slide = new SlideTransition();
			slide.Mode = SlideTransitionMode.SlideUpFadeIn;
            ITransition trans = slide.GetTransition(SettingsStack);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();			

			/*
            // Prepare button transition effect
            slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideLeftFadeOut;
            trans = slide.GetTransition(AddFavNumbersButton);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
			
            // Prepare text block transition effect         
            slide.Mode = SlideTransitionMode.SlideLeftFadeOut;
            trans = slide.GetTransition(this.ExcludedStatus);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
			*/
        }
        
        /// <summary>
        /// Enables the recurrence mode.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RecurrenceModeSwitch_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            var phoneSubtleBrush = (Brush)Application.Current.Resources["PhoneSubtleBrush"];
            this.RecFuncDescription.Foreground = phoneSubtleBrush;
            
            // Prepare text transition effect
            this.RecFuncDescription.Visibility = System.Windows.Visibility.Visible;

        }

        /// <summary>
        /// Disables the recurrence mode.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RecurrenceModeSwitch_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            var phoneInactiveBrush = (Brush)Application.Current.Resources["PhoneInactiveBrush"];
            this.RecFuncDescription.Foreground = phoneInactiveBrush;

            // Prepare text transition effect
            this.RecFuncDescription.Visibility = System.Windows.Visibility.Visible;
        }


        /// <summary>
        /// Click event handler for changing check box is checked value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RecurSavedSeqSwitch_Click(object sender, RoutedEventArgs e)
        {
            var toggleSwitch = sender as ToggleSwitchButton;
            // Change brushes
            var phoneSubtleBrush = (Brush)Application.Current.Resources["PhoneSubtleBrush"];
            var phoneInactiveBrush = (Brush)Application.Current.Resources["PhoneInactiveBrush"];
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideRightFadeIn;
            if (toggleSwitch.IsChecked.Value)
            {
                this.RecurSavedSequencesDescrTextbox.Foreground = phoneSubtleBrush; 
            }
            else
            {
                this.RecurSavedSequencesDescrTextbox.Foreground = phoneInactiveBrush;
            }
        }

        // Render recurring saved sequences switch  based on isolated storage settings
        private void RenderIsEnabledRecSavedSeqSwitch()
        {
            var phoneForegroundBrush = (Brush)Application.Current.Resources["PhoneForegroundBrush"];
            var phoneSubtleBrush = (Brush)Application.Current.Resources["PhoneSubtleBrush"];
            var phoneInactiveBrush = (Brush)Application.Current.Resources["PhoneInactiveBrush"];

            // Set switch properties
            if (this.RecurrenceModeSwitch.IsChecked.Value)
            {
                //if (this.isolatedKinoSettings.KinoSettings.SavedSequences != null 
                //    && this.isolatedKinoSettings.KinoSettings.SavedSequences.Count > 0)
                if (this.isolatedKinoSettings.KinoSettings.PersonalSequences != null
                    && this.isolatedKinoSettings.KinoSettings.PersonalSequences.Count > 0)
                {
                    this.RepeatSavedSeqLabel.Foreground = phoneSubtleBrush;
                    this.RecurSavedSeqValueTextBox.Foreground = phoneForegroundBrush;
                }

                if (this.isolatedKinoSettings.KinoSettings.IsSavedSequencesRepeated)
                    this.RecurSavedSequencesDescrTextbox.Foreground = phoneSubtleBrush;
                else
                    this.RecurSavedSequencesDescrTextbox.Foreground = phoneInactiveBrush;
            }
            else
            {
                //this.RecurSavedSeqSwitch.IsEnabled = false;
                this.RepeatSavedSeqLabel.Foreground = phoneInactiveBrush;
                this.RecurSavedSeqValueTextBox.Foreground = phoneInactiveBrush;
                this.RecurSavedSequencesDescrTextbox.Foreground = phoneInactiveBrush;
            }
        }

        private void RecurSavedSeqSwitch_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var phoneInactiveBrush = (Brush)Application.Current.Resources["PhoneInactiveBrush"];
            var phoneForegroundBrush = (Brush)Application.Current.Resources["PhoneForegroundBrush"];
            var phoneSubtleBrush = (Brush)Application.Current.Resources["PhoneSubtleBrush"];
            SlideTransition slide = new SlideTransition();            

            if((bool)e.NewValue == false)
            {
                this.RepeatSavedSeqLabel.Foreground = phoneInactiveBrush;
                this.RecurSavedSeqValueTextBox.Foreground = phoneInactiveBrush;
                this.RecurSavedSequencesDescrTextbox.Foreground = phoneInactiveBrush;

            }
            else
            {
                this.RepeatSavedSeqLabel.Foreground = phoneSubtleBrush;
                this.RecurSavedSeqValueTextBox.Foreground = phoneForegroundBrush;
                if(this.RecurSavedSeqSwitch.IsChecked.Value)
                    this.RecurSavedSequencesDescrTextbox.Foreground = phoneSubtleBrush;
                else
                    this.RecurSavedSequencesDescrTextbox.Foreground = phoneInactiveBrush;
            }
        }

        /// <summary>
        /// Navigation event handler prior to navigating to this page. It processes and sets the KinoSettings property.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {            
            base.OnNavigatedTo(e);
            
            var phoneForegroundBrush = (Brush)Application.Current.Resources["PhoneForegroundBrush"];
            var phoneSubtleBrush = (Brush)Application.Current.Resources["PhoneSubtleBrush"];
            var phoneInactiveBrush = (Brush)Application.Current.Resources["PhoneInactiveBrush"];
            if (this.RecurSavedSeqSwitch.IsChecked.Value && this.RecurSavedSeqSwitch.IsEnabled)
                this.RecurSavedSequencesDescrTextbox.Foreground = phoneSubtleBrush;
            else
                this.RecurSavedSequencesDescrTextbox.Foreground = phoneInactiveBrush;
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            base.OnBackKeyPress(e);
        }

        /// <summary>
        /// Button click event. It navigate forward to the exclude numbers page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddFavNumbersButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            // Navigate to favorites page
            this.NavigationService.Navigate(new Uri("/Views/ExcludedNumbers.xaml", UriKind.Relative)); 
        }



    }
}