﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Kino.Data;
using System.Collections;
using PanoramaApp1;

namespace Kino
{
    public partial class WinningNumbers : PhoneApplicationPage
    {
        private readonly int _countOfWinningsNumbers = 20;

        public WinningNumbers()
        {
            // Init phone component
            InitializeComponent();

            // Init app bar
            InitializeApplicationBar();

            // Make LongListMultiSelector a tiltable control
            //TiltEffect.TiltableItems.Add(typeof(LongListMultiSelector));

            // Set page data context to Kino ViewModel.
            //this.DataContext = App.KinoViewModel;
        }


        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            /*
            base.OnNavigatedTo(e);            
            //this.WinningNumbersSelector.Visibility = System.Windows.Visibility.Visible;            
            SwivelTransition swivel = new SwivelTransition();
            swivel.Mode = SwivelTransitionMode.FullScreenIn;
            PhoneApplicationPage page = (PhoneApplicationPage)((PhoneApplicationFrame)Application.Current.RootVisual).Content;
            ITransition trans = swivel.GetTransition(page);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
            */
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            base.OnBackKeyPress(e);
            SwivelFullScreenOut();
        }

        /// <summary>
        /// Initialize application bar
        /// </summary>
        private void InitializeApplicationBar()
        {
            ApplicationBar = new ApplicationBar();
            ApplicationBar.Mode = ApplicationBarMode.Minimized;
            ApplicationBar.IsVisible = true;           

            ApplicationBarIconButton saveBtn = new ApplicationBarIconButton(new Uri("/Assets/Icons/check.png", UriKind.Relative));
            saveBtn.Text = "done";
            ApplicationBar.Buttons.Add(saveBtn);
            saveBtn.Click += new EventHandler(DoneButton_Click);
            saveBtn.IsEnabled = false;

            ApplicationBarIconButton cancelBtn = new ApplicationBarIconButton(new Uri("/Assets/Icons/cancel1.png", UriKind.Relative));
            cancelBtn.Text = "cancel";
            ApplicationBar.Buttons.Add(cancelBtn);
            cancelBtn.IsEnabled = false;
            cancelBtn.Click += new EventHandler(CancelButton_Click);
        }

        /// <summary>
        /// Save app button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DoneButton_Click(object sender, EventArgs e)
        {            
            //NavigationService.GoBack();
            var sourceKey = "NavigationFromUri";
            var sourceValue = NavigationService.CurrentSource;
            this.NavigationContext.QueryString.Add(new KeyValuePair<string, string>(sourceKey, sourceValue.ToString()));

            // Save to database


            NavigationService.GoBack();           

        }

        
        /// <summary>
        /// Cancel app button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            // De-select all selected items
            var itemsSource = WinningNumbersSelector.ItemsSource;
            foreach(var item in itemsSource)
            {
                var container = WinningNumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                if (container != null && container.IsSelected)
                    container.IsSelected = false;
            }
        }

        /// <summary>
        /// Create the "exiting" effect
        /// </summary>
        private void SwivelFullScreenOut()
        {
            SwivelTransition swivel = new SwivelTransition();
            swivel.Mode = SwivelTransitionMode.FullScreenOut;            
            PhoneApplicationPage page = (PhoneApplicationPage)((PhoneApplicationFrame)Application.Current.RootVisual).Content;
            ITransition trans = swivel.GetTransition(page);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        /// <summary>
        /// Selection has occured, process event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WinningNumbersSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateApplicationBar();
            var selectedItems = WinningNumbersSelector.SelectedItems;
            ProcessSelectedNumbers(selectedItems); 
        }

        /// <summary>
        /// Updates the application bar
        /// </summary>
        private void UpdateApplicationBar()
        {
            var isItemSelected = ((WinningNumbersSelector.SelectedItems != null) && (WinningNumbersSelector.SelectedItems.Count > 0));
            if(isItemSelected)
                this.ApplicationBar.Mode = ApplicationBarMode.Default;
            else
                this.ApplicationBar.Mode = ApplicationBarMode.Minimized;
            foreach (var b in ApplicationBar.Buttons)
            {
                var button = b as ApplicationBarIconButton;
                button.IsEnabled = isItemSelected;                
            }
        }

        /// <summary>
        /// Saves selected items to app data store
        /// </summary>
        private void ProcessSelectedNumbers(IList collection)
        {            
            if(collection != null && collection.Count > 0)
            {
                // Selection overreached max limit
                if (collection.Count > _countOfWinningsNumbers)
                {
                    foreach (var b in ApplicationBar.Buttons)
                    {
                        var button = b as ApplicationBarIconButton;
                        button.IsEnabled = false;
                    }
                    this.LayoutRoot.IsHitTestVisible = false;
                    this.MaxSelectedReachedPopup.IsOpen = true;
                }
                // Selection is OK
                else if(collection.Count < _countOfWinningsNumbers)
                {
                    var buttons = ApplicationBar.Buttons;
                    foreach (var b in ApplicationBar.Buttons)
                    if(b is ApplicationBarIconButton)
                    {
                        var button = b as ApplicationBarIconButton;
                        if (button.IconUri.ToString() == "/Assets/Icons/check.png")
                            button.IsEnabled = false;
                    }
                }
            }
        }

        /// <summary>
        /// Event on pressing popup OK-close button
        /// Closes popup screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MaxReachedPopupOKButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            // Find excess and de-select it from list
            var selectedItems = WinningNumbersSelector.SelectedItems;
            var itemsSource = WinningNumbersSelector.ItemsSource;
            var lastSelected = selectedItems[selectedItems.Count - 1] as WinningNumberItem;
            var itemsSourceEnumerated = itemsSource as IEnumerable<WinningNumberItem>;
            var enumeratedLastSelected = itemsSourceEnumerated.SingleOrDefault(c => c == lastSelected);
            if (enumeratedLastSelected != null)
            {
                var container = WinningNumbersSelector.ContainerFromItem(enumeratedLastSelected) as LongListMultiSelectorItem;
                if(container != null && container.IsSelected)
                container.IsSelected = false;
            }
            // Close popup
            MaxSelectedReachedPopup.IsOpen = false;
        }

        /// <summary>
        /// Event after opening popup screen.
        /// Disables visibility of system tray, app bar, buttons and viewmodel's main grid hit test
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MaxSelectedReachedPopup_Opened(object sender, EventArgs e)
        {            
			SystemTray.IsVisible = false;
            ApplicationBar.IsVisible = true;
            ApplicationBar.IsMenuEnabled = false;
            LayoutRoot.IsHitTestVisible = false;
            foreach (var b in ApplicationBar.Buttons)
            {
                var button = b as ApplicationBarIconButton;
                button.IsEnabled = false;
            }
        }

        /// <summary>
        /// Event after closing popup screen.
        /// Re-enables visibility of system tray, app bar, buttons and viewmodel's main grid hit test
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MaxSelectedReachedPopup_Closed(object sender, EventArgs e)
        {
            ApplicationBar.IsVisible = true;
            LayoutRoot.IsHitTestVisible = true;
            foreach (var b in ApplicationBar.Buttons)
            {
                var button = b as ApplicationBarIconButton;
                button.IsEnabled = true;
            }
            SystemTray.IsVisible = true;
        }


    }
}