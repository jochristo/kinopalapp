﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Kino
{
    public partial class DrawResult : PhoneApplicationPage
    {
        public DrawResult()
        {
            // Init phone component
            InitializeComponent();

            // Init app bar
            InitializeApplicationBar();
        }

        /// <summary>
        /// Click event for select number button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NumbersButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // Navigate to favorites page
            this.NavigationService.Navigate(new Uri("/Views/WinningNumbers.xaml", UriKind.Relative));
            
            /*
            SwivelTransition swivel = new SwivelTransition();
            swivel.Mode = SwivelTransitionMode.BackwardOut;
            PhoneApplicationPage page = (PhoneApplicationPage)((PhoneApplicationFrame)Application.Current.RootVisual).Content;
            ITransition trans = swivel.GetTransition(page);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();            
            */
        }

        /// <summary>
        /// Prepare page navigation transition and other features
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            /*
            base.OnNavigatedTo(e);
            var navigationMode = e.NavigationMode;
            PhoneApplicationPage page = (PhoneApplicationPage)((PhoneApplicationFrame)Application.Current.RootVisual).Content;

            var datePickerPageUri = DateSelector.PickerPageUri;
            var timePickerPageUri = TimeSelector.PickerPageUri;
            
            if (navigationMode == NavigationMode.New || navigationMode == NavigationMode.Forward)
            {
                var slide = new SlideTransition();
                slide.Mode = SlideTransitionMode.SlideUpFadeIn;
                ITransition trans = slide.GetTransition(page);
                trans.Completed += delegate { trans.Stop(); };
                trans.Begin();
            }
            else if (navigationMode == NavigationMode.Back)
            {                
                var turnstile = new TurnstileTransition();
                turnstile.Mode = TurnstileTransitionMode.ForwardIn;

                SwivelTransition swivel = new SwivelTransition();
                //swivel.Mode = SwivelTransitionMode.BackwardIn;
                swivel.Mode = SwivelTransitionMode.FullScreenIn;
                ITransition trans = swivel.GetTransition(page);
                trans.Completed += delegate { trans.Stop(); };
                trans.Begin();
            }
            */
        }
        

        /// <summary>
        /// Initialize application bar
        /// </summary>
        private void InitializeApplicationBar()
        {
            ApplicationBar = new ApplicationBar();
            ApplicationBar.Mode = ApplicationBarMode.Minimized;
            ApplicationBar.IsVisible = true;
            
            ApplicationBarIconButton saveBtn = new ApplicationBarIconButton(new Uri("/Assets/Icons/save.png", UriKind.Relative));
            saveBtn.Text = "save";
            ApplicationBar.Buttons.Add(saveBtn);
            saveBtn.Click += new EventHandler(SaveButton_Click);

            ApplicationBarIconButton cancelBtn = new ApplicationBarIconButton(new Uri("/Assets/Icons/cancel1.png", UriKind.Relative));
            cancelBtn.Text = "cancel";
            ApplicationBar.Buttons.Add(cancelBtn);
            cancelBtn.Click += new EventHandler(CancelButton_Click);
        }

        /// <summary>
        /// Save app button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveButton_Click(object sender, EventArgs e)
        {
            NavigationService.GoBack();
        }

        /// <summary>
        /// Cancel app button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            NavigationService.GoBack();
        }

        /// <summary>
        /// Create the "exiting" effect
        /// </summary>
        private void SlideDownFadeOut()
        {
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideDownFadeOut;
            PhoneApplicationPage page = (PhoneApplicationPage)((PhoneApplicationFrame)Application.Current.RootVisual).Content;
            ITransition trans = slide.GetTransition(page);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        /// <summary>
        /// Create the full screen out effect
        /// </summary>
        private void FullScreenOut()
        {
            SwivelTransition swivel = new SwivelTransition();
            swivel.Mode = SwivelTransitionMode.FullScreenOut;
            PhoneApplicationPage page = (PhoneApplicationPage)((PhoneApplicationFrame)Application.Current.RootVisual).Content;
            ITransition trans = swivel.GetTransition(page);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            base.OnBackKeyPress(e);
            //SlideDownFadeOut();
        }


        private void TimeSelector_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            FullScreenOut();
        }


        private void DateSelector_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            FullScreenOut();
        }
		
		

    }
    
}