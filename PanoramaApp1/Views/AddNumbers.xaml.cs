﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Kino.IsolatedSettings;
using Kino.Binding;
using KinoGenerator;
using Kino.Data;
using System.Collections;
using System.Windows.Media;

namespace Kino
{
    public partial class AddNumbers : PhoneApplicationPage
    {
        // Holds the isolated storage kino settings
        private IsoKinoSettings isolatedKinoSettings;
        private readonly int _noOfAllowedNums = 12;
        

        public AddNumbers()
        {
            InitializeComponent();
            //SystemTray.SetIsVisible(this, false);
            // Show the progress of the system tray
            ProgressIndicator progress = new ProgressIndicator
            {
                IsVisible = true,
                IsIndeterminate = true,
                Text = "Loading..."
            };
            SystemTray.SetProgressIndicator(this, progress);

            // Initialize isolated settings
            isolatedKinoSettings = new IsoKinoSettings();

            AddNumbersSelector.IsSelectionEnabled = true;
            AddNumbersSelector.IsHitTestVisible = true;
            InitializeAppBarProperties();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.Back)
            {
                // Checl for unsaved numbers before exiting...
                if (this.AddNumbersSelector.IsSelectionEnabled)
                {
                    if (this.AddNumbersSelector.SelectedItems.Count != 0)
                    {
                        CustomMessageBox messageBox = new CustomMessageBox()
                        {
                            Caption = "Numbers not saved",
                            Message = "Save changes to phone?",
                            LeftButtonContent = "Yes",
                            RightButtonContent = "No",
                            IsFullScreen = false
                        };
                        //messageBox.SetValue(CustomMessageBox.CaptionProperty, 24);
                        messageBox.Dismissed += new EventHandler<DismissedEventArgs>(MessageBox_Dismissed);
                        messageBox.Show();                        
                    }
                }
            }              
        }

        /// <summary>
        /// Event handler for custom message box on back key press.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MessageBox_Dismissed(object sender, DismissedEventArgs e)
        {
            var cmb = sender as CustomMessageBox;
            switch (e.Result)
            {
                case CustomMessageBoxResult.LeftButton:
                    SaveChanges();
                    break;
                case CustomMessageBoxResult.RightButton:
                    break;

            }
        }

        private void InitializeAppBarProperties()
        {
            ApplicationBar = new ApplicationBar();
            ApplicationBar.Mode = ApplicationBarMode.Default;
            ApplicationBar.IsVisible = false;
        }

        private void CreateSaveButton()
        {
            ApplicationBarIconButton saveBtn = new ApplicationBarIconButton(new Uri("/Assets/Icons/save.png", UriKind.Relative));
            saveBtn.Text = "save";
            ApplicationBar.Buttons.Add(saveBtn);
            saveBtn.Click += new EventHandler(SaveButton_Click);
            saveBtn.IsEnabled = true;
        }

        private void UpdateApplicationBar(bool isSelected)
        {
            if (isSelected)
            {
                this.ApplicationBar.IsVisible = true;
                if(ApplicationBar.Buttons.Count == 0)
                {
                    CreateSaveButton();
                    CreateUnselectButton();
                }
            }
            else
            {                
                this.ApplicationBar.IsVisible = false;
                this.ApplicationBar.Buttons.Clear();
            }
        }

        private void CreateUnselectButton()
        {
            ApplicationBarIconButton cancelBtn = new ApplicationBarIconButton(new Uri("/Assets/Icons/cancel1.png", UriKind.Relative));
            cancelBtn.Text = "unselect all";
            ApplicationBar.Buttons.Add(cancelBtn);
            cancelBtn.IsEnabled = true;
            cancelBtn.Click += new EventHandler(CancelButton_Click);
        }


        /// <summary>
        /// Save app button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveButton_Click(object sender, EventArgs e)
        {
            // Save to iso storage
            var kinoSettings = isolatedKinoSettings.KinoSettings;
            var sequence = new SavedSequence();
            //var list = new List<Sequence>();
            var list = new HashSet<Sequence>();
            var itemSource = this.AddNumbersSelector.ItemsSource;
            foreach (var item in AddNumbersSelector.ItemsSource)
            {
                var container = AddNumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                if (container != null && container.IsSelected == true)
                {
                    var content = container.Content as AddedNumberItem;
                    if(content != null )
                        sequence.Add(content.Value);
                }
                
            }
            /*
            if (!this.isolatedKinoSettings.KinoSettings.SavedSequences.Exists(c => CompareSequence.IsEqual(c, sequence)))
            {
                list = this.isolatedKinoSettings.KinoSettings.SavedSequences;
                list.Add(sequence);
                isolatedKinoSettings.KinoSettings.SavedSequences = list;               

            }
            */
            if (!this.isolatedKinoSettings.KinoSettings.PersonalSequences.Contains(sequence))
            {
                list = isolatedKinoSettings.KinoSettings.PersonalSequences;
                list.Add(sequence);
                isolatedKinoSettings.KinoSettings.PersonalSequences = list;
            }

            // Unselect all selected items
            var items = AddNumbersSelector.ItemsSource;
            foreach (var item in items)
            {
                var container = AddNumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                if (container != null && container.IsSelected)
                    container.IsSelected = false;
            }

            MessageBoxResult result = MessageBox.Show("Numbers saved to phone.", "KinoPal message:", MessageBoxButton.OK);
            UpdateApplicationBar(false);
        }

        /// <summary>
        /// Updates isolated storage settings and returns back.
        /// </summary>
        /// <returns></returns>
        private bool SaveChanges()
        {
            // Save to iso storage
            var kinoSettings = isolatedKinoSettings.KinoSettings;
            var sequence = new SavedSequence();
            //var list = new List<Sequence>();
            var list = new HashSet<Sequence>();
            var itemSource = this.AddNumbersSelector.ItemsSource;
            foreach (var item in AddNumbersSelector.ItemsSource)
            {
                var container = AddNumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                if (container != null && container.IsSelected == true)
                {
                    var content = container.Content as AddedNumberItem;
                    if (content != null)
                        sequence.Add(content.Value);
                }
            }
            /*
            if (!this.isolatedKinoSettings.KinoSettings.SavedSequences.Exists(c => CompareSequence.IsEqual(c, sequence)))
            {
                list = this.isolatedKinoSettings.KinoSettings.SavedSequences;
                list.Add(sequence);
                isolatedKinoSettings.KinoSettings.SavedSequences = list;
            }
            */
            if (!this.isolatedKinoSettings.KinoSettings.PersonalSequences.Contains(sequence))
            {
                list = isolatedKinoSettings.KinoSettings.PersonalSequences;
                list.Add(sequence);
                isolatedKinoSettings.KinoSettings.PersonalSequences = list;
            }

            // Unselect all selected items
            var items = AddNumbersSelector.ItemsSource;
            foreach (var item in items)
            {
                var container = AddNumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                if (container != null && container.IsSelected)
                    container.IsSelected = false;
            }

            //MessageBoxResult result = MessageBox.Show("Numbers saved to phone.", "KinoPal message:", MessageBoxButton.OK);
            UpdateApplicationBar(false);
            MessageBoxResult result = MessageBox.Show("Numbers saved to phone.", "KinoPal message:", MessageBoxButton.OK);
            return true;
        }

        /// <summary>
        /// Cancel app button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, EventArgs e)
        {            
            // Unselect all selected items
            var items = AddNumbersSelector.ItemsSource;
            foreach (var item in items)
            {
                var container = AddNumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                if (container != null && container.IsSelected)
                    container.IsSelected = false;
            }
        }
        
        private void AddNumbersSelector_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeAppBarProperties();
        }


        private void AddNumbersSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //UndesiredNumbersSelector.IsSelectionEnabled = true;
            var selector = sender as LongListMultiSelector;
            var addedItems = e.AddedItems;
            var removedItems = e.RemovedItems;
            var originalSource = e.OriginalSource;
            var selectedItems = selector.SelectedItems;
            if(selectedItems != null)
            {
                if (selectedItems.Count == 0)
                    UpdateApplicationBar(false);
                else if(selectedItems.Count == 1)
                    UpdateApplicationBar(true);

                ProcessSelectedNumbers(selectedItems);
            }
        }

        /// <summary>
        /// Saves selected items to app data store
        /// </summary>
        private void ProcessSelectedNumbers(IList collection)
        {
            if (collection != null)// && collection.Count > 0)
            {
                // Selection overreached max limit
                if (collection.Count > _noOfAllowedNums)
                {                   
                    MessageBoxResult result = MessageBox.Show("Please select up to 12 numbers.", "Maximum number of selected items is overreached.", MessageBoxButton.OK);
                    if(result == MessageBoxResult.OK)
                    {
                        // Find excess and de-select it from list
                        var selectedItems = AddNumbersSelector.SelectedItems;
                        var itemsSource = AddNumbersSelector.ItemsSource;
                        var lastSelected = selectedItems[selectedItems.Count - 1] as AddedNumberItem;
                        var itemsSourceEnumerated = itemsSource as IEnumerable<AddedNumberItem>;
                        var enumeratedLastSelected = itemsSourceEnumerated.SingleOrDefault(c => c == lastSelected);
                        if (enumeratedLastSelected != null)
                        {
                            var container = AddNumbersSelector.ContainerFromItem(enumeratedLastSelected) as LongListMultiSelectorItem;
                            if (container != null && container.IsSelected)
                                container.IsSelected = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Event on pressing popup OK-close button
        /// Closes popup screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MaxReachedPopupOKButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            // Find excess and de-select it from list
            var selectedItems = AddNumbersSelector.SelectedItems;
            var itemsSource = AddNumbersSelector.ItemsSource;
            var lastSelected = selectedItems[selectedItems.Count - 1] as AddedNumberItem;
            var itemsSourceEnumerated = itemsSource as IEnumerable<AddedNumberItem>;
            var enumeratedLastSelected = itemsSourceEnumerated.SingleOrDefault(c => c == lastSelected);
            if (enumeratedLastSelected != null)
            {
                var container = AddNumbersSelector.ContainerFromItem(enumeratedLastSelected) as LongListMultiSelectorItem;
                if (container != null && container.IsSelected)
                    container.IsSelected = false;
            }

            // Close popup
            MaxSelectedReachedPopup.IsOpen = false;
            this.KinoPalPivot.IsLocked = false;
            this.KinoPalPivot.IsHitTestVisible = true;
            this.KinoPalPivot.ClearValue(OpacityMaskProperty);
        }

        /// <summary>
        /// Event after opening popup screen.
        /// Disables visibility of system tray, app bar, buttons and viewmodel's main grid hit test
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MaxSelectedReachedPopup_Opened(object sender, EventArgs e)
        {
            //UpdateApplicationBar(false);
        }

        /// <summary>
        /// Event after closing popup screen.
        /// Re-enables visibility of system tray, app bar, buttons and viewmodel's main grid hit test
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MaxSelectedReachedPopup_Closed(object sender, EventArgs e)
        {
            UpdateApplicationBar(true);
        }

        private void AddNumbersSelector_ItemRealized(object sender, ItemRealizationEventArgs e)
        {
            // disable progress bar when reached last item in ui
            if (this.AddNumbersSelector.ItemsSource.Count == 80)
            {
                SystemTray.SetProgressIndicator(this, null);
                SystemTray.SetIsVisible(this, false);
            }
        }
    }
}