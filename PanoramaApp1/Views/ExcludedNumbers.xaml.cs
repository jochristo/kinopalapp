﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Kino.Binding;
using Kino.Data;
using System.Collections.ObjectModel;
using System.Windows.Media;
using Kino.IsolatedSettings;
using System.Collections;
using Microsoft.Xna.Framework.GamerServices;

namespace Kino
{
    public partial class ExcludedNumbers : PhoneApplicationPage
    {
        private List<int> _Excluded;
        public List<int> Excluded
        {
            get { return _Excluded; }
            set { _Excluded = value; }
        }

        // Holds the isolated storage kino settings
        private IsoKinoSettings isolatedKinoSettings;
        //private bool CanNavigateBack = false;

        private bool hasSelectedItemsPropertyChanged; // checks if user has unselected all previously selected numbers at first page load
        private int OriginalSelectedItems;
        private int _MaxExcludedNumbersCount = 20;
        public ExcludedNumbers()
        {
            InitializeComponent();
            SystemTray.SetIsVisible(this, false);

            this.OriginalSelectedItems = 0;

            // Change background color of each pre-selected item on the numbers grid list            
            foreach (var item in ExcludedNumbersSelector.ItemsSource)
            {
                var i = (FavoritesGridItem)item;
                if (i.IsSelected)
                {
                    i.BackColor = (Brush)Application.Current.Resources["PhoneAccentBrush"];
                    OriginalSelectedItems++;
                }
                else
                    i.BackColor = (Brush)Application.Current.Resources["PhoneInactiveBrush"];
            }

            // Disable user interaction with numbers grid
            ExcludedNumbersSelector.IsSelectionEnabled = false;
            ExcludedNumbersSelector.IsHitTestVisible = false;

            // Initialize excluded list
            _Excluded = new List<int>();

            // Initialize isolated settings
            isolatedKinoSettings = new IsoKinoSettings();

            this.hasSelectedItemsPropertyChanged = false;
            
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            RenderExclusionsFromIsoSettings(); // Render isSelected property for each excluded numbers on grid            
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.Back)
            {
                // Ask to save changed in storage
                if (this.ExcludedNumbersSelector.IsSelectionEnabled)
                {
                    //if (this.ExcludedNumbersSelector.SelectedItems.Count != 0)
                    if (this.hasSelectedItemsPropertyChanged == true)
                    {
                        CustomMessageBox messageBox = new CustomMessageBox()
                        {
                            Caption = "Numbers not saved",
                            Message = "Save changes to phone?",
                            LeftButtonContent = "Yes",
                            RightButtonContent = "No"
                        };
                        messageBox.Dismissed += new EventHandler<DismissedEventArgs>(MessageBox_Dismissed);
                        messageBox.Show();
                    }
                    

                }
            }
        }       


        /// <summary>
        /// Event handler for custom message box on back key press.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MessageBox_Dismissed(object sender, DismissedEventArgs e)
        {
            var cmb = sender as CustomMessageBox;
            switch (e.Result)
            {
                case CustomMessageBoxResult.LeftButton:
                    SaveChanges();   
                    break;
                case CustomMessageBoxResult.RightButton:
                    break;
            }
        }

        private void InitializeAppBarProperties()
        {
            ApplicationBar = new ApplicationBar();
            ApplicationBar.Mode = ApplicationBarMode.Default;
            ApplicationBar.Opacity = 1.0;
            ApplicationBar.IsVisible = true;
            ApplicationBar.IsMenuEnabled = false;

            ApplicationBarIconButton button1 = new ApplicationBarIconButton();
            button1.IconUri = new Uri("/Assets/Icons/check.png", UriKind.Relative);
            button1.Text = "done";
            ApplicationBar.Buttons.Add(button1);
            button1.Click += new EventHandler(DoneButton_Click);
            button1.IsEnabled = false;

            ApplicationBarIconButton button2 = new ApplicationBarIconButton();
            button2.IconUri = new Uri("/Assets/Icons/cancel1.png", UriKind.Relative);
            button2.Text = "unselect all";
            ApplicationBar.Buttons.Add(button2);
            button2.Click += new EventHandler(CancelButton_Click);
            button2.IsEnabled = false;

            ApplicationBarIconButton button3 = new ApplicationBarIconButton();
            button3.IconUri = new Uri("/Assets/Icons/manage.png", UriKind.Relative);
            button3.Text = "select";
            ApplicationBar.Buttons.Add(button3);
            button3.Click += new EventHandler(SelectButton_Click);
            button3.IsEnabled = true;
        }

        #region Application Bar Initialization

        /// <summary>
        /// Initializes the application bar properties.
        /// </summary>
        private void InitializeApplicationBar()
        {
            ApplicationBar = new ApplicationBar();
            ApplicationBar.Mode = ApplicationBarMode.Default;
            ApplicationBar.IsVisible = true;
            CreateSelectButton();
        }

        /// <summary>
        /// Creates the save button.
        /// </summary>
        private void CreateSaveButton()
        {
            ApplicationBarIconButton saveBtn = new ApplicationBarIconButton(new Uri("/Assets/Icons/save.png", UriKind.Relative));
            saveBtn.Text = "save";
            ApplicationBar.Buttons.Add(saveBtn);
            saveBtn.Click += new EventHandler(DoneButton_Click);
            saveBtn.IsEnabled = true;
        }

        /// <summary>
        /// Creates the unselect button
        /// </summary>
        private void CreateUnselectAllButton()
        {
            ApplicationBarIconButton cancelBtn = new ApplicationBarIconButton(new Uri("/Assets/Icons/cancel1.png", UriKind.Relative));
            cancelBtn.Text = "unselect all";
            ApplicationBar.Buttons.Add(cancelBtn);
            cancelBtn.IsEnabled = true;
            cancelBtn.Click += new EventHandler(CancelButton_Click);
        }

        /// <summary>
        /// Creates the select button
        /// </summary>
        private void CreateSelectButton()
        {
            ApplicationBarIconButton select = new ApplicationBarIconButton(new Uri("/Assets/Icons/appbar.list.check.png", UriKind.Relative));
            select.Text = "select";
            ApplicationBar.Buttons.Add(select);
            select.IsEnabled = true;
            select.Click += new EventHandler(SelectButton_Click);
        }

        /// <summary>
        /// Updates the buttons on the bar accordingly.
        /// </summary>
        /// <param name="isSelectionMode"></param>
        private void UpdateApplicationBar(bool isSelectionMode, bool isItemSelected)
        {
            if (isSelectionMode) // selection mode
            {
                ApplicationBar.Buttons.Clear();
                CreateSaveButton();
                if (isItemSelected)
                {
                    CreateUnselectAllButton();
                }
            }
            else // no selection mode
            {
                this.ApplicationBar.Buttons.Clear();
                CreateSelectButton();
            }
        }

        #endregion Application Bar Initialization

        #region Bar Buttons Events

        private void DoneButton_Click(object sender, EventArgs e)
        {
            //UpdateDataTemplateBindingPropertiesOnDone();

            // Create the transition effect
            RotateTransition rotate = new RotateTransition();
            rotate.Mode = RotateTransitionMode.In180Clockwise;

            // Get kino settings key from iso storage if any, otherwise create it
            var kinoSettings = isolatedKinoSettings.KinoSettings;

            foreach (var item in ExcludedNumbersSelector.ItemsSource)
            {
                var i = (FavoritesGridItem)item;
                var container = ExcludedNumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;

                // Update binding value
                if (container.IsSelected)
                {
                    i.IsSelected = true;
                    i.BackColor = (Brush)Application.Current.Resources["PhoneAccentBrush"];

                    // add to iso storage kino settings list of excluded numbers
                    if (isolatedKinoSettings.KinoSettings.ExcludedNumbers != null)
                    {       
                        // Not contained in the list
                        if (!isolatedKinoSettings.KinoSettings.ExcludedNumbers.Contains(Convert.ToInt32(i.text)))
                        {                            
                            // Copy to new list and add item
                            // Reset the collection to propagate to the binding target
                            var source = isolatedKinoSettings.KinoSettings.ExcludedNumbers;
                            source.Add(Convert.ToInt32(i.text));
                            isolatedKinoSettings.KinoSettings.ExcludedNumbers = source;
                        }
                    }
                }
                else
                {
                    i.IsSelected = false;
                    i.BackColor = (Brush)Application.Current.Resources["PhoneInactiveBrush"];

                    // remove from iso storage kino settings list of excluded numbers
                    if (isolatedKinoSettings.KinoSettings.ExcludedNumbers != null)
                    {
                        if (isolatedKinoSettings.KinoSettings.ExcludedNumbers.Contains(Convert.ToInt32(i.text)))
                        {
                            // Copy to new list and remove item
                            // Reset the collection to propagate to the binding target
                            var source = isolatedKinoSettings.KinoSettings.ExcludedNumbers;
                            source.Remove(Convert.ToInt32(i.text));                            
                            isolatedKinoSettings.KinoSettings.ExcludedNumbers = source;
                        }
                    }
                }
                // Disable isSelected for rendering purposes prior to disabling selection mode
                container.IsSelected = false;

                // Make the transition effect
                //ITransition trans = rotate.GetTransition(container);
                //trans.Completed += delegate { trans.Stop(); };
                //trans.Begin();
            }
            // Disable selection mode and UI interaction
            ExcludedNumbersSelector.IsSelectionEnabled = false;
            ExcludedNumbersSelector.IsHitTestVisible = false;
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            RenderIsNotSelected();
        }

        private void SelectButton_Click(object sender, EventArgs e)
        {
            // Enable selection mode            
            ExcludedNumbersSelector.IsSelectionEnabled = true;
            ExcludedNumbersSelector.IsHitTestVisible = true;
            RenderIsSelected();            
        }

        #endregion Bar Buttons Events

        /// <summary>
        /// Processes app bar buttons and changes IsEnabled property on selector's selection changed event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExcludedNumbersSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.hasSelectedItemsPropertyChanged = false;
            var addedItems = e.AddedItems;
            var selectedItems = ExcludedNumbersSelector.SelectedItems;
            if (selectedItems.Count == 0)
                UpdateApplicationBar(true, false);
            else if (selectedItems.Count == 1 && e.RemovedItems.Count == 0)
                CreateUnselectAllButton();

            var isItemSelected = ((ExcludedNumbersSelector.SelectedItems != null) && (ExcludedNumbersSelector.SelectedItems.Count > 0));
            if (isItemSelected)
            {
                // Allow maximum reasonable number of excluded(non-favorite) number selection of _MaxExcludedNumbersCount
                var count = ExcludedNumbersSelector.SelectedItems.Count;
                var result = ProcessSelectedNumbers(selectedItems, _MaxExcludedNumbersCount);
                this.OriginalSelectedItems = count; //save currently selected items count
            }
            else
            {    
                UpdateApplicationBar(true, isItemSelected);
            }

            // check if excluded items list has changed to display save dialog on back key press            
            if (e.RemovedItems.Count > 0 || (e.RemovedItems.Count == 0 && e.AddedItems.Count > this.isolatedKinoSettings.KinoSettings.ExcludedCount))
            {
                this.hasSelectedItemsPropertyChanged = true;
            }
        }

        /// <summary>
        /// Updates application bar icon buttons IsEnabled properties and grid background properties
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExcludedNumbersSelector_IsSelectionEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {            
            var selector = sender as LongListMultiSelector;
            var isItemsSelected = selector.SelectedItems.Count > 0;
            if (ApplicationBar != null)
            {
                var buttons = ApplicationBar.Buttons;
                if ((bool)e.NewValue == true) // is enabled is true
                {
                    //UpdateApplicationBar((bool)e.NewValue, false);
                    ApplicationBar.Buttons.Clear();
                    CreateSaveButton();

                    //store currenly selected items count
                    //this.OriginalSelectedItems = this.ExcludedNumbersSelector.SelectedItems.Count;

                }
                else // is enabled is false
                {
                    UpdateApplicationBar((bool)e.NewValue, false);
                }
            }            
        }

        /// <summary>
        /// Iterates through  multi selector items and renders the IsSelected properties of the container item 
        /// according to the binding properties. Changes are also made to the background property of the data template item.
        /// </summary>
        private void RenderIsSelected()
        {
            // Create the transition effect
            RotateTransition rotate = new RotateTransition();
            rotate.Mode = RotateTransitionMode.In180Clockwise;
            var isSelectedAtLeastOnce = false;
            foreach (var item in ExcludedNumbersSelector.ItemsSource)
            {
                var i = (FavoritesGridItem)item;
                var container = ExcludedNumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                if (i.IsSelected)
                {
                    i.BackColor = (Brush)Application.Current.Resources["PhoneBackgroundBrush"];
                    container.IsSelected = true;
                    isSelectedAtLeastOnce = true;
                }
                else
                {
                    i.BackColor = (Brush)Application.Current.Resources["PhoneBackgroundBrush"];
                    container.IsSelected = false;
                }

                // Make the transition effect
                //ITransition trans = rotate.GetTransition(container);
                //trans.Completed += delegate { trans.Stop(); };
                //trans.Begin();
            }

            UpdateApplicationBar(true, isSelectedAtLeastOnce);
            /*
            foreach (var b in ApplicationBar.Buttons)
            {
                var button = b as ApplicationBarIconButton;
                if (button.Text == "done")
                    button.IsEnabled = true;
            }
            */
        }

        /// <summary>
        /// Iterates through  multi selector items and renders the IsSelected property of the container items 
        /// as false and changes the binding property value. Disables the app bar unselect all button.
        /// </summary>
        private void RenderIsNotSelected()
        {
            
            foreach (var item in ExcludedNumbersSelector.ItemsSource)
            {
                // Get grid item and container item
                var i = (FavoritesGridItem)item;
                var container = ExcludedNumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;

                // Update binding value
                if (container.IsSelected)
                    i.IsSelected = true;
                else
                    i.IsSelected = false;

                // Disable isSelected for rendering purposes prior to disabling selection mode
                container.IsSelected = false;
            }

            /*
            // disable unselect button
            foreach (var b in ApplicationBar.Buttons)
            {
                var button = b as ApplicationBarIconButton;
                if (button.Text == "unselect all")
                    button.IsEnabled = false;
            }
            */
        }

        /// <summary>
        /// Updates the binding properties of the data template of selected items on app bar "done" tap.
        /// Changes the background and IsSelected binding properties of data template items.
        /// </summary>
        private void UpdateDataTemplateBindingPropertiesOnDone()
        {
            var selectedItems = ExcludedNumbersSelector.SelectedItems;
            if (selectedItems != null && selectedItems.Count > 0)
            {
                foreach (var s in ExcludedNumbersSelector.SelectedItems)
                {
                    var selected = s as FavoritesGridItem;
                    var indexOfItemSource = ExcludedNumbersSelector.ItemsSource.IndexOf(selected);
                    if (indexOfItemSource >= -1)
                    {
                        var sourceItem = ExcludedNumbersSelector.ItemsSource[indexOfItemSource] as FavoritesGridItem;
                        var container = ExcludedNumbersSelector.ContainerFromItem(sourceItem) as LongListMultiSelectorItem;
                        if (sourceItem != null)
                        {
                            sourceItem.BackColor = (Brush)Application.Current.Resources["PhoneAccentBrush"];
                            sourceItem.IsSelected = true;
                        }
                    }
                }
            }
        }
        
        /// <summary>
        /// Adds excluded numbers from numbers grid to excluded numbers collection
        /// </summary>
        private void SetExcludedNumbers()
        {
            if (ExcludedNumbersSelector.ItemsSource != null)
            {
                // Empty excluded list first
                this.Excluded.Clear();

                foreach (var item in ExcludedNumbersSelector.ItemsSource)
                {
                    var i = (FavoritesGridItem)item;                   
                    // Update binding value
                    if (i.IsSelected)
                    {
                        this._Excluded.Add(Convert.ToInt32(i.text));
                    }
                }
            }
        }

        private void RenderExcludedNumbersFromState()
        {
            if (ExcludedNumbersSelector.ItemsSource != null)
            {
                foreach (var item in ExcludedNumbersSelector.ItemsSource)
                {
                    var i = (FavoritesGridItem)item;

                    // Update binding value
                    if (this.Excluded != null && this.Excluded.Contains(Convert.ToInt32(i.text)))
                    {
                        i.IsSelected = true;
                        i.BackColor = (Brush)Application.Current.Resources["PhoneAccentBrush"];
                    }
                }
            }
        }

        private void RenderExclusionsFromIsoSettings()
        {
            // Get kino settings key from iso storage if any, otherwise create it
            var kinoSettings = isolatedKinoSettings.KinoSettings;
            var excluded = new List<int>();

            // key was found, assign value to local excluded list
            if (kinoSettings != null)
            {
                if (kinoSettings.ExcludedNumbers != null && kinoSettings.ExcludedNumbers.Count > 0)
                    excluded = kinoSettings.ExcludedNumbers;
            }
            // key will be added with default (null) initialized value
            else
            {
                // list is empty, no excluded numbers defined yet
                isolatedKinoSettings.KinoSettings = new KinoSettings();
                isolatedKinoSettings.KinoSettings.ExcludedNumbers = new List<int>();
                excluded = isolatedKinoSettings.KinoSettings.ExcludedNumbers;
            }

            if (ExcludedNumbersSelector.ItemsSource != null)
            {
                foreach (var item in ExcludedNumbersSelector.ItemsSource)
                {
                    var i = (FavoritesGridItem)item;

                    // Update binding value
                    if (excluded != null && excluded.Contains(Convert.ToInt32(i.text)))
                    {
                        i.IsSelected = true;
                        i.BackColor = (Brush)Application.Current.Resources["PhoneAccentBrush"];
                    }
                }
            }
        }

        private void ExcludedNumbersSelector_Loaded(object sender, RoutedEventArgs e)
        {
            //InitializeAppBarProperties();
            InitializeApplicationBar();
        }

        /// <summary>
        /// Display a message if selected items exceed maximum number set.
        /// </summary>
        private bool ProcessSelectedNumbers(IList collection, int max)
        {
            if (collection != null)// && collection.Count > 0)
            {
                // Selection overreached max limit
                if (collection.Count > max)
                {
                    MessageBoxResult result = MessageBox.Show("Please select up to " +max+  " non-favorite numbers.", "Maximum number of selected items is overreached.", MessageBoxButton.OK);
                    if (result == MessageBoxResult.OK)
                    {
                        // Find excess and de-select it from list
                        var selectedItems = ExcludedNumbersSelector.SelectedItems;
                        var itemsSource = ExcludedNumbersSelector.ItemsSource;
                        var lastSelected = selectedItems[selectedItems.Count - 1] as FavoritesGridItem;
                        var itemsSourceEnumerated = itemsSource as IEnumerable<FavoritesGridItem>;
                        var enumeratedLastSelected = itemsSourceEnumerated.SingleOrDefault(c => c == lastSelected);
                        if (enumeratedLastSelected != null)
                        {
                            var container = ExcludedNumbersSelector.ContainerFromItem(enumeratedLastSelected) as LongListMultiSelectorItem;
                            if (container != null && container.IsSelected)
                                container.IsSelected = false;
                        }
                        return true;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Saves changes on back key press event.
        /// </summary>
        /// <returns></returns>
        private bool SaveChanges()
        {
            // Get kino settings key from iso storage if any, otherwise create it
            var kinoSettings = isolatedKinoSettings.KinoSettings;
            if (this.ExcludedNumbersSelector.IsSelectionEnabled)
            {
                foreach (var item in ExcludedNumbersSelector.ItemsSource)
                {
                    var i = (FavoritesGridItem)item;
                    var container = ExcludedNumbersSelector.ContainerFromItem(item) as LongListMultiSelectorItem;

                    // Update binding value
                    if (container.IsSelected)
                    {
                        i.IsSelected = true;
                        i.BackColor = (Brush)Application.Current.Resources["PhoneAccentBrush"];

                        // add to iso storage kino settings list of excluded numbers
                        if (isolatedKinoSettings.KinoSettings.ExcludedNumbers != null)
                        {
                            // Not contained in the list
                            if (!isolatedKinoSettings.KinoSettings.ExcludedNumbers.Contains(Convert.ToInt32(i.text)))
                            {
                                // Copy to new list and add item
                                // Reset the collection to propagate to the binding target
                                var source = isolatedKinoSettings.KinoSettings.ExcludedNumbers;
                                source.Add(Convert.ToInt32(i.text));
                                isolatedKinoSettings.KinoSettings.ExcludedNumbers = source;
                            }
                        }
                    }
                    else
                    {
                        i.IsSelected = false;
                        i.BackColor = (Brush)Application.Current.Resources["PhoneInactiveBrush"];

                        // remove from iso storage kino settings list of excluded numbers
                        if (isolatedKinoSettings.KinoSettings.ExcludedNumbers != null)
                        {
                            if (isolatedKinoSettings.KinoSettings.ExcludedNumbers.Contains(Convert.ToInt32(i.text)))
                            {
                                // Copy to new list and remove item
                                // Reset the collection to propagate to the binding target
                                var source = isolatedKinoSettings.KinoSettings.ExcludedNumbers;
                                source.Remove(Convert.ToInt32(i.text));
                                isolatedKinoSettings.KinoSettings.ExcludedNumbers = source;
                            }
                        }
                    }
                    // Disable isSelected for rendering purposes prior to disabling selection mode
                    container.IsSelected = false;

                    // Make the transition effect
                    //ITransition trans = rotate.GetTransition(container);
                    //trans.Completed += delegate { trans.Stop(); };
                    //trans.Begin();
                }
                // Disable selection mode and UI interaction
                //ExcludedNumbersSelector.IsSelectionEnabled = false;
                //ExcludedNumbersSelector.IsHitTestVisible = false;
                return true;
            }
            else
                return false;
        }
    }
}