﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Controls.Primitives;
using System.Threading;

namespace Kino.Views
{
    public partial class AddDrawEntry : PhoneApplicationPage
    {
        public AddDrawEntry()
        {
            // Initialize
            InitializeComponent();

            // Construct new app bar
            ApplicationBar = new ApplicationBar();
            this.InitializeAppBarProperties();

            // Hide numbers grid
            this.NumbersGrid.Visibility = System.Windows.Visibility.Collapsed;
        }
		
		private void InitializeAppBarProperties()
		{
            ApplicationBar.Mode = ApplicationBarMode.Minimized;
    		ApplicationBar.Opacity = 1.0; 
    		ApplicationBar.IsVisible = true;
    		ApplicationBar.IsMenuEnabled = false;
			
    		ApplicationBarIconButton button1 = new ApplicationBarIconButton();
            button1.IconUri = new Uri("/Assets/Icons/save.png", UriKind.Relative);
            button1.Text = "save";
            ApplicationBar.Buttons.Add(button1);
            button1.Click += new EventHandler(SaveDrawEntryButton_Click);
            
			ApplicationBarIconButton button2 = new ApplicationBarIconButton();    		
			button2.IconUri = new Uri("/Assets/Icons/cancel1.png", UriKind.Relative);    		
			button2.Text = "cancel";    		
			ApplicationBar.Buttons.Add(button2);
            button2.Click += new EventHandler(CancelButton_Click);
			
		}

        private void SaveDrawEntryButton_Click(object sender, EventArgs e)
        {
            // Navigate back to generator page
            NavigationService.GoBack();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            // Navigate back to generator page
            NavigationService.GoBack();
        }
        
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {             
            base.OnNavigatedTo(e);
            ElementSlideUpFadeIn((PhoneApplicationPage)this);
            //AnimateNumbersGridOnNavigatedTo();

            /*
            // Create the transition effect when loading this page
            SlideTransition transition = new SlideTransition();
            transition.Mode = SlideTransitionMode.SlideUpFadeIn;
            PhoneApplicationPage page = (PhoneApplicationPage)((PhoneApplicationFrame)Application.Current.RootVisual).Content;
            ITransition trans = transition.GetTransition(page);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();                     
             */
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            base.OnBackKeyPress(e);
            
            // Create the transition effect when leaving this page
            SlideDownFadeOut();
        }
        
        private void SlideDownFadeOut()
        {
            // Create the transition effect when leaving this page
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideDownFadeOut;
            PhoneApplicationPage page = (PhoneApplicationPage)((PhoneApplicationFrame)Application.Current.RootVisual).Content;
            ITransition trans = slide.GetTransition(page);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }     

        private void ElementSlideUpFadeIn(UIElement element)
        {
            // Create the transition effect when leaving this page
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideUpFadeIn;
            ITransition trans = slide.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
            
        }
        
        private void AnimateNumbersGridOnNavigatedTo()
        {
            var grid = this.NumbersGrid;
            var grids = grid.Children.Where(c => c is Grid);        
            foreach(var g in grids)
            {
                var subGrid = g as Grid;
                var buttons = subGrid.Children.Where(c => c is ToggleButton);
                foreach(var b in buttons)
                {
                    var button = b as ToggleButton;
                    button.Visibility = System.Windows.Visibility.Visible;
                    ElementSlideUpFadeIn(button);
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/WinningNumbers.xaml", UriKind.Relative));
        }

    }
}