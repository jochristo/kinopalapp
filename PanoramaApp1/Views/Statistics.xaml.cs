﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Kino.Binding;
using Kino.IsolatedSettings;
using System.Windows.Media;
using System.Collections.ObjectModel;

namespace Kino
{
    public partial class Statistics : PhoneApplicationPage
    {
        /// <summary>
        /// Private fields
        /// </summary>
        private KinoGenerator.Kino kino;
        private string msg = string.Empty;
        private KinoSettings _KinoSettings;
        // Holds the isolated storage kino settings
        private IsoKinoSettings isolatedKinoSettings;
        public KinoSettings KinoSettings
        {
            get { return _KinoSettings; }
            set { _KinoSettings = value; }
        }

        #region ApplicationBar Menu Items
        //private ApplicationBarMenuItem saveNumsMenuItem = new ApplicationBarMenuItem();
        //private ApplicationBarMenuItem viewStoredNumsMenuItem = new ApplicationBarMenuItem();
        private ApplicationBarMenuItem SortByNumberMenuItem = new ApplicationBarMenuItem();
        private ApplicationBarMenuItem SortByFrequencyMenuItem = new ApplicationBarMenuItem();
        #endregion

        private bool isSortAsc = true;
        private bool isSortByNumsAsc = true;

        /// <summary>
        /// Public constructor
        /// </summary>
        public Statistics()
        {
            InitializeComponent();

            // Show the progress of the system tray
            ProgressIndicator progress = new ProgressIndicator
            {
                IsVisible = true,
                IsIndeterminate = true,
                Text = "Loading..."
            };
            SystemTray.SetProgressIndicator(this, progress);

            //SystemTray.SetIsVisible(this, false);   // hide status bar            
            kino = new KinoGenerator.Kino();        // construct new kino object
            InitializeIsolatedKinoSettings();       // Initialize isolated storage kino settings            
            SetBindings();                          // Set UI control bindings to kino settings       

            InitializeAppBarMenuItems();
        }

        /// <summary>
        /// Initializes app's isolated storage kino settings.
        /// </summary>
        private void InitializeIsolatedKinoSettings()
        {
            isolatedKinoSettings = new IsoKinoSettings(); // Get kino settings key from iso storage if any, otherwise create it
            var kinoSettings = isolatedKinoSettings.KinoSettings;

            if (kinoSettings == null) // key was not found, create one
            {
                isolatedKinoSettings.KinoSettings = new KinoSettings();
            }
        }

        /// <summary>
        /// Sets UI control binding properties to KinoSettings
        /// </summary>
        private void SetBindings()
        {
            this.FrequencyNumbersList.DataContext = isolatedKinoSettings.KinoSettings;
            this.FrequencyNumbersList.SetBinding(LongListMultiSelector.ItemsSourceProperty, new System.Windows.Data.Binding()
            { Mode = System.Windows.Data.BindingMode.TwoWay, Path = new PropertyPath("ObservableStatisticalDataItems") });

            //Set binding to control to display the count of saved items
            this.SavedSequencesCounter.DataContext = isolatedKinoSettings.KinoSettings;
            this.SavedSequencesCounter.SetBinding(HyperlinkButton.ContentProperty, new System.Windows.Data.Binding() { Mode = System.Windows.Data.BindingMode.OneWay, Path = new PropertyPath("DrawsCount") });
        }

        /// <summary>
        /// Navigates to Statistics.xaml view.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DetailsButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //this.NavigationService.Navigate(new Uri("/ViewModels/ViewSavedNumbers.xaml", UriKind.Relative));
            this.NavigationService.Navigate(new Uri("/ViewModels/MySavedNumbers.xaml", UriKind.Relative));
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var targetUri = e.Uri;
            this.isolatedKinoSettings.KinoSettings.UpdateStatisticalData();            
            if(this.isolatedKinoSettings.KinoSettings.SavedSequencesCount == 0)
            {
                SortByFrequencyMenuItem.IsEnabled = false;
                SortByNumberMenuItem.IsEnabled = false;
            }
            else
            {
                SortByFrequencyMenuItem.IsEnabled = true;
                SortByNumberMenuItem.IsEnabled = true;
            }
        }

        private void FrequencyNumbersList_ItemRealized(object sender, ItemRealizationEventArgs e)
        {
            // disable progress bar when reached last item in ui
            if (this.FrequencyNumbersList.ItemsSource.Count == 80)
            {
                SystemTray.SetProgressIndicator(this, null);
                SystemTray.SetIsVisible(this, false);
            }
        }

        private void InitializeAppBarMenuItems()
        {
            this.ApplicationBar = new ApplicationBar();
            this.ApplicationBar.Mode = ApplicationBarMode.Minimized;
            this.ApplicationBar.IsVisible = true;
            SystemTray.IsVisible = true;

            SortByFrequencyMenuItem.Text = "sort by frequency";
            ApplicationBar.MenuItems.Add(SortByFrequencyMenuItem);
            SortByFrequencyMenuItem.Click += OnSortByFrequency_Click;

            SortByNumberMenuItem.Text = "sort by number";
            ApplicationBar.MenuItems.Add(SortByNumberMenuItem);
            SortByNumberMenuItem.Click += OnSortByNumber_Click;
            
        }

        private void OnSortByFrequency_Click(object sender, EventArgs e)
        {
            List<StatisticalDataEntry> orderedT = new List<StatisticalDataEntry>();
            if (this.isSortAsc)
            {
                orderedT = isolatedKinoSettings.KinoSettings.ObservableStatisticalDataItems.OrderByDescending(t => t.Frequency).ThenBy(t=>t.Value).ToList();
                this.isSortAsc = false;
            }
            else
            {
                orderedT = isolatedKinoSettings.KinoSettings.ObservableStatisticalDataItems.OrderBy(t => t.Frequency).ThenBy(t => t.Value).ToList();
                this.isSortAsc = true;
            }

            if (orderedT.Count > 0)
            {
                var ordered = new StatisticalDataItems(orderedT);
                this.FrequencyNumbersList.ItemsSource = ordered;
            }
        }

        private void OnSortByNumber_Click(object sender, EventArgs e)
        {
            List<StatisticalDataEntry> orderedT = new List<StatisticalDataEntry>();
            if (this.isSortByNumsAsc)
            {
                orderedT = isolatedKinoSettings.KinoSettings.ObservableStatisticalDataItems.OrderByDescending(t => t.Value).ToList();
                this.isSortByNumsAsc = false;
            }
            else
            {
                orderedT = isolatedKinoSettings.KinoSettings.ObservableStatisticalDataItems.OrderBy(t => t.Value).ToList();
                this.isSortByNumsAsc = true;
            }

            if (orderedT.Count > 0)
            {
                var ordered = new StatisticalDataItems(orderedT);
                this.FrequencyNumbersList.ItemsSource = ordered;
            }
        }
    }
}