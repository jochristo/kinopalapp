﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Kino.Data.Json;
using Kino.Transition;
using KinoGenerator;
using Windows.Networking.Connectivity;
using Kino.Data;
using KinoGenerator.Statistics;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Globalization;
using System.Windows.Media;
using Kino.Binding;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
using Kino.IsolatedSettings;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Json;
using System.Threading;
using System.Diagnostics;
using Kino.Exceptions;

namespace Kino.Views
{
    public partial class MyResults : PhoneApplicationPage
    {
        private IsoKinoSettings isolatedKinoSettings;
        private List<Draw> Draws;   // holds the results
        private List<string> Dates; // holds the string-based list of dates transferred with app's state.
        private HashSet<Sequence> Sequences; // Cache of downloaded Sequences
        private HashSet<DayDraw> DayDraws;
        private string LowDate;     // the date search criteria
        private string Hours;       // hours criteria
        private string Minutes;     // minutes criteria        
        private bool IsSingleDateSearchMode = false;

        private OrderingType frequencyOrderingType; // frequency ordering type
        private OrderingType skipsOrderingType; // skips ordering type
        private OrderingType numbersOrderingType; // numbers ordering type

        private Uri dark_ascIconUri;
        private Uri dark_descIconUri;
        private Uri light_ascIconUri;
        private Uri light_descIconUri;
        private Uri dark_sortIconUri;
        private ObservableCollection<StatisticalDataEntry> frequencySortedAsc; // holds the items in asceding order
        private ObservableCollection<StatisticalDataEntry> frequencySortedDesc; // holds the items in descending order
        private ObservableCollection<StatisticalDataEntry> skipsSortedAsc; // holds the items in asceding order
        private ObservableCollection<StatisticalDataEntry> skipsSortedDesc; // holds the items in descending order
        private ObservableCollection<StatisticalDataEntry> numbersSortedAsc; // holds the items in descending order
        private ObservableCollection<StatisticalDataEntry> numbersSortedDesc; // holds the items in descending order

        // incremental downloading/loading stuff
        double TotalBytes = 0;
        double Downloaded = 0;
        private List<Draw> TotalDownloadedDraws = new List<Draw>();
        private ObservableCollection<Draw> CurrentLoadedDraws = new ObservableCollection<Draw>(); // Holds the draws currently loaded for display
        private IncrementalLoader<Draw> IncrementalDrawLoader = new IncrementalLoader<Draw>(); // Incremental loader of downloaded draws
        private ObservableCollection<Draw> BoundDraws = new ObservableCollection<Draw>(); // Holds the draws data bind to itemsource property if LongListLSelector
        private DownloadedDrawsViewModel ddvm = new DownloadedDrawsViewModel();
        private int firstBatchcounter = 0;
        private int CurrentRealizedIndex = 0;
        private int StatisticsPivotItemSelectedCounter = 0;
        private int KinoBonusPivotItemSelectedCounter = 0;
        private const int _maxFirstBatchSize = 20;

        // Data loading identification
        public bool IsLoading = false;
        // Thread lock object
        private object o = new object();

        // pairs loading indicators and locking object
        private bool isPairsLoading = false;
        private object pobj = new object();
        // view model classes
        private PairsViewModel pvm;
        private TripletsViewModel tvm;
        private PairComparer pc;
        public Dictionary<Pair, int> dictionary;
        // cancellation tokens: downloaded sequences, pairs, triplets
        CancellationTokenSource pair_cts = new CancellationTokenSource();
        CancellationTokenSource triplet_cts = new CancellationTokenSource();
        CancellationTokenSource sequence_cts = new CancellationTokenSource();
        // view model class for downloaded draw sequences
        private SequencesViewModel<Draw> svm;
        private HashSet<Sequence> DownloadedSequences;
        private Collection<Draw> MyDrawCache;
        private Collection<Draw> StatsDraws;
        private List<int> BonusNumbers;
        private bool isSearchRangeTenDaySpan = true; // holds the span of days in date search range

        public MyResults()
        {
            /*
            ProgressIndicator progress = new ProgressIndicator
            {
                IsVisible = true,
                IsIndeterminate = true,
                Text = "Downloading results..."
            };
            SystemTray.SetProgressIndicator(this, progress); 
            */

            InitializeComponent();
            InitializeIsolatedKinoSettings();       // Initialize isolated storage kino settings             
            Draws = new List<Draw>(); //initialize draw list
            Sequences = new HashSet<Sequence>();
            DayDraws = new HashSet<DayDraw>();
            Dates = new List<string>();

            frequencyOrderingType = OrderingType.Undefined; // assume undefine at first run
            skipsOrderingType = OrderingType.Undefined;
            numbersOrderingType = OrderingType.Undefined;

            FrequencySortImageBtn.Visibility = System.Windows.Visibility.Collapsed;
            SkipsSortImageBtn.Visibility = System.Windows.Visibility.Collapsed;
            NumbersSortImageBtn.Visibility = System.Windows.Visibility.Collapsed;

            dark_ascIconUri = new Uri("/KinoPalApp;component/Assets/Icons/dark/appbar.arrow.up.png", UriKind.Relative);
            dark_descIconUri = new Uri("/KinoPalApp;component/Assets/Icons/dark/appbar.arrow.down.png", UriKind.Relative);
            light_ascIconUri = new Uri("/KinoPalApp;component/Assets/Icons/light/appbar.arrow.up.png", UriKind.Relative);
            light_descIconUri = new Uri("/KinoPalApp;component/Assets/Icons/light/appbar.arrow.down.png", UriKind.Relative);
            dark_sortIconUri = new Uri("/KinoPalApp;component/Assets/Icons/appbar.sort.png", UriKind.Relative);

            this.DataSelector.DataContext = this.ddvm;
            DataSelector.SetBinding(LongListMultiSelector.ItemsSourceProperty,
                new System.Windows.Data.Binding()
                {
                    Mode = System.Windows.Data.BindingMode.TwoWay,
                    Path = new PropertyPath("DownloadedDraws")
                });

            pvm = new PairsViewModel();
            tvm = new TripletsViewModel();

            this.PairsSelector.DataContext = this.pvm;
            PairsSelector.SetBinding(LongListSelector.ItemsSourceProperty,
                new System.Windows.Data.Binding()
                {
                    Mode = System.Windows.Data.BindingMode.TwoWay,
                    Path = new PropertyPath("Pairs")
                });

            this.TripletsSelector.DataContext = this.tvm;
            this.TripletsSelector.SetBinding(LongListSelector.ItemsSourceProperty,
                new System.Windows.Data.Binding()
                {
                    Mode = System.Windows.Data.BindingMode.TwoWay,
                    Path = new PropertyPath("Triplets")
                });

            this.pc = new PairComparer();
            this.dictionary = new Dictionary<Pair, int>(pc);
            this.PairLoader.Visibility = System.Windows.Visibility.Collapsed;
            this.ProgressHeading.Visibility = System.Windows.Visibility.Collapsed;
            this.TripletLoader.Visibility = System.Windows.Visibility.Collapsed;
            this.tripletsProgressHeading.Visibility = System.Windows.Visibility.Collapsed;
            this.DownloadCancelBtn.Visibility = Visibility.Collapsed;

            // data context view model class for Sequence long list selector
            this.svm = new SequencesViewModel<Draw>();
            DownloadedSequences = new HashSet<Sequence>();
            MyDrawCache = new Collection<Draw>();
            StatsDraws = new Collection<Draw>();

            this.DateRangeStackPanel.Visibility = Visibility.Collapsed; // hide date range information panel on startup in RESULTS pivot item
            this.BonusNumbers = new List<int>();
        }

        /// <summary>
        /// Initializes app's isolated storage kino settings.
        /// </summary>
        private void InitializeIsolatedKinoSettings()
        {
            isolatedKinoSettings = new IsoKinoSettings(); // Get kino settings key from iso storage if any, otherwise create it
            var kinoSettings = isolatedKinoSettings.KinoSettings;

            if (kinoSettings == null) // key was not found, create one
            {
                isolatedKinoSettings.KinoSettings = new KinoSettings();
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e.NavigationMode == NavigationMode.New) // new navigation from Search viewmodel
            {
                if (PhoneApplicationService.Current.State.ContainsKey("Dates"))
                    Dates = (List<string>)PhoneApplicationService.Current.State["Dates"];

                var isSingleDateTimeMode = PhoneApplicationService.Current.State.ContainsKey("IsSingleDateTimeMode");
                if (isSingleDateTimeMode && (bool)PhoneApplicationService.Current.State["IsSingleDateTimeMode"] == true) // single date+time search mode
                {
                    IsSingleDateSearchMode = true; //flag

                    this.LowDate = (string)PhoneApplicationService.Current.State["singleDate"];
                    this.lowDate.Text = (string)PhoneApplicationService.Current.State["singleDate"];
                    this.highDate.Text = (string)PhoneApplicationService.Current.State["singleDate"];

                    this.lowDate1.Text = (string)PhoneApplicationService.Current.State["singleDate"];
                    this.highDate1.Text = (string)PhoneApplicationService.Current.State["singleDate"];

                    if (PhoneApplicationService.Current.State.ContainsKey("Hours"))
                        this.Hours = (string)PhoneApplicationService.Current.State["Hours"];
                    if (PhoneApplicationService.Current.State.ContainsKey("Minutes"))
                        this.Minutes = (string)PhoneApplicationService.Current.State["Minutes"];

                    // disable pairs and triplets pivot item
                    this.PairsTriplets.IsEnabled = false;
                }
                else // dual date search mode
                {
                    if (PhoneApplicationService.Current.State.ContainsKey("LowDate"))
                    {
                        LowDate = (string)PhoneApplicationService.Current.State["LowDate"];
                        this.lowDate.Text = (string)PhoneApplicationService.Current.State["LowDate"];
                        this.lowDate1.Text = (string)PhoneApplicationService.Current.State["LowDate"];
                    }

                    if (PhoneApplicationService.Current.State.ContainsKey("toDate"))
                    {
                        this.highDate.Text = (string)PhoneApplicationService.Current.State["toDate"];
                        this.highDate1.Text = (string)PhoneApplicationService.Current.State["toDate"];
                    }
                }
            }

        }


        private void OnResultsPivot_Loaded(object sender, RoutedEventArgs e)
        {
            //this.MainPivot.IsEnabled = false;
            //this.MainPivot.IsLocked = true;
            //this.MainPivot.IsHitTestVisible = false;

            //this.progress.Visibility = System.Windows.Visibility.Collapsed;
            //this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;

            // fetch draws in JSON format
            if (IsSingleDateSearchMode)
            {
                FetchByDateAndTime();
            }
            else
                //FetchDraws();
                //FetchByDates();
                //FetchByDatesAsync();
                FetchByDatesTaskAsync();

        }

        /// <summary>
        /// Binds the property path for the preload of item source property of longlistselector of downloaded draws
        /// </summary>
        private void SetSequencesDataContext(Collection<Draw> source)
        {
            // set data context for long list selector and attach binding property
            if (source == null)
            {
                throw new ArgumentException("Draws source property is null");
            }

            var copy = from s in source select s;
            svm.Source = new ObservableCollection<Draw>(copy);
            this.DataSelector.DataContext = svm;
            this.DataSelector.SetBinding(LongListMultiSelector.ItemsSourceProperty,
                new System.Windows.Data.Binding()
                {
                    Mode = System.Windows.Data.BindingMode.TwoWay,
                    Path = new PropertyPath("Items")
                });

        }


        /// <summary>
        /// Fetches by date range.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void FetchByDates()
        {
            this.Sequences.Clear(); // Clear the Sequence cache
            Calendar calendar = new System.Globalization.GregorianCalendar();
            var draws = new List<Draw>(); //Holds the selector items source
            MessageBoxResult result = new MessageBoxResult(); // The error message

            //this.progress.Visibility = System.Windows.Visibility.Visible;
            //this.progressStatus.Visibility = System.Windows.Visibility.Visible;
            //this.resultsPivot.IsEnabled = false;           
            //this.progressStatus.Text = "Searching...";      

            /*
            // get content size
            foreach(var d in this.Dates)
            {
                WebClient wc = new WebClient();
                var uri = "http://applications.opap.gr/DrawsRestServices/kino/drawDate/" + d + ".json";
                var task = AsyncOpenRead(wc, new Uri(uri)); //download the json resourceAsyncDownloadString
                var Result = await task; //wait to complete the json download before parsing                
            }
            */

            //this.progress.Maximum = this.TotalBytes;

            // Fetch data
            foreach (var date in this.Dates)
            {
                string uri = "http://applications.opap.gr/DrawsRestServices/kino/drawDate/" + date + ".json";
                WebClient client = new WebClient();
                client.Headers["Accept"] = "application/json";
                //var task = JsonAsync.DownloadStringTaskAsync(client, new Uri(uri)); //download the json resource
                //string Result = await task; //wait to complete the json download before parsing

                var profile = NetworkInformation.GetInternetConnectionProfile();
                if (profile == null)
                {
                    this.progress.Visibility = System.Windows.Visibility.Collapsed;
                    this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                    SystemTray.SetProgressIndicator(this, null);
                    SystemTray.SetIsVisible(this, false);
                    this.resultsPivot.IsEnabled = true;
                    result = MessageBox.Show("Please check your data and/or WiFi connection.", "No internet connection available", MessageBoxButton.OK);
                    break;
                }
                else
                {

                    ProgressIndicator progress = new ProgressIndicator
                    {
                        IsVisible = true,
                        IsIndeterminate = true,
                        Text = "Downloading data..."
                    };
                    SystemTray.SetProgressIndicator(this, progress);

                    try
                    {
                        //client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(DownloadProgressCallback);                     
                        //var task = JsonAsync.DownloadStringTaskAsync(client, new Uri(uri)); //download the json resource
                        var task = AsyncDownloadString(client, new Uri(uri)); //download the json resourceAsyncDownloadString
                        string Result = await task; //wait to complete the json download before parsing                                              
                        var jsonObj = JObject.Parse(Result);
                        var deserialized = JsonConvert.DeserializeObject<RootObject>(Result);

                        if (Result == "{}")
                        {
                            this.progress.Visibility = System.Windows.Visibility.Collapsed;
                            this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                            SystemTray.SetProgressIndicator(this, null);
                            SystemTray.SetIsVisible(this, false);
                            this.resultsPivot.IsEnabled = true;
                            result = MessageBox.Show("Please check with OPAP web services.", "The date of the draw is invalid", MessageBoxButton.OK);
                            break;
                        }
                        else if (deserialized.draws.draw == null)
                        {
                            this.progress.Visibility = System.Windows.Visibility.Collapsed;
                            this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                            SystemTray.SetProgressIndicator(this, null);
                            SystemTray.SetIsVisible(this, false);
                            this.resultsPivot.IsEnabled = true;
                            result = MessageBox.Show("Please check with OPAP web services.", "The game given in the URL of web services is invalid", MessageBoxButton.OK);
                            break;
                        }
                        else if (deserialized.draws.draw != null && deserialized.draws.draw.Count == 0)
                        {
                            if (this.Sequences.Count == 0)  //in case already downloaded some draws
                            {
                                this.progress.Visibility = System.Windows.Visibility.Collapsed;
                                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                                SystemTray.SetProgressIndicator(this, null);
                                SystemTray.SetIsVisible(this, false);
                                this.resultsPivot.IsEnabled = true;
                                result = MessageBox.Show("Please select a different date.", "No draws found", MessageBoxButton.OK);
                                break;
                            }


                        }
                        else //if (result != MessageBoxResult.OK)
                        {
                            var draw = deserialized.draws.draw;
                            foreach (var d in draw)
                            {
                                var sequence = new Sequence();
                                foreach (var item in d.results)
                                {
                                    sequence.Add(item);
                                }
                                this.Sequences.Add(sequence); // add to cache
                            }

                            DayDraw currentDraw = new DayDraw();
                            currentDraw.Date = this.LowDate;
                            currentDraw.Draws = deserialized.draws;
                            var exists = DayDraws.Contains<DayDraw>(currentDraw); // check day's draws
                            if (exists == false)
                            {
                                var daysCurrentDraw = DayDraws.SingleOrDefault(d => d.Date == this.LowDate); //check for same day draws
                                if (currentDraw != null && DaydrawDateComparer.IsEqual(currentDraw, daysCurrentDraw))
                                {
                                    DayDraws.Remove(daysCurrentDraw); //remove previous day's draws
                                    DayDraws.Add(currentDraw); //update with latest downloads
                                }
                                else
                                    DayDraws.Add(currentDraw);
                            }

                            var list = deserialized.draws.draw;
                            //var collection = new ObservableCollection<Draw>();
                            foreach (var d in list)
                            {
                                draws.Add(d);
                                //collection.Add(d);
                                this.CurrentLoadedDraws.Add(d);

                                this.ddvm.DownloadedDraws.Add(d); // NEW
                            }

                            firstBatchcounter++; //indicates data has come in, ready to load first batch

                            // update selector data context as first day draw results come in
                            this.DataSelector.Visibility = System.Windows.Visibility.Visible;
                            //this.DataSelector.DataContext = collection;
                            //this.ddvm.DownloadedDraws = collection;

                            //if(firstBatchcounter == 1)
                            //    this.LoadFirstBatch();

                            // NEW for v.1.0.4
                            this.MainPivot.IsEnabled = true;
                            this.MainPivot.IsLocked = false;
                            this.MainPivot.IsHitTestVisible = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        var innerException = ex.InnerException;
                        if (innerException is WebException)
                        {
                            var we = innerException as WebException;
                            var message = string.Empty;
                            if (we.Response is HttpWebResponse)
                            {
                                switch (((HttpWebResponse)we.Response).StatusCode)
                                {
                                    case HttpStatusCode.NotFound:
                                        message = "The requested resource does not exist on the server";
                                        break;
                                    case HttpStatusCode.InternalServerError:
                                        message = "The server returned an internal error";
                                        break;
                                    case HttpStatusCode.BadRequest:
                                        message = "The request could not be understood by the server";
                                        break;
                                    default:
                                        message = "Something went wrong";
                                        break;
                                }
                                this.progress.Visibility = System.Windows.Visibility.Collapsed;
                                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                                this.resultsPivot.IsEnabled = true;
                                MessageBoxResult error = MessageBox.Show("Please check with OPAP web services.", message, MessageBoxButton.OK);
                                SystemTray.SetProgressIndicator(this, null);
                                SystemTray.SetIsVisible(this, false);
                            }
                        }
                    }
                }
            }

            // data downloaded, prepare item source
            if (result == MessageBoxResult.None) //ok
            {
                KinoTransition.SlideLeftFadeOut(this.progress);
                KinoTransition.SlideLeftFadeOut(this.progressStatus);
                this.resultsPivot.IsEnabled = true;

                // remove first'days draws according to last day's draw and time **** NEW STUFF  in 1.0.5***
                var copiedDraws = draws;
                var temp = new List<Draw>();
                var lastDayDrawTime = draws[draws.Count - 1].drawTime;
                var lastDraw = draws[draws.Count - 1];
                var firstDayDrawTime = draws[0].drawTime;
                var ci = new CultureInfo("el-GR");
                ci.DateTimeFormat = new DateTimeFormatInfo();
                ci.DateTimeFormat.FullDateTimePattern = "dd-MM-yyyyTHH:mm:ss";
                var firstDateTime = DateTime.ParseExact(firstDayDrawTime, "dd-MM-yyyyTHH:mm:ss", ci);
                var greek = new CultureInfo("el-GR").DateTimeFormat;
                DateTime parsedLastDraw = DateTime.ParseExact(lastDayDrawTime, "dd-MM-yyyyTHH:mm:ss", ci);
                foreach (var d in copiedDraws)
                {
                    var date = DateTime.ParseExact(d.drawTime, "dd-MM-yyyyTHH:mm:ss", ci);
                    if (firstDateTime.Date == date.Date)
                    {
                        if (JsonDrawTimeComparer.IsDrawTimeEarlier(d, lastDraw))
                        {
                            temp.Add(d);
                        }
                    }
                }

                ///// END - NEW STUFF in 1.0.5 => removed all first day draws where time is less than the last day's draw time.



                Draws = draws;
                KinoTransition.SlideUpFadeIn(this.DataSelector);
                this.progress.Visibility = System.Windows.Visibility.Collapsed;
                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                this.TotalDraws.Text = Draws.Count.ToString();
                this.MainPivot.IsEnabled = true;
                this.MainPivot.IsLocked = false;
                this.MainPivot.IsHitTestVisible = true;

                // get last draw's datetime
                if (draws.Count != 0)
                {
                    var last = draws[draws.Count - 1];
                    string inputFormat = "dd-MM-yyyyTHH:mm:ss";
                    CultureInfo provider = CultureInfo.CurrentCulture;
                    DateTime date = DateTime.ParseExact(last.drawTime, inputFormat, provider);
                    string displayTimeFormat = "HH:mm";
                    this.lastDrawTime.Text = date.ToString(displayTimeFormat);
                }
            }
            else
            {
                // new in 1.0.5
                this.NavigationService.GoBack();

                /*
                this.progress.Visibility = System.Windows.Visibility.Collapsed; // stop displaying progress bar
                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                this.resultsPivot.IsEnabled = true;

                SystemTray.SetProgressIndicator(this, null);
                SystemTray.SetIsVisible(this, false);
                */
            }
            SystemTray.SetProgressIndicator(this, null);
            SystemTray.SetIsVisible(this, false);
        }

        /// <summary>
        /// Download progress changed event to calculate total content length to download.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DownloadProgressCallback(object sender, DownloadProgressChangedEventArgs e)
        {
            var total = e.TotalBytesToReceive;
            var received = e.BytesReceived;
            var progPercent = e.ProgressPercentage;
            var client = sender as WebClient;
            var responseHeaders = client.ResponseHeaders;
            double content = 0;
            if (responseHeaders != null)
            {
                //content = Convert.ToDouble(client.ResponseHeaders["Content-Length"]);
                content = Convert.ToDouble(e.BytesReceived);
                TotalBytes += content;
                this.progress.Maximum = this.TotalBytes;
            }
        }

        /// <summary>
        /// Download progress changed event to update download progress indicator value.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DownloadProgressChangedEvent(object sender, DownloadProgressChangedEventArgs e)
        {
            var total = e.TotalBytesToReceive;
            var received = e.BytesReceived;
            var client = sender as WebClient;
            var responseHeaders = client.ResponseHeaders;
            if (responseHeaders != null)
            {
                Downloaded = Convert.ToDouble(e.BytesReceived);
                if (this.progressStatus.Text == "Searching...")
                    this.progressStatus.Text = "Downloading...";
            }
            this.progress.Value += Downloaded;
        }

        /// <summary>
        /// Downloads the resource at the specified Uri and with given WebClient as an synchronous operation using a task object.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="uri"></param>
        /// <returns></returns>
        public async Task<string> AsyncDownloadString(WebClient client, Uri uri)
        {
            var tcs = new TaskCompletionSource<string>();

            client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(DownloadProgressChangedEvent);
            client.DownloadStringCompleted += (o, e) =>
            {
                if (e.Cancelled)
                    tcs.SetCanceled();
                else
                {
                    try
                    {
                        tcs.SetResult(e.Result);
                    }
                    catch (System.Exception ex)
                    {
                        tcs.SetException(ex);
                    }
                }
            };

            client.DownloadStringAsync(uri);
            return await tcs.Task;
        }

        public async Task<string> AsyncOpenRead(WebClient client, Uri uri)
        {
            var tcs = new TaskCompletionSource<string>();

            client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(DownloadProgressCallback);
            client.OpenReadCompleted += (o, e) =>
            {
                if (e.Cancelled)
                    tcs.SetCanceled();
                else
                {
                    try
                    {
                        tcs.SetResult(e.Result.ToString());
                    }
                    catch (System.Exception ex)
                    {
                        tcs.SetException(ex);
                    }
                }
            };

            client.OpenReadAsync(uri);
            return await tcs.Task;
        }

        private void LoadDownloadedData(string sourceUrl)
        {
            //isCurrentlyLoading = true;            
            WebClient client = new WebClient();
            client.DownloadStringCompleted += WebClient_DownloadStringCompleted;
            client.DownloadStringAsync(new Uri(sourceUrl));

        }

        void WebClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            using (var reader = new MemoryStream(Encoding.Unicode.GetBytes(e.Result)))
            {
                var ser = new DataContractJsonSerializer(typeof(RootObject));
                RootObject obj = (RootObject)ser.ReadObject(reader);
                if (obj != null)
                {
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        foreach (var draw in obj.draws.draw)
                        {
                            CurrentLoadedDraws.Add(draw);
                        }
                        //isCurrentlyLoading = false;
                    });
                }
            }
        }


        /// <summary>
        /// Fetches by date range.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void FetchDraws()
        {
            this.Sequences.Clear(); // Clear the Sequence cache
            Calendar calendar = new System.Globalization.GregorianCalendar();
            var draws = new List<Draw>(); //Holds the selector items source
            MessageBoxResult result = new MessageBoxResult(); // The error message

            this.progress.Visibility = System.Windows.Visibility.Visible;
            this.progressStatus.Visibility = System.Windows.Visibility.Visible;
            this.resultsPivot.IsEnabled = false;
            this.progressStatus.Text = "Searching...";

            // get content size
            foreach (var d in this.Dates)
            {
                WebClient wc = new WebClient();
                var uri = "http://applications.opap.gr/DrawsRestServices/kino/drawDate/" + d + ".json";
                var task = AsyncOpenRead(wc, new Uri(uri)); //download the json resourceAsyncDownloadString
                var Result = await task; //wait to complete the json download before parsing                
            }

            // Fetch data
            foreach (var date in this.Dates)
            {
                string uri = "http://applications.opap.gr/DrawsRestServices/kino/drawDate/" + date + ".json";
                WebClient client = new WebClient();
                client.Headers["Accept"] = "application/json";
                var profile = NetworkInformation.GetInternetConnectionProfile();
                if (profile == null)
                {
                    this.progress.Visibility = System.Windows.Visibility.Collapsed;
                    this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                    this.resultsPivot.IsEnabled = true;
                    result = MessageBox.Show("Please check your data and/or WiFi connection.", "No internet connection available", MessageBoxButton.OK);
                    break;
                }
                else
                {
                    try
                    {

                        //client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(DownloadProgressCallback);                     
                        //var task = JsonAsync.DownloadStringTaskAsync(client, new Uri(uri)); //download the json resource
                        var task = AsyncDownloadString(client, new Uri(uri)); //download the json resourceAsyncDownloadString
                        string Result = await task; //wait to complete the json download before parsing                                              
                        var jsonObj = JObject.Parse(Result);
                        var deserialized = JsonConvert.DeserializeObject<RootObject>(Result);

                        LoadDownloadedData(uri);
                        this.DataSelector.Visibility = System.Windows.Visibility.Visible;
                        this.DataSelector.DataContext = this.CurrentLoadedDraws;

                        /*
                        if (Result == "{}")
                        {
                            this.progress.Visibility = System.Windows.Visibility.Collapsed;
                            this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                            this.resultsPivot.IsEnabled = true;
                            result = MessageBox.Show("Please check with OPAP web services.", "The date of the draw is invalid", MessageBoxButton.OK);
                            break;
                        }
                        else if (deserialized.draws.draw == null)
                        {
                            this.progress.Visibility = System.Windows.Visibility.Collapsed;
                            this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                            this.resultsPivot.IsEnabled = true;
                            result = MessageBox.Show("Please check with OPAP web services.", "The game given in the URL of web services is invalid", MessageBoxButton.OK);
                            break;
                        }
                        else if (deserialized.draws.draw != null && deserialized.draws.draw.Count == 0)
                        {
                            if (this.Sequences.Count == 0)  //in case already downloaded some draws
                            {
                                this.progress.Visibility = System.Windows.Visibility.Collapsed;
                                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                                this.resultsPivot.IsEnabled = true;
                                result = MessageBox.Show("Please select a different date.", "No draws found", MessageBoxButton.OK);
                                break;
                            }
                        }
                        else //if (result != MessageBoxResult.OK)
                        {
                            var draw = deserialized.draws.draw;
                            foreach (var d in draw)
                            {
                                var sequence = new Sequence();
                                foreach (var item in d.results)
                                {
                                    sequence.Add(item);
                                }
                                this.Sequences.Add(sequence); // add to cache
                            }

                            DayDraw currentDraw = new DayDraw();
                            currentDraw.Date = this.LowDate;
                            currentDraw.Draws = deserialized.draws;
                            var exists = DayDraws.Contains<DayDraw>(currentDraw); // check day's draws
                            if (exists == false)
                            {
                                var daysCurrentDraw = DayDraws.SingleOrDefault(d => d.Date == this.LowDate); //check for same day draws
                                if (currentDraw != null && DaydrawDateComparer.IsEqual(currentDraw, daysCurrentDraw))
                                {
                                    DayDraws.Remove(daysCurrentDraw); //remove previous day's draws
                                    DayDraws.Add(currentDraw); //update with latest downloads
                                }
                                else
                                    DayDraws.Add(currentDraw);
                            }

                            var list = deserialized.draws.draw;
                            foreach (var d in list)
                            {
                                draws.Add(d);
                            }

                            // update selector data context as data comes in

                            this.DataSelector.Visibility = System.Windows.Visibility.Visible;
                            this.DataSelector.DataContext = list;
                        }
                        */
                    }
                    catch (Exception ex)
                    {
                        var innerException = ex.InnerException;
                        if (innerException is WebException)
                        {
                            var we = innerException as WebException;
                            var message = string.Empty;
                            if (we.Response is HttpWebResponse)
                            {
                                switch (((HttpWebResponse)we.Response).StatusCode)
                                {
                                    case HttpStatusCode.NotFound:
                                        message = "The requested resource does not exist on the server";
                                        break;
                                    case HttpStatusCode.InternalServerError:
                                        message = "The server returned an internal error";
                                        break;
                                    case HttpStatusCode.BadRequest:
                                        message = "The request could not be understood by the server";
                                        break;
                                    default:
                                        message = "Something went wrong";
                                        break;
                                }
                                this.progress.Visibility = System.Windows.Visibility.Collapsed;
                                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                                this.resultsPivot.IsEnabled = true;
                                MessageBoxResult error = MessageBox.Show("Please check with OPAP web services.", message, MessageBoxButton.OK);
                            }
                        }
                    }
                }
            }

            // data downloaded, prepare item source
            if (result == MessageBoxResult.None) //ok
            {
                KinoTransition.SlideLeftFadeOut(this.progress);
                KinoTransition.SlideLeftFadeOut(this.progressStatus);
                this.resultsPivot.IsEnabled = true;
                Draws = draws;
                //this.DataSelector.ItemsSource = Draws;
                //this.DataSelector.UpdateLayout();
                //this.DataSelector.Visibility = System.Windows.Visibility.Visible;
                //KinoTransition.SlideLeftFadeIn(this.DataSelector);
                this.progress.Visibility = System.Windows.Visibility.Collapsed;
                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                this.TotalDraws.Text = Draws.Count.ToString();


                var items = KinoSettings.GetNumberFrequencies(this.Sequences);
                numbersSortedAsc = new ObservableCollection<StatisticalDataEntry>(items.OrderBy(c => c.Value));
                numbersSortedDesc = new ObservableCollection<StatisticalDataEntry>(items.OrderByDescending(c => c.Value));
                frequencySortedAsc = new ObservableCollection<StatisticalDataEntry>(items.OrderBy(c => c.Frequency).ThenBy(c => c.Overdues));
                frequencySortedDesc = new ObservableCollection<StatisticalDataEntry>(items.OrderByDescending(c => c.Frequency).ThenByDescending(c => c.Overdues));
                skipsSortedAsc = new ObservableCollection<StatisticalDataEntry>(items.OrderBy(c => c.Overdues).ThenBy(c => c.Frequency));
                skipsSortedDesc = new ObservableCollection<StatisticalDataEntry>(items.OrderByDescending(c => c.Overdues).ThenByDescending(c => c.Frequency));
                this.FrequencyNumbersList.ItemsSource = items;


                this.MainPivot.IsEnabled = true;
                this.MainPivot.IsLocked = false;
                this.MainPivot.IsHitTestVisible = true;

                // get last draw's datetime
                if (draws.Count != 0)
                {
                    var last = draws[draws.Count - 1];
                    string inputFormat = "dd-MM-yyyyTHH:mm:ss";
                    CultureInfo provider = CultureInfo.CurrentCulture;
                    DateTime date = DateTime.ParseExact(last.drawTime, inputFormat, provider);
                    string displayTimeFormat = "HH:mm";
                    this.lastDrawTime.Text = date.ToString(displayTimeFormat);
                }
            }
            else
            {
                this.progress.Visibility = System.Windows.Visibility.Collapsed; // stop displaying progress bar
                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                this.resultsPivot.IsEnabled = true;
            }
        }

        /// <summary>
        /// Fetches by single date and time
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void FetchByDateAndTime()
        {
            //this.Sequences.Clear(); // Clear the Sequence cache
            this.DownloadedSequences.Clear();
            Calendar calendar = new System.Globalization.GregorianCalendar();
            var draws = new List<Draw>(); //Holds the selector items source
            MessageBoxResult result = new MessageBoxResult(); // The error message

            //this.progress.Visibility = System.Windows.Visibility.Visible;
            //this.progressStatus.Visibility = System.Windows.Visibility.Visible;
            this.resultsPivot.IsEnabled = false;
            //this.progressStatus.Text = "Searching...";

            // Fetch data
            foreach (var date in this.Dates)
            {
                string uri = "http://applications.opap.gr/DrawsRestServices/kino/drawDate/" + date + ".json";
                WebClient client = new WebClient();
                client.Headers["Accept"] = "application/json";
                //var task = JsonAsync.DownloadStringTaskAsync(client, new Uri(uri)); //download the json resource
                //string Result = await task; //wait to complete the json download before parsing

                var profile = NetworkInformation.GetInternetConnectionProfile();
                if (profile == null)
                {
                    this.progress.Visibility = System.Windows.Visibility.Collapsed;
                    this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                    SystemTray.SetProgressIndicator(this, null);
                    SystemTray.SetIsVisible(this, false);
                    this.resultsPivot.IsEnabled = true;
                    result = MessageBox.Show("Please check your data and/or WiFi connection.", "No internet connection available", MessageBoxButton.OK);
                    break;
                }
                else
                {
                    ProgressIndicator progress = new ProgressIndicator
                    {
                        IsVisible = true,
                        IsIndeterminate = true,
                        Text = "Downloading data..."
                    };
                    SystemTray.SetProgressIndicator(this, progress);

                    try
                    {
                        var task = JsonAsync.DownloadStringTaskAsync(client, new Uri(uri)); //download the json resource
                        string Result = await task; //wait to complete the json download before parsing
                        var jsonObj = JObject.Parse(Result);
                        var deserialized = JsonConvert.DeserializeObject<RootObject>(Result);
                        //MessageBoxResult result = new MessageBoxResult();

                        if (Result == "{}")
                        {
                            this.progress.Visibility = System.Windows.Visibility.Collapsed;
                            this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                            SystemTray.SetProgressIndicator(this, null);
                            SystemTray.SetIsVisible(this, false);
                            this.resultsPivot.IsEnabled = true;
                            result = MessageBox.Show("Please check with OPAP web services.", "The date of the draw is invalid", MessageBoxButton.OK);
                            break;
                        }
                        else if (deserialized.draws.draw == null)
                        {
                            this.progress.Visibility = System.Windows.Visibility.Collapsed;
                            this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                            SystemTray.SetProgressIndicator(this, null);
                            SystemTray.SetIsVisible(this, false);
                            this.resultsPivot.IsEnabled = true;
                            result = MessageBox.Show("Please check with OPAP web services.", "The game given in the URL of web services is invalid", MessageBoxButton.OK);
                            break;
                        }
                        else if (deserialized.draws.draw != null && deserialized.draws.draw.Count == 0)
                        {
                            //if (this.Sequences.Count == 0)  //in case already downloaded some draws
                            if (this.DownloadedSequences.Count == 0)  //in case already downloaded some draws
                            {
                                this.progress.Visibility = System.Windows.Visibility.Collapsed;
                                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                                SystemTray.SetProgressIndicator(this, null);
                                SystemTray.SetIsVisible(this, false);
                                this.resultsPivot.IsEnabled = true;
                                result = MessageBox.Show("Please select a different date.", "No draws found", MessageBoxButton.OK);
                                break;
                            }
                        }
                        else //if (result != MessageBoxResult.OK)
                        {
                            var draw = deserialized.draws.draw; // store draws

                            // single draw search mode, find match
                            if (PhoneApplicationService.Current.State.ContainsKey("singleDate"))
                            {
                                var matchedDraw = draw.SingleOrDefault(d => DrawUtilities.IsDrawTimeFound(d, this.Hours + ":" + this.Minutes));
                                if (matchedDraw != null) //draw time is found, stop the download
                                {
                                    draw.Clear();
                                    draw.Add(matchedDraw);
                                }
                                else
                                {
                                    draw.Clear(); // draw not found, clear list;                                   
                                }
                            }

                            foreach (var d in draw)
                            {
                                //var sequence = new Sequence();
                                var sequence = new DownloadedSequence();
                                foreach (var item in d.results)
                                {
                                    sequence.Add(item);
                                }
                                //this.Sequences.Add(sequence); // add to cache
                                this.DownloadedSequences.Add(sequence); // add to cache
                            }


                            DayDraw currentDraw = new DayDraw();
                            currentDraw.Date = this.LowDate;
                            currentDraw.Draws = deserialized.draws;
                            var exists = DayDraws.Contains<DayDraw>(currentDraw); // check day's draws
                            if (exists == false)
                            {
                                var daysCurrentDraw = DayDraws.SingleOrDefault(d => d.Date == this.LowDate); //check for same day draws
                                if (currentDraw != null && DaydrawDateComparer.IsEqual(currentDraw, daysCurrentDraw))
                                {
                                    DayDraws.Remove(daysCurrentDraw); //remove previous day's draws
                                    DayDraws.Add(currentDraw); //update with latest downloads
                                }
                                else
                                    DayDraws.Add(currentDraw);
                            }

                            var list = deserialized.draws.draw;
                            foreach (var d in list)
                            {
                                draws.Add(d); //add to draw items
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        var innerException = ex.InnerException;
                        if (innerException is WebException)
                        {
                            var we = innerException as WebException;
                            var message = string.Empty;
                            if (we.Response is HttpWebResponse)
                            {
                                switch (((HttpWebResponse)we.Response).StatusCode)
                                {
                                    case HttpStatusCode.NotFound:
                                        message = "The requested resource does not exist on the server";
                                        break;
                                    case HttpStatusCode.InternalServerError:
                                        message = "The server returned an internal error";
                                        break;
                                    case HttpStatusCode.BadRequest:
                                        message = "The request could not be understood by the server";
                                        break;
                                    default:
                                        message = "Something went wrong";
                                        break;
                                }
                                this.progress.Visibility = System.Windows.Visibility.Collapsed;
                                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                                this.resultsPivot.IsEnabled = true;
                                MessageBoxResult error = MessageBox.Show("Please check with OPAP web services.", message, MessageBoxButton.OK);
                                SystemTray.SetProgressIndicator(this, null);
                                SystemTray.SetIsVisible(this, false);
                            }
                        }
                        else if(innerException is System.Reflection.TargetInvocationException)
                        {
                            this.progress.Visibility = System.Windows.Visibility.Collapsed;
                            this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                            this.resultsPivot.IsEnabled = true;
                            MessageBoxResult mbr = MessageBox.Show("Please try again.", "Reading OPAP data failed.", MessageBoxButton.OK);
                            SystemTray.SetProgressIndicator(this, null);
                            SystemTray.SetIsVisible(this, false);


                        }
                    }
                }
            }

            // data downloaded, prepare item source
            if (result == MessageBoxResult.None) //ok
            {
                KinoTransition.SlideLeftFadeOut(this.progress);
                KinoTransition.SlideLeftFadeOut(this.progressStatus);
                this.resultsPivot.IsEnabled = true;
                Draws = draws;
                this.DataSelector.ItemsSource = Draws;
                this.DataSelector.UpdateLayout();
                this.DataSelector.Visibility = System.Windows.Visibility.Visible;
                KinoTransition.SlideLeftFadeIn(this.DataSelector);
                this.progress.Visibility = System.Windows.Visibility.Collapsed;
                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                this.TotalDraws.Text = Draws.Count.ToString();

                this.MainPivot.IsEnabled = true;
                this.MainPivot.IsLocked = false;
                this.MainPivot.IsHitTestVisible = true;

                // get last draw's datetime
                if (draws.Count != 0)
                {
                    var last = draws[draws.Count - 1];
                    string inputFormat = "dd-MM-yyyyTHH:mm:ss";
                    CultureInfo provider = CultureInfo.CurrentCulture;
                    DateTime lastDate = DateTime.ParseExact(last.drawTime, inputFormat, provider);
                    string displayTimeFormat = "HH:mm";

                    this.lastDrawTime.Text = lastDate.ToString(displayTimeFormat);
                    this.lastDrawTime1.Text = lastDate.ToString(displayTimeFormat);
                    var first = draws[0];
                    DateTime firstDate = DateTime.ParseExact(first.drawTime, inputFormat, provider);
                    this.firstDrawTime.Text = firstDate.ToString(displayTimeFormat);
                }
                else
                {
                    MessageBoxResult error = MessageBox.Show("Please try a different date and/or time.", "No draw found", MessageBoxButton.OK);
                    this.ResultMessage.Visibility = System.Windows.Visibility.Visible;
                    this.MainPivot.IsEnabled = false;
                    // new in 1.0.5
                    this.NavigationService.GoBack();
                }

                SystemTray.SetProgressIndicator(this, null);
                SystemTray.SetIsVisible(this, false);
            }
            else
            {
                // new in 1.0.5
                this.NavigationService.GoBack();
            }

            SystemTray.SetProgressIndicator(this, null);
            SystemTray.SetIsVisible(this, false);
        }

        #region OLD sort events

        private void FrequencySortBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            SkipsSortImageBtn.Visibility = System.Windows.Visibility.Collapsed; //disable skips sorting image
            FrequencySortImageBtn.Visibility = System.Windows.Visibility.Visible; //enable frequency sorting image
            NumbersSortImageBtn.Visibility = System.Windows.Visibility.Collapsed;
            KinoTransition.SwivelFullScreenIn(FrequencySortImageBtn);

            // order list based on the existing ordering type
            if (frequencyOrderingType == OrderingType.Undefined || frequencyOrderingType == OrderingType.Desc)
            {
                KinoTransition.SlideDownFadeOut(FrequencyNumbersList);
                FrequencyNumbersList.ItemsSource = frequencySortedAsc;
                FrequencyNumbersList.UpdateLayout();
                KinoTransition.SlideUpFadeIn(FrequencyNumbersList);
                frequencyOrderingType = OrderingType.Asc;
                SetSortingIcon(FrequencySortImageBtn, frequencyOrderingType);
            }
            else
            {
                KinoTransition.SlideDownFadeOut(FrequencyNumbersList);
                FrequencyNumbersList.ItemsSource = frequencySortedDesc;
                FrequencyNumbersList.UpdateLayout();
                KinoTransition.SlideUpFadeIn(FrequencyNumbersList);
                frequencyOrderingType = OrderingType.Desc;
                SetSortingIcon(FrequencySortImageBtn, frequencyOrderingType);
            }
        }

        private void SkipsSortBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {

            SkipsSortImageBtn.Visibility = System.Windows.Visibility.Visible; //enable skips sorting image
            FrequencySortImageBtn.Visibility = System.Windows.Visibility.Collapsed; //disable frequency sorting image
            NumbersSortImageBtn.Visibility = System.Windows.Visibility.Collapsed;
            KinoTransition.TurnstileBackwardIn(SkipsSortImageBtn);

            // order list based on the existing ordering type
            if (skipsOrderingType == OrderingType.Undefined || skipsOrderingType == OrderingType.Desc)
            {
                KinoTransition.SlideDownFadeOut(FrequencyNumbersList);
                FrequencyNumbersList.ItemsSource = skipsSortedAsc;
                FrequencyNumbersList.UpdateLayout();
                KinoTransition.SlideUpFadeIn(FrequencyNumbersList);
                skipsOrderingType = OrderingType.Asc;
                SetSortingIcon(SkipsSortImageBtn, skipsOrderingType);
            }
            else
            {
                KinoTransition.SlideDownFadeOut(FrequencyNumbersList);
                FrequencyNumbersList.ItemsSource = skipsSortedDesc;
                FrequencyNumbersList.UpdateLayout();
                KinoTransition.SlideUpFadeIn(FrequencyNumbersList);
                skipsOrderingType = OrderingType.Desc;
                SetSortingIcon(SkipsSortImageBtn, skipsOrderingType);
            }
        }

        private void NumbersBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NumbersSortImageBtn.Visibility = System.Windows.Visibility.Visible;
            SkipsSortImageBtn.Visibility = System.Windows.Visibility.Collapsed; //disable skips sorting image
            FrequencySortImageBtn.Visibility = System.Windows.Visibility.Collapsed; //enable frequency sorting image
            KinoTransition.TurnstileBackwardIn(NumbersSortImageBtn);

            // order list based on the existing ordering type
            if (numbersOrderingType == OrderingType.Undefined || numbersOrderingType == OrderingType.Desc)
            {
                KinoTransition.SlideDownFadeOut(FrequencyNumbersList);
                FrequencyNumbersList.ItemsSource = numbersSortedAsc;
                FrequencyNumbersList.UpdateLayout();
                KinoTransition.SlideUpFadeIn(FrequencyNumbersList);
                numbersOrderingType = OrderingType.Asc;
                SetSortingIcon(NumbersSortImageBtn, numbersOrderingType);
            }
            else
            {
                KinoTransition.SlideDownFadeOut(FrequencyNumbersList);
                FrequencyNumbersList.ItemsSource = numbersSortedDesc;
                FrequencyNumbersList.UpdateLayout();
                KinoTransition.SlideUpFadeIn(FrequencyNumbersList);
                numbersOrderingType = OrderingType.Desc;
                SetSortingIcon(NumbersSortImageBtn, numbersOrderingType);
            }
        }

        /// <summary>
        /// Changes button's background icon based on given ordering type
        /// </summary>
        private void SetSortingIcon(Button button, OrderingType orderingType)
        {
            if (button != null)
            {
                for (var k = 0; k < VisualTreeHelper.GetChildrenCount(button); k++)
                {
                    var grid = VisualTreeHelper.GetChild(button, k) as Grid;
                    if (grid != null)
                    {
                        var gridChildren = grid.Children;
                        if (gridChildren != null && gridChildren.Count == 1)
                        {
                            var buttonBorder = gridChildren[0] as Border;
                            //var imageBorder = buttonBorder.Child as Border;
                            var contentControl = buttonBorder.Child as ContentControl;
                            var image = contentControl.Content as Image;
                            Image img = new Image();
                            BitmapImage bm = new BitmapImage();
                            if (orderingType == OrderingType.Asc)
                                bm.UriSource = dark_ascIconUri;
                            else if (orderingType == OrderingType.Desc)
                                bm.UriSource = dark_descIconUri;

                            image.Source = bm;
                        }
                    }
                }
            }
        }

        #endregion OLD sort events

        /// <summary>
        /// Changes sort icon based on given ordering type
        /// </summary>
        private void SetIcon(Button button, OrderingType orderingType)
        {
            if (button != null)
            {
                var grid = VisualTreeHelper.GetChild(button, 0) as Grid;
                var border = VisualTreeHelper.GetChild(grid, 0) as Border;
                var stack = VisualTreeHelper.GetChild(border, 0) as StackPanel;
                border = VisualTreeHelper.GetChild(stack, 0) as Border;
                var image = VisualTreeHelper.GetChild(border, 0) as Image;
                Image img = new Image();
                BitmapImage bm = new BitmapImage();
                if (orderingType == OrderingType.Asc)
                    bm.UriSource = dark_ascIconUri;
                else if (orderingType == OrderingType.Desc)
                    bm.UriSource = dark_descIconUri;

                image.Source = bm;

            }
        }

        /// <summary>
        /// Changes sort icon based
        /// </summary>
        private void UnsetIcon(Button button)
        {
            if (button != null)
            {
                //try
                //{
                var grid = VisualTreeHelper.GetChild(button, 0) as Grid;
                var border = VisualTreeHelper.GetChild(grid, 0) as Border;
                var stack = VisualTreeHelper.GetChild(border, 0) as StackPanel;
                border = VisualTreeHelper.GetChild(stack, 0) as Border;
                var image = VisualTreeHelper.GetChild(border, 0) as Image;
                Image img = new Image();
                BitmapImage bm = new BitmapImage();
                bm.UriSource = dark_sortIconUri; // initial sort icon
                image.Source = bm;
                //}
                //catch (ArgumentOutOfRangeException are)
                //{

                //}
                //catch (InvalidOperationException ioe)
                //{

                //}
            }
        }

        #region sort events

        private void NumbersSort_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            // order list based on the existing ordering type
            if (numbersOrderingType == OrderingType.Undefined || numbersOrderingType == OrderingType.Desc)
            {
                //KinoTransition.SlideDownFadeOut(FrequencyNumbersList);
                FrequencyNumbersList.ItemsSource = numbersSortedAsc;
                FrequencyNumbersList.UpdateLayout();
                KinoTransition.SlideUpFadeIn(FrequencyNumbersList);
                numbersOrderingType = OrderingType.Asc;
                SetIcon(NumbersSort, numbersOrderingType);
            }
            else
            {
                //KinoTransition.SlideDownFadeOut(FrequencyNumbersList);
                FrequencyNumbersList.ItemsSource = numbersSortedDesc;
                FrequencyNumbersList.UpdateLayout();
                KinoTransition.SlideUpFadeIn(FrequencyNumbersList);
                numbersOrderingType = OrderingType.Desc;
                SetIcon(NumbersSort, numbersOrderingType);
            }

            // Reset icons for other sorting buttons
            UnsetIcon(FrequencySort);
            UnsetIcon(SkipsSort);

        }

        private void FrequencySort_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            // order list based on the existing ordering type
            if (numbersOrderingType == OrderingType.Undefined || numbersOrderingType == OrderingType.Desc)
            {
                //KinoTransition.SlideDownFadeOut(FrequencyNumbersList);
                FrequencyNumbersList.ItemsSource = frequencySortedAsc;
                FrequencyNumbersList.UpdateLayout();
                KinoTransition.SlideUpFadeIn(FrequencyNumbersList);
                numbersOrderingType = OrderingType.Asc;
                SetIcon(FrequencySort, numbersOrderingType);
            }
            else
            {
                //KinoTransition.SlideDownFadeOut(FrequencyNumbersList);
                FrequencyNumbersList.ItemsSource = frequencySortedDesc;
                FrequencyNumbersList.UpdateLayout();
                KinoTransition.SlideUpFadeIn(FrequencyNumbersList);
                numbersOrderingType = OrderingType.Desc;
                SetIcon(FrequencySort, numbersOrderingType);
            }

            // Reset icons for other sorting buttons
            UnsetIcon(SkipsSort);
            UnsetIcon(NumbersSort);
        }

        private void SkipsSort_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            // order list based on the existing ordering type
            if (numbersOrderingType == OrderingType.Undefined || numbersOrderingType == OrderingType.Desc)
            {
                //KinoTransition.SlideDownFadeOut(FrequencyNumbersList);
                FrequencyNumbersList.ItemsSource = skipsSortedAsc;
                FrequencyNumbersList.UpdateLayout();
                KinoTransition.SlideUpFadeIn(FrequencyNumbersList);
                numbersOrderingType = OrderingType.Asc;
                SetIcon(SkipsSort, numbersOrderingType);
            }
            else
            {
                //KinoTransition.SlideDownFadeOut(FrequencyNumbersList);
                FrequencyNumbersList.ItemsSource = skipsSortedDesc;
                FrequencyNumbersList.UpdateLayout();
                KinoTransition.SlideUpFadeIn(FrequencyNumbersList);
                numbersOrderingType = OrderingType.Desc;
                SetIcon(SkipsSort, numbersOrderingType);
            }

            // Reset icons for other sorting buttons
            UnsetIcon(FrequencySort);
            UnsetIcon(NumbersSort);
        }

        #endregion sort events

        private void SetNumberItemForeground()
        {
            //Change item template if in light phone theme
            if (((Application.Current.Resources["PhoneBackgroundBrush"] as SolidColorBrush).Color) == Colors.White)
            {
                var template = this.DataSelector.ItemTemplate as DataTemplate;
                if (template != null)
                {
                    var border = template.LoadContent() as Border;
                    var topStackpanel = border.Child as StackPanel;
                    var innerStackPanel = VisualTreeHelper.GetChild(topStackpanel, 1) as StackPanel;
                    //var children = VisualTreeHelper.GetChildrenCount(innerStackPanel);
                    var llm = VisualTreeHelper.GetChild(innerStackPanel, 1) as LongListMultiSelector;
                    var grid = llm.ItemTemplate.LoadContent() as Grid;
                    var textblock = VisualTreeHelper.GetChild(grid, 1) as TextBlock;
                    textblock.Foreground = (Brush)Application.Current.Resources["PhoneAccentBrush"];
                }
            }
        }


        private void DataSelector_ItemRealized(object sender, ItemRealizationEventArgs e)
        {

            // Because the event will be multiple threads to enter, so adding thread lock, code can only be a single thread to execute control below

            lock (o)
            {
                if (!IsLoading && !DataSelector.IsSelectionEnabled)
                {
                    if (e.ItemKind == LongListSelectorItemKind.Item)
                    {
                        if ((e.Container.Content as Draw).Equals(DataSelector.ItemsSource[DataSelector.ItemsSource.Count - 1]))
                        {
                            // Set IsLoading to true, in the loading process, ban repeatedly into the
                            IsLoading = true;

                            // Simulated background time-consuming tasks pull data of the scene
                            Task.Factory.StartNew(async () =>
                            {
                                await Task.Delay(1000);

                                // Call the UI threads add data
                                this.Dispatcher.BeginInvoke(() =>
                                {
                                    var draw = e.Container.Content as Draw;
                                    // load next batch
                                    //LoadNextBatch(draw);
                                    // Modify the state of loading
                                    IsLoading = false;
                                });
                            });
                        }
                    }
                }
            }


            if (this.ddvm.DownloadedDraws.Count == _maxFirstBatchSize)
            {
                SystemTray.SetProgressIndicator(this, null);
                SystemTray.SetIsVisible(this, false);
            }
        }

        private void DrawsSelector_ItemRealized(object sender, ItemRealizationEventArgs e)
        {
            // Because the event will be multiple threads to enter, so adding thread lock, code can only be a single thread to execute control below
            lock (o)
            {
                if (!IsLoading && !DataSelector.IsSelectionEnabled)
                {
                    if (e.ItemKind == LongListSelectorItemKind.Item)
                    {
                        if ((e.Container.Content as Draw).Equals(DataSelector.ItemsSource[DataSelector.ItemsSource.Count - 1]))
                        {
                            // Set IsLoading to true, in the loading process, ban repeatedly into the
                            IsLoading = true;

                            // Simulated background time-consuming tasks pull data of the scene
                            Task.Factory.StartNew(async () =>
                            {
                                await Task.Delay(0);

                                // Call the UI threads add data
                                this.Dispatcher.BeginInvoke(() =>
                                {
                                    // load next batch
                                    svm.LoadNextBatch(40);
                                    // Modify the state of loading
                                    IsLoading = false;
                                });
                            });
                        }
                    }
                }
            }
            // disable progress bar when reached last item in ui


            //if (this.svm.Items.Count == this.svm.Source.Count)
            if (this.svm.Items.Count > 0)
            {
                //SystemTray.SetProgressIndicator(this, null);
                //SystemTray.SetIsVisible(this, false);
                this.DateRangeStackPanel.Visibility = Visibility.Visible;
            }
            

        }

        private async Task LoadMoreDraws(Draw currentDraw)
        {
            if (currentDraw != null)
            {
                var index = this.CurrentLoadedDraws.IndexOf(currentDraw);
                if (this.CurrentLoadedDraws.Count - 3 > index)
                {
                    return;
                }
            }
            // Load next batch of draws from downloaded.
            this.CurrentLoadedDraws = await this.IncrementalDrawLoader.LoadNextBatch();

            foreach (var draw in this.CurrentLoadedDraws)
            {
                this.BoundDraws.Add(draw);
            }
        }

        /// <summary>
        /// Loads the first batch of downloaded draws of pre-defined number of items.
        /// </summary>
        /// <param name="incoming"></param>
        private void LoadFirstBatch()
        {

            var obsCol = new ObservableCollection<Draw>();
            var counter = this.firstBatchcounter;
            if (this.CurrentLoadedDraws.Count != 0)
            {
                var max = _maxFirstBatchSize - 1; //6;
                var index = 0;
                while (index <= max && index <= this.CurrentLoadedDraws.Count - 1)
                {
                    CurrentRealizedIndex = index;
                    obsCol.Add(CurrentLoadedDraws.ElementAt(index));
                    //this.ddvm.DownloadedDraws.Add(CurrentLoadedDraws.ElementAt(index));
                    index++;
                }

                this.ddvm.DownloadedDraws = obsCol;
            }
        }

        /// <summary>
        /// Loads the next batch of downloaded draws of pre-defined number of items.
        /// </summary>
        /// <param name="draw"></param>
        private void LoadNextBatch(Draw draw)
        {
            if (draw != null)
            {
                var obsCol = new ObservableCollection<Draw>();

                if (this.CurrentLoadedDraws.Count != 0)
                {
                    var index = CurrentRealizedIndex + 1; //update index from last batch index
                    var max = index + 30; //limit                    
                    while (index <= max && index <= this.CurrentLoadedDraws.Count - 1)
                    {
                        CurrentRealizedIndex = index;
                        this.ddvm.DownloadedDraws.Add(CurrentLoadedDraws.ElementAt(index));
                        index++;
                    }

                }
            }
        }

        /// <summary>
        /// Generates statistics data and updates longlistselector control item source property.
        /// </summary>
        private void GenerateStatistics()
        {
            //var items = KinoSettings.GetNumberFrequencies(this.Sequences);
            var items = KinoSettings.GetNumberFrequencies(this.DownloadedSequences);
            numbersSortedAsc = new ObservableCollection<StatisticalDataEntry>(items.OrderBy(c => c.Value));
            numbersSortedDesc = new ObservableCollection<StatisticalDataEntry>(items.OrderByDescending(c => c.Value));
            frequencySortedAsc = new ObservableCollection<StatisticalDataEntry>(items.OrderBy(c => c.Frequency).ThenBy(c => c.Overdues));
            frequencySortedDesc = new ObservableCollection<StatisticalDataEntry>(items.OrderByDescending(c => c.Frequency).ThenByDescending(c => c.Overdues));
            skipsSortedAsc = new ObservableCollection<StatisticalDataEntry>(items.OrderBy(c => c.Overdues).ThenBy(c => c.Frequency));
            skipsSortedDesc = new ObservableCollection<StatisticalDataEntry>(items.OrderByDescending(c => c.Overdues).ThenByDescending(c => c.Frequency));
            this.FrequencyNumbersList.ItemsSource = items;                       

            //GetPairsAndFrequencies(this.Sequences);
            //var pairs = KinoStatistics.Pairs(this.Sequences);
            //var triplets = KinoStatistics.Triplets(this.Sequences);
        }

        
        private static StatisticalDataItems BonusNumbersStatistics(ICollection<Draw> draws)
        {
            var sequences = new Collection<Sequence>();
            // var items = 
            // add bonus number to list
            foreach (var d in draws)
            {
                var results = d.ResultsItems;
                var sequence = new Sequence();                
                foreach (var n in results)
                {
                    if (n.IsBonusNumber)
                    {
                        sequence.Add(n.Value);
                        sequences.Add(sequence);                        
                    }
                }
            }
            return KinoSettings.GetNumFrequenciesFromCollection(sequences);
        }

        private void MainPivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var pivot = sender as Pivot;
            if (pivot.SelectedItem == this.StatsPivot)
            {
                StatisticsPivotItemSelectedCounter++;
                if (StatisticsPivotItemSelectedCounter == 1)
                {
                    //if (this.Sequences != null && this.Sequences.Count > 1)
                    //{
                    GenerateStatistics();
                    //}
                }
            }
            else if (pivot.SelectedItem == this.PairsTriplets)
            {
                
                if (StatisticsPivotItemSelectedCounter == 1)
                {
                    // set pairs view model source property value from downloaded sequences                                        
                    //pvm.Source = this.Sequences;
                    pvm.Source = this.DownloadedSequences;                    
                    //tvm.Source = this.Sequences;
                    tvm.Source = this.DownloadedSequences;
                    

                    // calculate maximum number of possible pairs-triplets within downloaded sequences
                    /*
                    pvm.MaxPairsCalc();
                    PairLoader.Minimum = 0.0;
                    PairLoader.Maximum = (long)pvm.TotalPairsCounter;
                    tvm.MaximumTripletsCalculation();
                    TripletLoader.Minimum = 0.0;
                    TripletLoader.Maximum = (long)tvm.TotalTripletsCounter;
                    */
                }
                
            }
            else if(pivot.SelectedItem == this.KinoBonus)
            {
                KinoBonusPivotItemSelectedCounter++;
                if (KinoBonusPivotItemSelectedCounter == 1)
                {
                    // calculate kino bonus stats and data bind item source of UI long list selectors
                    var bonusStats = BonusNumbersStatistics(this.MyDrawCache).ToList();
                    var topTenSkips = bonusStats.Where(t=>t.Overdues > 0).OrderByDescending(d => d.Overdues).Take(10).ToList();
                    var topTenFrequencies = bonusStats.Where(t => t.Frequency > 0).OrderByDescending(d => d.Frequency).Take(10).ToList();
                    this.TopBonusFrequenciesSelector.ItemsSource = topTenFrequencies;
                    this.TopBonusSkipsSelector.ItemsSource = topTenSkips;
                }
            }
        }


        private void CalcPairsFrequency(HashSet<Sequence> sequences)
        {

            Dictionary<int, Dictionary<int, int>> frequencies = new Dictionary<int, Dictionary<int, int>>(); // row, num->frequency
            var resultRow = 1;
            var fr = new Dictionary<int, List<int>>();
            foreach (var s in sequences)
            {
                var numbers = s.Numbers;
                var rowNumbers = new List<int>();
                rowNumbers = s.Numbers;
                if (!fr.ContainsKey(resultRow))
                {
                    fr.Add(resultRow, rowNumbers);
                }
                resultRow++;
            }

            var rowNumKeys = fr.Keys;
            var dictionary = new Dictionary<int, List<int>>();
            foreach (var row in rowNumKeys)
            {
                var list = fr[row];
                foreach (var n in list)
                {

                }

            }


        }

        private void GetPairsAndFrequencies(HashSet<Sequence> sequences)
        {
            var dictionary = new Dictionary<Sequence, int>(new SequenceComparer());

            //pairs stuff
            foreach (var s in sequences)
            {
                var row = 0;
                var numbers = s.Numbers;
                numbers.Sort();
                var count = numbers.Count();
                var limit = count - 2;
                do
                {
                    var secondItemPos = row + 1;
                    do
                    {
                        var firstItemPos = row;
                        //var firstItemMaxPos = numbers.Count - 1;
                        //var secondItemMaxPos = firstItemMaxPos + 1;
                        var firstItem = numbers.ElementAt(firstItemPos);
                        var secondItem = numbers.ElementAt(secondItemPos);
                        var sequence = new Sequence();
                        sequence.Numbers.Add(firstItem);
                        sequence.Numbers.Add(secondItem);

                        if (dictionary.ContainsKey(sequence))
                        {
                            dictionary[sequence]++;
                        }
                        else
                        {
                            dictionary.Add(sequence, 1);
                        }

                        secondItemPos++;
                    }
                    while (secondItemPos < count && numbers != null);
                    secondItemPos = 0;
                    row++;

                } while (row < limit && numbers != null);
            }
        }
        
        private void PairsSelector_ItemRealized(object sender, ItemRealizationEventArgs e)
        {
            // Because the event will be multiple threads to enter, so adding thread lock, code can only be a single thread to execute control below
            //var selector = sender as LongListMultiSelector;

            lock (pobj)
            {
                if (!isPairsLoading)
                {
                    if (e.ItemKind == LongListSelectorItemKind.Item)
                    {
                        if ((e.Container.Content as Pair).Equals(PairsSelector.ItemsSource[PairsSelector.ItemsSource.Count - 1])) // was - 1 
                        {
                            // Set IsLoading to true, in the loading process, ban repeatedly into the
                            //isPairsLoading = true;

                            // Simulated background time-consuming tasks pull data of the scene
                            Task.Factory.StartNew(async () =>
                            {
                                await Task.Delay(3000);

                                // Call the UI threads add data
                                this.Dispatcher.BeginInvoke(() =>
                                {
                                    /*
                                    //this.PairLoader.Value++;
                                    var count = this.pvm.FuckingPairs.Count;
                                    //var frequency = 0;
                                    for (int i = 2; i < count; i++)
                                    {

                                        //Pair pair = new Pair() { Frequency = i, NumberOne = 23, NumberTwo = 32 };
                                        this.pvm.Pairs.Add(this.pvm.FuckingPairs[i]);
                                        //this.pvm.MyPairs.Add(pair);
                                        //frequency++;

                                    }
                                    */

                                    //isPairsLoading = true;
                                });
                            });
                        }
                    }
                }

            }



        }
                
        private void calcPairsBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {

        }

        private void calcPairsBtn_Click(object sender, RoutedEventArgs e)
        {
            // warn the user that if search range exceeds 10 days, only 10 days span will be calculcated
            if (this.isSearchRangeTenDaySpan == false)
            {
                MessageBoxResult mbr = new MessageBoxResult();
                mbr = MessageBox.Show("", "Only pairs of ten day span will be calculated.", MessageBoxButton.OK);
            }

            pvm.MaxPairsCalc(); // calculate maximum pairs from sequences
            var maximumPairs = (double)pvm.TotalPairsCounter;
            PairLoader.Maximum = maximumPairs;
            ProgressIndicator pairsCalcProgress = new ProgressIndicator
            {
                IsVisible = true,
                IsIndeterminate = true,
                Text = "Calculating pairs, please wait..."
            };

            this.PairLoader.IsIndeterminate = true;
            this.PairLoader.Visibility = System.Windows.Visibility.Visible;
            this.cancelBtn.Visibility = System.Windows.Visibility.Visible;
            this.calcPairsBtn.Visibility = System.Windows.Visibility.Collapsed;
            this.MainStackPanel.Visibility = System.Windows.Visibility.Collapsed;
            this.progressText.Visibility = System.Windows.Visibility.Visible;
            //this.ProgressHeading.Visibility = System.Windows.Visibility.Visible;
            this.MainPivot.Foreground = (Brush)Application.Current.Resources["PhoneInactiveBrush"];
            this.MyPairsPivot.Foreground = (Brush)Application.Current.Resources["PhoneInactiveBrush"];
            this.MainPivot.IsLocked = true;

            this.pair_cts = new CancellationTokenSource();
            Task.Factory.StartNew(() =>
            {
                this.Dispatcher.BeginInvoke(async () =>
                {
                    Stopwatch sw = Stopwatch.StartNew();

                    //store progress report counter and update UI's progress bar value
                    var progress = new Progress<int>();
                    progress.ProgressChanged += (o, report) =>
                    {
                        this.PairLoader.Value = (double)report;
                        var percentage = Math.Round((this.PairLoader.Value / maximumPairs) * 100);
                        this.progressText.Text = "Progress: " + percentage.ToString() + "% done"; ;
                    };

                    try
                    {
                        var set = await PairsWorkerAsync(progress, pair_cts.Token);
                        var ordered = set.OrderByDescending(t => t.Frequency).ToList<Pair>();
                        var take10 = (IEnumerable<Pair>)this.pvm.FuckingPairs.OrderByDescending(t => t.Frequency).Take(10);
                        this.pvm.Pairs.Clear();
                        KinoTransition.SlideUpFadeIn(this.PairsSelector);
                        foreach (var p in take10)
                        {
                            this.pvm.Pairs.Add(p);
                        }
                        this.calcPairsBtn.Visibility = System.Windows.Visibility.Collapsed;
                        this.MainStackPanel.Visibility = System.Windows.Visibility.Visible;
                        this.progressText.Visibility = System.Windows.Visibility.Collapsed;
                        this.ProgressHeading.Visibility = System.Windows.Visibility.Collapsed;
                        this.MainPivot.IsLocked = false;
                    }
                    catch (OperationCanceledException ex)
                    {

                        this.pvm.FuckingPairs.Clear();
                        var token = ex.CancellationToken;
                        this.calcPairsBtn.Visibility = System.Windows.Visibility.Visible;
                        this.progress.Value = 0.0;
                        //this.progressText.Text = "Calculation canceled";
                        this.progressText.Visibility = System.Windows.Visibility.Collapsed;
                        this.ProgressHeading.Visibility = System.Windows.Visibility.Collapsed;
                        this.MainStackPanel.Visibility = System.Windows.Visibility.Collapsed;
                        //this.progressText.Visibility = System.Windows.Visibility.Visible;
                        MessageBoxResult result = new MessageBoxResult();
                        result = MessageBox.Show("", "Generation of pairs was canceled.", MessageBoxButton.OK);
                    }
                    finally
                    {
                        pair_cts.Dispose();
                    }
                    this.PairLoader.Visibility = System.Windows.Visibility.Collapsed;
                    this.cancelBtn.Visibility = System.Windows.Visibility.Collapsed;
                    this.MainPivot.ClearValue(ForegroundProperty);
                    this.MyPairsPivot.ClearValue(ForegroundProperty);
                    this.MyPairsPivot.IsEnabled = true;
                    SystemTray.SetProgressIndicator(this, null);
                    SystemTray.IsVisible = false;

                    sw.Stop();
                });
            }, pair_cts.Token);
        }

        /// <summary>Returns enumerated collection of the calculated pairs and its frequencies using asynchronous System.Threading.Task object.
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Pair>> GetMyPairsAsync()
        {
            return Task<IEnumerable<Pair>>.Run(() => { return this.CalculatePairs(); });
        }


        /// <summary>
        /// Calculates ayncronously a hashset of all single pairs and its frequencies from view model's set of downloaded-searched sequences.
        /// Updates progress changes as integer counter increments.
        /// </summary>
        /// <param name="progress"></param>
        /// <returns></returns>
        public Task<HashSet<Pair>> PairsWorkerAsync(IProgress<int> progress, CancellationToken ct)
        {
            return Task<HashSet<Pair>>.Run(() =>
            {
                //var sequences = this.Sequences;
                //var sequences = this.DownloadedSequences;
                var sequences = new HashSet<Sequence>();
                if(this.StatsDraws.Count == 0)
                {
                    this.StatsDraws = Kino.Data.DrawUtilities.TakeTenDaysOldDraws(MyDrawCache);
                }

                if (StatsDraws.Count > 0)
                {
                    foreach (var d in this.StatsDraws)
                    {
                        var sequence = new DownloadedSequence();
                        sequence.Numbers = d.results;
                        sequence.Numbers.Sort(); // for pairs to work!
                        sequences.Add(sequence);
                    }
                }

                if (sequences == null)
                    throw new ArgumentException("Collection cannot be null.");

                var progressCounter = 0;                
                var dictionary = new Dictionary<Pair, int>();
                //Stopwatch sw = Stopwatch.StartNew();
                foreach (var s in sequences)
                {
                    var row = 0;
                    var numbers = s.Numbers; 
                    //numbers.Sort(); // NEW STUFF in 1.0.5
                    var count = numbers.Count();
                    var limit = count - 2; // limit for sequence element's index (row)
                    do
                    {
                        var secondItemPos = row + 1;
                        do
                        {
                            // Cancellation token requested check : throw exception to propagate to caller and cancel pair calculation
                            if (ct.IsCancellationRequested)
                            {
                                progress.Report(0);
                                ct.ThrowIfCancellationRequested();
                            }

                            var firstItemPos = row;
                            var firstItem = numbers.ElementAt(firstItemPos);
                            var secondItem = numbers.ElementAt(secondItemPos);

                            // initialize new pair object and frequency
                            var frequency = 1;
                            var pair = new Pair() { Frequency = frequency, NumberOne = firstItem, NumberTwo = secondItem }; // new Pair object

                            
                            if (!dictionary.ContainsKey(pair))
                            {
                                dictionary.Add(pair, 1);
                            }
                            else
                            {                                
                                dictionary[pair]++;
                            }

                            progressCounter++; // increment progress counter
                            //progress.Report(progressCounter); // report change
                            secondItemPos++; // get next second item
                        }
                        while (secondItemPos < count && numbers != null);
                        secondItemPos = 0;
                        row++; // get next first number item in sequences

                    } while (row <= limit && numbers != null);
                }
                //sw.Stop();
                //var timer = sw.Elapsed.Seconds;
                //var p = new Pair();
                foreach (var d in dictionary)
                {
                    var p = new Pair();
                    p.Frequency = d.Value;
                    p.NumberOne = d.Key.NumberOne;
                    p.NumberTwo = d.Key.NumberTwo;
                    this.pvm.FuckingPairs.Add(p);                    
                }
                //sw.Stop();
                //var timer = sw.Elapsed.Seconds;
                return this.pvm.FuckingPairs;
            });
        }


        /// <summary> Communicates a request for cancelling the generation of pairs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelCalcPairsBtn_Click(object sender, RoutedEventArgs e)
        {
            pair_cts.Cancel();
        }


        /// <summary> Calculates ayncronously a hashset of all single triplets and its frequencies from view model's set of downloaded-searched sequences.
        /// Updates progress changes as integer counter increments and can notify that the operation could be canceled.
        /// </summary>
        /// <param name="progress"></param>
        /// <returns></returns>
        public Task<HashSet<Triplet>> TripletsWorkerAsync(IProgress<int> progress, CancellationToken ct) 
        {
            return Task<HashSet<Triplet>>.Run(() =>
            {
                //var sequences = this.Sequences;
                //var sequences = this.DownloadedSequences;
                var sequences = new HashSet<Sequence>();
                if (this.StatsDraws.Count == 0)
                {
                    this.StatsDraws = Kino.Data.DrawUtilities.TakeTenDaysOldDraws(MyDrawCache);
                }

                if (StatsDraws.Count > 0)
                {
                    foreach (var d in this.StatsDraws)
                    {
                        var sequence = new DownloadedSequence();
                        sequence.Numbers = d.results;
                        sequence.Numbers.Sort(); // for triplets to work!
                        sequences.Add(sequence);
                    }
                }

                if (sequences == null)
                {
                    throw new ArgumentException("Collection cannot be null.");
                }

                var dictionary = new Dictionary<Triplet, int>();
                var progressCounter = 0;
                //Stopwatch sw = Stopwatch.StartNew();
                foreach (var s in sequences)
                {
                    var row = 0;
                    var numbers = s.Numbers;
                    var count = numbers.Count();
                    var limit = count - 3; // limit for sequence element's index (row)
                    do
                    {
                        var secondItemPos = row + 1;
                        do
                        {
                            var thirdItemPos = secondItemPos + 1;
                            do
                            {
                                // Cancellation token requested check : throw exception to propagate to caller and cancel calculation
                                if (ct.IsCancellationRequested)
                                {
                                    progress.Report(0);
                                    ct.ThrowIfCancellationRequested();
                                }

                                var firstItemPos = row;
                                var firstItem = numbers.ElementAt(firstItemPos);
                                var secondItem = numbers.ElementAt(secondItemPos);
                                var thirdItem = numbers.ElementAt(thirdItemPos);

                                // initialize new triplet object and frequency
                                var frequency = 1;
                                var triplet = new Triplet() { Frequency = frequency, NumberOne = firstItem, NumberTwo = secondItem, NumberThree = thirdItem }; // new triplet object

                                if (!dictionary.ContainsKey(triplet))
                                {
                                    dictionary.Add(triplet, 1);
                                }
                                else
                                {
                                    dictionary[triplet]++;
                                }

                                //progress.Report(progressCounter); // report change
                                //progressCounter++; // increment progress counter
                                thirdItemPos++; // get next second item
                            }
                            while (thirdItemPos < count);
                            //while (thirdItemPos < count - 1);
                            thirdItemPos = 0;
                            secondItemPos++; // get next second item
                        }
                        while (secondItemPos < count - 1 && numbers != null);
                        //while (secondItemPos < count - 2 && numbers != null);
                        secondItemPos = 0;
                        row++; // get next first number item in sequences

                    } while (row <= limit && numbers != null);
                }
                //sw.Stop();
                //var timer = sw.Elapsed.Seconds;
                //sw.Start();
                foreach (var d in dictionary)
                {
                    var p = new Triplet();
                    p.Frequency = d.Value;
                    p.NumberOne = d.Key.NumberOne;
                    p.NumberTwo = d.Key.NumberTwo;
                    p.NumberThree = d.Key.NumberThree;
                    this.tvm.FuckingTriplets.Add(p);
                }
                //timer = sw.Elapsed.Seconds;
                return this.tvm.FuckingTriplets;
            });
        }

        /// <summary> Button click event to initiate generation of triplets and frequencies using the sequence source of this page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void calcTripletsBtn_Click(object sender, RoutedEventArgs e)
        {
            // warn the user that it may take some time before the generation
            MessageBoxResult mbr = new MessageBoxResult();
            mbr = MessageBox.Show("Proceed with the generation?", "Calculation may take up to a few seconds to complete.", MessageBoxButton.OKCancel);

            if (mbr == MessageBoxResult.OK)
            {
                // warn the user that if search range exceeds 10 days, only 10 days span will be calculcated
                if (this.isSearchRangeTenDaySpan == false)
                {
                    MessageBoxResult mbr1 = new MessageBoxResult();
                    mbr1 = MessageBox.Show("", "Only triplets of ten day span will be calculated.", MessageBoxButton.OK);
                }

                //tvm.MaximumTripletsCalculation(); // calculate maximum triplets from sequences
                var maximumTriplets = (double)tvm.TotalTripletsCounter;
                TripletLoader.Maximum = maximumTriplets;

                this.TripletLoader.IsIndeterminate = true;
                this.TripletLoader.Visibility = System.Windows.Visibility.Visible;
                this.tripletsCancelBtn.Visibility = System.Windows.Visibility.Visible;
                this.calcTripletsBtn.Visibility = System.Windows.Visibility.Collapsed;
                this.MainTripletsStackPanel.Visibility = System.Windows.Visibility.Collapsed;
                this.tripletsProgressText.Visibility = System.Windows.Visibility.Visible;
                //this.tripletsProgressHeading.Visibility = System.Windows.Visibility.Visible;
                this.MainPivot.Foreground = (Brush)Application.Current.Resources["PhoneInactiveBrush"];
                this.MyPairsPivot.Foreground = (Brush)Application.Current.Resources["PhoneInactiveBrush"];
                this.MainPivot.IsLocked = true;

                this.triplet_cts = new CancellationTokenSource();
                Task.Factory.StartNew(() =>
                {
                    this.Dispatcher.BeginInvoke(async () =>
                    {
                        //store progress report counter and update UI's progress bar value
                        var progress = new Progress<int>();
                        progress.ProgressChanged += (o, report) =>
                        {
                            //this.TripletLoader.Value = (double)report;
                            //var percentage = Math.Round((this.TripletLoader.Value / maximumTriplets) * 100);
                            //this.tripletsProgressText.Text = "Progress: " + percentage.ToString() + "% done"; ;
                        };

                        try
                        {                            
                            var set = await TripletsWorkerAsync(progress, triplet_cts.Token);
                            var ordered = set.OrderByDescending(t => t.Frequency).ToList<Triplet>();
                            var take10 = (IEnumerable<Triplet>)this.tvm.FuckingTriplets.OrderByDescending(t => t.Frequency).Take(10);                            
                            this.tvm.Triplets.Clear();
                            KinoTransition.SlideUpFadeIn(this.TripletsSelector);
                            foreach (var p in take10)
                            {
                                this.tvm.Triplets.Add(p);
                            }
                            this.calcTripletsBtn.Visibility = System.Windows.Visibility.Collapsed;
                            this.MainTripletsStackPanel.Visibility = System.Windows.Visibility.Visible;
                            //this.tripletsProgressText.Visibility = System.Windows.Visibility.Collapsed;
                            //this.tripletsProgressHeading.Visibility = System.Windows.Visibility.Collapsed;
                            this.MainPivot.IsLocked = false;
                        }
                        catch (OperationCanceledException ex)
                        {
                            this.tvm.FuckingTriplets.Clear();
                            var token = ex.CancellationToken;
                            this.calcTripletsBtn.Visibility = System.Windows.Visibility.Visible;
                            //this.tripletsCancelBtn.Visibility = System.Windows.Visibility.Collapsed;
                            this.progress.Value = 0.0;
                            //this.tripletsProgressText.Text = "Calculation canceled";
                            this.tripletsProgressHeading.Visibility = System.Windows.Visibility.Collapsed;
                            this.MainTripletsStackPanel.Visibility = System.Windows.Visibility.Collapsed;
                            this.tripletsProgressText.Visibility = System.Windows.Visibility.Collapsed;
                            MessageBoxResult result = new MessageBoxResult();
                            result = MessageBox.Show("", "Generation of triplets was canceled.", MessageBoxButton.OK);

                        }
                        finally
                        {
                            triplet_cts.Dispose();
                        }
                        this.TripletLoader.Visibility = System.Windows.Visibility.Collapsed;
                    //this.calcTripletsBtn.Visibility = System.Windows.Visibility.Collapsed;
                    this.tripletsCancelBtn.Visibility = System.Windows.Visibility.Collapsed;
                        this.tripletsProgressText.Visibility = System.Windows.Visibility.Collapsed;
                        this.tripletsProgressHeading.Visibility = System.Windows.Visibility.Collapsed;
                        this.MainPivot.ClearValue(ForegroundProperty);
                        this.MyPairsPivot.ClearValue(ForegroundProperty);
                        this.MyPairsPivot.IsEnabled = true;
                        SystemTray.SetProgressIndicator(this, null);
                        SystemTray.IsVisible = false;
                    });
                }, triplet_cts.Token);
            } // ok pressed
        }


        /// <summary> Click event to request cancelation of generating triplets and ther frequencies.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tripletsCancelBtn_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            triplet_cts.Cancel();
        }

        /// <summary> Fetches sequence results by date range async
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void FetchByDatesAsync()
        {
            this.Sequences.Clear(); // Clear the Sequence cache
            Calendar calendar = new System.Globalization.GregorianCalendar();
            var draws = new List<Draw>(); //Holds the selector items source
            MessageBoxResult result = new MessageBoxResult(); // The error message
            
            // Fetch data
            foreach (var date in this.Dates)
            {
                string uri = "http://applications.opap.gr/DrawsRestServices/kino/drawDate/" + date + ".json";
                WebClient client = new WebClient();
                client.Headers["Accept"] = "application/json";
                //var task = JsonAsync.DownloadStringTaskAsync(client, new Uri(uri)); //download the json resource
                //string Result = await task; //wait to complete the json download before parsing

                var profile = NetworkInformation.GetInternetConnectionProfile();
                if (profile == null)
                {
                    this.progress.Visibility = System.Windows.Visibility.Collapsed;
                    this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                    SystemTray.SetProgressIndicator(this, null);
                    SystemTray.SetIsVisible(this, false);
                    this.resultsPivot.IsEnabled = true;
                    result = MessageBox.Show("Please check your data and/or WiFi connection.", "No internet connection available", MessageBoxButton.OK);
                    break;
                }
                else
                {
                    ProgressIndicator progress = new ProgressIndicator
                    {
                        IsVisible = true,
                        IsIndeterminate = true,
                        Text = "Downloading data..."
                    };
                    SystemTray.SetProgressIndicator(this, progress);

                    try
                    {
                        var task = AsyncDownloadString(client, new Uri(uri)); //download the json resourceAsyncDownloadString
                        string Result = await task; //wait to complete the json download before parsing                                              
                        var jsonObj = JObject.Parse(Result);
                        var deserialized = JsonConvert.DeserializeObject<RootObject>(Result);

                        if (Result == "{}")
                        {
                            this.progress.Visibility = System.Windows.Visibility.Collapsed;
                            this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                            SystemTray.SetProgressIndicator(this, null);
                            SystemTray.SetIsVisible(this, false);
                            this.resultsPivot.IsEnabled = true;
                            result = MessageBox.Show("Please check with OPAP web services.", "The date of the draw is invalid", MessageBoxButton.OK);
                            break;
                        }
                        else if (deserialized.draws.draw == null)
                        {
                            this.progress.Visibility = System.Windows.Visibility.Collapsed;
                            this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                            SystemTray.SetProgressIndicator(this, null);
                            SystemTray.SetIsVisible(this, false);
                            this.resultsPivot.IsEnabled = true;
                            result = MessageBox.Show("Please check with OPAP web services.", "The game given in the URL of web services is invalid", MessageBoxButton.OK);
                            break;
                        }
                        else if (deserialized.draws.draw != null && deserialized.draws.draw.Count == 0)
                        {
                            if (this.Sequences.Count == 0)  //in case already downloaded some draws
                            {
                                this.progress.Visibility = System.Windows.Visibility.Collapsed;
                                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                                SystemTray.SetProgressIndicator(this, null);
                                SystemTray.SetIsVisible(this, false);
                                this.resultsPivot.IsEnabled = true;
                                result = MessageBox.Show("Please select a different date.", "No draws found", MessageBoxButton.OK);
                                break;
                            }
                        }
                        else //if (result != MessageBoxResult.OK)
                        {
                            var draw = deserialized.draws.draw;
                            foreach (var d in draw)
                            {
                                var sequence = new Sequence();
                                foreach (var item in d.results)
                                {
                                    sequence.Add(item);
                                }
                                this.Sequences.Add(sequence); // add to cache
                            }

                            DayDraw currentDraw = new DayDraw();
                            currentDraw.Date = this.LowDate;
                            currentDraw.Draws = deserialized.draws;
                            var exists = DayDraws.Contains<DayDraw>(currentDraw); // check day's draws
                            if (exists == false)
                            {
                                var daysCurrentDraw = DayDraws.SingleOrDefault(d => d.Date == this.LowDate); //check for same day draws
                                if (currentDraw != null && DaydrawDateComparer.IsEqual(currentDraw, daysCurrentDraw))
                                {
                                    DayDraws.Remove(daysCurrentDraw); //remove previous day's draws
                                    DayDraws.Add(currentDraw); //update with latest downloads
                                }
                                else
                                    DayDraws.Add(currentDraw);
                            }

                            var list = deserialized.draws.draw;
                            //var collection = new ObservableCollection<Draw>();
                            foreach (var d in list)
                            {
                                draws.Add(d);
                                //collection.Add(d);
                                //this.CurrentLoadedDraws.Add(d);

                                //this.ddvm.DownloadedDraws.Add(d); // NEW
                            }

                            firstBatchcounter++; //indicates data has come in, ready to load first batch

                            // update selector data context as first day draw results come in
                            //this.DataSelector.Visibility = System.Windows.Visibility.Visible;

                            //if(firstBatchcounter == 1)
                            //    this.LoadFirstBatch();

                            // NEW for v.1.0.4
                            this.MainPivot.IsEnabled = true;
                            this.MainPivot.IsLocked = false;
                            this.MainPivot.IsHitTestVisible = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        var innerException = ex.InnerException;
                        if (innerException is WebException)
                        {
                            var we = innerException as WebException;
                            var message = string.Empty;
                            if (we.Response is HttpWebResponse)
                            {
                                switch (((HttpWebResponse)we.Response).StatusCode)
                                {
                                    case HttpStatusCode.NotFound:
                                        message = "The requested resource does not exist on the server";
                                        break;
                                    case HttpStatusCode.InternalServerError:
                                        message = "The server returned an internal error";
                                        break;
                                    case HttpStatusCode.BadRequest:
                                        message = "The request could not be understood by the server";
                                        break;
                                    default:
                                        message = "Something went wrong";
                                        break;
                                }
                                this.progress.Visibility = System.Windows.Visibility.Collapsed;
                                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                                this.resultsPivot.IsEnabled = true;
                                MessageBoxResult error = MessageBox.Show("Please check with OPAP web services.", message, MessageBoxButton.OK);
                                SystemTray.SetProgressIndicator(this, null);
                                SystemTray.SetIsVisible(this, false);

                                // new in 1.0.5
                                if (error == MessageBoxResult.OK)
                                {
                                    this.NavigationService.GoBack();
                                }
                            }
                        }
                    }
                }
            }

            // data downloaded, prepare item source
            if (result == MessageBoxResult.None) //ok
            {
                KinoTransition.SlideLeftFadeOut(this.progress);
                KinoTransition.SlideLeftFadeOut(this.progressStatus);
                this.resultsPivot.IsEnabled = true;

                ///// NEW STUFF in 1.0.5  - Remove first'days draws according to last day's draw and time **** NEW STUFF  in 1.0.5*** 

                var lastDayDrawTime = draws[draws.Count - 1].drawTime;
                var lastDraw = draws[draws.Count - 1];
                var firstDayDrawTime = draws[0].drawTime;
                var ci = new CultureInfo("el-GR");
                ci.DateTimeFormat = new DateTimeFormatInfo();
                ci.DateTimeFormat.FullDateTimePattern = "dd-MM-yyyyTHH:mm:ss";
                var firstDateTime = DateTime.ParseExact(firstDayDrawTime, "dd-MM-yyyyTHH:mm:ss", ci);
                var greek = new CultureInfo("el-GR").DateTimeFormat;
                DateTime parsedLastDraw = DateTime.ParseExact(lastDayDrawTime, "dd-MM-yyyyTHH:mm:ss", ci);
                foreach (var d in draws)
                {
                    var date = DateTime.ParseExact(d.drawTime, "dd-MM-yyyyTHH:mm:ss", ci);
                    if (firstDateTime.Day != parsedLastDraw.Day) // search span is greater that 1 day
                    {
                        if (firstDateTime.Month == date.Month && date.Day == firstDateTime.Day) // current day is the first day of the date search range
                        {
                            // dont add draw with draw time earlier the last day's draw time
                            if (!JsonDrawTimeComparer.IsDrawTimeEarlier(d, lastDraw))
                            {
                                this.ddvm.DownloadedDraws.Add(d); // NEW
                            }
                        }
                        else if (firstDateTime.Month == date.Month && date.Day > firstDateTime.Day) // current day is not the first day of the date range, add to collection
                        {
                            this.ddvm.DownloadedDraws.Add(d);
                        }
                    }
                    else // date search span is 1 day, include all draws
                    {
                        this.ddvm.DownloadedDraws.Add(d);
                    }
                }

                this.DataSelector.Visibility = System.Windows.Visibility.Visible;
                this.Sequences.Clear();
                foreach (var d in this.ddvm.DownloadedDraws)
                {
                    var sequence = new Sequence();
                    foreach (var item in d.results)
                    {
                        sequence.Add(item);
                    }
                    this.Sequences.Add(sequence); // add to cache
                }
                ///// END - NEW STUFF in 1.0.5 => removed all first day draws where time is less than the last day's draw time.

                Draws = draws;
                KinoTransition.SlideUpFadeIn(this.DataSelector);
                this.progress.Visibility = System.Windows.Visibility.Collapsed;
                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                this.TotalDraws.Text = this.ddvm.DownloadedDraws.Count.ToString();
                this.MainPivot.IsEnabled = true;
                this.MainPivot.IsLocked = false;
                this.MainPivot.IsHitTestVisible = true;

                // get last draw's datetime
                if (draws.Count != 0)
                {
                    var last = draws[draws.Count - 1];
                    string inputFormat = "dd-MM-yyyyTHH:mm:ss";
                    CultureInfo provider = CultureInfo.CurrentCulture;
                    DateTime date = DateTime.ParseExact(last.drawTime, inputFormat, provider);
                    string displayTimeFormat = "HH:mm";
                    this.lastDrawTime.Text = date.ToString(displayTimeFormat);
                }
            }
            else
            {
                // new in 1.0.5
                this.NavigationService.GoBack();
            }
            SystemTray.SetProgressIndicator(this, null);
            SystemTray.SetIsVisible(this, false);
        }

        /// <summary> Calculates asynchronously all triplets from sequences using intersection of each sequence item with one another.
        /// </summary>
        /// <param name="progress"></param>
        /// <returns></returns>
        public Task<HashSet<Triplet>> IntersectionTripletsWorkerAsync(IProgress<int> progress, CancellationToken ct)
        {
            return Task<HashSet<Triplet>>.Run(() =>
            {
                var sequences = this.Sequences;
                var list = IntersectedSequencesAsList(sequences);

                /*
                var copy = sequences; // creates a copy of downloaded sequence items
                var intersected = new HashSet<Sequence>(new SequenceIntersectionComparer());
                var dictionary = new Dictionary<Sequence, int>(new SequenceIntersectionComparer());
                var copyCount = copy.Count();
                var index = 1;
                var indexRef = 0;
                foreach(var s in sequences)
                {
                    for (var i = index; i < copyCount; i++ )
                    {
                        indexRef++;
                        var nextCopied = copy.ElementAt(i);
                        var intersection = s.Numbers.Intersect(nextCopied.Numbers);
                        var sequence = new Sequence();
                        foreach (var n in intersection)
                        {
                            sequence.Add(n);
                        }
                        if (sequence.Numbers.Count > 0)
                        {
                            if (dictionary.ContainsKey(sequence))
                            {
                                dictionary[sequence]++;
                            }
                            else
                                dictionary.Add(sequence, 1);
                        }                        
                    }
                    index++; 
                }

                var ordered = dictionary.Where(t=>t.Value == 20  ).ToList();
                var dics = ordered.Take(20);
                var triplets = intersected.SkipWhile(t => t.Numbers.Count > 2);
                */

                if (sequences == null)
                {
                    throw new ArgumentException("Collection cannot be null.");
                }

                var progressCounter = 0;
                //Stopwatch sw = Stopwatch.StartNew();
                foreach (var s in sequences)
                {
                    var row = 0;
                    var numbers = s.Numbers;
                    var count = numbers.Count();
                    var limit = count - 3; // limit for sequence element's index (row)
                    do
                    {
                        var secondItemPos = row + 1;
                        do
                        {
                            var thirdItemPos = secondItemPos + 1;
                            do
                            {
                                // Cancellation token requested check : throw exception to propagate to caller and cancel pair calculation
                                if (ct.IsCancellationRequested)
                                {
                                    progress.Report(0);
                                    ct.ThrowIfCancellationRequested();
                                }

                                var firstItemPos = row;
                                var firstItem = numbers.ElementAt(firstItemPos);
                                var secondItem = numbers.ElementAt(secondItemPos);
                                var thirdItem = numbers.ElementAt(thirdItemPos);
                                var sequence = new Sequence();
                                sequence.Numbers.Add(firstItem);
                                sequence.Numbers.Add(secondItem);
                                sequence.Numbers.Add(thirdItem);

                                // initialize new pair object and frequency
                                var frequency = 1;
                                var triplet = new Triplet() { Frequency = frequency, NumberOne = firstItem, NumberTwo = secondItem, NumberThree = thirdItem }; // new Pair object

                                // find existing triplet in the collection                        
                                var item = this.tvm.FuckingTriplets.FirstOrDefault(t =>
                                (t.NumberOne == triplet.NumberOne && t.NumberTwo == triplet.NumberTwo && t.NumberThree == triplet.NumberThree)
                                    || (t.NumberOne == triplet.NumberOne && t.NumberTwo == triplet.NumberThree && t.NumberThree == triplet.NumberTwo)
                                    || (t.NumberOne == triplet.NumberTwo && t.NumberTwo == triplet.NumberOne && t.NumberThree == triplet.NumberThree)
                                    || (t.NumberOne == triplet.NumberTwo && t.NumberTwo == triplet.NumberThree && t.NumberThree == triplet.NumberOne)
                                    || (t.NumberOne == triplet.NumberThree && t.NumberTwo == triplet.NumberOne && t.NumberThree == triplet.NumberTwo)
                                    || (t.NumberOne == triplet.NumberThree && t.NumberTwo == triplet.NumberTwo && t.NumberThree == triplet.NumberOne));
                                if (item != null)
                                {
                                    item.Frequency++;
                                }
                                else
                                {
                                    this.tvm.FuckingTriplets.Add(triplet); // add new pair object
                                }

                                thirdItemPos++; // get next second item
                            }
                            while (thirdItemPos < count - 1);
                            progressCounter++; // increment progress counter
                            progress.Report(progressCounter); // report change
                            thirdItemPos = 0;
                            secondItemPos++; // get next second item
                        }
                        while (secondItemPos < count - 2 && numbers != null);
                        secondItemPos = 0;
                        row++; // get next first number item in sequences

                    } while (row <= limit && numbers != null);
                }

                //sw.Stop();
                //var timer = sw.Elapsed.Seconds;                
                return this.tvm.FuckingTriplets;
            });
        }

        /// <summary> Produces a collection of the set intersection of the given sequence set
        /// </summary>
        /// <param name="Sequences"></param>
        /// <returns></returns>
        public static ICollection<Sequence> IntersectedSequencesAsList(HashSet<Sequence> Sequences)
        {
            var sequences = Sequences;
            var copy = sequences; // creates a copy of downloaded sequence items
            var intersected = new HashSet<Sequence>(new SequenceIntersectionComparer());
            var dictionary = new Dictionary<Sequence, int>(new SequenceIntersectionComparer());
            var copyCount = copy.Count();
            var index = 1;
            var indexRef = 0;
            foreach (var s in sequences)
            {
                for (var i = index; i < copyCount; i++)
                {
                    indexRef++;
                    var nextCopied = copy.ElementAt(i);
                    var intersection = s.Numbers.Intersect(nextCopied.Numbers);
                    var sequence = new Sequence();
                    foreach (var n in intersection)
                    {
                        sequence.Add(n);
                    }
                    if (sequence.Numbers.Count > 0)
                    {
                        //if (dictionary.ContainsKey(sequence))
                        //{
                        //    dictionary[sequence]++;
                        //}
                        //else
                        //dictionary.Add(sequence, 1);
                        intersected.Add(sequence);
                    }
                }
                index++;
            }

            //var keys = dictionary.Keys; //get sequence items            
            //return keys.ToList<Sequence>();
            return intersected; //.ToList<Sequence>();
        }

        /// <summary> Calculates ayncronously a hashset of all single triplets and its frequencies from view model's set of downloaded-searched sequences.
        /// Updates progress changes as integer counter increments.
        /// </summary>
        /// <param name="progress"></param>
        /// <returns></returns>
        public Task<HashSet<Triplet>> TripletsIntersectedWorkerAsync(IProgress<int> progress, CancellationToken ct, ICollection<Sequence> IntersectedSequences)
        {
            return Task<HashSet<Triplet>>.Run(() =>
            {
                //var sequences = Sequences;
                Stopwatch sw = Stopwatch.StartNew();
                var sequences = IntersectedSequences.Where(t => t.Numbers.Count >= 3).ToList<Sequence>();
                if (sequences == null)
                {
                    throw new ArgumentException("Collection cannot be null.");
                }

                var progressCounter = 0;

                foreach (var s in sequences)
                {
                    var row = 0;
                    var numbers = s.Numbers;
                    var count = numbers.Count();
                    var limit = count - 3; // limit for sequence element's index (row)
                    do
                    {
                        var secondItemPos = row + 1;
                        do
                        {
                            var thirdItemPos = secondItemPos + 1;
                            do
                            {
                                // Cancellation token requested check : throw exception to propagate to caller and cancel pair calculation
                                if (ct.IsCancellationRequested)
                                {
                                    progress.Report(0);
                                    ct.ThrowIfCancellationRequested();
                                }

                                var firstItemPos = row;
                                var firstItem = numbers.ElementAt(firstItemPos);
                                var secondItem = numbers.ElementAt(secondItemPos);
                                var thirdItem = numbers.ElementAt(thirdItemPos);
                                var sequence = new Sequence();
                                sequence.Numbers.Add(firstItem);
                                sequence.Numbers.Add(secondItem);
                                sequence.Numbers.Add(thirdItem);

                                // initialize new pair object and frequency
                                var frequency = 1;
                                var triplet = new Triplet() { Frequency = frequency, NumberOne = firstItem, NumberTwo = secondItem, NumberThree = thirdItem }; // new Pair object

                                // find existing triplet in the collection                        
                                var item = this.tvm.FuckingTriplets.FirstOrDefault(t =>
                                (t.NumberOne == triplet.NumberOne && t.NumberTwo == triplet.NumberTwo && t.NumberThree == triplet.NumberThree)
                                    || (t.NumberOne == triplet.NumberOne && t.NumberTwo == triplet.NumberThree && t.NumberThree == triplet.NumberTwo)
                                    || (t.NumberOne == triplet.NumberTwo && t.NumberTwo == triplet.NumberOne && t.NumberThree == triplet.NumberThree)
                                    || (t.NumberOne == triplet.NumberTwo && t.NumberTwo == triplet.NumberThree && t.NumberThree == triplet.NumberOne)
                                    || (t.NumberOne == triplet.NumberThree && t.NumberTwo == triplet.NumberOne && t.NumberThree == triplet.NumberTwo)
                                    || (t.NumberOne == triplet.NumberThree && t.NumberTwo == triplet.NumberTwo && t.NumberThree == triplet.NumberOne));
                                if (item != null)
                                {
                                    item.Frequency++;
                                }
                                else
                                {
                                    this.tvm.FuckingTriplets.Add(triplet); // add new pair object
                                }

                                thirdItemPos++; // get next second item
                            }
                            while (thirdItemPos < count - 1);
                            progressCounter++; // increment progress counter
                            progress.Report(progressCounter); // report change
                            thirdItemPos = 0;
                            secondItemPos++; // get next second item
                        }
                        while (secondItemPos < count - 2 && numbers != null);
                        secondItemPos = 0;
                        row++; // get next first number item in sequences

                    } while (row <= limit && numbers != null);
                }

                sw.Stop();
                var timer = sw.Elapsed.Seconds;
                return this.tvm.FuckingTriplets;
            });
        }

        /// <summary>
        /// Calculates ayncronously a hashset of all single pairs and its frequencies from view model's set of downloaded-searched sequences.
        /// Updates progress changes as integer counter increments.
        /// </summary>
        /// <param name="progress"></param>
        /// <returns></returns>
        public Task<HashSet<Pair>> PairsIntersectedWorkerAsync(IProgress<int> progress, CancellationToken ct, ICollection<Sequence> IntersectedSequences)
        {
            return Task<HashSet<Pair>>.Run(() =>
            {

                Stopwatch sw = Stopwatch.StartNew();
                var sequences = IntersectedSequences.Where(t => t.Numbers.Count >= 2);
                //var sequences = this.Sequences;
                if (sequences == null)
                    throw new ArgumentException("Collection cannot be null.");
                var progressCounter = 0;
                var FFFF = 0;
                //Stopwatch sw = Stopwatch.StartNew();
                foreach (var s in sequences)
                {
                    var row = 0;
                    var numbers = s.Numbers;
                    var count = numbers.Count();
                    var limit = count - 2; // limit for sequence element's index (row)
                    do
                    {
                        var secondItemPos = row + 1;
                        do
                        {
                            // Cancellation token requested check : throw exception to propagate to caller and cancel pair calculation
                            if (ct.IsCancellationRequested)
                            {
                                progress.Report(0);
                                ct.ThrowIfCancellationRequested();
                            }

                            var firstItemPos = row;
                            var firstItem = numbers.ElementAt(firstItemPos);
                            var secondItem = numbers.ElementAt(secondItemPos);
                            var sequence = new Sequence();
                            sequence.Numbers.Add(firstItem);
                            sequence.Numbers.Add(secondItem);

                            // initialize new pair object and frequency
                            var frequency = 1;
                            var pair = new Pair() { Frequency = frequency, NumberOne = firstItem, NumberTwo = secondItem }; // new Pair object

                            if ((pair.NumberOne == 61 && pair.NumberTwo == 70) || (pair.NumberOne == 70 && pair.NumberTwo == 61))
                            {
                                FFFF++;
                            }

                            // find existing pair in the collection                        
                            var item = this.pvm.FuckingPairs.FirstOrDefault(t => (t.NumberOne == pair.NumberOne && t.NumberTwo == pair.NumberTwo)
                                || (t.NumberOne == pair.NumberTwo && t.NumberTwo == pair.NumberOne));
                            if (item != null)
                            {
                                item.Frequency++;
                            }
                            else
                            {
                                this.pvm.FuckingPairs.Add(pair); // add new pair object
                            }
                            progressCounter++; // increment progress counter
                            progress.Report(progressCounter); // report change
                            secondItemPos++; // get next second item
                        }
                        while (secondItemPos < count && numbers != null);
                        secondItemPos = 0;
                        row++; // get next first number item in sequences

                    } while (row <= limit && numbers != null);
                }

                sw.Stop();
                var timer = sw.Elapsed.Seconds;
                return this.pvm.FuckingPairs;
            });
        }


        /// <summary> Fetches sequence results by date range in async mode with progress
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FetchByDatesTaskAsync()
        {
            // Clear the Sequence cache
            DownloadedSequences.Clear();
            //this.Sequences.Clear();          
              
            this.MainPivot.IsEnabled = true;
            //this.MainPivot.IsLocked = true;
            this.MainPivot.IsHitTestVisible = true;

            //Holds the selector items source                          
            var draws = new List<Draw>();
            var drawCache = new Collection<Draw>(); // holds the downloaded draws in a cache   

            //progress counter  calculation: foreach date string get an estimate of json draw objects to convert from
            var dateCounter = this.Dates.Count;
            this.progress.Minimum = 0.0;
            this.progress.Value = 0.0;
            var maxDailyDraws = 157.0;
            this.progress.Maximum = (double)dateCounter * maxDailyDraws;                      
            var Iprogress = new Progress<double>();
            this.DownloadCancelBtn.Visibility = Visibility.Visible;

            sequence_cts = new CancellationTokenSource(); // reset calculation token  
            Task.Factory.StartNew(() =>
            {
                this.Dispatcher.BeginInvoke(async () =>
                {
                    //store progress report counter and update UI's progress bar value                    
                    Iprogress.ProgressChanged += (o, report) =>
                    {
                        this.progress.Value = report;
                        var percentage = Math.Round((this.progress.Value / this.progress.Maximum) * 100.0);
                        this.progressStatus.Text = "Progress: " + percentage.ToString() + "% done"; ;
                    };

                    try
                    {
                        this.DataSelector.Visibility = System.Windows.Visibility.Visible;

                        // display system tray indicator
                        ProgressIndicator progressIndicator = new ProgressIndicator
                        {
                            IsVisible = true,
                            IsIndeterminate = true,
                            Text = "Downloading results..."
                        };
                        SystemTray.SetProgressIndicator(this, progressIndicator);

                        //Stopwatch sw = Stopwatch.StartNew();
                        //sw.Start();

                        // download sequences using REST service from OPAP's url
                        draws = await GetDrawsTaskAsync(this.Dates, Iprogress, sequence_cts.Token);

                        //sw.Stop();
                        //var timer = sw.Elapsed.Seconds;

                        ///// NEW STUFF in 1.0.5  - Remove first'days draws according to last day's draw and time **** NEW STUFF  in 1.0.5*** 
                        var lastDayDrawTime = draws[draws.Count - 1].drawTime;
                        var lastDraw = draws[draws.Count - 1];
                        var firstDayDrawTime = draws[0].drawTime;
                        var ci = new CultureInfo("el-GR");
                        ci.DateTimeFormat = new DateTimeFormatInfo();
                        ci.DateTimeFormat.FullDateTimePattern = "dd-MM-yyyyTHH:mm:ss";
                        var firstDateTime = DateTime.ParseExact(firstDayDrawTime, "dd-MM-yyyyTHH:mm:ss", ci);
                        var greek = new CultureInfo("el-GR").DateTimeFormat;
                        DateTime parsedLastDraw = DateTime.ParseExact(lastDayDrawTime, "dd-MM-yyyyTHH:mm:ss", ci);
                        var today = DateTime.Now;

                        // update progress status, hide cancel button, delay process to wait to load data to selector
                        this.progressStatus.Text = "Download complete";
                        this.resultsPivot.IsEnabled = true;
                        this.DownloadCancelBtn.Visibility = Visibility.Collapsed;
                        //await Task.Delay(3000);
                        this.progress.Visibility = System.Windows.Visibility.Collapsed;
                        this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                        progressIndicator.Text = "Loading list...";

                        // Return draws within 24-hour based search range
                        MyDrawCache = Kino.Data.DrawUtilities.GetActualDraws(draws);

                        // copy number and define bonus number into observable collection property
                        /*
                        foreach(var d in MyDrawCache)
                        {
                            d.ResultsItems = ResultItems(d);
                        }
                        */

                        // set selector's data context and load first batch of draws                        
                        //this.DataSelector.Visibility = System.Windows.Visibility.Visible;
                        this.SetSequencesDataContext(MyDrawCache);
                        this.svm.LoadFirstBatch(10);                        
                        KinoTransition.SlideUpFadeIn(this.DataSelector);

                        SystemTray.SetProgressIndicator(this, null);
                        SystemTray.SetIsVisible(this, false);
                        
                        this.DownloadedSequences.Clear();
                        
                        //foreach (var d in this.ddvm.DownloadedDraws)
                        foreach (var d in MyDrawCache)
                        {
                            
                            //var sequence = new Sequence();
                            var dSequence = new DownloadedSequence();
                            foreach (var item in d.results)
                            {
                                //sequence.Add(item);
                                dSequence.Add(item);
                            }
                            //this.Sequences.Add(sequence); // add to cache
                            this.DownloadedSequences.Add(dSequence); // ΝΕW STUFF IN 1.0.5 (old Sequences set did not contain duplicates -> Number sum)
                        }

                        ///// END - NEW STUFF in 1.0.5 => removed all first day draws where time is less than the last day's draw time.

                        Draws = draws;
                        //KinoTransition.SlideUpFadeIn(this.DataSelector);
                        this.TotalDraws.Text = MyDrawCache.Count.ToString(); //this.ddvm.DownloadedDraws.Count.ToString();
                        this.MainPivot.IsEnabled = true;
                        this.MainPivot.IsLocked = false;
                        this.MainPivot.IsHitTestVisible = true;                        

                        // get last draw's datetime
                        if (MyDrawCache.Count != 0)
                        {
                            var last = MyDrawCache[MyDrawCache.Count - 1];
                            string inputFormat = "dd-MM-yyyyTHH:mm:ss";
                            CultureInfo provider = CultureInfo.CurrentCulture;
                            DateTime lastDate = DateTime.ParseExact(last.drawTime, inputFormat, provider);
                            string displayTimeFormat = "HH:mm";
                            
                            this.lastDrawTime.Text = lastDate.ToString(displayTimeFormat);
                            this.lastDrawTime1.Text = lastDate.ToString(displayTimeFormat);
                            var first = MyDrawCache[0];
                            DateTime firstDate = DateTime.ParseExact(first.drawTime, inputFormat, provider);
                            this.firstDrawTime.Text = firstDate.ToString(displayTimeFormat);

                            //// NEW STUFF 1.0.5
                            //set display data in result pivot item also
                            var displayDateFormat = "dd/MM/yyyy";
                            this.lastDrawTime2.Text = lastDate.ToString(displayTimeFormat);
                            this.firstDrawTime2.Text = firstDate.ToString(displayTimeFormat);
                            this.lowDate2.Text = firstDate.ToString(displayDateFormat);
                            this.highDate1.Text = lastDate.ToString(displayDateFormat);
                            this.highDate2.Text = lastDate.ToString(displayDateFormat);
                            this.TotalDraws1.Text = MyDrawCache.Count.ToString();

                            // display date range data in bonus pivot item
                            this.bonuslastDrawTime1.Text = lastDate.ToString(displayTimeFormat);
                            this.bonusfirstDrawTime.Text = firstDate.ToString(displayTimeFormat);
                            this.bonuslowDate1.Text = firstDate.ToString(displayDateFormat);
                            this.bonushighDate1.Text = lastDate.ToString(displayDateFormat);

                            // set days span
                            this.isSearchRangeTenDaySpan = firstDate.AddDays(10) >= lastDate;

                        }
                    } //end try

                    /*** HERE HANDLE ALL EXCEPTIONS FROM returning TASK object******/
                    catch (WebException ex)
                    {
                        var message = string.Empty;
                        if (ex.Response is HttpWebResponse)
                        {
                            switch (((HttpWebResponse)ex.Response).StatusCode)
                            {
                                case HttpStatusCode.NotFound:
                                    message = "The requested resource does not exist on the server";
                                    break;
                                case HttpStatusCode.InternalServerError:
                                    message = "The server returned an internal error";
                                    break;
                                case HttpStatusCode.BadRequest:
                                    message = "The request could not be understood by the server";
                                    break;
                                default:
                                    message = "Something went wrong";
                                    break;
                            }
                            MessageBoxResult mbr = MessageBox.Show("Please check with OPAP web services.", message, MessageBoxButton.OK);                            
                            if (mbr == MessageBoxResult.OK)
                            {
                                this.NavigationService.GoBack();
                            }
                        }
                    }
                    catch (OperationCanceledException ex)
                    {
                        this.DownloadCancelBtn.Visibility = Visibility.Collapsed;
                        this.progress.Value = 0.0;
                        this.progressStatus.Text = "Downloading of draw results canceled.";
                        var message = ex.Message;
                        MessageBoxResult mbr = MessageBox.Show("", "Downloading of KINO results was canceled.", MessageBoxButton.OK);
                        if (mbr == MessageBoxResult.OK)
                        {
                            this.NavigationService.GoBack();
                        }

                    }
                    catch (KinoExceptions.InternetConnectionProfileException ex)
                    {
                        // new in 1.0.5
                        MessageBoxResult mbr = MessageBox.Show("Please check your data and/or WiFi connection.", ex.Message, MessageBoxButton.OK);
                        if (mbr == MessageBoxResult.OK)
                        {
                            this.NavigationService.GoBack();
                        }
                    }
                    catch (KinoExceptions.InvalidDrawDateException ex)
                    {
                        // new in 1.0.5
                        MessageBoxResult mbr = MessageBox.Show("Please check with OPAP web services.", ex.Message, MessageBoxButton.OK);
                        if (mbr == MessageBoxResult.OK)
                        {
                            this.NavigationService.GoBack();
                        }
                    }
                    catch (KinoExceptions.InvalidKinoGameUrlException ex)
                    {
                        // new in 1.0.5
                        MessageBoxResult mbr = MessageBox.Show("Please check with OPAP web services.", ex.Message, MessageBoxButton.OK);
                        if (mbr == MessageBoxResult.OK)
                        {
                            this.NavigationService.GoBack();
                        }
                    }
                    catch (KinoExceptions.NoDrawFoundException ex)
                    {
                        // new in 1.0.5                                                
                        MessageBoxResult mbr = MessageBox.Show("Please select a different date.", ex.Message, MessageBoxButton.OK);
                        if (mbr == MessageBoxResult.OK)
                        {
                            this.NavigationService.GoBack();
                        }
                    }
                    catch (System.Reflection.TargetInvocationException tie) // sometime json object is not read or parsed properly, exception is not being handled!
                    {
                        // new in 1.0.5                     
                        var msg = tie.Message;
                        MessageBoxResult mbr = MessageBox.Show("Please try again.", "Reading OPAP data failed.", MessageBoxButton.OK);
                        if (mbr == MessageBoxResult.OK)
                        {
                            this.NavigationService.GoBack();
                        }
                    }
                    finally
                    {
                        sequence_cts.Dispose();                        
                    }
                });
            }, sequence_cts.Token);
        }
        
        /// <summary> Receives asyncronously json data with given uri an reports progress to caller.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="uri"></param>
        /// <param name="progress"></param>
        /// <param name="ct"> Cancellation token to propagate to caller that operation was cenceled</param>
        /// <param name="dCounter"> Date counter given to calculate json draw download progress</param>
        /// <returns></returns>
        public async Task<string> GetJsonDrawStringTaskAsync(WebClient client, Uri uri, IProgress<double> progress, CancellationToken ct, int dCounter)
        {
            var tcs = new TaskCompletionSource<string>();
            double maxDailyDraws = 157.0;
            var daycounter = (double)dCounter;

            //client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(DownloadProgressChangedEvent);
            client.DownloadStringCompleted += (o, e) =>
            {
                if (e.Cancelled)
                    tcs.SetCanceled();
                else
                {
                    try
                    {
                        // Cancellation token requested check : throw exception to propagate to caller and cancel calculation
                        if (ct.IsCancellationRequested)
                        {
                            progress.Report(0);
                            ct.ThrowIfCancellationRequested();
                        }
                        progress.Report(maxDailyDraws * daycounter); // first json draw string downloaded, report to caller
                        tcs.SetResult(e.Result);
                    }
                    catch (System.Exception ex)
                    {
                        tcs.SetException(ex);
                    }
                }
            };
            client.DownloadStringAsync(uri);
            return await tcs.Task;
        }


        public Task<List<Draw>> DownloadedDrawsUsingDateRangeTaskAsync(List<string> dates, IProgress<double> progress, CancellationToken ct)
        {
            return Task<List<Draw>>.Run(async() =>
            {
                var draws = new List<Draw>(); //Holds the selector items source
                //var result = new MessageBoxResult(); // The error message                
                //sequence_cts = new CancellationTokenSource(); // reset calculation token                            
                var dateCounter = 1;

                // Fetch data
                foreach (var date in dates)
                {

                    // Cancellation token requested check : throw exception to propagate to caller and cancel calculation
                    if (ct.IsCancellationRequested)
                    {
                        progress.Report(0);
                        ct.ThrowIfCancellationRequested();
                    }

                    string uri = "http://applications.opap.gr/DrawsRestServices/kino/drawDate/" + date + ".json";
                    WebClient client = new WebClient();
                    client.Headers["Accept"] = "application/json";

                    var profile = NetworkInformation.GetInternetConnectionProfile();
                    if (profile == null)
                    {
                        /*
                        this.progress.Visibility = System.Windows.Visibility.Collapsed;
                        this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                        SystemTray.SetProgressIndicator(this, null);
                        SystemTray.SetIsVisible(this, false);
                        this.resultsPivot.IsEnabled = true;
                        result = MessageBox.Show("Please check your data and/or WiFi connection.", "No internet connection available", MessageBoxButton.OK);
                        break;
                        */
                        throw new KinoExceptions.InternetConnectionProfileException("No internet connection available");
                    }
                    else
                    {
                        /*
                        ProgressIndicator progressIndicator = new ProgressIndicator
                        {
                            IsVisible = true,
                            IsIndeterminate = true
                            //Text = "Downloading data..."
                        };
                        SystemTray.SetProgressIndicator(this, progressIndicator);
                        */
                        try
                        {

                            // var task = await AsyncDownloadString(client, new Uri(uri)); //download the json resourceAsyncDownloadString
                            // wait to complete the json download before parsing                                              
                            var task = GetJsonDrawStringTaskAsync(client, new Uri(uri), progress, ct, dateCounter);                            
                            string Result = await task;
                            var jsonObj = JObject.Parse(Result);
                            var deserialized = JsonConvert.DeserializeObject<RootObject>(Result);

                            if (Result == "{}")
                            {
                                throw new KinoExceptions.InvalidDrawDateException("The date of the draw is invalid");
                                /*
                                this.progress.Visibility = System.Windows.Visibility.Collapsed;
                                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                                SystemTray.SetProgressIndicator(this, null);
                                SystemTray.SetIsVisible(this, false);
                                this.resultsPivot.IsEnabled = true;
                                result = MessageBox.Show("Please check with OPAP web services.", "The date of the draw is invalid", MessageBoxButton.OK);
                                break;
                                */
                            }
                            else if (deserialized.draws.draw == null)
                            {
                                throw new KinoExceptions.InvalidKinoGameUrlException("The game given in the URL of web services is invalid");
                                /*
                                this.progress.Visibility = System.Windows.Visibility.Collapsed;
                                this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                                SystemTray.SetProgressIndicator(this, null);
                                SystemTray.SetIsVisible(this, false);
                                this.resultsPivot.IsEnabled = true;
                                result = MessageBox.Show("Please check with OPAP web services.", "The game given in the URL of web services is invalid", MessageBoxButton.OK);
                                break;
                                */
                            }
                            else if (deserialized.draws.draw != null && deserialized.draws.draw.Count == 0)
                            {
                                if (this.Sequences.Count == 0)  //in case already downloaded some draws
                                {
                                    throw new KinoExceptions.NoDrawFoundException("No draws found");
                                    /*
                                    this.progress.Visibility = System.Windows.Visibility.Collapsed;
                                    this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                                    SystemTray.SetProgressIndicator(this, null);
                                    SystemTray.SetIsVisible(this, false);
                                    this.resultsPivot.IsEnabled = true;
                                    result = MessageBox.Show("Please select a different date.", "No draws found", MessageBoxButton.OK);
                                    break;
                                    */
                                }
                            }
                            else //if (result != MessageBoxResult.OK)
                            {
                                var draw = deserialized.draws.draw;
                                foreach (var d in draw)
                                {
                                    var sequence = new Sequence();
                                    foreach (var item in d.results)
                                    {
                                        sequence.Add(item);
                                    }
                                    this.Sequences.Add(sequence); // add to cache
                                }

                                DayDraw currentDraw = new DayDraw();
                                currentDraw.Date = this.LowDate;
                                currentDraw.Draws = deserialized.draws;
                                var exists = DayDraws.Contains<DayDraw>(currentDraw); // check day's draws
                                if (exists == false)
                                {
                                    var daysCurrentDraw = DayDraws.SingleOrDefault(d => d.Date == this.LowDate); //check for same day draws
                                    if (currentDraw != null && DaydrawDateComparer.IsEqual(currentDraw, daysCurrentDraw))
                                    {
                                        DayDraws.Remove(daysCurrentDraw); //remove previous day's draws
                                        DayDraws.Add(currentDraw); //update with latest downloads
                                    }
                                    else
                                    {
                                        DayDraws.Add(currentDraw);
                                    }
                                }

                                var list = deserialized.draws.draw;                                
                                foreach (var d in list)
                                {
                                    draws.Add(d);                                    
                                }

                                firstBatchcounter++; //indicates data has come in, ready to load first batch                                

                                // NEW for v.1.0.4
                                this.MainPivot.IsEnabled = true;
                                this.MainPivot.IsLocked = false;
                                this.MainPivot.IsHitTestVisible = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            var innerException = ex.InnerException;
                            if (innerException is WebException)
                            {
                                var we = innerException as WebException;
                                var message = string.Empty;
                                if (we.Response is HttpWebResponse)
                                {
                                    switch (((HttpWebResponse)we.Response).StatusCode)
                                    {
                                        case HttpStatusCode.NotFound:
                                            message = "The requested resource does not exist on the server";
                                            break;
                                        case HttpStatusCode.InternalServerError:
                                            message = "The server returned an internal error";
                                            break;
                                        case HttpStatusCode.BadRequest:
                                            message = "The request could not be understood by the server";
                                            break;
                                        default:
                                            message = "Something went wrong";
                                            break;
                                    }
                                    this.progress.Visibility = System.Windows.Visibility.Collapsed;
                                    this.progressStatus.Visibility = System.Windows.Visibility.Collapsed;
                                    this.resultsPivot.IsEnabled = true;
                                    MessageBoxResult error = MessageBox.Show("Please check with OPAP web services.", message, MessageBoxButton.OK);
                                    SystemTray.SetProgressIndicator(this, null);
                                    SystemTray.SetIsVisible(this, false);

                                    // new in 1.0.5
                                    if (error == MessageBoxResult.OK)
                                    {
                                        this.NavigationService.GoBack();
                                    }
                                }
                            }
                            else if (innerException is OperationCanceledException)
                            {                                
                                this.progress.Value = 0.0;
                                this.progressStatus.Text = "Downloading of draw results canceled";
                            }
                        }
                    }
                    dateCounter++;
                }
                return draws;     
        });
        }

        /// <summary> Fetches a list asynchronoiusly as a task object iterating over the given date range. Reports progress percentage and cancels the operation
        /// upon user cancellation.
        /// </summary>
        /// <param name="dates"></param>
        /// <param name="progress"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        public Task<List<Draw>> GetDrawsTaskAsync(List<string> dates, IProgress<double> progress, CancellationToken ct)
        {
            //Holds the selector items source 
            var draws = new List<Draw>();                                          
            var dateCounter = 0;

            return Task<List<Draw>>.Run(async () =>
            {
                // Fetch data
                foreach (var date in dates)
                {
                    // Cancellation token requested check : throw exception to propagate to caller and cancel calculation
                    if (ct.IsCancellationRequested)
                    {
                        progress.Report(0);
                        ct.ThrowIfCancellationRequested();                        
                    }

                    // initialize how to receive data using web client and KINO url and REST service
                    string uri = "http://applications.opap.gr/DrawsRestServices/kino/drawDate/" + date + ".json";
                    WebClient client = new WebClient();
                    client.Headers["Accept"] = "application/json";
                    var profile = NetworkInformation.GetInternetConnectionProfile();
                    if (profile == null)
                    {
                        throw new KinoExceptions.InternetConnectionProfileException("No internet connection available");
                    }

                    // wait to complete the json download before parsing                                              
                    var task = GetJsonDrawStringTaskAsync(client, new Uri(uri), progress, ct, dateCounter);
                    string Result = await task;
                    var jsonObj = JObject.Parse(Result);
                    var deserialized = JsonConvert.DeserializeObject<RootObject>(Result);

                    // handle exceptions from web client object when receiving data from url
                    if (Result == "{}")
                    {
                        progress.Report(0);
                        throw new KinoExceptions.InvalidDrawDateException("The date of the draw is invalid");
                    }
                    if (deserialized.draws.draw == null)
                    {
                        progress.Report(0);
                        throw new KinoExceptions.InvalidKinoGameUrlException("The game given in the URL of web services is invalid");

                    }
                    else if (deserialized.draws.draw != null && deserialized.draws.draw.Count == 0)
                    {
                        /*
                        if (this.Sequences.Count == 0)  //in case already downloaded some draws
                        {
                            progress.Report(0);
                            throw new KinoExceptions.NoDrawFoundException("No draws found");
                        }
                        */
                        if (this.DownloadedSequences.Count == 0)  //in case already downloaded some draws
                        {
                            progress.Report(0);
                            throw new KinoExceptions.NoDrawFoundException("No draws found");
                        }
                    }

                    // if draws found set date counter to report progress
                    //dateCounter++;

                    // download ok, process draws: if date range is used remove first day's draws where time is earlier that last day's last draw time
                    var draw = deserialized.draws.draw;

                    
                    foreach (var d in draw)
                    {
                        // add to sequence set
                        var sequence = new DownloadedSequence();                        
                        foreach (var item in d.results)
                        {
                            sequence.Add(item);
                        }
                        this.DownloadedSequences.Add(sequence);
                        
                    }
                    

                    // check for same day draws
                    DayDraw currentDraw = new DayDraw();
                    currentDraw.Date = this.LowDate;
                    currentDraw.Draws = deserialized.draws;
                    var exists = DayDraws.Contains<DayDraw>(currentDraw); // check day's draws
                    if (exists == false)
                    {
                        var daysCurrentDraw = DayDraws.SingleOrDefault(d => d.Date == this.LowDate); //check for same day draws
                        if (currentDraw != null && DaydrawDateComparer.IsEqual(currentDraw, daysCurrentDraw))
                        {
                            DayDraws.Remove(daysCurrentDraw); //remove previous day's draws
                            DayDraws.Add(currentDraw); //update with latest downloads
                        }
                        else
                        {
                            DayDraws.Add(currentDraw);
                        }
                    }

                    // add to task's returning draw list
                    var list = deserialized.draws.draw;
                    foreach (var d in list)
                    {
                        // add to draws list
                        draws.Add(d);
                    }

                    //indicates data has come in, ready to load first batch  
                    firstBatchcounter++;  
                    
                    // up the date counter to propagate to progress indicator report
                    dateCounter++;
                }
                return draws;
            });
        }
        

        private ObservableCollection<NumberItem> ResultItems(Draw d)
        {
            var collection = new ObservableCollection<NumberItem>();
            if (d != null && d.results.Count > 0)
            {
                var bonusNumber = d.results[d.results.Count - 1];
                foreach (var n in d.results)
                {
                    var item = new NumberItem();
                    item.Value = n;
                    item.IsBonusNumber = false;
                    if (item.Value.Equals(bonusNumber))
                    {

                        //paint different backcolo in UI                    
                        RadialGradientBrush radialGradientBrush = new RadialGradientBrush();
                        GradientStop blackGS = new GradientStop();
                        blackGS.Color = ((SolidColorBrush)Application.Current.Resources["PhoneAccentBrush"]).Color;
                        blackGS.Offset = 0.0;
                        radialGradientBrush.GradientStops.Add(blackGS);
                        GradientStop phoneAccentBrushGS = new GradientStop();
                        var brush = (SolidColorBrush)Application.Current.Resources["PhoneAccentBrush"];
                        var color = brush.Color;
                        phoneAccentBrushGS.Color = color;
                        phoneAccentBrushGS.Offset = 1.0;
                        radialGradientBrush.GradientStops.Add(phoneAccentBrushGS);
                        item.BackColor = radialGradientBrush;
                        item.IsBonusNumber = true;
                    }
                    else
                    {
                        item.BackColor = (SolidColorBrush)Application.Current.Resources["PhoneBackgroundBrush"];
                    }

                    collection.Add(item);
                }
                var ordered = collection.OrderBy(t => t.Value);
                collection = new System.Collections.ObjectModel.ObservableCollection<NumberItem>(ordered.ToList());
                return collection;
            }
            return null;
        }

        /// <summary> Click event to request cancelation of downloading draw results
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DownloadCancelBtn_Click(object sender, RoutedEventArgs e)
        {
            // request cancellation of sequences download
            sequence_cts.Cancel();
        }

        #region OLD Pairs calculation stuff

        /// <summary> Calculates pairs and frequencies using this instance's collection of Sequences and returns the enumerated pair collection.        
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Pair> CalculatePairs()
        {
            var sequences = this.Sequences;
            if (sequences == null)
                throw new ArgumentException("Collection cannot be null.");

            Stopwatch sw = Stopwatch.StartNew();
            foreach (var s in sequences)
            {
                var row = 0;
                var numbers = s.Numbers;
                var count = numbers.Count();
                var limit = count - 2;
                do
                {
                    var secondItemPos = row + 1;
                    do
                    {
                        var firstItemPos = row;
                        var firstItem = numbers.ElementAt(firstItemPos);
                        var secondItem = numbers.ElementAt(secondItemPos);
                        var sequence = new Sequence();
                        sequence.Numbers.Add(firstItem);
                        sequence.Numbers.Add(secondItem);

                        // initialize new pair object and frequency
                        var frequency = 1;
                        var pair = new Pair() { Frequency = frequency, NumberOne = firstItem, NumberTwo = secondItem }; // new Pair object

                        // find existing pair in the collection                        
                        var item = this.pvm.FuckingPairs.FirstOrDefault(t => (t.NumberOne == pair.NumberOne && t.NumberTwo == pair.NumberTwo)
                            || (t.NumberOne == pair.NumberTwo && t.NumberTwo == pair.NumberOne));
                        if (item != null)
                        {
                            // increase current frequency of found pair, replace item
                            var currentFrequency = item.Frequency;
                            currentFrequency++;
                            item.Frequency = currentFrequency;
                            /*
                            if (this.pvm.FuckingPairs.Count  < 3)
                            {
                                this.pvm.Pairs.Add(item);
                            }
                            */
                        }
                        else
                        {
                            // add new pair object
                            this.pvm.FuckingPairs.Add(pair);
                            /*
                            if (this.pvm.FuckingPairs.Count < 3)
                            {
                                this.pvm.Pairs.Add(pair);
                            } 
                            */
                        }

                        // increase pairs added counter in view model
                        //this.pvm.PairsCounter++;
                        //this.PairLoader.Value = this.pvm.PairsCounter;
                        secondItemPos++; // get next second item
                    }
                    while (secondItemPos < count && numbers != null);
                    secondItemPos = 0;
                    row++; // get next row in sequences

                } while (row < limit && numbers != null);
            }

            sw.Stop();
            var timer = sw.Elapsed.Seconds;
            // return top ten element with the highest frequency value
            //return this.ObsPairs.OrderByDescending(t => t.Frequency).Take(10);            
            //var take10 = (IEnumerable<Pair>)this.pvm.FuckingPairs.OrderByDescending(t => t.Frequency).Take(10);
            /*
            this.pvm.Pairs.Clear();
            foreach(var p in take10)
            {
                this.pvm.Pairs.Add(p);
            }
            */
            return this.pvm.FuckingPairs;

        }

        public void FuckedUpLinq()
        {
            var sequences = this.Sequences;
            if (sequences == null)
                throw new ArgumentException("Collection cannot be null.");


            Stopwatch sw = Stopwatch.StartNew();

            foreach (var s in sequences)
            {
                var row = 0;
                var numbers = s.Numbers;
                var count = numbers.Count();
                var limit = count - 2;
                do
                {
                    var secondItemPos = row + 1;
                    do
                    {
                        var firstItemPos = row;
                        var firstItem = numbers.ElementAt(firstItemPos);
                        var secondItem = numbers.ElementAt(secondItemPos);
                        var sequence = new Sequence();
                        sequence.Numbers.Add(firstItem);
                        sequence.Numbers.Add(secondItem);

                        // initialize new pair object and frequency
                        var frequency = 1;
                        var pair = new Pair() { Frequency = frequency, NumberOne = firstItem, NumberTwo = secondItem }; // new Pair object

                        // find existing pair in the collection
                        var existing = this.pvm.FuckingPairs.SingleOrDefault(t => t.NumberOne == pair.NumberOne && t.NumberTwo == pair.NumberTwo);
                        if (existing != null)
                        {
                            // increase current frequency of found pair, replace item
                            var currentFrequency = existing.Frequency;
                            currentFrequency++;
                            existing.Frequency = currentFrequency;
                            if (this.pvm.FuckingPairs.Count < 3)
                            {
                                this.pvm.Pairs.Add(existing);
                            }

                        }
                        else
                        {
                            // add new pair object
                            this.pvm.FuckingPairs.Add(pair);
                            if (this.pvm.FuckingPairs.Count < 3)
                            {
                                this.pvm.Pairs.Add(pair);
                            }
                        }


                        secondItemPos++; // get next second item
                    }
                    while (secondItemPos < count && numbers != null);
                    secondItemPos = 0;
                    row++; // get next row in sequences

                } while (row < limit && numbers != null);
            }

            sw.Stop();
            var timer = sw.Elapsed.Seconds;
            // return top ten element with the highest frequency value
            //return this.ObsPairs.OrderByDescending(t => t.Frequency).Take(10);
            var take10 = (IEnumerable<Pair>)this.pvm.FuckingPairs.OrderByDescending(t => t.Frequency).Take(10);
            this.pvm.Pairs.Clear();
            foreach (var p in take10)
            {
                this.pvm.Pairs.Add(p);
            }


        }

        public void DictionaryPairsDude()
        {
            var sequences = this.Sequences;
            if (sequences == null)
                throw new ArgumentException("Collection cannot be null.");

            //pairs stuff
            //this.PairsSelector.ItemsSource = new List<Pair>();


            Stopwatch sw = Stopwatch.StartNew();

            foreach (var s in sequences)
            {
                var row = 0;
                var numbers = s.Numbers;
                var count = numbers.Count();
                var limit = count - 2;
                do
                {
                    var secondItemPos = row + 1;
                    do
                    {
                        var firstItemPos = row;
                        var firstItem = numbers.ElementAt(firstItemPos);
                        var secondItem = numbers.ElementAt(secondItemPos);
                        var sequence = new Sequence();
                        sequence.Numbers.Add(firstItem);
                        sequence.Numbers.Add(secondItem);

                        // initialize new pair object and frequency
                        var frequency = 1;
                        var pair = new Pair() { Frequency = frequency, NumberOne = firstItem, NumberTwo = secondItem }; // new Pair object

                        // find existing pair in the collection
                        //var existing = this.pvm.FuckingPairs.SingleOrDefault(t => t.NumberOne == pair.NumberOne && t.NumberTwo == pair.NumberTwo);
                        //var existing = this.pvm.FuckingPairs.Where(t => t.NumberOne == pair.NumberOne && t.NumberTwo == pair.NumberTwo);
                        //var exists = this.pvm.FuckingPairs.Contains(pair, new PairComparer());
                        //var item = this.pvm.FuckingPairs.FirstOrDefault(t => (t.NumberOne == pair.NumberOne && t.NumberTwo == pair.NumberTwo)
                        //    || (t.NumberOne == pair.NumberTwo && t.NumberTwo == pair.NumberOne));
                        try
                        {
                            var fr = dictionary[pair];
                            // increase current frequency of found pair, replace item
                            var currentFrequency = fr;
                            currentFrequency++;
                            dictionary[pair] = currentFrequency;
                            pair.Frequency++;

                            if (this.dictionary.Count < 3)
                            {
                                this.pvm.Pairs.Add(pair);
                            }

                        }
                        catch (KeyNotFoundException e)
                        {
                            dictionary[pair] = frequency;
                            if (this.dictionary.Count < 3)
                            {
                                this.pvm.Pairs.Add(pair);
                            }
                            //break;
                        }


                        secondItemPos++; // get next second item
                    }
                    while (secondItemPos < count && numbers != null);
                    secondItemPos = 0;
                    row++; // get next row in sequences

                } while (row < limit && numbers != null);
            }

            sw.Stop();
            var timer = sw.Elapsed.Seconds;
            // return top ten element with the highest frequency value
            //return this.ObsPairs.OrderByDescending(t => t.Frequency).Take(10);
            //var take10 = (IEnumerable<Pair>)this.pvm.FuckingPairs.OrderByDescending(t => t.Frequency).Take(10);
            var take10 = (IEnumerable<Pair>)this.dictionary.OrderByDescending(t => t.Key).Take(10);
            this.pvm.Pairs.Clear();
            foreach (var p in take10)
            {
                this.pvm.Pairs.Add(p);
            }


        }

        #endregion old stuff


    }
}