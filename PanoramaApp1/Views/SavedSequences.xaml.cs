﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Kino.IsolatedSettings;
using Kino.Binding;
using System.Windows.Media;
using System.Collections.ObjectModel;
using KinoGenerator;
using Kino.ControlTiltEffect;
using Microsoft.Phone.Controls.Primitives;

namespace Kino.Views
{
    public partial class SavedSequences : PhoneApplicationPage
    {
        // Holds the isolated storage kino settings
        private IsoKinoSettings isolatedKinoSettings;

        public SavedSequences()
        {  
            InitializeComponent();
            SystemTray.SetIsVisible(this, false);
            InitializeIsolatedKinoSettings();            
            InitializeAppBarProperties();
        }

        /// <summary>
        /// Initializes the class to hold the isolated application settings of kino generator app.
        /// </summary>
        private void InitializeIsolatedKinoSettings()
        {
            // Get kino settings key from iso storage if any, otherwise create it
            isolatedKinoSettings = new IsoKinoSettings();
            var kinoSettings = isolatedKinoSettings.KinoSettings;

            // key was not found, create one
            if (kinoSettings == null)
            {
                isolatedKinoSettings.KinoSettings = new KinoSettings();
            }
        }

        /// <summary>
        /// Sets the value of the items source of stored sequences selector control.
        /// </summary>
        private void SetItemsSource()
        {
            StoredSequencesSelector.DataContext = isolatedKinoSettings.KinoSettings;
            StoredSequencesSelector.SetBinding(LongListMultiSelector.ItemsSourceProperty, 
                new System.Windows.Data.Binding(){ Mode = System.Windows.Data.BindingMode.TwoWay, 
                Path = new PropertyPath("ObservableSavedSequences")});

            //Set binding to control to display the count of saved items
            this.SavedSequencesCounter.DataContext = isolatedKinoSettings.KinoSettings;       
            this.SavedSequencesCounter.SetBinding(TextBlock.TextProperty, new System.Windows.Data.Binding() 
                { Mode = System.Windows.Data.BindingMode.OneWay, Path = new PropertyPath("SavedSequencesCount")});            
        }

        private void StoredSequencesSelector_Loaded(object sender, RoutedEventArgs e)
        {
            // Attach bindings
            SetItemsSource();

            if (StoredSequencesSelector.ItemsSource.Count == 0)
                ApplicationBar.IsVisible = false;
            else
                ApplicationBar.IsVisible = true;

            if (StoredSequencesSelector.ItemsSource != null && StoredSequencesSelector.ItemsSource.Count > 0)
                CreateSelectAppBarButton();
        }

        private void InitializeAppBarProperties()
        {
            ApplicationBar = new ApplicationBar();
            ApplicationBar.Mode = ApplicationBarMode.Default;
            ApplicationBar.Opacity = 1.0;
            ApplicationBar.IsVisible = true;
            ApplicationBar.IsMenuEnabled = true;
            //UpdateSelectAllAppBarMenuItem();
        }

        private void CreateDeleteAppBarButton()
        {
            ApplicationBarIconButton button2 = new ApplicationBarIconButton();
            button2.IconUri = new Uri("/Assets/Icons/delete.png", UriKind.Relative);
            button2.Text = "delete";
            ApplicationBar.Buttons.Add(button2);
            button2.Click += new EventHandler(DeleteButton_Click);
            button2.IsEnabled = true;
        }

        private void CreateSelectAppBarButton()
        {
            ApplicationBarIconButton button1 = new ApplicationBarIconButton();
            button1.IconUri = new Uri("/Assets/Icons/manage.png", UriKind.Relative);
            button1.Text = "select";
            ApplicationBar.Buttons.Add(button1);
            button1.Click += new EventHandler(SelectButton_Click);
            button1.IsEnabled = true;
        }

        private void CreateSelectAllAppBarButton()
        {
            ApplicationBarIconButton button = new ApplicationBarIconButton();
            button.IconUri = new Uri("/Assets/Icons/manage.png", UriKind.Relative);
            button.Text = "select all";
            ApplicationBar.Buttons.Add(button);
            button.Click += new EventHandler(SelectAllButton_Click);
            button.IsEnabled = true;
        }

        private void UpdateSelectAllAppBarMenuItem()
        {
            if (ApplicationBar != null && ApplicationBar.IsVisible)
            {
                ApplicationBar.MenuItems.Clear();
                ApplicationBarMenuItem menu = new ApplicationBarMenuItem();
                menu.Text = "select all";
                ApplicationBar.MenuItems.Add(menu);
                menu.Click += new EventHandler(SelectAllMenuITem_Click);
                if(StoredSequencesSelector.IsSelectionEnabled)                
                    menu.IsEnabled = true;               
            }
        }

        private void UpdateUnSelectAllAppBarMenuItem()
        {
            if (ApplicationBar != null && ApplicationBar.IsVisible)
            {
                ApplicationBar.MenuItems.Clear();
                ApplicationBarMenuItem menu = new ApplicationBarMenuItem();
                menu.Text = "unselect all";
                ApplicationBar.MenuItems.Add(menu);
                menu.Click += new EventHandler(UnSelectAllMenuITem_Click);
                if (StoredSequencesSelector.IsSelectionEnabled)
                    menu.IsEnabled = true;
            }
        } 

        private void SelectButton_Click(object sender, EventArgs e)
        {
            // Enable selection mode            
            UpdateSelectAllAppBarMenuItem();
            StoredSequencesSelector.IsSelectionEnabled = true;            
        }

        private void SelectAllButton_Click(object sender, EventArgs e)
        {
            var count = StoredSequencesSelector.ItemsSource.Count;
            for (var i = 0; i < count; i++)
            {                
                var item = StoredSequencesSelector.ItemsSource[i];
                var container = StoredSequencesSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                if (container == null)
                {
                    // item has't been assigned to UI
                    StoredSequencesSelector.ScrollTo(item);
                    container = StoredSequencesSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                }
                container.IsSelected = true;
            }

            UpdateUnSelectAllAppBarMenuItem();
        }

        private void SelectAllMenuITem_Click(object sender, EventArgs e)
        {
            var count = StoredSequencesSelector.ItemsSource.Count;
            for (var i = 0; i < count; i++)
            {
                var item = StoredSequencesSelector.ItemsSource[i];
                var container = StoredSequencesSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                if (container == null)
                {
                    // item has't been assigned to UI
                    StoredSequencesSelector.ScrollTo(item);
                    container = StoredSequencesSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                }
                container.IsSelected = true;
            }
            UpdateUnSelectAllAppBarMenuItem();
        }

        private void UnSelectAllMenuITem_Click(object sender, EventArgs e)
        {
            var count = StoredSequencesSelector.ItemsSource.Count;
            for (var i = 0; i < count; i++)
            {
                var item = StoredSequencesSelector.ItemsSource[i];
                var container = StoredSequencesSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                if (container == null)
                {
                    // item has't been assigned to UI
                    StoredSequencesSelector.ScrollTo(item);
                    container = StoredSequencesSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                }
                container.IsSelected = false;
            }

            ApplicationBar.MenuItems.Clear();

        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            // copy iso storage list
            var storedSequences = isolatedKinoSettings.KinoSettings.SavedSequences;
            var count = StoredSequencesSelector.ItemsSource.Count;
            for (var i = 0; i < count; i++)
            {
                var item = StoredSequencesSelector.ItemsSource[i];
                var container = StoredSequencesSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                if(container != null && container.IsSelected)
                {
                    var sequence = item as SavedSequence;
                    var match = storedSequences.SingleOrDefault(c => CompareSequence.IsEqual(c, sequence));
                    if (match != null)
                        storedSequences.Remove(match);
                }                
            }

            //Reset iso storage list value to propagate to the binding target
            isolatedKinoSettings.KinoSettings.SavedSequences = storedSequences;
            StoredSequencesSelector.UpdateLayout();           
            
            // Disable the foreach loop in case the tilt effect is not enabled after item deletion
            /*
            // Create fade up transition for remaining items except at index 0 position
            foreach (var item in StoredSequencesSelector.ItemsSource)
            {
                var container = StoredSequencesSelector.ContainerFromItem(item) as LongListMultiSelectorItem;
                var index = StoredSequencesSelector.ItemsSource.IndexOf(item);
                if (index != 0)
                {
                    SlideTransition slide = new SlideTransition();
                    slide.Mode = SlideTransitionMode.SlideUpFadeIn;
                    ITransition trans = slide.GetTransition(container);
                    trans.Completed += delegate { trans.Stop(); };
                    trans.Begin();
                }                
            }            
            */

            if (StoredSequencesSelector.ItemsSource.Count == 0)
            {
                ApplicationBar.IsVisible = false;
                StoredSequencesSelector.IsSelectionEnabled = false;
            }
        }

        private void StoredSequencesSelector_IsSelectionEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
			var selector = sender as LongListMultiSelector;
            //var layout = selector.LayoutMode;
            //var gridCellSize = selector.GridCellSize;

            // Update app bar buttons
            if ((bool)e.NewValue == true)
            {
                //selector.GridCellSize = new Size(0, 0);
                selector.LayoutMode = LongListSelectorLayoutMode.List;
                ApplicationBar.Buttons.RemoveAt(0);              
            }
            else
            {
                selector.GridCellSize = new Size(460,55);
                selector.LayoutMode = LongListSelectorLayoutMode.Grid;

                if (ApplicationBar.Buttons.Count > 0)
                {
                    ApplicationBar.Buttons.Clear();
                }
                ApplicationBar.IsVisible = true;
                ApplicationBar.MenuItems.Clear();
                CreateSelectAppBarButton();
            }
        }

        private void StoredSequencesSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selector = sender as LongListMultiSelector;
            if(e.AddedItems.Count > 0)
            {
                ApplicationBar.IsVisible = true;                
                if (e.AddedItems.Count == 1 && selector.SelectedItems.Count == 1)
                {
                    CreateDeleteAppBarButton();                    
                }                
                if (selector.SelectedItems.Count == selector.ItemsSource.Count)
                    UpdateUnSelectAllAppBarMenuItem();
            }
            else if (e.RemovedItems.Count == 1)
            {
                UpdateSelectAllAppBarMenuItem();
                if (!selector.IsSelectionEnabled)
                    ApplicationBar.MenuItems.Clear();                
            }
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if(StoredSequencesSelector.IsSelectionEnabled)
            {
                e.Cancel = true;
                StoredSequencesSelector.IsSelectionEnabled = false;
            }
        }


        private void StoredSequencesSelector_Hold(object sender, System.Windows.Input.GestureEventArgs e)
        {            
            var selector = sender as LongListMultiSelector;
            var source = e.OriginalSource as Border;
            source.OpacityMask = (Brush)Application.Current.Resources["PhoneDisabledBrush"];                                    
            var container = selector.ContainerFromItem(source) as LongListMultiSelectorItem;
            selector.OpacityMask = (Brush)Application.Current.Resources["PhoneSubtleBrush"];


        }

        private void StoredSequencesSelector_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            
            var handled = e.Handled;
            var selector = sender as LongListMultiSelector;
            var source = e.OriginalSource as Border;
            var grid = source.FindName("LayoutRoot") as Grid;
            //grid.OpacityMask = (Brush)Application.Current.Resources["PhoneSubtleBrush"];
            var container = selector.ContainerFromItem(source) as LongListMultiSelectorItem;
            //container.OpacityMask = (Brush)Application.Current.Resources["PhoneSubtleBrush"];
            source.OpacityMask = (Brush)Application.Current.Resources["PhoneSubtleBrush"];
            
        }


        private void DeleteEntry_Click(object sender, RoutedEventArgs e)
        {
            var button = sender;
            var menuItem = sender as MenuItem;
            // Get data context as data bound object : clicked SavedSequence item
            SavedSequence selected = (menuItem).DataContext as SavedSequence;            
            MessageBoxResult result = MessageBox.Show("do you want to delete the sequence?", "Delete sequence?", MessageBoxButton.OKCancel);
            if(result == MessageBoxResult.OK)
            {
                var savedSequences = isolatedKinoSettings.KinoSettings.SavedSequences;
                var match = savedSequences.SingleOrDefault(c => CompareSequence.IsEqual(c, selected));
                if (match == null)
                    return;
                else
                {
                    savedSequences.Remove(match);                    
                    isolatedKinoSettings.KinoSettings.SavedSequences = savedSequences; // reset list
                    StoredSequencesSelector.UpdateLayout();
                    if (StoredSequencesSelector.ItemsSource.Count == 0)
                    {
                        ApplicationBar.IsVisible = false;
                        StoredSequencesSelector.IsSelectionEnabled = false;
                    } 
                }
            }
        }


    }
}