﻿#pragma checksum "C:\Users\Administrator\Documents\Visual Studio 2013\Projects\KinoApplication\KinoPalApp\PanoramaApp1\Views\AddNumbers.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "4174580BC9E58662EC379454FB95636E"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Kino {
    
    
    public partial class AddNumbers : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal Microsoft.Phone.Controls.Pivot KinoPalPivot;
        
        internal Microsoft.Phone.Controls.PivotItem AddNumbersPivotItem;
        
        internal Microsoft.Phone.Controls.LongListMultiSelector AddNumbersSelector;
        
        internal System.Windows.Controls.Primitives.Popup MaxSelectedReachedPopup;
        
        internal System.Windows.Controls.Grid PopupGrid;
        
        internal System.Windows.Controls.Button MaxReachedOKBtn;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/KinoPalApp;component/Views/AddNumbers.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.KinoPalPivot = ((Microsoft.Phone.Controls.Pivot)(this.FindName("KinoPalPivot")));
            this.AddNumbersPivotItem = ((Microsoft.Phone.Controls.PivotItem)(this.FindName("AddNumbersPivotItem")));
            this.AddNumbersSelector = ((Microsoft.Phone.Controls.LongListMultiSelector)(this.FindName("AddNumbersSelector")));
            this.MaxSelectedReachedPopup = ((System.Windows.Controls.Primitives.Popup)(this.FindName("MaxSelectedReachedPopup")));
            this.PopupGrid = ((System.Windows.Controls.Grid)(this.FindName("PopupGrid")));
            this.MaxReachedOKBtn = ((System.Windows.Controls.Button)(this.FindName("MaxReachedOKBtn")));
        }
    }
}

