﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Kino.Data
{
    /// <summary>
    /// Represents a generated number object by KINO generator
    /// </summary>
    public class NumberObject : INotifyPropertyChanged
    {
        public int? Value { get; set; }
        public string Tag { get; set; }
        public Brush BackColor { get; internal set; }
        public Brush TextColor { get; internal set; }
 
        public NumberObject() { }

        public NumberObject(int? value)
        {
            this.Value = value;
        }

        public NumberObject(int? value, string tag)
        {
            Value = value;
            Tag = tag;
            if (value == null)
            {
                BackColor = (Brush)Application.Current.Resources["PhoneInactiveBrush"];
            }
            else
            {
                BackColor = (Brush)Application.Current.Resources["PhoneAccentBrush"];
                TextColor = (Brush)Application.Current.Resources["PhoneForegroundBrush"];
            }
        }

        public NumberObject(int? value, string tag, Brush brush )
        {
            Value = value;
            Tag = tag;
            BackColor = brush;
        }

        public NumberObject(int? value, string tag, Brush brush, Brush textColor)
        {
            Value = value;
            Tag = tag;
            BackColor = brush;
            TextColor = textColor;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
