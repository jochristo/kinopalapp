﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Kino.Data
{
    /// <summary>
    /// Class for winning numbers grid items
    /// </summary>
    public class WinningNumberItem
    {
        public WinningNumberItem(int value)
        {
            this.Value = value;
            this.Text = value.ToString();
            ContentText = this.Text;
        }

        /// <summary>
        /// Get the Value of the item
        /// </summary>
        public int Value
        {
            get;
            private set;
        }

        /// <summary>
        /// Get the string value of the Value property
        /// </summary>
        public string Text
        {
            get;
            private set;
        }

        public object ContentText
        {
            get;
            private set;
        }


    }

    /// <summary>
    /// Populate collection with numbers data from one to 80
    /// </summary>
    public class WinningNumbersCollection : ObservableCollection<WinningNumberItem>
    {
        public WinningNumbersCollection()
        {
            for (var i = 1; i < 81; i++)
            {
                this.Add(new WinningNumberItem(i));
            }
        }
    }

    public static class SelectorItems
    {
        public static List<ListBoxItem> Numbers()
        {
            var list = new List<ListBoxItem>();
            var numcollection = new WinningNumbersCollection();
            foreach (var item in numcollection)
            {
                var listBoxItem = new ListBoxItem();
                listBoxItem.Content = item.Value;
                listBoxItem.Height = 90;
                listBoxItem.Width = 90;
                listBoxItem.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                list.Add(listBoxItem);
            }
            return list;
        }
    }


    
}
