﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Kino.Binding
{
    public class AddedNumberItem : INotifyPropertyChanged
    {
        private int _value;
        private string _text;
        private bool _isSelected;        

        public AddedNumberItem()
        {

        }

        public AddedNumberItem(int i)
        {
            this._value = i;
            this._text = i.ToString();
            this._isSelected = true;
        }

        public int Value
        {
            get
            {
                return this._value;
            }
            set
            {
                this._value = value;
                NotifyPropertyChanged("Value");
            }
        }

        public string text
        {
            get
            {
                return this._text;
            }
            set
            {
                this._text = value;
                NotifyPropertyChanged("text");
            }
        }

        public bool IsSelected
        {
            get
            {
                return this._isSelected;
            }
            set
            {
                this._isSelected = value;
                NotifyPropertyChanged("IsSelected");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        } 
    }
}
