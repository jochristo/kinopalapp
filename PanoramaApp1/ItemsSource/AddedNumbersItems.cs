﻿using Kino.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Binding
{
    /// <summary>
    /// Represents an items source collection of Kino.Binding.AddedNumberItem objects used in data binding.
    /// </summary>
    public class AddedNumbersItems : ObservableCollection<AddedNumberItem>
    {
        public AddedNumbersItems()
        {
            for (var i = 1; i < 81; i++)
            {
                this.Add(new AddedNumberItem(i));
            }
        }
    }
}
