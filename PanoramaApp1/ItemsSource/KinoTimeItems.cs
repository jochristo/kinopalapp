﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Kino.ItemsSource
{

    public class HourItems : List<HourItem>
    {
        public HourItems()
        {
            for (var i = 9; i <= 22; i++)
            {
                var hi = new HourItem();
                var item = i.ToString();                
                if (i < 10)
                    item = "0" + i.ToString();

                hi.Value = item;                
                this.Add(hi);
            }
        }
    }

    public class HourItem : INotifyPropertyChanged
    {
        public int Index { get; set; }
        public string Value { get; set; }
        //private Dictionary<int, string> _lookup;
        private Brush _selected;

        public HourItem()
        {
            this.Value = DateTime.Now.ToString("HH");
        }

        public Brush Selected
        {
            get{return _selected;} 
            set{ _selected = value; NotifyPropertyChanged("Selected"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }

    public class MinutesItem : INotifyPropertyChanged
    {
        public int Index { get; set; }
        public string Value { get; set; }
        //private Dictionary<int, string> _lookup;
        private Brush _selected;

        public MinutesItem()
        {
            this.Value = "00";
        }

        public MinutesItem(string value)
        {
            this.Value = value;
        }

        public Brush Selected
        {
            get { return _selected; }
            set { _selected = value; NotifyPropertyChanged("Selected"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }

    public class MinutesItems : List<MinutesItem>
    {
        public MinutesItems()
        {
            this.Add(new MinutesItem("00"));
            this.Add(new MinutesItem("05"));
            this.Add(new MinutesItem("10"));
            this.Add(new MinutesItem("15"));
            this.Add(new MinutesItem("20"));
            this.Add(new MinutesItem("25"));
            this.Add(new MinutesItem("30"));
            this.Add(new MinutesItem("35"));
            this.Add(new MinutesItem("40"));
            this.Add(new MinutesItem("45"));
            this.Add(new MinutesItem("50"));
            this.Add(new MinutesItem("55"));
        }
    }








    public class KinoHours : List<string>
    {
        public KinoHours()
        {
            for (var i = 9; i <= 22; i++)
            {
                var item = i.ToString();
                if (i < 10)
                    item = "0" + i.ToString();

                this.Add(item.ToString());
            }
        }
    }

    public class KinoMinutes : List<string>
    {
        public KinoMinutes()
        {
            for (var i = 0; i <= 59; i++ )
            {
                var number = i;
                var item = i.ToString();
                if (i == 0)
                    item = "0" + i.ToString();
                else
                {
                    
                    if (i == 55)
                        break;
                    i += 5;
                }
                this.Add(item.ToString());
            }
               
                       
        }
    }


}
