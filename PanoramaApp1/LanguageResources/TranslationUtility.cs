﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.LanguageResources
{
    /// <summary>
    /// It provides the resources to alter the app's labels and custom messages to specified language.
    /// </summary>
    public class TranslationUtility
    {
        public class GeneratorPivot
        {
            private Dictionary<string,string> _dictionary;
            public Dictionary<string,string> Dictionary
            {
                get { return _dictionary; }
                set { _dictionary = value; }
            }

            protected void RealizeItems()
            {
                _dictionary.Add("Generator","Μηχανή αριθμών");
                _dictionary.Add("Generator", "Μηχανή αριθμών");
            }
        }
    }
}
