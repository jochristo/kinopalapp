﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using System.Threading;
using Kino.Data;
using System.Collections.ObjectModel;
using Kino.Binding;
using Kino.IsolatedSettings;
using KinoGenerator;
using Microsoft.Phone.Tasks;

namespace Kino
{
    public partial class GeneratorPage : PhoneApplicationPage
    {
        /// <summary>
        /// Private fields
        /// </summary>
        private KinoGenerator.Kino kino;
        private string msg = string.Empty;
        private KinoSettings _KinoSettings;

        #region ApplicationBar Menu Items
        private ApplicationBarMenuItem saveNumsMenuItem = new ApplicationBarMenuItem();
        private ApplicationBarMenuItem viewStoredNumsMenuItem = new ApplicationBarMenuItem();
        private ApplicationBarMenuItem viewStatsMenuItem = new ApplicationBarMenuItem();
        private ApplicationBarMenuItem addNumbersMenuItem = new ApplicationBarMenuItem();
        #endregion

        public KinoSettings KinoSettings
        {
            get {return _KinoSettings;}
            set { _KinoSettings = value; }
        }
                
        // Store gesture event arguments for game type button template container
        private System.Windows.Input.GestureEventArgs gea = null;

        // Holds the isolated storage kino settings
        private IsoKinoSettings isolatedKinoSettings;
        
        /// <summary>
        /// Generator page contructor.
        /// </summary>
        public GeneratorPage()
        {
            // Initialize component first
            InitializeComponent();
            
			// hide status bar
            SystemTray.SetIsVisible(this, false);
			
            // Construct new app bar
            ApplicationBar = new ApplicationBar();
            InitializeApplicationBar();
			
            // Initialize sequence length list
            //InitSequenceLength();

            // construct new kino object
            kino = new KinoGenerator.Kino();
                       
            // disable visibility
            //this.RecFuncDescription.Visibility = System.Windows.Visibility.Collapsed;

            // Set buttons event handlers: Tap, LostFocus
            SetDrawButtonsTapEvent();

            //SetDrawButtonsLostFocusEvent();
            //RecurrenceSwitch.IsChecked = false;

            // Initialize isolated storage kino settings            
            InitializeIsolatedKinoSettings();

            // Setup application bar menu items
            //UpdateApplicatiobBarMenuItems();
            
            // Set UI control bindings to kino settings
            SetBindings();

            // Select pivot item 
            //KinoPalPivot.SelectedIndex = 4;
            KinoPalPivot.SelectedItem = this.Generator;
        }

        /// <summary>
        /// Initializes app's isolated storage kino settings.
        /// </summary>
        private void InitializeIsolatedKinoSettings()
        {
            // Get kino settings key from iso storage if any, otherwise create it
            isolatedKinoSettings = new IsoKinoSettings();
            var kinoSettings = isolatedKinoSettings.KinoSettings;            

            // key was not found, create one
            if (kinoSettings == null)
            {
                isolatedKinoSettings.KinoSettings = new KinoSettings();
                
            }

            // Set application bar menu items
            //UpdateApplicatiobBarMenuItems();
            //SetIsAppBarMenuEnabled();

            /*
            var savedNumsCount = kinoSettings.SavedSequencesCount;
            if (savedNumsCount != 0)
                ApplicationBar.IsMenuEnabled = true;
             */

        }

        /// <summary>
        /// Sets UI control binding properties to KinoSettings
        /// </summary>
        private void SetBindings()
        {
            //this.FavoritesModeText.DataContext = isolatedKinoSettings.KinoSettings;
            //this.RecurrenceModeText.DataContext = isolatedKinoSettings.KinoSettings;
            //this.FavoritesModeText.SetBinding(TextBlock.TextProperty, new System.Windows.Data.Binding() { Mode = System.Windows.Data.BindingMode.TwoWay, Path = new PropertyPath("IsFavoritesModeValue") });
            //this.RecurrenceModeText.SetBinding(TextBlock.TextProperty, new System.Windows.Data.Binding() { Mode = System.Windows.Data.BindingMode.TwoWay, Path = new PropertyPath("IsRecurrenceModeValue") });
                        
            this.FavoritesModeHyperBtn.DataContext = isolatedKinoSettings.KinoSettings;
            this.RecurrenceModeHyperBtn.DataContext = isolatedKinoSettings.KinoSettings;
            this.FavoritesModeHyperBtn.SetBinding(HyperlinkButton.ContentProperty, new System.Windows.Data.Binding() { Mode = System.Windows.Data.BindingMode.TwoWay, Path = new PropertyPath("IsFavoritesModeValue") });
            this.RecurrenceModeHyperBtn.SetBinding(HyperlinkButton.ContentProperty, new System.Windows.Data.Binding() { Mode = System.Windows.Data.BindingMode.TwoWay, Path = new PropertyPath("IsRecurrenceModeValue") });
            
        }

        /// <summary>
        /// Event handler for pressing the clear history button.
        /// It clears and reset kino's internal memory of generated sequences.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void OnClearHistoryBtn_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			var sequences = this.kino.Sequences;
			if(sequences != null && sequences.Count > 0)
			{
				var isCleared = kino.Clear();
				if(isCleared)
				{
					this.HistoryStatusValue.Text = "Cleared";
					this.SequencesCountTxtBox.Text = kino.Sequences.Count.ToString() + " sequences";
					this.ClearHistoryBtn.IsEnabled = false;
					this.ShowHistoryBtn.IsEnabled = false;
				}	
			}
		}

        /// <summary>
        /// Event handler on reaching the maximum possible sequence combinations of selected game type(length).
        /// Enables and disables the app bar buttons accordingly and unlocks the main pivot element.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void OnMaxReachedOKBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
		{
            // Create the transition effect
            
            SwivelTransition swivel = new SwivelTransition();
            TurnstileTransition turnstile = new TurnstileTransition();
            turnstile.Mode = TurnstileTransitionMode.ForwardOut;
            swivel.Mode = SwivelTransitionMode.FullScreenOut;
            
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideDownFadeOut;
            ITransition trans = slide.GetTransition(this.MaxCombsPopupGrid);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();

			this.MaxReachedPopup.IsOpen = false;
            this.MaxReachedPopup.Visibility = System.Windows.Visibility.Collapsed;

			//this.GoButton.IsEnabled = true;

			// Lock main pivot
			this.KinoPalPivot.IsLocked = false;
            this.KinoPalPivot.IsHitTestVisible = true;
            // Re-enable app bar menu
            this.ApplicationBar.IsMenuEnabled = true;
			// Re-enable app bar buttons
            var appBarButtons = this.ApplicationBar.Buttons;
            foreach(var button in appBarButtons)
            {
                var b = button as ApplicationBarIconButton;
               	b.IsEnabled = true;
            }			
		}
        
        /// <summary>
        /// Initializes the application bar
        /// </summary>
        private void InitializeApplicationBar()
        {
            ApplicationBar.Mode = ApplicationBarMode.Minimized;
            ApplicationBar.Opacity = 1.0;
            ApplicationBar.IsVisible = true;

            /*
            ApplicationBarIconButton addButton = new ApplicationBarIconButton();
            addButton.IconUri = new Uri("/Assets/Icons/add.png", UriKind.Relative);            
            addButton.Text = "add draw";			
            ApplicationBar.Buttons.Add(addButton);
            addButton.Click += new EventHandler(OnAddDrawEntryButton_Click);
            */

            ApplicationBarIconButton rateButton = new ApplicationBarIconButton();
            rateButton.IconUri = new Uri("/Assets/Icons/dark/appbar.star.png", UriKind.Relative);
            rateButton.Text = "rate";
            ApplicationBar.Buttons.Add(rateButton);
            rateButton.Click += new EventHandler(OnRateButton_Click);

            ApplicationBarIconButton settingsButton = new ApplicationBarIconButton();
            settingsButton.IconUri = new Uri("/Assets/Icons/settings.png", UriKind.Relative);
            settingsButton.Text = "settings";
            ApplicationBar.Buttons.Add(settingsButton);
            settingsButton.Click += new EventHandler(OnSettingsButton_Click);

            ApplicationBarIconButton aboutButton = new ApplicationBarIconButton();
            aboutButton.IconUri = new Uri("/Assets/Icons/appbar.information.circle.png", UriKind.Relative);
            aboutButton.Text = "about";
            ApplicationBar.Buttons.Add(aboutButton);
            aboutButton.Click += new EventHandler(OnAboutButton_Click);

            
            // Initialize menu items                      
            saveNumsMenuItem = new ApplicationBarMenuItem();
            saveNumsMenuItem.Text = "save current numbers";
            ApplicationBar.MenuItems.Add(saveNumsMenuItem);
            //saveNumsMenuItem.Click += OnSaveNumbersMenuItem_Click;
            saveNumsMenuItem.Click += OnSaveGeneratedNumbers_Click; // new stuff in 1.0.5: better , quicker search dude!

            /*
            viewStoredNumsMenuItem = new ApplicationBarMenuItem();
            viewStoredNumsMenuItem.Text = "view saved numbers";
            ApplicationBar.MenuItems.Add(viewStoredNumsMenuItem);            
            viewStoredNumsMenuItem.Click += OnViewSavedNumbersButton_Click;
            */

            addNumbersMenuItem = new ApplicationBarMenuItem();
            addNumbersMenuItem.Text = "add new numbers";
            ApplicationBar.MenuItems.Add(addNumbersMenuItem);
            addNumbersMenuItem.Click += OnAddNumbersMenuItem_Click;

            viewStatsMenuItem = new ApplicationBarMenuItem();
            viewStatsMenuItem.Text = "saved numbers frequencies";
            ApplicationBar.MenuItems.Add(viewStatsMenuItem);
            viewStatsMenuItem.Click += OnViewStatisticsButton_Click;

            ApplicationBarMenuItem storedseqMenuItem = new ApplicationBarMenuItem();            
            storedseqMenuItem.Text = "my saved numbers";
            ApplicationBar.MenuItems.Add(storedseqMenuItem);
            storedseqMenuItem.Click += OnStoredSeqButton_Click;
            
            /*
            var dataMenuItem = new ApplicationBarMenuItem();
            dataMenuItem.Text = "get real data";
            ApplicationBar.MenuItems.Add(dataMenuItem);
            dataMenuItem.Click += OnRealDataMenuItem_Click;
            */
			
            var searchMenuItem = new ApplicationBarMenuItem();
            searchMenuItem.Text = "O.P.A.P. draws";
            ApplicationBar.MenuItems.Add(searchMenuItem);
            searchMenuItem.Click += OnSearchMenuItem_Click;			
			
        }


        /// <summary>
        /// Enables or disables the menu items on the application bar.
        /// </summary>
        private void UpdateApplicatiobBarMenuItems()
        {
            viewStatsMenuItem.IsEnabled = true;

            if (isolatedKinoSettings.KinoSettings != null) // numbers exist in isolated storage file
            {
                if (isolatedKinoSettings.KinoSettings.SavedSequencesCount != 0)
                {
                    viewStoredNumsMenuItem.IsEnabled = true;
                }
                else
                    viewStoredNumsMenuItem.IsEnabled = false;
            }

            if (kino != null && kino.Sequences != null) // check for generated numbers
            {
                saveNumsMenuItem.IsEnabled = true;
                var items = GeneratedNumbersList.ItemsSource;
                var sequence = new Sequence();
                if (items != null) // check numbers on generation grid
                {
                    foreach (var item in items)
                    {
                        if (item != null)
                        {
                            var number = item as NumberObject;
                            if (number is NumberObject && number.Value != null)
                                sequence.Add(number.Value.Value);
                        }
                    }
                }

                if (sequence.Numbers.Count == 0)  // nothing generated yet, disable menu item
                    saveNumsMenuItem.IsEnabled = false;
                else
                {
                    /*
                    if (this.isolatedKinoSettings.KinoSettings.SavedSequences.Exists(c => CompareSequence.IsEqual(c, sequence)))
                        saveNumsMenuItem.IsEnabled = false; //numbers exists in iso storage file
                    */

                    if (this.isolatedKinoSettings.KinoSettings.PersonalSequences.Contains(sequence))
                        saveNumsMenuItem.IsEnabled = false; //numbers exists in iso storage file
                }
            }
        }

        
        #region Navigation Events

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var mode = e.NavigationMode;
            if (mode == NavigationMode.New || mode == NavigationMode.Back)
            {
                UpdateApplicatiobBarMenuItems();
                SystemTray.SetIsVisible(this, false);
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            //this.isolatedKinoSettings.KinoSettings.UpdateStatisticalData(); // Updates isolated storage statistical data
            if(e.Uri.OriginalString == "/Views/Statistics.xaml")
            {
                this.isolatedKinoSettings.KinoSettings.UpdateStatisticalData();
            }

        }

        #endregion Navigation Events


        #region Click/Tap buttons Events

        /// <summary>
        /// Selection changed event for KinoPal Pivot. Enables or disables the menu item "save sequence" when moving away from the generator pivot.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnKinoPalPivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // disable menu item beyond zero index
            var pivot = sender as Pivot;
            var menuItems = ApplicationBar.MenuItems;
            var selectedItem = pivot.SelectedItem as PivotItem;
            if (selectedItem.Name != "Generator")
            {
                foreach (var item in menuItems)
                {
                    var menuItem = item as ApplicationBarMenuItem;
                    if (menuItem.Text == "save numbers")// || )menuItem.Text == "view saved numbers")
                    {
                        menuItem.IsEnabled = false;
                    }
                }
            }
            else
            {
               UpdateApplicatiobBarMenuItems();
            }
        }

        /// <summary> 
        /// Generator pivot item lost focus event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnGenerator_LostFocus(object sender, RoutedEventArgs e)
        {
            // hide menu item
            var menuItems = ApplicationBar.MenuItems;
            foreach (var item in menuItems)
            {
                var menuItem = item as ApplicationBarMenuItem;
                if (menuItem.Text == "save numbers" || menuItem.Text == "view saved numbers")
                    ApplicationBar.IsMenuEnabled = false;
            }
        }

        /// <summary>
        /// Event handler when clicking the save sequence menu item on the generator page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSaveNumbersMenuItem_Click(object sender, EventArgs e)
        {
            var sequence = new Sequence();
            var list = new List<Sequence>();
            var itemSource = this.GeneratedNumbersList.ItemsSource;
            foreach (var item in GeneratedNumbersList.ItemsSource)
            {
                var container = GeneratedNumbersList.ContainerFromItem(item) as LongListMultiSelectorItem;
                var content = container.Content as NumberObject;
                if(content.Value != null)
                    sequence.Add(content.Value.Value);
            }
            if (!this.isolatedKinoSettings.KinoSettings.SavedSequences.Exists(c => CompareSequence.IsEqual(c, sequence)))
            {
                list = this.isolatedKinoSettings.KinoSettings.SavedSequences;
                list.Add(sequence);
                isolatedKinoSettings.KinoSettings.SavedSequences = list;
                //isolatedKinoSettings.KinoSettings.SavedSequences.Add(sequence);
                //var single = this.isolatedKinoSettings.KinoSettings.SavedSequences.Single(c => CompareSequence.IsEqual(c, sequence));
                //this.isolatedKinoSettings.KinoSettings.SavedSequences.Remove(single);

                //this.isolatedKinoSettings.Save();

                // Update menu items
                UpdateApplicatiobBarMenuItems();
                MessageBoxResult result = MessageBox.Show("Current numbers saved to phone.", "KinoPal message:", MessageBoxButton.OK);
                
                /*
                //open popup
                this.StoredOkPopup.IsOpen = true;
                this.StoredOkPopup.Visibility = System.Windows.Visibility.Visible;
                
                // Create the transition effect
                SlideTransition slide = new SlideTransition();
                SwivelTransition swivel = new SwivelTransition();
                TurnstileTransition turnstile = new TurnstileTransition();
                turnstile.Mode = TurnstileTransitionMode.BackwardIn;                
                swivel.Mode = SwivelTransitionMode.ForwardIn;
                slide.Mode = SlideTransitionMode.SlideUpFadeIn;
                //ITransition trans = slide.GetTransition(this.PopupMsgGrid);
				ITransition trans = slide.GetTransition(this.StoredOKGrid);
                trans.Completed += delegate { trans.Stop(); };
                trans.Begin();
                */
            }
        }


        /// <summary>
        /// Event handler when clicking the save sequence menu item on the generator page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSaveGeneratedNumbers_Click(object sender, EventArgs e)
        {
            var sequence = new Sequence();
            var list = new HashSet<Sequence>();

            //var fsequence = new Sequence(); // new
            //var flist = new HashSet<Sequence>(); //new

            var itemSource = this.GeneratedNumbersList.ItemsSource;
            // add numbers into sequence
            foreach (var item in GeneratedNumbersList.ItemsSource)
            {
                var container = GeneratedNumbersList.ContainerFromItem(item) as LongListMultiSelectorItem;
                var content = container.Content as NumberObject;                
                if (content.Value != null)
                {
                    sequence.Add(content.Value.Value);
                    //fsequence.Add(content.Value.Value); // new 
                }                
            }

            // check for existing in personal sequence set in iso storage
            
            if (!this.isolatedKinoSettings.KinoSettings.PersonalSequences.Contains(sequence))            
            {                
                list = isolatedKinoSettings.KinoSettings.PersonalSequences;
                list.Add(sequence);
                isolatedKinoSettings.KinoSettings.PersonalSequences = list;                
                UpdateApplicatiobBarMenuItems();
                MessageBoxResult result = MessageBox.Show("Current numbers saved to phone.", "KinoPal message:", MessageBoxButton.OK);
            }
            /*
            if (!this.isolatedKinoSettings.KinoSettings.FuckingSequences.Contains(fsequence))
            {
                flist = isolatedKinoSettings.KinoSettings.FuckingSequences;
                //list.Add(sequence);
                flist.Add(fsequence);
                isolatedKinoSettings.KinoSettings.FuckingSequences = flist;
                //isolatedKinoSettings.Save();
                UpdateApplicatiobBarMenuItems();
                MessageBoxResult result = MessageBox.Show("Current numbers saved to phone.", "KinoPal message:", MessageBoxButton.OK);
            }
            */
        }

        /// <summary>
        /// Popup closing button event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnStoredOkBtn_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // Create the transition effect
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideDownFadeOut;
            ITransition trans = slide.GetTransition(this.StoredOKGrid);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();

            // Enable/disable components
            this.StoredOkPopup.IsOpen = false;
            this.StoredOkPopup.Visibility = System.Windows.Visibility.Collapsed;
            this.KinoPalPivot.IsLocked = false;
            this.KinoPalPivot.IsHitTestVisible = true;
            this.ApplicationBar.IsVisible = true;
        }

        /// <summary>
        /// Handling event on popup opened.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnStoredOkPopup_Opened(object sender, EventArgs e)
        {
            this.KinoPalPivot.IsLocked = true;
            this.KinoPalPivot.IsHitTestVisible = false;
            this.ApplicationBar.IsVisible = false;
        }


        /// <summary>
        /// Navigates to add draw entry page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnAddDrawEntryButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/DrawResult.xaml", UriKind.Relative));
            SlideUpFadeOut();
        }
		
        /// <summary>
        /// Navigates to settings page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSettingsButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/Settings.xaml", UriKind.Relative));
            SlideUpFadeOut();
        }

        /// <summary>
        /// Navigates to about page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnAboutButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/About.xaml", UriKind.Relative));
            SlideUpFadeOut();
        }

        private void OnRateButton_Click(object sender, EventArgs e)
        {
            MarketplaceReviewTask marketplaceReviewTask = new MarketplaceReviewTask();
            marketplaceReviewTask.Show();
        }

        /// <summary>
        /// Navigates to settings page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnViewSavedNumbersButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/SavedSequences.xaml", UriKind.Relative));
            SlideUpFadeOut();
        }

        /// <summary>
        /// Navigates to statistics page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnViewStatisticsButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/Statistics.xaml", UriKind.Relative));
            SlideUpFadeOut();
        }

        private void OnAddNumbersMenuItem_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/AddNumbers.xaml", UriKind.Relative));
            SlideUpFadeOut();
        }

        /// <summary>
        /// Event on popup opening.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMaxReachedPopup_Opened(object sender, System.EventArgs e)
        {
            this.ApplicationBar.IsMenuEnabled = false;
            var appBarButtons = this.ApplicationBar.Buttons;
            foreach (var button in appBarButtons)
            {
                var b = button as ApplicationBarIconButton;
                b.IsEnabled = false;
            }
        }

        #endregion Click/Tap Events

        private void SlideUpFadeOut()
        {
            // Create the transition effect when leaving this page
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideUpFadeOut;
            PhoneApplicationPage page = (PhoneApplicationPage)((PhoneApplicationFrame)Application.Current.RootVisual).Content;
            ITransition trans = slide.GetTransition(page);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        #region Generator buttons tap events

        /// <summary>
        /// Set buttons Tap event delegate function
        /// </summary>
        private void SetDrawButtonsTapEvent()
        {            
            GameType1Button.Tap += new EventHandler<System.Windows.Input.GestureEventArgs>(OnGameTypeButtonsTap);
            GameType2Button.Tap += new EventHandler<System.Windows.Input.GestureEventArgs>(OnGameTypeButtonsTap);
            GameType3Button.Tap += new EventHandler<System.Windows.Input.GestureEventArgs>(OnGameTypeButtonsTap);
            GameType4Button.Tap += new EventHandler<System.Windows.Input.GestureEventArgs>(OnGameTypeButtonsTap);
            GameType5Button.Tap += new EventHandler<System.Windows.Input.GestureEventArgs>(OnGameTypeButtonsTap);
            GameType6Button.Tap += new EventHandler<System.Windows.Input.GestureEventArgs>(OnGameTypeButtonsTap);
            GameType7Button.Tap += new EventHandler<System.Windows.Input.GestureEventArgs>(OnGameTypeButtonsTap);
            GameType8Button.Tap += new EventHandler<System.Windows.Input.GestureEventArgs>(OnGameTypeButtonsTap);
            GameType9Button.Tap += new EventHandler<System.Windows.Input.GestureEventArgs>(OnGameTypeButtonsTap);
            GameType10Button.Tap += new EventHandler<System.Windows.Input.GestureEventArgs>(OnGameTypeButtonsTap);
            GameType11Button.Tap += new EventHandler<System.Windows.Input.GestureEventArgs>(OnGameTypeButtonsTap);
            GameType12Button.Tap += new EventHandler<System.Windows.Input.GestureEventArgs>(OnGameTypeButtonsTap);                       
        }

        /// <summary>
        /// Set buttons Lost Focus event delegate function
        /// </summary>
        private void SetDrawButtonsLostFocusEvent()
        {
            GameType1Button.LostFocus += new RoutedEventHandler(GameTypeButton_LostFocus);
            GameType2Button.LostFocus += new RoutedEventHandler(GameTypeButton_LostFocus);
            GameType3Button.LostFocus += new RoutedEventHandler(GameTypeButton_LostFocus);
            GameType4Button.LostFocus += new RoutedEventHandler(GameTypeButton_LostFocus);
            GameType5Button.LostFocus += new RoutedEventHandler(GameTypeButton_LostFocus);
            GameType6Button.LostFocus += new RoutedEventHandler(GameTypeButton_LostFocus);
            GameType7Button.LostFocus += new RoutedEventHandler(GameTypeButton_LostFocus);
            GameType8Button.LostFocus += new RoutedEventHandler(GameTypeButton_LostFocus);
            GameType9Button.LostFocus += new RoutedEventHandler(GameTypeButton_LostFocus);
            GameType10Button.LostFocus += new RoutedEventHandler(GameTypeButton_LostFocus);
            GameType11Button.LostFocus += new RoutedEventHandler(GameTypeButton_LostFocus);
            GameType12Button.LostFocus += new RoutedEventHandler(GameTypeButton_LostFocus);
        }

        /// <summary>
        /// Delegate event handler for the tap action on game type buttons.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnGameTypeButtonsTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            /** USED IN PAIRS AND TRIPLETS - NICE FUCKING COLOR STUFF! **
            <RadialGradientBrush>
            <GradientStop Color="Black"/>
            <GradientStop Color="#FF2F4B87" Offset="1"/>
            </RadialGradientBrush>            
            */

            // Store gesture event arguments containing text block around button
            gea = e;                        
            var textBlock = e.OriginalSource as TextBlock;
            if (textBlock != null) // Change button's template content backgroung
            {
                var parent = textBlock.Parent;
                if (parent != null && parent is Border) // change background value
                {                    
                    var border = parent as Border;
                    border.Background = (Brush)Application.Current.Resources["PhoneAccentBrush"];
                    ClearUnTappedButtonsBackgroundValue(sender);
                    
                    // Change text foreground to White if in dark theme
                    if (((Application.Current.Resources["PhoneBackgroundBrush"] as SolidColorBrush).Color) == Colors.White)
                        textBlock.Foreground = (Brush)Application.Current.Resources["PhoneBackgroundBrush"];
                    else
                        textBlock.Foreground = (Brush)Application.Current.Resources["PhoneForegroundBrush"];
                }

                // Set kino settings properties
                var text = textBlock.Text;
                var length = Convert.ToInt32(text);
                kino.Length = length;
                kino.IsRecursiveMode = this.isolatedKinoSettings.KinoSettings.IsRecurrenceMode;
                kino.IsFavoritesMode = this.isolatedKinoSettings.KinoSettings.IsFavoritesMode;
                kino.ExcludedNumbers = this.isolatedKinoSettings.KinoSettings.ExcludedNumbers;
                kino.IsStoredSequencesRepeated = this.isolatedKinoSettings.KinoSettings.IsSavedSequencesRepeated;
                bool isMaxCombinationsReached = false;
                MaxCombinationsType maxCombType = MaxCombinationsType.AllSequences;
                var isSavedSequenceOkToRepeat = this.isolatedKinoSettings.KinoSettings.IsSavedSequencesRepeated;
                
                // Generate numbers
                var generated = false;
                if (!this.isolatedKinoSettings.KinoSettings.IsRecurrenceMode)
                    generated = kino.Generate(out isMaxCombinationsReached); // Not Recurrence Mode
                else
                {
                    if (isSavedSequenceOkToRepeat)
                    {
                        generated = kino.Generate(out isMaxCombinationsReached); // Saved Sequences Repeat Mode
                    }
                    else
                    {
                        generated = kino.Generate(out isMaxCombinationsReached, this.isolatedKinoSettings.KinoSettings.PersonalSequences.ToList(), out maxCombType);
                        //generated = kino.Generate(out isMaxCombinationsReached, this.isolatedKinoSettings.KinoSettings.SavedSequences, out maxCombType);
                    }
                }
                

                // Maximum propable combinations for the selected sequence length reached, open popup
                if (generated == false && isMaxCombinationsReached == true)
                {
                    /*
                    this.KinoPalPivot.IsLocked = true;
                    this.KinoPalPivot.IsHitTestVisible = false;                                        
                    this.MaxReachedPopup.IsOpen = true;
                    this.MaxReachedPopup.Visibility = System.Windows.Visibility.Visible;
                    */

                    // Display message based on the result type
                    if (maxCombType == MaxCombinationsType.AllSequences)
                    {
                        MessageBoxResult result1 = MessageBox.Show("Enable the recurrence option or clear the history of the generated sequences of numbers.", "Maximum combinations reached for this game type.", MessageBoxButton.OK);
                    }
                    else if (maxCombType == MaxCombinationsType.AllSavedSequences)
                    {
                        MessageBoxResult result2 = MessageBox.Show("Enable the repeat mode for saved sequences or delete the sequence from phone.", "Maximum combinations reached for this game type.", MessageBoxButton.OK);
                    }
                }
                else if (generated == true)
                {
                    // Show menu item to store the sequence
                    //ApplicationBar.IsMenuEnabled = true;
                    //SetIsAppBarMenuEnabled();

                    // Update grid of numbers with generated sequence of Kino generator
                    var itemsSource = (ObservableCollection<NumberObject>)(this.GeneratedNumbersList.ItemsSource);
                    var sequences = kino.Sequences;
                    var lastInserted = sequences[sequences.Count - 1];

                    // Sort the sequence
                    //lastInserted.Numbers.Sort();

                    // Update items source value
                    var count = lastInserted.Numbers.Count;
                    for (var i = 0; i < count; i++)
                    {
                        itemsSource[i].Value = lastInserted.Numbers[i];
                        //itemsSource[i].BackColor = (Brush)Application.Current.Resources["PhoneAccentBrush"];

                        RadialGradientBrush radialGradientBrush = new RadialGradientBrush();                        
                        GradientStop blackGS = new GradientStop();
                        blackGS.Color = Colors.Black;
                        blackGS.Offset = 0.0;
                        radialGradientBrush.GradientStops.Add(blackGS);
                        GradientStop phoneAccentBrushGS = new GradientStop();
                        var brush = (SolidColorBrush)Application.Current.Resources["PhoneAccentBrush"];
                        var color = brush.Color;
                        phoneAccentBrushGS.Color = color;
                        phoneAccentBrushGS.Offset = 1.0;
                        radialGradientBrush.GradientStops.Add(phoneAccentBrushGS);
                        itemsSource[i].BackColor = radialGradientBrush;


                        //check device background color
                        // If in white theme display generated text in white color
                        if (((Application.Current.Resources["PhoneBackgroundBrush"] as SolidColorBrush).Color) == Colors.White)
                            itemsSource[i].TextColor = (Brush)Application.Current.Resources["PhoneBackgroundBrush"];
                        else
                            itemsSource[i].TextColor = (Brush)Application.Current.Resources["PhoneForegroundBrush"];

                    }
                    // change back color and value of non displayed items in the numbers 'selection' grid
                    count = lastInserted.Numbers.Count;
                    for (var i = count; i < itemsSource.Count; i++)
                    {
                        itemsSource[i].Value = null;
                        itemsSource[i].BackColor = (Brush)Application.Current.Resources["PhoneInactiveBrush"];
                    }

                    // Clear and reset the value of the items source
                    GeneratedNumbersList.ClearValue(LongListMultiSelector.ItemsSourceProperty);
                    this.GeneratedNumbersList.ItemsSource = itemsSource;
                    GeneratedNumbersList.UpdateLayout();

                    // Create rotation effect
                    foreach (var item in GeneratedNumbersList.ItemsSource)
                    {
                        var container = GeneratedNumbersList.ContainerFromItem(item) as LongListMultiSelectorItem;
                        var content = container.Content as NumberObject;
                        if (content.Value != null)
                        {
                            RotateTransition rotate = new RotateTransition();
                            rotate.Mode = RotateTransitionMode.In180Clockwise;
                            ITransition trans = rotate.GetTransition(container);
                            trans.Completed += delegate { trans.Stop(); };
                            trans.Begin();
                        }
                    }
                    
                    // Update application bar menu items
                    UpdateApplicatiobBarMenuItems();
                }

                //update history page controls
                if (kino.Sequences != null)
                {
                    this.SequencesCountTxtBox.Text = kino.Sequences.Count.ToString() + " sequence(s)";
                    this.HistoryStatusValue.Text = "In use";
                    this.ShowHistoryBtn.IsEnabled = true;
                    this.ClearHistoryBtn.IsEnabled = true;
                }
            }            
        }
              
        /// <summary>
        /// Delegate event handler of game type buttons lost focus event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GameTypeButton_LostFocus(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if(button.IsPressed == false)
            {
                var textBlock = gea.OriginalSource as TextBlock;
                if (textBlock != null)
                {
                    var parent = textBlock.Parent;
                    if (parent != null && parent is Border)
                    {
                        var border = parent as Border;
                        border.Background = (Brush)Application.Current.Resources["PhoneBackgroundBrush"];
                        
                    }
                }
            }
        }

        /// <summary>
        /// Clears and reset the background value of unpressed game buttons
        /// </summary>
        private void ClearUnTappedButtonsBackgroundValue(object sender)
        {
            // Clear background value of other buttons
            var childrenOfGrid = VisualTreeHelper.GetChildrenCount(this.GeneratorGrid);
            for (var i = 0; i < childrenOfGrid; i++)
            {
                var child = VisualTreeHelper.GetChild(this.GeneratorGrid, i);
                if (child != null && child is StackPanel)
                {
                    var stackPanel = child as StackPanel;
                    var children = stackPanel.Children;
                    if (children != null)
                    {
                        foreach (var c in children)
                        {
                            var button = c as Button;
                            if (button != null)
                            {
                                for (var k = 0; k < VisualTreeHelper.GetChildrenCount(button); k++)
                                {
                                    var grid = VisualTreeHelper.GetChild(button, k) as Grid;
                                    if (grid != null)
                                    {
                                        var gridChildren = grid.Children;
                                        if (gridChildren != null && gridChildren.Count == 1)
                                        {
                                            var buttonBorder = gridChildren[0] as Border;
                                            var current = sender as Button;
                                            if (buttonBorder != null && button.Name != current.Name)
                                            {
                                                buttonBorder.Background = (Brush)Application.Current.Resources["PhoneBackgroundBrush"];
                                                

                                                // change textblock foreground if in light theme
                                                var textBlock = buttonBorder.Child as TextBlock;
                                                if (textBlock != null)
                                                {
                                                    if (((Application.Current.Resources["PhoneBackgroundBrush"] as SolidColorBrush).Color) == Colors.White)
                                                        textBlock.Foreground = (Brush)Application.Current.Resources["PhoneForegroundBrush"];
                                                }
                                                
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion


        private void FrequencyBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var bar = sender as ProgressBar;
            bar.Maximum = 100;
            var value = bar.Value;
            bar.Value = value / bar.Maximum * 100;
            bar.UpdateLayout();
        }

        private void FrequencyBar_Loaded(object sender, RoutedEventArgs e)
        {
            var bar = sender as ProgressBar;
            bar.Maximum = 100;
            var value = bar.Value;
            bar.Value = value / bar.Maximum * 100;
            bar.UpdateLayout();
            
        }

        private void FavoritesModeHyperBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var button = sender as HyperlinkButton;            
            var mode = this.isolatedKinoSettings.KinoSettings.IsFavoritesMode;
            var content = this.isolatedKinoSettings.KinoSettings.IsFavoritesMode ? "Off" : "On";
            button.Content = content;
            this.isolatedKinoSettings.KinoSettings.IsFavoritesMode = !mode;
        }

        private void RecurrenceModeHyperBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var button = sender as HyperlinkButton;            
            var mode = this.isolatedKinoSettings.KinoSettings.IsRecurrenceMode;
            var content = this.isolatedKinoSettings.KinoSettings.IsRecurrenceMode ? "Off" : "On";
            button.Content = content;
            this.isolatedKinoSettings.KinoSettings.IsRecurrenceMode = !mode;
        }


        /// <summary>
        /// Navigates to statistics page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnStoredSeqButton_Click(object sender, EventArgs e)
        {
            //NavigationService.Navigate(new Uri("/ViewModels/ViewSavedNumbers.xaml", UriKind.Relative));
            NavigationService.Navigate(new Uri("/ViewModels/MySavedNumbers.xaml", UriKind.Relative));
            //NavigationService.Navigate(new Uri("/Views/SavedSequences.xaml", UriKind.Relative));

            SlideUpFadeOut();
        }

        /// <summary>
        /// Navigates to live data page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnRealDataMenuItem_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/ViewModels/Draws.xaml", UriKind.Relative));
            SlideUpFadeOut();
        }
		
        /// <summary>
        /// Navigates to live data page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSearchMenuItem_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/ViewModels/Search.xaml", UriKind.Relative));
            SlideUpFadeOut();
        }


        #region OLD Unused Code

        /*
        /// <summary>
        /// Initialize KinoSettings properties and data context
        /// </summary>
        private void InitializeKinoSettings()
        {
            this._KinoSettings = new KinoSettings();
            this.FavoritesModeText.DataContext = this.KinoSettings;
            this.RecurrenceModeText.DataContext = this.KinoSettings;
            this.FavoritesModeText.SetBinding(TextBlock.TextProperty, new System.Windows.Data.Binding() { Mode = System.Windows.Data.BindingMode.TwoWay, Path = new PropertyPath("IsFavoritesModeValue") });
            this.RecurrenceModeText.SetBinding(TextBlock.TextProperty, new System.Windows.Data.Binding() { Mode = System.Windows.Data.BindingMode.TwoWay, Path = new PropertyPath("IsRecurrenceModeValue") });
        }
		*/

        private void GeneratedNumbersList_ItemRealized(object sender, ItemRealizationEventArgs e)
        {
            foreach (var item in GeneratedNumbersList.ItemsSource)
            {
                var container = GeneratedNumbersList.ContainerFromItem(item) as LongListMultiSelectorItem;
                if (container.IsSelected)
                {
                    // Create the transition effect when leaving this 
                    RotateTransition rotate = new RotateTransition();
                    rotate.Mode = RotateTransitionMode.In180Counterclockwise;
                    ITransition trans = rotate.GetTransition(container);
                    trans.Completed += delegate { trans.Stop(); };
                    trans.Begin();
                }
            }
        }

        /*
        private void RecurrenceSwitch_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            this.RecurrenceValueTextBox.Text = "Enabled";
            var phoneForegroundBrush = (Brush)Application.Current.Resources["PhoneForegroundBrush"];
            var phoneSubtleBrush = (Brush)Application.Current.Resources["PhoneSubtleBrush"];
            this.RecFuncDescription.Foreground = phoneSubtleBrush;

            // Prepare text transition effect
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideRightFadeIn;
            this.RecFuncDescription.Visibility = System.Windows.Visibility.Visible;
            ITransition trans = slide.GetTransition(this.RecFuncDescription);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();

        }

        private void RecurrenceSwitch_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            this.RecurrenceValueTextBox.Text = "Disabled";
            var phoneInactiveBrush = (Brush)Application.Current.Resources["PhoneInactiveBrush"];
            this.RecFuncDescription.Foreground = phoneInactiveBrush;
            this.RecFuncDescription.Visibility = System.Windows.Visibility.Collapsed;

        }

        private void GenerateButton_Tap(object sender, System.Windows.RoutedEventArgs e)
        {

            var selectedItem = this.SeqLengthSelection.SelectedItem as ListPickerItem;
            var selected = selectedItem.Content.ToString();
            var length = Convert.ToInt32(selected);
            var isChecked = this.RecurrenceSwitch.IsChecked;
            var numGrid = this.NumbersDrawGrid;
            var phoneTextBoxes = new List<PhoneTextBox>();

            if (numGrid != null)
            {
                var children = numGrid.Children;
                if (children != null)
                {
                    foreach (var c in children)
                    {
                        if (c is PhoneTextBox)
                            phoneTextBoxes.Add(c as PhoneTextBox);
                    }
                }
            }

            kino.IsRecursiveMode = isChecked.Value;
            kino.Length = length;
            bool isMaxCombinationsReached = false;
            var generated = kino.Generate(out isMaxCombinationsReached);
            if (generated == false && isMaxCombinationsReached == true)
            {
                //this.Messages.Text = "Max combinations reached!";
                DisableGridColors();
                this.GoButton.IsEnabled = false;
                this.KinoPalPivot.IsLocked = true;
                this.MaxReachedPopup.IsOpen = true;
            }
            else if (generated == true)
            {
                DisableGridColors();
                var sequence = kino.Sequences;
                var lastInserted = sequence[sequence.Count - 1];
                var results = string.Empty;
                foreach (var n in lastInserted.Numbers)
                {
                    results += n.ToString() + " ";
                    var matchedTextBox = phoneTextBoxes.SingleOrDefault(c => c.Text.ToString() == n.ToString());
                    if (matchedTextBox != null)
                    {
                        var phoneAccentColor = (Brush)Application.Current.Resources["PhoneAccentBrush"];
                        var phoneBackgroundBrush = (Brush)Application.Current.Resources["PhoneBackgroundBrush"];
                        matchedTextBox.Background = phoneAccentColor;
                        matchedTextBox.Foreground = new SolidColorBrush(Colors.White);
                    }
                }
                //this.Messages.Text = results;
            }

            //update history page controls
            if (kino.Sequences != null)
            {
                this.SequencesCountTxtBox.Text = kino.Sequences.Count.ToString() + " sequence(s)";
                this.HistoryStatusValue.Text = "In use";
                this.ShowHistoryBtn.IsEnabled = true;
                this.ClearHistoryBtn.IsEnabled = true;
            }
        }

        private void DisableGridColors()
        {
            var grid = this.NumbersDrawGrid;
            var textboxes = new List<PhoneTextBox>();
            if (grid != null)
            {
                var children = grid.Children;
                if (children != null)
                {
                    foreach (var t in children)
                    {
                        if (t is PhoneTextBox)
                        {
                            var text = t as PhoneTextBox;
                            var phoneSubtleBrush = (Brush)Application.Current.Resources["PhoneSubtleBrush"];
                            var phoneForegroundBrush = (Brush)Application.Current.Resources["PhoneForegroundBrush"];
                            text.Foreground = phoneForegroundBrush;
                            text.Background = new SolidColorBrush(Colors.Black);
                        }
                    }
                }
            }
        }
        */
        private List<Button> NumbersGridButtons()
        {
            var numGrid = ((System.Windows.Controls.Grid)(this.FindName("NumbersBtnGrid")));
            var buttons = new List<Button>();
            var list = new List<Button>();
            if (numGrid != null)
            {
                var children = numGrid.Children;
                if (children != null)
                {
                    foreach (var c in children)
                    {
                        if (c is Button)
                        {
                            var button = c as Button;
                            list.Add(button);
                        }
                    }
                }
            }
            return list;
        }

        private List<ListPickerItem> LengthPickerItems()
        {
            var list = SequenceLengthGenerator.Populate();
            var items = new List<ListPickerItem>();
            foreach (var i in list)
            {
                var item = new ListPickerItem();
                item.Content = i;
                items.Add(item);
            }
            return items;
        }

        private void InitSequenceLength()
        {
            /*
            this.SeqLengthSelection.SetValue(ListPicker.ItemCountThresholdProperty, 12);
            this.SeqLengthSelection.ItemsSource = this.LengthPickerItems();
            this.SeqLengthSelection.SelectedIndex = 11;
            */
        }


        #endregion
    }
}