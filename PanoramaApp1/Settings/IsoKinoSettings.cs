﻿using Kino.Binding;
using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.IsolatedSettings
{
    /// <summary>
    /// Stores the kino application settings in isolated storage.
    /// </summary>
    public class IsoKinoSettings
    {
        // Isolated storage settings
        IsolatedStorageSettings settings;

        // Key names
        const string KinoSettingsKey = "KinoSettings";
        //const string SavedSequencesKey = "MySavedSequencesSettings";

        // Default value for kino settings
        KinoSettings KinoSettingsDefault = null;
        //List<KinoGenerator.Sequence> SavedSequences = null;
                        
        // Gets the app storage settings or creates a new settings storage file if not found
        public IsoKinoSettings()
        {
            settings = IsolatedStorageSettings.ApplicationSettings;
        }

        /// <summary>
        /// Adds or updates a settings value of the app. If it does not exist,
        /// then a new pair of key/value is added.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool AddOrUpdateValue(string key, Object value)
        {
            bool isValueChanged = false;

            // Key exists
            if(settings.Contains(key))
            {
                // value has changed
                if(settings[key] != value)
                {
                    // store new value
                    settings[key] = value;
                    isValueChanged = true;
                }
            }
            // else create the new key
            else
            {
                settings.Add(key,value);
                isValueChanged = true;
            }
            return isValueChanged;
        }

        /// <summary>
        /// Gets existing key value or default if not found, i.e. null;
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public T GetValueOrDefault<T>(string key, T defaultValue)
        {
            T value;
            // key exists
            if (settings.Contains(key))
                value = (T)settings[key];
            // key does not exist, return default value
            else
                value = defaultValue;
            return value;
        }

        /// <summary>
        /// Saves the settings.
        /// </summary>
        public void Save()
        {
            settings.Save();
        }
        
        /// <summary>
        /// Removes the settings entry with the specified key name.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Remove(string key)
        {
            var removed = false;
            removed = settings.Remove(key);
            return removed;
        }

        /// <summary>
        /// Gets or sets the KinoSettings key
        /// </summary>
        public KinoSettings KinoSettings
        {
            get
            {
                return GetValueOrDefault<KinoSettings>(KinoSettingsKey, KinoSettingsDefault);
            }
            set
            {
                if(AddOrUpdateValue(KinoSettingsKey,value))
                {
                    Save();
                }
            }
        }
        /*
        public List<KinoGenerator.Sequence> MySavedSequences
        {
            get
            {
                return GetValueOrDefault<List<KinoGenerator.Sequence>>(SavedSequencesKey, SavedSequences);
            }
            set
            {
                if (AddOrUpdateValue(SavedSequencesKey, value))
                {
                    Save();
                }
            }
        }
        */
    }
}
