﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Kino.Transition
{
    public static class KinoTransition
    {
        #region Transition effects

        public static void SlideUpFadeIn(UIElement element)
        {
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideUpFadeIn;
            ITransition trans = slide.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        public static void SlideUpFadeOut(FrameworkElement element)
        {
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideUpFadeOut;
            ITransition trans = slide.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        public static void SlideDownFadeIn(UIElement element)
        {
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideDownFadeIn;
            ITransition trans = slide.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        public static void SlideDownFadeOut(UIElement element)
        {
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideDownFadeOut;
            ITransition trans = slide.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        public static void SlideRightFadeIn(FrameworkElement element)
        {
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideRightFadeIn;
            ITransition trans = slide.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        public static void SlideLeftFadeIn(FrameworkElement element)
        {
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideLeftFadeIn;
            ITransition trans = slide.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        public static void SlideLeftFadeOut(FrameworkElement element)
        {
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideLeftFadeOut;
            ITransition trans = slide.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        public static void SlideRightFadeOut(FrameworkElement element)
        {
            SlideTransition slide = new SlideTransition();
            slide.Mode = SlideTransitionMode.SlideRightFadeOut;
            ITransition trans = slide.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        public static void TurnstileFeatherBackwardOut(FrameworkElement element)
        {
            TurnstileFeatherTransition transition = new TurnstileFeatherTransition();
            transition.Mode = TurnstileFeatherTransitionMode.BackwardOut;
            ITransition trans = transition.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        public static void TurnstileFeatherBackwardIn(FrameworkElement element)
        {
            TurnstileFeatherTransition transition = new TurnstileFeatherTransition();
            transition.Mode = TurnstileFeatherTransitionMode.BackwardIn;
            ITransition trans = transition.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }


        public static void TurnstileBackwardIn(FrameworkElement element)
        {
            TurnstileTransition transition = new TurnstileTransition();
            transition.Mode = TurnstileTransitionMode.BackwardIn;
            ITransition trans = transition.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        public static void TurnstileBackwardOut(FrameworkElement element)
        {
            TurnstileTransition transition = new TurnstileTransition();
            transition.Mode = TurnstileTransitionMode.BackwardOut;
            ITransition trans = transition.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        public static void SwivelFullScreenIn(FrameworkElement element)
        {
            SwivelTransition transition = new SwivelTransition();
            transition.Mode = SwivelTransitionMode.FullScreenIn;
            ITransition trans = transition.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        public static void SwivelFullScreenOut(FrameworkElement element)
        {
            SwivelTransition transition = new SwivelTransition();
            transition.Mode = SwivelTransitionMode.FullScreenOut;
            ITransition trans = transition.GetTransition(element);
            trans.Completed += delegate { trans.Stop(); };
            trans.Begin();
        }

        //var _TopFrom = this.RenderSize.Height;
        //var _TopTo = 0;
        //var _OpacityFrom = 0;
        //var _OpacityTo = 1;

        //ShowHide(LayoutRoot, null, _TopFrom, _TopTo, _OpacityFrom, _OpacityTo);



        //private static void ShowHide(UIElement item, Action callback, double topFrom, double topTo, double opacityFrom, double opacityTo)
        private static void ShowHide(UIElement item, double topFrom, double topTo, double opacityFrom, double opacityTo)
        {
            // setup
            var _Translate = new TranslateTransform
            {
                X = 0,
                Y = topFrom
            };
            item.RenderTransform = _Translate;
            item.Visibility = Visibility.Visible;
            item.Opacity = opacityFrom;

            // animate
            var _Storyboard = new Storyboard();
            var _Duration = TimeSpan.FromSeconds(.5);

            // opacity
            var _OpacityAnimate = new DoubleAnimation
            {
                To = opacityTo,
                Duration = _Duration,
            };
            _Storyboard.Children.Add(_OpacityAnimate);
            Storyboard.SetTarget(_OpacityAnimate, item);
            Storyboard.SetTargetProperty(_OpacityAnimate,
                new PropertyPath(UIElement.OpacityProperty));

            // translate (location)
            var _TranslateAnimate = new DoubleAnimation
            {
                To = topTo,
                Duration = _Duration,
                EasingFunction = new SineEase
                {
                    EasingMode = EasingMode.EaseOut
                }
            };
            _Storyboard.Children.Add(_TranslateAnimate);
            Storyboard.SetTarget(_TranslateAnimate, _Translate);
            Storyboard.SetTargetProperty(_TranslateAnimate,
                new PropertyPath(TranslateTransform.YProperty));

            // finalize
            _TranslateAnimate.Completed += (s, arg) =>
            {
                //if (callback != null)
                //    callback();
            };
            _Storyboard.Begin();
        }

        #endregion Transition effects
    }
}
